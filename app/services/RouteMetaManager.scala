package services

import javax.inject.Inject
import anorm._
import com.google.inject.{ImplementedBy, Singleton}
import common.{AnormMacro, Crud, CrudMacro}
import model.RouteMeta
import org.slf4j.LoggerFactory
import play.api.db.{Database, _}
import scala.util.{Failure, Success, Try}
import anorm.JodaParameterMetaData._
import org.joda.time.DateTime

@ImplementedBy(classOf[RouteMetaImpl])
trait RouteMetaManager extends Crud[RouteMeta, Long] {
  def setDeleted(routeId: Long, by: String): Try[RouteMeta]
}

@Singleton
class RouteMetaImpl @Inject()(@NamedDatabase("default") db: Database) extends RouteMetaManager {

  val logger = LoggerFactory.getLogger(getClass)

  val routeMetaParser: RowParser[RouteMeta] = AnormMacro.namedParser[RouteMeta](AnormMacro.ColumnNaming.SnakeCase)

  val routeIdColumn = "route_ref"

  override def create(routeMeta: RouteMeta): Try[RouteMeta] = {
    Try({
      db.withTransaction {
        implicit c => {
          val id = CrudMacro.query[RouteMeta](routeMeta, "route_metadata", Set("id"), true,
            CrudMacro.SnakeCase, CrudMacro.SQLInsert).executeInsert[Option[Long]]()
          routeMeta.copy(id = id)
        }
      }
    })
  }

  override def get(routeId: Long): Try[RouteMeta] = db.withConnection { implicit c =>
    Try( SQL(s"select * from route_metadata where ${routeIdColumn}={id}").on("id" -> routeId).as(routeMetaParser.single) )
  }

  override def update(routeMeta: RouteMeta): Try[RouteMeta] = {
    db.withTransaction {
      implicit c => {
        get(routeMeta.routeRef) match {
          case Failure(f) => Failure(new Exception("Route metadata to update not found"))
          case Success(oldRouteMeta) =>
            val updatedCnt = CrudMacro.query[RouteMeta](routeMeta, "route_metadata", Set("id"), true,
              CrudMacro.SnakeCase, CrudMacro.SQLUpdate).executeUpdate()
            if (updatedCnt != 1) {
              throw new Exception("Unable to update Route metadata")
            }
            return Success(routeMeta)
        }
      }
    }
  }

  override def delete(routeId: Long): Try[RouteMeta] = db.withConnection { implicit c =>
    val entry = get(routeId)
    if (entry.isFailure) entry
    else {
      val deletedCount = SQL(s"delete from route_metadata where ${routeIdColumn}={id}").on("id" -> routeId).executeUpdate()
      if (deletedCount == 1) entry
      else Failure(new Exception("Cannot delete Route metadata"))
    }
  }

  override def setDeleted(routeId: Long, by: String): Try[RouteMeta] = {
    get(routeId) match {
      case Success(routeMeta) =>
        val now = new DateTime()
        update(routeMeta.copy(modifiedDate = now, modifiedBy = by, releasedDate = now, releasedBy = by))
      case Failure(f) => Failure(new Exception("Unable to find metadata for routeId: " + routeId))
    }
  }

  override def all: Try[List[RouteMeta]] = db.withConnection { implicit c =>
    Try( SQL("select * from route_metadata").as(routeMetaParser.*) )
  }
  
}

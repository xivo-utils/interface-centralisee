package services

import java.util.UUID
import javax.inject.Inject

import anorm.SqlParser.get
import anorm.{RowParser, SQL, ~}
import com.google.inject.Singleton
import model.utils.CachedFinder
import model.{Entity, EntityInterface, User, UserList}
import org.slf4j.LoggerFactory
import play.api.db.{Database, NamedDatabase}
import services.cache.CachedXivoUser

import scala.collection.mutable.ListBuffer

@Singleton
class UserFinder @Inject()(@NamedDatabase("default") db: Database) {

  val logger = LoggerFactory.getLogger(getClass)

  def listBothUserIdsForEntity(entityId: Long): List[(Long, Long)] = {
    def parseTuple: RowParser[(Long, Long)] =
      get[Long]("id") ~ get[Long]("id_on_xivo") map {
        case id ~ id_on_xivo => (id, id_on_xivo)
      }

    db.withConnection { implicit c => {
      SQL( "SELECT id, id_on_xivo FROM users WHERE entity_id = {eId} ORDER BY id")
        .on('eId -> entityId)
        .as(parseTuple.*)
    }}
  }

  def listUserIdsForEntity(entityId: Long): List[Long] = db.withConnection { implicit c =>
    SQL("SELECT id FROM users WHERE entity_id = {entityId} ORDER BY id")
      .on('entityId -> entityId)
      .as(get[Long]("id").*)
  }

  def listAcrossXivosFromCache(entityInterface: EntityInterface, cachedUsers: List[CachedXivoUser], limit: Int = 100): List[User] =
    if (cachedUsers.isEmpty) Nil
    else db.withConnection(implicit c => {
      val select = """
        SELECT u.id, u.entity_id, x.uuid AS xivo_uuid, u.id_on_xivo, r.digits, u.interval_id, u.template_id
        FROM users u
        LEFT JOIN route r ON u.external_route_id = r.id
        LEFT JOIN entities e ON u.entity_id = e.id
        LEFT JOIN xivo x ON e.xivo_id = x.id
      """
      val cachedUsersMap: Map[(UUID,Int),CachedXivoUser] = cachedUsers.map(xu => (xu.xivoUuid, xu.xivoUserId) -> xu)(collection.breakOut)
      val where = cachedUsersMap.keys.foldLeft(" WHERE FALSE ") { case (cond: String, (xivoUuid: UUID, xivoUserId: Int)) =>
        cond + s" OR (x.uuid = '$xivoUuid' AND u.id_on_xivo = $xivoUserId)"
      }
      val order = " ORDER BY u.id"

      def findEntity(key: Long) = entityInterface.getEntity(key)
      val entityFinder = CachedFinder(findEntity)

      SQL(select + where + order)
        .as(parseAndGetUserUsingCacheAcrossXivos(entityFinder, cachedUsersMap).*)
        .flatten
    })

  def listForEntityFromCache(entity: Entity, cachedUsers: Map[Int,CachedXivoUser]): UserList = {
    val ghostIds = ListBuffer[Long]()
    val users = db.withConnection(implicit c => {
      SQL( """
        SELECT u.id, id_on_xivo, digits, interval_id, template_id
        FROM users u
        LEFT JOIN route r ON u.external_route_id = r.id
        WHERE entity_id = {eId}
        ORDER BY u.id
      """)
        .on('eId -> entity.id)
        .as(parseAndGetUserUsingCache(entity, cachedUsers, ghostIds).*)
        .flatten
    })

    UserList(users, ghostIds.toList)
  }

  def getUserUsingCache(id: Long, entity: Entity, cachedUsers: Map[Int,CachedXivoUser]): Option[User] = {
    db.withConnection { implicit c =>
      SQL("""
        SELECT u.id, u.entity_id, u.id_on_xivo, r.digits, u.interval_id, u.template_id
        FROM users u
        LEFT JOIN route r ON u.external_route_id = r.id
        WHERE u.id = {id}
        LIMIT 1
       """)
        .on('id -> id)
        .as(parseAndGetUserUsingCache(entity, cachedUsers, ListBuffer[Long]()).*)
        .headOption
        .flatten
    }
  }

  private def parseAndGetUserUsingCacheAcrossXivos(findEntity: Long => Option[Entity], cachedUsers: Map[(UUID,Int),CachedXivoUser]) : RowParser[Option[User]] =
    get[Option[Long]]("id") ~ get[Int]("entity_id") ~ get[UUID]("xivo_uuid") ~ get[Int]("id_on_xivo") ~ get[Option[String]]("digits") ~ get[Option[Int]]("interval_id") ~ get[Option[Int]]("template_id") map {
      case id ~ entityId ~ xivoUuid ~ idOnXivo ~ extNum ~ intervalId ~ templateId =>
        cachedUsers.get((xivoUuid, idOnXivo)) flatMap { cachedUser =>
          findEntity(entityId).map { entity =>
            cachedUser.toUser(id, entity, intervalId, templateId)
          }
        }
    }

  private def parseAndGetUserUsingCache(entity: Entity, cachedUsers: Map[Int,CachedXivoUser], ghostIds: ListBuffer[Long]): RowParser[Option[User]] =
    get[Option[Long]]("id") ~ get[Int]("id_on_xivo") ~ get[Option[String]]("digits") ~ get[Option[Int]]("interval_id") ~ get[Option[Int]]("template_id") map {
      case id ~ idOnXivo ~ extNum ~ intervalId ~ templateId =>
        if (cachedUsers.contains(idOnXivo)) {
          Some(cachedUsers(idOnXivo).toUser(id, entity, intervalId, templateId))
        } else {
          ghostIds.append(id.get)
          None
        }
    }

}

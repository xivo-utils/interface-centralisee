package services

import com.google.inject.{ImplementedBy, Inject, Singleton}
import model.{IncoherentUser, _}

@ImplementedBy(classOf[UserRouteCoherencyImpl])
trait UserRouteCoherencyInterface {
  def getIncoherentUsers(): List[User]
  def getIncoherentUsersAsCsv(): String
}

@Singleton
class UserRouteCoherencyImpl @Inject()(users: UserManager, entities: EntityInterface, route: RouteInterface)
  extends UserRouteCoherencyInterface {

  def getIncoherentUsersAsCsv(): IncoherentUser.CSV = {
    IncoherentUser.convertToCsv(getIncoherentUsers().map(IncoherentUser(_)))
  }

  def getIncoherentUsers(): List[User] = {
    val e: List[Entity] = entities.listFromDb
    val ue: List[(User, Entity)] = e.flatMap(e => users.getFromXivoAndCacheForEntity(e).users.map(u => (u, e)))
    ue.filter({ case (u: User, e:Entity) => ! userRoutePresent(u, e)}).map(r => r._1)
  }

  private def userRoutePresent(u: User, e: Entity): Boolean = {
    u.externalNumber.fold(
      route.routeExists(u.internalNumber)
    )(
      extNo => {
        route.routeExists(u.internalNumber) && route.routeExists(extNo.replace("\"", ""))
      }
    )
  }
}

package services

import model.VoiceMailNumberMode.{Custom, ShortNumber}
import model._
import model.utils.{ConfigParser, GlobalXivoConnectorPool, XivoConnectorPool}
import xivo.restapi.model.{CtiConfiguration, IncomingCall, Line, Voicemail, User => XivoUser}

import java.security.InvalidParameterException
import javax.inject.{Inject, Singleton}

object XivoUserTransformer {
  def createVoicemail(context: String, vmNumber: String ): Voicemail = {
    val res = new Voicemail()
    res.setName(vmNumber)
    res.setContext(context)
    res.setNumber(vmNumber)
    res.setPassword(vmNumber)
    res
  }
}

@Singleton
class XivoUserTransformer @Inject()(templateManager: TemplateManager, config: ConfigParser, globalXivoConnectorPool: GlobalXivoConnectorPool) {

  import XivoUserTransformer._
  var connectorPool: XivoConnectorPool = globalXivoConnectorPool

  def createXivoUser(user: User): XivoUser = connectorPool.withConnector(user.entity.xivo)(connector => {
    val template: Template = user.templateId match {
      case Some(id) => templateManager.get(id).getOrElse(throw new InvalidParameterException("Unable to get template"))
      case None => throw new InvalidParameterException("Unable to get template")
    }

    val line = new Line(user.internalNumber)
    line.setContext(user.entity.name)

    user.peerSipName.getOrElse(template.properties.peerSipName) match {
      case model.PeerSipNameMode.Auto =>
      case model.PeerSipNameMode.Model => line.setName(user.internalNumber)
      case model.PeerSipNameMode.WebRTC => line.setOptions("webrtc", "yes")
      case model.PeerSipNameMode.UniqueAccount => line.setOptions("webrtc", "ua")
    }

    line.setRegistrar(user.entity.mdsName)

    val ringTime = user.ringingTime.getOrElse(template.properties.ringingTime)

    val xivoUser = new XivoUser()
    xivoUser.setFirstname(user.firstName)
    xivoUser.setLastname(user.lastName)
    xivoUser.setLine(line)
    xivoUser.setRingSeconds(ringTime)
    xivoUser.setEmail(user.mail.orNull)

    user.intervalId match {
      case None =>
        throw new InvalidParameterException("Invalid interval id")
      case Some(intervalId) =>
        xivoUser.setIncomingCall(incomingCallForUser(user))
    }

    if(user.ctiLogin.isDefined && user.ctiPassword.isDefined) {
      xivoUser.setUsername(user.ctiLogin.get)
      xivoUser.setPassword(user.ctiPassword.get)
      xivoUser.setCtiConfiguration(new CtiConfiguration(true, connector.getDefaultCtiProfile))
    }
    if(!config.disableOutcallerIdProcessing) {
      user.callerIdMode.getOrElse(template.properties.callerIdMode) match {
        case CallerIdMode.Anonymous =>
          xivoUser.setOutcallerid(config.anonymousCallerId)
        case CallerIdMode.Custom =>
          xivoUser.setOutcallerid(
            user.customCallerId
              .getOrElse(template.properties.customCallerId.getOrElse(config.anonymousCallerId)))
        case CallerIdMode.IncomingNo =>
          xivoUser.setOutcallerid(user.externalNumber.getOrElse(config.anonymousCallerId))
      }
    }
    xivoUser
  })

  def addNewVoicemailToUser(xivoUser: XivoUser, user: User, voicemailSettings: VoicemailSettings): Unit = {
    val voicemail = voicemailSettings.numberMode match {
      case None =>
        null
      case Some(ShortNumber) =>
        val vm = createVoicemail(user.entity.name, user.internalNumber)
        updatePrivateVoicemail(vm, UserWithVM(user, voicemailSettings))
        vm
      case Some(Custom) =>
        createVoicemail(user.entity.name, voicemailSettings.customNumber.get)
      case Some(other) =>
        throw new Exception(s"Unhandled number mode $other")
    }
    xivoUser.setVoicemail(voicemail)
  }

  def updatePrivateVoicemail(voicemail: Voicemail, updated: UserWithVM): Unit = {
    require(updated.voicemail.numberMode.contains(ShortNumber))
    voicemail.setNumber(updated.user.internalNumber)
    voicemail.setName(updated.user.firstName + " " + updated.user.lastName)
    voicemail.setEmail(updated.user.mail.getOrElse(""))
    val sendEmail = updated.voicemail.sendEmail.getOrElse(false)
    voicemail.setAttach(sendEmail)
    val deleteMessage = updated.voicemail.deleteAfterNotif.getOrElse(false)
    voicemail.setDeleteMessages(deleteMessage)
  }

  private def incomingCallForUser(u: User): IncomingCall = {
    val ic = new IncomingCall()
    ic.setContext(config.incallContext)
    ic.setSda(u.internalNumber)
    ic
  }

  def updateXivoUser(xivoUser: XivoUser, user: User): XivoUser = {
    val line = xivoUser.getLine
    line.setContext(user.entity.name)
    line.setNumber(user.internalNumber)

    user.peerSipName match {
      case Some(model.PeerSipNameMode.WebRTC) =>
        line.removeOptions("webrtc")
        line.setOptions("webrtc", "yes")
      case Some(model.PeerSipNameMode.UniqueAccount) =>
        line.removeOptions("webrtc")
        line.setOptions("webrtc", "ua")
      case Some(model.PeerSipNameMode.Auto) => line.removeOptions("webrtc")
      case Some(_) =>
      case None =>
    }

    xivoUser.setCallerid(s""""${user.firstName} ${user.lastName}"""")
    xivoUser.setNameWithoutCalledIdUpdate(user.firstName, user.lastName)
    xivoUser.setEmail(user.mail.orNull)

    user match {
      case _ if user.ctiLogin.isDefined && user.ctiPassword.isDefined =>
        xivoUser.setUsername(user.ctiLogin.get)
        xivoUser.setPassword(user.ctiPassword.get)
        xivoUser.setCtiConfiguration(new CtiConfiguration(true, connectorPool.withConnector(user.entity.xivo)(c => c.getDefaultCtiProfile)))
      case _ if !user.ctiLogin.isDefined && !user.ctiPassword.isDefined =>
        xivoUser.setUsername("")
        xivoUser.setPassword("")
        xivoUser.setCtiConfiguration(new CtiConfiguration(false, connectorPool.withConnector(user.entity.xivo)(c => c.getDefaultCtiProfile)))
      case _ =>
        xivoUser.setCtiConfiguration(null)
        xivoUser.setUsername("")
        xivoUser.setPassword(null)
    }

    if(!config.disableOutcallerIdProcessing) {
      user.callerIdMode.map {
        case CallerIdMode.Anonymous => xivoUser.setOutcallerid(config.anonymousCallerId)
        case CallerIdMode.Custom => user.customCallerId.map(xivoUser.setOutcallerid)
        case CallerIdMode.IncomingNo => user.externalNumber.map(xivoUser.setOutcallerid)
      }
    } else {
      xivoUser.setOutcallerid(null)
    }

    xivoUser.setLine(line)

    user.ringingTime.foreach(xivoUser.setRingSeconds(_))

    xivoUser
  }
}

package services

import java.util.UUID
import javax.inject.Inject

import anorm._
import com.google.inject.{ImplementedBy, Singleton}
import common.{AnormMacro, Crud, CrudMacro}
import model.{Xivo, XivoObj}
import org.slf4j.LoggerFactory
import play.api.db.{Database, _}
import services.dbmapper.UuidDbMapper._

import scala.util.{Failure, Try}

case class XivoDb(id: Option[Long], uuid: UUID, name: String, host: String, contextName: String, playAuthToken: Option[String], isProxy: Boolean = false) extends XivoObj
object XivoDb {
  def fromXivo(x: Xivo) = XivoDb(x.id, x.uuid, x.name, x.host, x.contextName, x.playAuthToken, x.isProxy)
}

@ImplementedBy(classOf[XivoManagerImpl])
trait XivoManager extends Crud[XivoDb, Long] {
  def get(uuid: UUID): Try[XivoDb]
}

@Singleton
class XivoManagerImpl @Inject()(@NamedDatabase("default") db: Database) extends XivoManager {
  val logger = LoggerFactory.getLogger (getClass)

  val xivoParser: RowParser[XivoDb] =
    AnormMacro.namedParser[XivoDb](AnormMacro.ColumnNaming.SnakeCase)

  override def create(xivo: XivoDb): Try[XivoDb] = {
    Try({
      db.withTransaction {
        implicit c => {
          val id = CrudMacro.query[XivoDb](xivo, "xivo", Set("id"), true, CrudMacro.SnakeCase, CrudMacro.SQLInsert)
            .executeInsert[Option[Long]]()
          xivo.copy(id = id)
        }
      }
    })
  }

  override def get(id: Long): Try[XivoDb] = db.withConnection { implicit c =>
    Try( SQL("select * from xivo where id={id}").on("id" -> id).as(xivoParser.single) )
  }

  override def get(uuid: UUID): Try[XivoDb] = db.withConnection { implicit c =>
    Try( SQL("select * from xivo where uuid={id}:uuid").on("id" -> uuid).as(xivoParser.single) )
  }


  override def update(xivo: XivoDb): Try[XivoDb] ={
    Try({
      db.withTransaction {
        implicit c => {
          val updatedCnt = CrudMacro.query[XivoDb](xivo, "xivo", Set("id"), true, CrudMacro.SnakeCase, CrudMacro.SQLUpdate)
            .executeUpdate()(c)
          if (updatedCnt != 1) {
            throw new Exception("Unable to update Xivo entities")
          }
        }
      }
      xivo
    })
  }

  override def delete(id: Long): Try[XivoDb] = db.withConnection { implicit c =>
    val entry = get(id)
    if (entry.isFailure) entry
    else {
      val deletedCount = SQL("delete from xivo where id={id}").on("id" -> id).executeUpdate()
      if (deletedCount == 1) entry
      else Failure(new Exception("Cannot delete Xivo"))
    }
  }

  override def all: Try[List[XivoDb]] = db.withConnection { implicit c =>
    Try( SQL("select * from xivo").as(xivoParser.*) )
  }

}

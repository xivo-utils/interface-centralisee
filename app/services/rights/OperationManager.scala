package services.rights

import javax.inject.Inject
import anorm._
import com.google.inject.{ImplementedBy, Singleton}
import common.{AnormMacro, Crud, CrudMacro}
import model.rights.Operation
import org.slf4j.LoggerFactory
import play.api.db.{Database, _}

import scala.util.{Failure, Try}

@ImplementedBy(classOf[OperationManagerImpl])
trait OperationManager extends Crud[Operation, Long] {}

@Singleton
class OperationManagerImpl @Inject()(@NamedDatabase("default") db: Database) extends OperationManager {
  val logger = LoggerFactory.getLogger(getClass)

  val operationParser: RowParser[Operation] =
    AnormMacro.namedParser[Operation](AnormMacro.ColumnNaming.SnakeCase)

  val table = "operations"
  val idColumn = "id"

  override def create(operation: Operation): Try[Operation] = {
    Try({
      db.withTransaction {
        implicit c => {
          val id = CrudMacro.query[Operation](operation, table, Set(idColumn), true, CrudMacro.SnakeCase, CrudMacro.SQLInsert)
            .executeInsert[Option[Long]]()
          operation.copy(id = id)
        }
      }
    })
  }

  override def get(id: Long): Try[Operation] = db.withConnection { implicit c =>
    Try( SQL(s"select * from $table where $idColumn={id}").on("id" -> id).as(operationParser.single) )
  }


  override def update(operation: Operation): Try[Operation] ={
    Try({
      db.withTransaction {
        implicit c => {
          val updatedCnt = CrudMacro.query[Operation](operation, table, Set(idColumn), true, CrudMacro.SnakeCase, CrudMacro.SQLUpdate)
            .executeUpdate()(c)
          if (updatedCnt != 1) {
            throw new Exception("Unable to update Operation")
          }
        }
      }
      operation
    })
  }

  override def delete(id: Long): Try[Operation] = db.withConnection { implicit c =>
    val entry = get(id)
    if (entry.isFailure) entry
    else {
      val deletedCount = SQL(s"delete from $table where $idColumn={id}").on("id" -> id).executeUpdate()
      if (deletedCount == 1) entry
      else Failure(new Exception("Cannot delete operation"))
    }
  }

  override def all: Try[List[Operation]] = db.withConnection { implicit c =>
    Try( SQL(s"select * from $table").as(operationParser.*) )
  }

}

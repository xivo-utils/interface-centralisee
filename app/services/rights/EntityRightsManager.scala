package services.rights

import com.google.inject.{ImplementedBy, Inject, Singleton}
import common.Crud
import model.{Entity, EntityInterface, XivoInterface}
import model.rights._
import org.slf4j.LoggerFactory

import scala.util.{Failure, Success, Try}

@ImplementedBy(classOf[EntityRightsImpl])
trait EntityRightsInterface extends Crud[UIRole, Long]{
  def getEmptyRole(): UIRole
  def getSuperadminRole(): UIRole
}

@Singleton
class EntityRightsImpl @Inject()(xivoManager: XivoInterface, entityManager: EntityInterface,
                                 operationManager: OperationManager, permissionManager: PermissionManager,
                                 roleManager: RoleManager, rolePermissionManager: RolePermissionManager)
  extends EntityRightsInterface {

  val logger = LoggerFactory.getLogger(getClass)

  override def getEmptyRole(): UIRole = {
    getCompleteRole(UIOperations())
  }

  override def getSuperadminRole(): UIRole = {
    getCompleteRole(UIOperations(true, true, true, true))
  }

  private def getCompleteRole(o: UIOperations): UIRole = {
    val xivos = xivoManager.list
    val entities: List[Entity] = entityManager.listFromDb
    UIRole(None, None, xivos.map(x =>
      UIXivoPermission(x.id.get, x.name, entities.filter(e => e.xivo.id.get == x.id.get).map( e =>
        UIEntityPermission(e.id.get, e.name, o)))))
  }

  override def get(id: Long): Try[UIRole] = {
    Try {
      val role = roleManager.get(id).get
      val rolePermission = rolePermissionManager.getForRole(role.id.get).get
      val xivos = xivoManager.list
      val entities = entityManager.listFromDb
      val uiXivoPermissions = xivos.map(x =>
        UIXivoPermission(x.id.get, x.name, entities.filter(e => e.xivo.id.get == x.id.get).map(e =>
          getUIEntityPermission(e, rolePermission)))
      )
      val uir = UIRole(role.id, Some(role.name), uiXivoPermissions)
      logger.debug(s"Returning uiRole $uir")
      uir
    }
  }

  private def getUIEntityPermission(e: Entity, rps: List[RolePermission]): UIEntityPermission = {
    permissionManager.getForEntity(e.id.get) match {
      case Success(all) =>
        all.find(p => rps.find(rp => rp.permissionId == p.id.get).nonEmpty) match {
          case Some(p) =>
            logger.debug(s"Got permission for entity $p, $e")
            UIEntityPermission(e.id.get, e.name, getUIOperation(p.operationId))
          case None =>
            logger.debug("No permission for entity matching role permissions")
            UIEntityPermission(e.id.get, e.name, UIOperations())
          }
      case Failure(f) =>
        logger.debug(s"No permission for entity $e")
        UIEntityPermission(e.id.get, e.name, UIOperations())
    }
  }

  private def getUIOperation(id: Long): UIOperations = {
    operationManager.get(id) match {
      case Success(o) =>
        UIOperations(o.reading, o.creating, o.editing, o.deleting)
      case Failure(f) =>
        logger.error(s"Unable to find references operation with id: $id")
        UIOperations()
    }
  }

  override def create(uir: UIRole): Try[UIRole] = {
    Try {
      val role = roleManager.create(Role(uir.id, uir.name.get)).get
      uir.xivo.foreach(x => x.entities.foreach(e => insertRightForEntity(role, e)))
      uir.copy(id = role.id)
    }
  }

  private def insertRightForEntity(role: Role, e: UIEntityPermission) = {
    val operation = operationManager.create(
      Operation(None, role.name, e.operations.reading, e.operations.creating, e.operations.editing, e.operations.deleting))
      .get
    val permission = permissionManager.create(Permissions(None, e.entityId, operation.id.get, role.name)).get
    rolePermissionManager.create(RolePermission(role.id.get, permission.id.get))
  }

  override def all(): Try[List[UIRole]] = Try {
    roleManager.all().get.map(r => UIRole(r.id, Some(r.name), List()))
  }

  override def delete(id: Long): Try[UIRole] = {
    get(id) match {
      case Success(role) =>
        deleteRole(role)
      case Failure(f) =>
        logger.warn(s"Unable to delete unexisting role with id $id")
        Failure(new Exception("Role does not exist"))
    }
  }

  private def deleteRole(uiRole: UIRole): Try [UIRole] = Try {
    roleManager.get(uiRole.id.get).foreach(role => {
      deletePermissions(role)
      roleManager.delete(role.id.get)
      logger.debug(s"deleted role $role")
    })
    uiRole
  }

  private def deletePermissions(role: Role) = Try {
    rolePermissionManager.getForRole(role.id.get).foreach(rps =>
      rps.foreach(rp => (
        rolePermissionManager.delete(rp.permissionId).foreach(rp =>
          permissionManager.delete(rp.permissionId).foreach(p =>
            operationManager.delete(p.id.get)
          )
        )
        ))
    )
  }


  override def update(uiRole: UIRole): Try[UIRole] = {
    uiRole.id match {
      case None => Failure(new Exception("Unable to update role without id"))
      case Some(id) =>
        roleManager.get(uiRole.id.get) match {
          case Failure(f) => Failure(new Exception("Unable to find role to update"))
          case Success(role) =>
            deletePermissions(role).isFailure
            uiRole.xivo.foreach(x => x.entities.foreach(e => insertRightForEntity(role, e)))
            if(uiRole.name.get != role.name) {
              roleManager.update(Role(role.id, uiRole.name.get))
            }
            Success(uiRole)
        }
    }
  }

}

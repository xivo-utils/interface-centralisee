package services.rights

import javax.inject.Inject

import anorm._
import com.google.inject.{ImplementedBy, Singleton}
import common.{AnormMacro, CrudMacro}
import model.rights.AdministratorRole
import org.slf4j.LoggerFactory
import play.api.db.{Database, _}

import scala.util.{Failure, Try}

@ImplementedBy(classOf[AdministratorRoleManagerImpl])
trait AdministratorRoleManager {
  def create(t: AdministratorRole): Try[AdministratorRole]
  def update(t: AdministratorRole): Try[AdministratorRole]
  def get(adminId: Long): Try[List[AdministratorRole]]
  def delete(adminId: Long): Try[List[AdministratorRole]]
  def all(): Try[List[AdministratorRole]]
}

@Singleton
class AdministratorRoleManagerImpl @Inject()(@NamedDatabase("default") db: Database) extends AdministratorRoleManager {
  val logger = LoggerFactory.getLogger(getClass)

  val administratorRoleParser: RowParser[AdministratorRole] =
    AnormMacro.namedParser[AdministratorRole](AnormMacro.ColumnNaming.SnakeCase)

  val table = "administrator_role"
  val adminIdColumn = "administrator_id"

  override def create(adminRole: AdministratorRole): Try[AdministratorRole] = {
    Try({
      db.withTransaction {
        implicit c => {
          val id = CrudMacro.query[AdministratorRole](adminRole, table, Set(), false, CrudMacro.SnakeCase,
            CrudMacro.SQLInsert).executeInsert()
          adminRole
        }
      }
    })
  }

  override def get(adminId: Long): Try[List[AdministratorRole]] = db.withConnection { implicit c =>
    Try( SQL(s"select * from $table where $adminIdColumn={id}").on("id" -> adminId).as(administratorRoleParser.*) )
  }

  override def update(adminRole: AdministratorRole): Try[AdministratorRole] ={
    Try({
      db.withTransaction {
        implicit c => {
          val updatedCnt = CrudMacro.query[AdministratorRole](adminRole, table, Set("administratorId"), false, CrudMacro.SnakeCase,
            CrudMacro.SQLUpdate).executeUpdate()(c)
          if (updatedCnt != 1) {
            throw new Exception("Unable to update AdministratorRole")
          }
        }
      }
      adminRole
    })
  }

  override def delete(adminId: Long): Try[List[AdministratorRole]] = db.withConnection { implicit c =>
    val entry = get(adminId)
    if (entry.isFailure) entry
    else {
      entry.get.foreach(e => {
        val deletedCount = SQL(s"delete from $table where $adminIdColumn={id}").on("id" -> adminId).executeUpdate()
        if (deletedCount == 1) { entry }
        else { Failure(new Exception("Cannot delete AdministratorRole")) }
      })
      entry
    }
  }

  override def all: Try[List[AdministratorRole]] = db.withConnection { implicit c =>
    Try( SQL(s"select * from $table").as(administratorRoleParser.*) )
  }

}

package services.rights

import javax.inject.Inject
import anorm._
import com.google.inject.{ImplementedBy, Singleton}
import common.{AnormMacro, Crud, CrudMacro}
import model.rights.RolePermission
import org.slf4j.LoggerFactory
import play.api.db.{Database, _}
import scala.util.{Failure, Try}

@ImplementedBy(classOf[RolePermissionManagerImpl])
trait RolePermissionManager extends Crud[RolePermission, Long] {
  def getForRole(roleId: Long): Try[List[RolePermission]]
}

@Singleton
class RolePermissionManagerImpl @Inject()(@NamedDatabase("default") db: Database) extends RolePermissionManager {
  val logger = LoggerFactory.getLogger(getClass)

  val rolePermissionParser: RowParser[RolePermission] =
    AnormMacro.namedParser[RolePermission](AnormMacro.ColumnNaming.SnakeCase)

  val table = "role_permission"
  val roleIdColumn = "role_id"
  val permissionIdColumn = "permission_id"

  override def create(rolePermission: RolePermission): Try[RolePermission] = {
    Try({
      db.withTransaction {
        implicit c => {
          val id = CrudMacro.query[RolePermission](rolePermission, table, Set(), false, CrudMacro.SnakeCase,
            CrudMacro.SQLInsert).executeInsert()
          rolePermission
        }
      }
    })
  }

  override def get(id: Long): Try[RolePermission] = db.withConnection { implicit c =>
    Try( SQL(s"select * from $table where $permissionIdColumn={id}").on("id" -> id).as(rolePermissionParser.single) )
  }

  override def getForRole(roleId: Long): Try[List[RolePermission]] = db.withConnection { implicit c =>
    Try(SQL(s"select * from $table where $roleIdColumn={rId}").on("rId" -> roleId).as(rolePermissionParser.*))
  }

  override def update(permissionId: RolePermission): Try[RolePermission] ={
    Try({
      db.withTransaction {
        implicit c => {
          val updatedCnt = CrudMacro.query[RolePermission](permissionId, table, Set("roleId"), false, CrudMacro.SnakeCase,
            CrudMacro.SQLUpdate).executeUpdate()(c)
          if (updatedCnt != 1) {
            throw new Exception("Unable to update RolePermission")
          }
        }
      }
      permissionId
    })
  }

  override def delete(permissionId: Long): Try[RolePermission] = db.withConnection { implicit c =>
    val entry = get(permissionId)
    if (entry.isFailure) entry
    else {
      val deletedCount = SQL(s"delete from $table where $permissionIdColumn={id}").on("id" -> permissionId).executeUpdate()
      if (deletedCount == 1) entry
      else Failure(new Exception("Cannot delete RolePermission"))
    }
  }

  override def all: Try[List[RolePermission]] = db.withConnection { implicit c =>
    Try( SQL(s"select * from $table").as(rolePermissionParser.*) )
  }

}

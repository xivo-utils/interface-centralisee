package services.rights

import javax.inject.Inject
import anorm._
import com.google.inject.{ImplementedBy, Singleton}
import common.{AnormMacro, Crud, CrudMacro}
import model.rights.Permissions
import org.slf4j.LoggerFactory
import play.api.db.{Database, _}

import scala.util.{Failure, Try}

@ImplementedBy(classOf[PermissionManagerImpl])
trait PermissionManager extends Crud[Permissions, Long] {
  def getForEntity(entityId: Long): Try[List[Permissions]]
}

@Singleton
class PermissionManagerImpl @Inject()(@NamedDatabase("default") db: Database) extends PermissionManager {
  val logger = LoggerFactory.getLogger(getClass)

  val permissionParser: RowParser[Permissions] =
    AnormMacro.namedParser[Permissions](AnormMacro.ColumnNaming.SnakeCase)

  val table = "permissions"
  val idColumn = "id"
  val entityIdColumn = "entity_id"

  override def create(permission: Permissions): Try[Permissions] = {
    Try({
      db.withTransaction {
        implicit c => {
          val id = CrudMacro.query[Permissions](permission, table, Set(idColumn), true, CrudMacro.SnakeCase,
            CrudMacro.SQLInsert).executeInsert[Option[Long]]()
          permission.copy(id = id)
        }
      }
    })
  }

  override def get(id: Long): Try[Permissions] = db.withConnection { implicit c =>
    Try( SQL(s"select * from $table where $idColumn={id}").on("id" -> id).as(permissionParser.single) )
  }

  override def getForEntity(entityId: Long): Try[List[Permissions]] = db.withConnection { implicit c =>
    Try( SQL(s"select * from $table where $entityIdColumn={eId}").on("eId" -> entityId).as(permissionParser.*) )
  }

  override def update(permission: Permissions): Try[Permissions] ={
    Try({
      db.withTransaction {
        implicit c => {
          val updatedCnt = CrudMacro.query[Permissions](permission, table, Set(idColumn), true, CrudMacro.SnakeCase,
            CrudMacro.SQLUpdate).executeUpdate()(c)
          if (updatedCnt != 1) {
            throw new Exception("Unable to update permission")
          }
        }
      }
      permission
    })
  }

  override def delete(id: Long): Try[Permissions] = db.withConnection { implicit c =>
    val entry = get(id)
    if (entry.isFailure) entry
    else {
      val deletedCount = SQL(s"delete from $table where $idColumn={id}").on("id" -> id).executeUpdate()
      if (deletedCount == 1) entry
      else Failure(new Exception("Cannot delete permission"))
    }
  }

  override def all: Try[List[Permissions]] = db.withConnection { implicit c =>
    Try( SQL(s"select * from $table").as(permissionParser.*) )
  }

}

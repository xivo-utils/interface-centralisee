package services.rights

import javax.inject.Inject
import anorm._
import com.google.inject.{ImplementedBy, Singleton}
import common.{AnormMacro, Crud, CrudMacro}
import model.rights.Role
import org.slf4j.LoggerFactory
import play.api.db.{Database, _}

import scala.util.{Failure, Try}

@ImplementedBy(classOf[RoleManagerImpl])
trait RoleManager extends Crud[Role, Long] {}

@Singleton
class RoleManagerImpl @Inject()(@NamedDatabase("default") db: Database) extends RoleManager {
  val logger = LoggerFactory.getLogger(getClass)

  val roleParser: RowParser[Role] =
    AnormMacro.namedParser[Role](AnormMacro.ColumnNaming.SnakeCase)

  override def create(role: Role): Try[Role] = {
    Try({
      db.withTransaction {
        implicit c => {
          val id = CrudMacro.query[Role](role, "roles", Set("id"), true, CrudMacro.SnakeCase, CrudMacro.SQLInsert)
            .executeInsert[Option[Long]]()
          role.copy(id = id)
        }
      }
    })
  }

  override def get(id: Long): Try[Role] = db.withConnection { implicit c =>
    Try( SQL("select * from roles where id={id}").on("id" -> id).as(roleParser.single) )
  }


  override def update(role: Role): Try[Role] ={
    Try({
      db.withTransaction {
        implicit c => {
          val updatedCnt = CrudMacro.query[Role](role, "roles", Set("id"), true, CrudMacro.SnakeCase, CrudMacro.SQLUpdate)
            .executeUpdate()(c)
          if (updatedCnt != 1) {
            throw new Exception("Unable to update Role")
          }
        }
      }
      role
    })
  }

  override def delete(id: Long): Try[Role] = db.withConnection { implicit c =>
    val entry = get(id)
    if (entry.isFailure) entry
    else {
      val deletedCount = SQL("delete from roles where id={id}").on("id" -> id).executeUpdate()
      if (deletedCount == 1) entry
      else Failure(new Exception("Cannot delete role"))
    }
  }

  override def all: Try[List[Role]] = db.withConnection { implicit c =>
    Try( SQL("select * from roles").as(roleParser.*) )
  }

}

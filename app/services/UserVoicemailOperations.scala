package services

import javax.inject.Inject

import com.google.inject.Singleton
import model.UserWithVM
import model.VoiceMailNumberMode.{Custom, ShortNumber}
import model.utils.{Step}
import org.slf4j.LoggerFactory
import xivo.ldap.xivoconnection.XivoConnector

import scala.collection.JavaConverters._
import xivo.restapi.model.{Voicemail, User => XivoUser}

@Singleton
class UserVoicemailOperations @Inject()(xivoUserTransformer: XivoUserTransformer) {

  private val logger = LoggerFactory.getLogger(getClass)

  private def findVoicemail(number: String)(implicit connector: XivoConnector): Option[Voicemail] =
    connector.listVoicemails.asScala.find(_.getNumber == number)

  def stepCreateNewVoicemail(xivoUser: XivoUser, updated: UserWithVM)(implicit connector: XivoConnector): Step = {
    var createdVoicemail: Option[Voicemail] = None
    Step(
      executeFun = {
        assert(xivoUser.getVoicemail == null, "Expecting XivoUser without voicemail")
        xivoUserTransformer.addNewVoicemailToUser(xivoUser, updated.user, updated.voicemail)
        connector.createVoicemailForUser(xivoUser)
        createdVoicemail = Some(xivoUser.getVoicemail)
      },
      rollbackFun = {
        createdVoicemail.foreach { voicemail =>
          xivoUser.setVoicemail(voicemail)
          connector.deleteVoicemailForUser(xivoUser)
          xivoUser.setVoicemail(null)
        }
      }
    )
  }

  def stepDeleteVoicemail(xivoUser: XivoUser)(implicit connector: XivoConnector): Step = {
    var deletedVoicemail: Option[Voicemail] = None
    Step(
      executeFun = {
        assert(xivoUser.getVoicemail != null, "Expecting XivoUser with voicemail")
        deletedVoicemail = Some(xivoUser.getVoicemail)
        connector.deleteVoicemailForUser(xivoUser)
        xivoUser.setVoicemail(null)
      },
      rollbackFun = {
        deletedVoicemail.foreach { voicemail =>
          xivoUser.setVoicemail(voicemail)
          connector.createVoicemailForUser(xivoUser)
        }
      }
    )
  }

  def stepAssociateCustomVoicemail(xivoUser: XivoUser, updated: UserWithVM)(implicit connector: XivoConnector): Step = {
    require(updated.voicemail.numberMode.contains(Custom))
    findVoicemail(updated.voicemail.customNumber.get) match {
      case None =>
        stepCreateNewVoicemail(xivoUser, updated)
      case Some(voicemail) =>
        Step(
          executeFun = {
            assert(xivoUser.getVoicemail == null, "Expecting XivoUser without voicemail")
            xivoUser.setVoicemail(voicemail)
            connector.associateVoicemailToUser(xivoUser)
          },
          rollbackFun = {
            connector.dissociateVoicemailFromUser(xivoUser)
            xivoUser.setVoicemail(null)
          }
        )
    }
  }

  def stepDisassociateCustomVoicemailFromUser(xivoUser: XivoUser, old: UserWithVM)(implicit connector: XivoConnector): Step = {
    require(old.voicemail.numberMode.contains(Custom))
    val vmNumber = old.voicemail.customNumber.get
    findVoicemail(vmNumber) match {
      case None =>
        throw new Exception(s"Voicemail number=$vmNumber not found")
      case Some(voicemail) =>
        Step(
          executeFun = {
            assert(Option(xivoUser.getVoicemail).exists(_.getNumber == vmNumber),
              s"Expecting XivoUser with voicemail having number=$vmNumber")
            connector.dissociateVoicemailFromUser(xivoUser)
            xivoUser.setVoicemail(null)
          },
          rollbackFun = {
            xivoUser.setVoicemail(voicemail)
            connector.associateVoicemailToUser(xivoUser)
          }
        )
    }
  }

  def stepOnCreateUser(xivoUser: XivoUser, created: UserWithVM)(implicit connector: XivoConnector): Option[Step] =
    created.voicemail.numberMode.map {
      case ShortNumber =>
        stepCreateNewVoicemail(xivoUser, created)
      case Custom =>
        stepAssociateCustomVoicemail(xivoUser, created)
      case other =>
        throw new Exception(s"Internal error: unhandled voicemail transition $other user voicemail create $created")
    }

  def stepsOnUpdateUser(oldXivoUser: XivoUser, xivoUser: XivoUser, old: UserWithVM, updated: UserWithVM)(implicit connector: XivoConnector): Seq[Step] =
    (old.voicemail.numberMode, updated.voicemail.numberMode) match {
      case (None, None) =>
        Nil

      case (None, Some(ShortNumber)) =>
        stepCreateNewVoicemail(xivoUser, updated) :: Nil

      case (None, Some(Custom)) =>
        stepAssociateCustomVoicemail(xivoUser, updated) :: Nil

      case (Some(ShortNumber), None) =>
        stepDeleteVoicemail(xivoUser) :: Nil

      case (Some(ShortNumber), Some(ShortNumber)) =>
        Step (
          executeFun = {
            xivoUserTransformer.updatePrivateVoicemail(xivoUser.getVoicemail, updated)
            connector.updateVoicemailForUser(xivoUser)
          },
          rollbackFun = {
            xivoUser.setVoicemail(oldXivoUser.getVoicemail)
            connector.updateVoicemailForUser(xivoUser)
          }
        ) :: Nil

      case (Some(ShortNumber), Some(Custom)) =>
        Seq(
          stepDeleteVoicemail(xivoUser),
          stepAssociateCustomVoicemail(xivoUser, updated)
        )

      case (Some(Custom), None) =>
        stepDisassociateCustomVoicemailFromUser(xivoUser, old) :: Nil

      case (Some(Custom), Some(ShortNumber)) =>
        Seq(
          stepDisassociateCustomVoicemailFromUser(xivoUser, old),
          stepCreateNewVoicemail(xivoUser, updated)
        )

      case (Some(Custom), Some(Custom)) if old.voicemail.customNumber == updated.voicemail.customNumber =>
        Nil

      case (Some(Custom), Some(Custom)) if old.voicemail.customNumber != updated.voicemail.customNumber =>
        Seq (
          stepDisassociateCustomVoicemailFromUser(xivoUser, old),
          stepAssociateCustomVoicemail(xivoUser, updated)
        )

      case other =>
        throw new Exception(s"Internal error: unhandled voicemail transition $other user voicemail update $old -> $updated")
    }


  def stepOnDeleteUser(xivoUser: XivoUser, deleted: UserWithVM)(implicit connector: XivoConnector): Option[Step] =
    deleted.voicemail.numberMode.map {
      case ShortNumber =>
        stepDeleteVoicemail(xivoUser)
      case Custom =>
        stepDisassociateCustomVoicemailFromUser(xivoUser, deleted)
      case other =>
        throw new Exception(s"Internal error: unhandled voicemail transition $other user voicemail delete $deleted")
    }

}

package services

import java.sql.Connection
import javax.inject.Inject

import anorm._
import com.google.inject.{ImplementedBy, Singleton}
import common.{AnormMacro, Crud, CrudMacro, DBUtil}
import model._
import org.slf4j.LoggerFactory
import play.api.db.{Database, _}

import scala.util.{Failure, Try}

@ImplementedBy(classOf[TemplateManagerImpl])
trait TemplateManager extends Crud[Template, Long] {
  def forEntity(eId: CombinedId): Try[List[Template]]
}

@Singleton
class TemplateManagerImpl @Inject()(@NamedDatabase("default") db: Database) extends TemplateManager {
  val logger = LoggerFactory.getLogger(getClass)

  implicit val peerSipNameModeToStatement = DBUtil.enumToStatement(PeerSipNameMode.enumValues)
  implicit val optionPeerSipNameModeToStatement = DBUtil.enumTypeMetaData[PeerSipNameMode.Type]()
  implicit def enumToPeerSipNameMode = DBUtil.columnToEnum(PeerSipNameMode.enumValues)

  implicit val callerIdModeToStatement = DBUtil.enumToStatement(CallerIdMode.enumValues)
  implicit val optionCallerIdModeToStatement = DBUtil.enumTypeMetaData[CallerIdMode.Type]()
  implicit def enumToCallerIdMode = DBUtil.columnToEnum(CallerIdMode.enumValues)

  implicit val voiceMailNumberModeToStatement = DBUtil.enumToStatement(VoiceMailNumberMode.enumValues)
  implicit val optionVoiceMailNumberModeToStatement = DBUtil.enumTypeMetaData[VoiceMailNumberMode.Type]()
  implicit def enumToVoiceMailNumberMode = DBUtil.columnToEnum(VoiceMailNumberMode.enumValues)

  val templatePropertiesParser: RowParser[TemplateProperties] =
    AnormMacro.namedParser[TemplateProperties](AnormMacro.ColumnNaming.SnakeCase)
  val templateXivosParser: RowParser[TemplateXivo] =
    AnormMacro.namedParser[TemplateXivo](AnormMacro.ColumnNaming.SnakeCase)
  val templateEntitiesParser: RowParser[TemplateEntity] =
    AnormMacro.namedParser[TemplateEntity](AnormMacro.ColumnNaming.SnakeCase)
  val templateIntervalsParser: RowParser[TemplateInterval] =
    AnormMacro.namedParser[TemplateInterval](AnormMacro.ColumnNaming.SnakeCase)

  override def create(template: Template): Try[Template] = {
    Try({
      db.withTransaction {
        implicit c => {
          val id = CrudMacro.query[TemplateProperties](template.properties, "template", Set("id"), true, CrudMacro.SnakeCase, CrudMacro.SQLInsert)
            .executeInsert[Option[Long]]()
          val properties = template.properties.copy(id = id)

          template.entities.foreach(te => create(te, c, id))
          val entities = template.entities.map{case TemplateEntity(entityCombinedId, _) => TemplateEntity(entityCombinedId, id)}
          template.xivos.foreach(tx => create(tx, c, id))
          val intervals = template.intervals.map{case TemplateInterval(intervalId, _) => TemplateInterval(intervalId, id)}
          template.intervals.foreach(ti => create(ti, c, id))
          val xivos = template.xivos.map{case TemplateXivo(xivoId, _) => TemplateXivo(xivoId, id)}
          template.copy(properties = properties, xivos = xivos, entities = entities, intervals = intervals)
        }
      }
    })
  }
  private def create[T](te: TemplateElementWithId[T], c: Connection, id: Option[Long]): Unit = {
    te match {
      case e: TemplateEntity =>
        CrudMacro.query[TemplateEntity](e.copy(templateId = id), "template_entities", Set(), true, CrudMacro.SnakeCase, CrudMacro.SQLInsert)
        .executeInsert()(c)
      case x: TemplateXivo =>
        CrudMacro.query[TemplateXivo](x.copy(templateId = id), "template_xivo", Set(), true, CrudMacro.SnakeCase, CrudMacro.SQLInsert)
          .executeInsert()(c)
      case i: TemplateInterval =>
        CrudMacro.query[TemplateInterval](i.copy(templateId = id), "template_intervals", Set(), true, CrudMacro.SnakeCase, CrudMacro.SQLInsert)
          .executeInsert()(c)
    }
  }

  override def get(id: Long): Try[Template] = db.withConnection { implicit c =>
    Try(
      Template(
        SQL("select * from template where id={id}").on("id" -> id).as(templatePropertiesParser.single),
        SQL("select * from template_xivo where template_id={id}").on("id" -> id).as(templateXivosParser.*),
        SQL("select * from template_entities where template_id={id}").on("id" -> id).as(templateEntitiesParser.*),
        SQL("select * from template_intervals where template_id={id}").on("id" -> id).as(templateIntervalsParser.*)
      )
    )
  }

  override def update(template: Template): Try[Template] ={

    Try({
        db.withTransaction {
          implicit c => {
            val actual = get(template.properties.id.get).get
            updateTemplateProperties(actual, template)
            updateList(actual.entities, template.entities)
            updateList(actual.xivos, template.xivos)
            updateList(actual.intervals, template.intervals)
            template
          }
        }
      })
  }
  private def update[T](te: TemplateElementWithId[T], c: Connection) = {
    te match {
      case e: TemplateEntity =>
        val updatedCnt = CrudMacro.query[TemplateEntity](e, "template_entities", Set("templateId"), true, CrudMacro.SnakeCase, CrudMacro.SQLUpdate)
          .executeUpdate()(c)
        if (updatedCnt != 1) {
          throw new Exception("Unable to update Template entities")
        }
      case x: TemplateXivo =>
        val updatedCnt = CrudMacro.query[TemplateXivo](x, "template_xivo", Set("templateId"), true, CrudMacro.SnakeCase, CrudMacro.SQLUpdate)
          .executeUpdate()(c)
        if (updatedCnt != 1) {
          throw new Exception("Unable to update Template xivo")
        }
      case i: TemplateInterval =>
        val updatedCnt = CrudMacro.query[TemplateInterval](i, "template_intervals", Set("templateId"), true, CrudMacro.SnakeCase, CrudMacro.SQLUpdate)
          .executeUpdate()(c)
        if (updatedCnt != 1) {
          throw new Exception("Unable to update Template intervals")
        }
    }
  }

  private def updateTemplateProperties(actual: Template, updated: Template)(implicit c: Connection): Unit = {
    if (actual.properties != updated.properties) {
      val updatedCnt = CrudMacro.query[TemplateProperties](updated.properties,
        "template", Set("id"), true, CrudMacro.SnakeCase, CrudMacro.SQLUpdate).executeUpdate()
      if (updatedCnt != 1) {
        throw new Exception("Unable to update Template properties")
      }
    }
  }

  private def updateList[T](current: List[TemplateElementWithId[T]], updated: List[TemplateElementWithId[T]])(implicit c: Connection): Unit = {
    if (current != updated) {
      val toBeProcessed= updated.filterNot(e => current.contains(e))
      val actualWithoutUnchanged = current.filterNot(e => updated.contains(e))
      findUpdated(actualWithoutUnchanged, toBeProcessed) match {
        case Nil => logger.debug(s"$actualWithoutUnchanged doesn't need to be updated")
        case x =>
          logger.debug(s"Updating: $x")
          x.foreach(e => update[T](e, c))
      }
      findInserted(actualWithoutUnchanged, toBeProcessed) match {
        case Nil => logger.debug(s"Nothing to be created for $toBeProcessed")
        case x =>
          logger.debug(s"Creating: $x")
          x.foreach(e => create[T](e, c, e.getTemplateId))
      }
      findDeleted(actualWithoutUnchanged, toBeProcessed) match {
        case Nil => logger.debug(s"Nothing to be deleted for $toBeProcessed")
        case x =>
          logger.debug(s"Deleting: $x")
          x.foreach(e => delete[T](e, c))
      }
    }
  }

  private def findDeleted[T](actual: List[TemplateElementWithId[T]],
                             updated: List[TemplateElementWithId[T]]): List[TemplateElementWithId[T]] = {
    actual.filterNot(e => updated.exists(ue => ue.getElementId == e.getElementId))
  }
  private def findInserted[T](actual: List[TemplateElementWithId[T]],
                              updated: List[TemplateElementWithId[T]]): List[TemplateElementWithId[T]] = {
    updated.filterNot(e => actual.exists(ae => ae.getElementId == e.getElementId))
  }
  private def findUpdated[T](actual: List[TemplateElementWithId[T]],
                             updated: List[TemplateElementWithId[T]]): List[TemplateElementWithId[T]] = {
    updated.filter(e => actual.exists(ae => ae.getElementId == e.getElementId))
  }

  override def delete(id: Long): Try[Template] = db.withConnection { implicit c =>
    val entry = get(id)
    if (entry.isFailure) entry
    else {
      val deletedCount = SQL("delete from template where id={id}").on("id" -> id).executeUpdate()
      if (deletedCount == 1) entry
      else Failure(new Exception("Cannot delete Template"))
    }
  }
  private def delete[T] (te: TemplateElementWithId[T], c: Connection) {
    te match {
      case e: TemplateEntity =>
        val deletedCount = SQL("delete from template_entities where template_id={tId} and entity_combined_id={eId}")
          .on("tId" -> e.templateId, "eId" -> e.entityCombinedId).executeUpdate()(c)
        if (deletedCount != 1) Failure(new Exception("Cannot delete template_entity"))
      case x: TemplateXivo =>
        val deletedCount = SQL("delete from template_xivo where template_id={tId} and xivo_id={xId}")
          .on("tId" -> x.templateId, "xId" -> x.xivoId).executeUpdate()(c)
        if (deletedCount != 1) Failure(new Exception("Cannot delete template_xivo"))
      case i: TemplateInterval =>
        val deletedCount = SQL("delete from template_intervals where template_id={tId} and interval_id={iId}")
          .on("tId" -> i.templateId, "iId" -> i.intervalId).executeUpdate()(c)
        if (deletedCount != 1) Failure(new Exception("Cannot delete template_entity"))
    }
  }


  override def all: Try[List[Template]] = db.withConnection { implicit c =>
    Try({
      val properties = SQL("select * from template order by name, id").as(templatePropertiesParser.*)
      properties.map(p =>
        Template(
          p,
          SQL("select * from template_xivo where template_id={id}").on("id" -> p.id).as(templateXivosParser.*),
          SQL("select * from template_entities where template_id={id}").on("id" -> p.id).as(templateEntitiesParser.*),
          SQL("select * from template_intervals where template_id={id}").on("id" -> p.id).as(templateIntervalsParser.*)
        )
      )
    })
  }

  override def forEntity(eId: CombinedId): Try[List[Template]] = {
    all.map(list => list.filter(t => t.entities.find(te => te.entityCombinedId == eId).isDefined))
  }
}

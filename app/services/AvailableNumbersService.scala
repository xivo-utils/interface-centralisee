package services

import javax.inject.Inject

import anorm.SqlParser
import com.google.inject.Singleton
import model.Interval
import play.api.db._
import anorm._
import model.Route.targetDisabled

@Singleton
class AvailableNumbersService @Inject()(@NamedDatabase("default") db: Database) {



  def findAvailableNumbers(intervals: Seq[Interval]): List[String] = {
    def intervalToSql(interval: Interval): String = {
      val start = interval.start.toInt
      val end = interval.end.toInt
      val length = interval.end.length
      s"( SELECT LPAD(num::TEXT, $length, '0') AS digits FROM generate_series($start, $end) num )"
    }

    if (intervals.isEmpty) List()
    else {
      val intervalsInSql = intervals.map(intervalToSql).mkString(" UNION \n                 ")
      db.withConnection( implicit c =>
        SQL"""
          SELECT intervals.digits
          FROM (
                 #$intervalsInSql
               ) intervals
            LEFT JOIN route ON intervals.digits = route.digits
            LEFT JOIN route_metadata ON route.id = route_metadata.route_ref
            LEFT JOIN
              (SELECT DISTINCT(internal_number) AS digits FROM xivo_users) xivo_users_numbers
              ON intervals.digits = xivo_users_numbers.digits
          WHERE
            (
              route.id IS NULL                          -- digits were never used
              OR
              route.context = '#$targetDisabled'        -- digits were used, but are released now
            ) AND xivo_users_numbers.digits IS NULL     -- digits not found in xivo_users cache
          GROUP BY intervals.digits, route.id
          ORDER BY
            max(route_metadata.id) IS NULL DESC,             -- then released, but without route_metadata record (old data)
            max(route_metadata.released_date) IS NULL DESC,  -- then unknown released_date
            max(route_metadata.released_date),               -- then oldest released
            intervals.digits :: INT                     -- at least sort by number
        """
          .as(SqlParser.get[String]("digits").*)
      )
    }
  }

}

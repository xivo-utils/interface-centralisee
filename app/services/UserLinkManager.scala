package services

import javax.inject.Inject

import anorm.{RowParser, SQL, ~}
import com.google.inject.Singleton
import common.Crud
import model.UserLink
import org.slf4j.LoggerFactory
import play.api.db.{Database, NamedDatabase}
import anorm.SqlParser.{get => _get}
import org.postgresql.util.PSQLException
import scala.util.{Failure, Try}

object UserLinkManager {
  final class UserLinkNotFound(message: String) extends Exception(message)
}

@Singleton
class UserLinkManager @Inject()(@NamedDatabase("default") db: Database) extends Crud[UserLink, Long] {
  import UserLinkManager._
  val logger = LoggerFactory.getLogger(getClass)

  override def create(link: UserLink): Try[UserLink] = db.withConnection { implicit c =>
    Try {
      require(link.id.isEmpty, "UserLink.id is not empty")
      val genId: Option[Long] = SQL("INSERT INTO users(entity_id, id_on_xivo, external_route_id, internal_route_id, " +
        "interval_id, template_id) VALUES ({entityId}, {idOnXivo}, {eRouteId}, {iRouteId}, {intervalId}, {templateId})").on(
        'entityId -> link.entityId, 'idOnXivo -> link.idOnXivo, 'eRouteId -> link.externalRouteId,
        'iRouteId -> link.internalRouteId, 'intervalId -> link.intervalId, 'templateId -> link.templateId).executeInsert()
      assert(genId.nonEmpty, "UserLink insert returned empty id")
      link.copy(id = genId)
    }.recoverWith(logDbError)
  }

  override def update(link: UserLink): Try[UserLink] = db.withConnection { implicit c =>
    Try {
      require(link.id.nonEmpty, "UserLink.id is empty")
      val res = SQL(
        """
        UPDATE users
        SET entity_id = {entityId}, id_on_xivo = {xivoId}, external_route_id = {eRouteId}, internal_route_id = {iRouteId}, interval_id = {intervalId}, template_id = {templateId}
        WHERE id = {id}
      """)
        .on('id -> link.id, 'entityId -> link.entityId, 'xivoId -> link.idOnXivo, 'eRouteId -> link.externalRouteId, 'iRouteId -> link.internalRouteId, 'intervalId -> link.intervalId, 'templateId -> link.templateId)
        .executeUpdate()
      if (res != 1) {
        throw new UserLinkNotFound(s"Unable to update UserLink.id=${link.id.get} - not found")
      }
    }
      .flatMap(_ => get(link.id.get))
      .recoverWith(logDbError)
  }

  val userLinkParser: RowParser[UserLink] = _get[Option[Long]]("id") ~ _get[Long]("entity_id") ~ _get[Long]("id_on_xivo") ~
    _get[Option[Long]]("external_route_id") ~ _get[Option[Long]]("internal_route_id") ~
    _get[Option[Int]]("interval_id") ~ _get[Option[Int]]("template_id") map {
    case id ~ entityId ~ idOnXivo ~ eRouteId ~ iRouteId ~ intervalId ~ templateId =>
      UserLink(id, entityId, idOnXivo, eRouteId, iRouteId, intervalId, templateId)
  }

  override def delete(id: Long): Try[UserLink] = db.withConnection { implicit c =>
    val res = for {
      userLink <- get(id)
      _ <- Try {
        if (SQL("DELETE FROM users WHERE id = {id}").on('id -> id).executeUpdate() != 1) {
          throw new UserLinkNotFound(s"Unable to delete UserLink.id=$id - not found")
        }
      }
    } yield {
      userLink
    }
    res.recoverWith(logDbError)
  }

  val select = "SELECT u.id, u.entity_id, u.id_on_xivo, u.external_route_id, u.internal_route_id, u.interval_id, u.template_id FROM users u "

  override def get(id: Long): Try[UserLink] = db.withConnection { implicit c =>
    Try {
      SQL(select + " WHERE u.id = {id}")
        .on('id -> id)
        .as(userLinkParser.singleOpt)
        .getOrElse(throw new UserLinkNotFound(s"UserLink.id=$id not found"))
    }.recoverWith(logDbError)
  }

  override def all(): Try[List[UserLink]] = db.withConnection { implicit c =>
    Try {
      SQL(select + " ORDER BY u.id").as(userLinkParser.*)
    }.recoverWith(logDbError)
  }

  def findForXivo(xivoId: Long): Try[List[UserLink]] = db.withConnection { implicit c =>
    Try {
      SQL(select + "LEFT JOIN entities e ON u.entity_id = e.id WHERE e.xivo_id = {xivoId} ORDER BY u.id")
        .on('xivoId -> xivoId)
        .as(userLinkParser.*)
    }.recoverWith(logDbError)
  }

  private def logDbError[U] = PartialFunction[Throwable, Try[U]] {
    case e: PSQLException =>
      logger.warn(s"UserLinkManager db action failed", e)
      Failure(e)
  }

}

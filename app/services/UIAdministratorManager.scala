package services

import javax.inject.Inject
import com.google.inject.{ImplementedBy, Singleton}
import common.Crud
import model.UIAdministrator
import org.slf4j.LoggerFactory
import services.rights.{AdministratorRoleManager, RoleManager}
import model.rights.{AdministratorRole, Role}
import scala.util.{Failure, Success, Try}

@ImplementedBy(classOf[UIAdministratorManagerImpl])
trait UIAdministratorManager extends Crud[UIAdministrator, Long] {
  def getByLogin(login: String): Try[UIAdministrator]
}

@Singleton
class UIAdministratorManagerImpl @Inject()(adminManager: AdministratorManager, roleManager: RoleManager,
                                           adminRoleManager: AdministratorRoleManager) extends UIAdministratorManager {
  val logger = LoggerFactory.getLogger(getClass)
  
  override def create(admin: UIAdministrator): Try[UIAdministrator] = {
    Try {
      val adminDb = AdministratorDb.fromAdministrator(admin)
      val id = adminManager.create(adminDb).get.id
      admin.roles.foreach(role => adminRoleManager.create(AdministratorRole(id.get, role.id.get)))
      admin.copy(id = id)
    }
  }

  override def get(id: Long): Try[UIAdministrator] = {
    adminManager.get(id) match {
      case Success(adminDb) =>
        adminRoleManager.get(adminDb.id.get) match {
          case Success(roleIds) =>
            Success(UIAdministrator(adminDb, roleIds.map(role => roleManager.get(role.roleId).get)))
          case Failure(f) =>
            logger.error("Unable to get admin-role association " + f.getMessage)
            Failure(new Exception("Unable to get admin-role association"))
        }
      case Failure(f) =>
        logger.error("Unable to get admin" + f.getMessage)
        Failure(new Exception("Unable to get admin"))
    }
  }

  override def getByLogin(login: String): Try[UIAdministrator] = {
    adminManager.getByLogin(login) match {
      case Success(admin) =>
        get(admin.id.get)
      case Failure(f) =>
        logger.error("Unable to get admin by login")
        Failure(new Exception("Unable to get admin by login"))
    }
  }

  override def update(newAdmin: UIAdministrator): Try[UIAdministrator] = {
    Try {
      adminManager.update(AdministratorDb.fromAdministrator(newAdmin)) match {
        case Failure(f) => Failure(new Exception("Unable to update administrator"))
        case Success(_) =>
          adminRoleManager.delete(newAdmin.id.get)
          newAdmin.roles.foreach(role => adminRoleManager.create(AdministratorRole(newAdmin.id.get, role.id.get)))
      }
      newAdmin
    }
  }

  override def delete(id: Long): Try[UIAdministrator] = {
    get(id) match {
      case Success(uia) =>
        adminRoleManager.delete(id) match {
          case Success(_) =>
          case Failure(f) =>
            logger.error("Unable to delete admin role association")
            Failure(new Exception("Unable to delete admin role association"))
        }
        adminManager.delete(id) match {
          case Success(_) => Success(uia)
          case Failure(f) =>
            uia.roles.foreach(role => adminRoleManager.create(AdministratorRole(id, role.id.get)))
            logger.info("Unable to delete admin")
            Failure(new Exception("Unable to delete admin"))
        }

      case Failure(f) =>
        logger.info("Unable to delete admin")
        Failure(new Exception("Unable to delete admin"))
    }
  }

  override def all: Try[List[UIAdministrator]] = {
    (adminManager.all(), adminRoleManager.all(), roleManager.all()) match {
      case (Success(admins), Success(adminRoles), Success(roles)) =>
        convertToUIAdminList(admins, adminRoles, roles)
      case _ =>
        logger.error("Unable to get all admins")
        Failure(new Exception("Unable to get all admins"))
    }
  }

  private def convertToUIAdminList(admins: List[AdministratorDb], adminRoles: List[AdministratorRole],
                                   roles: List[Role]): Try[List[UIAdministrator]] = {
    Try {
      admins.map(admin => {
        UIAdministrator(
          admin,
          adminRoles.filter(ar => ar.administratorId == admin.id.get)
            .map(ar => roles.find(r => r.id.get == ar.roleId).get)
        )
      }
      )
    }
  }

}

package services

import anorm.SqlParser._
import anorm._
import com.google.inject.{ImplementedBy, Singleton}
import model.utils.{AutoRollbackTransaction, GlobalXivoConnectorPool, XivoConnectorPool}
import model.{PeerSipNameMode, _}
import org.joda.time.DateTime
import org.slf4j.LoggerFactory
import services.cache.CachedXivoUser.fromXivoUser
import services.cache.{CachedXivoUser, CachedXivoUserManager}
import xivo.ldap.xivoconnection.XivoConnector
import xivo.restapi.connection.WebServicesException
import xivo.restapi.model.{Device, IncomingCall, Line, Voicemail, User => XivoUser}

import java.security.InvalidParameterException
import java.util.UUID
import javax.inject.Inject
import scala.collection.JavaConversions._
import scala.util.{Failure, Success, Try}

@ImplementedBy(classOf[UserManagerImpl])
trait UserManager {
  val applicationRouteMetaByUser = "__application"

  def create(user: User, by: String = applicationRouteMetaByUser, withConfiguration: Boolean = true): Try[User]

  def getFromXivoAndCacheForEntity(entity: Entity): UserList

  def listForEntityFromCache(entity: Entity): UserList

  def delete(userId: Long, entity: EntityInterface, by: String = applicationRouteMetaByUser, withConfiguration: Boolean = true): Unit

  def disconnectDevice(userId: Long, entity: EntityInterface, by: String = applicationRouteMetaByUser): Boolean

  def getUserWithVoicemailFromXivo(userId: Long, entities: EntityInterface): Option[UserWithVM]

  def getUserFromCache(userId: Long, entities: EntityInterface): Option[User]

  def getUserWithVoicemailCombiningXivoAndCache(userId: Long, entities: EntityInterface): Option[UserWithVM]

  def update(old: UserWithVM, updated: UserWithVM, by: String = applicationRouteMetaByUser, withConfiguration: Boolean = true): Unit

  def findAcrossXivos(entityInterface: EntityInterface, search: String, limit: Int = 100): List[User]

  def getAndCacheXivoUsers(xivoUuid: UUID, xivo: XivoObj): List[XivoUser]
}

@Singleton
class UserManagerImpl @Inject()(routeManager: RouteInterface,
                                xivoUserTransformer: XivoUserTransformer,
                                routeMetaManager: RouteMetaManager,
                                cachedXivoUserManager: CachedXivoUserManager,
                                templateManager: TemplateManager,
                                globalXivoConnectorPool: GlobalXivoConnectorPool,
                                userLinkManager: UserLinkManager,
                                userFinder: UserFinder,
                                userVoicemailOperations: UserVoicemailOperations) extends UserManager {

  val logger = LoggerFactory.getLogger(getClass)
  var connectorPool: XivoConnectorPool = globalXivoConnectorPool

  def create(user: User, by: String = applicationRouteMetaByUser, withConfiguration: Boolean = true): Try[User] =
    if (withConfiguration) {
      Try(createImpl(user, by))
    } else {
      Try(createProxyUserImpl(user, by))
    }

  private def createProxyUserImpl(user: User, by: String): User = {
    logger.info(s"Create Proxy user $user")

    cachedXivoUserManager.getNextIdForXivo(user.entity.xivo.uuid).map(nextId => {
      val transaction: AutoRollbackTransaction = new AutoRollbackTransaction {
        var externalRouteId: Option[Long] = None
        var internalRouteId: Option[Long] = None

        step(executeFun = {
          if (user.externalNumber.isDefined) {
            externalRouteId = routeManager.createExternal(user).id
            val now = new DateTime()
            routeMetaManager.create(
              RouteMeta(None, externalRouteId.get, now, by, now, by, now, by, manual = false))
          }
        },
          rollbackFun = {
            externalRouteId.foreach(id => {
              routeManager.setDeleted(id)
              routeMetaManager.setDeleted(externalRouteId.get, by)
            })

          })

        step(
          executeFun = internalRouteId = createInternalRouteStep(user, by),
          rollbackFun = rollbackInternalRouteStep(internalRouteId, by)
        )

        step(
          executeFun = createUserLinkStep(user, nextId, externalRouteId, internalRouteId),
          rollbackFun = Unit
        )
      }
      transaction.execute()

      val cachedUser = CachedXivoUser.fromUser(user.entity.xivo.uuid, user, nextId.toInt)
      cachedXivoUserManager.createOrUpdateForXivoUser(cachedUser)
    })
    user
  }

  private def createImpl(user: User, by: String): User = connectorPool.withConnector(user.entity.xivo) { implicit connector => {
    logger.info(s"Starting creation of user $user")

    val xivoUser = xivoUserTransformer.createXivoUser(user)
    logger.info(s"XivoUser to be created: $xivoUser")

    val template: Template = user.templateId.flatMap(id => templateManager.get(id).toOption)
      .getOrElse(throw new InvalidParameterException("Unable to get template"))
    var createdUser: XivoUser = connector.createUser(xivoUser)

    val transaction = new AutoRollbackTransaction {
      var externalRouteId: Option[Long] = None
      var internalRouteId: Option[Long] = None
      var userId: Option[Long] = None
      var createdVm = false
      var incCall: Option[IncomingCall] = None

      step(
        executeFun = (),
        rollbackFun = connector.deleteUser(createdUser)
      )

      step(
        executeFun = connector.createLineForUser(createdUser),
        rollbackFun = connector.deleteLineForUser(createdUser)
      )

      addSteps(
        userVoicemailOperations.stepOnCreateUser(createdUser, UserWithVM(user, template.properties.voicemailSettings))
      )

      step(
        executeFun = {
          createdUser.getIncomingCall match {
            case null =>
            case updatedIncomingCall =>
              updatedIncomingCall.setExtensionId(createdUser.getLine.getExtensionId)
              createdUser.setIncomingCall(updatedIncomingCall)
              connector.createIncallForUser(createdUser)
              incCall = Some(createdUser.getIncomingCall)
          }
        },
        rollbackFun = {
          connector.deleteIncallForUser(createdUser)
        }
      )

      step(executeFun = {
        if (user.externalNumber.isDefined) {
          externalRouteId = routeManager.createExternal(user).id
          val now = new DateTime()
          routeMetaManager.create(
            RouteMeta(None, externalRouteId.get, now, by, now, by, now, by, manual = false))
        }
      },
        rollbackFun = {
          externalRouteId.foreach(id => {
            routeManager.setDeleted(id)
            routeMetaManager.setDeleted(externalRouteId.get, by)
          })

        })

      step(
        executeFun = internalRouteId = createInternalRouteStep(user, by),
        rollbackFun = rollbackInternalRouteStep(internalRouteId, by)
      )

      step(
        executeFun = userId = createUserLinkStep(user, createdUser.getId.toLong, externalRouteId, internalRouteId),
        rollbackFun = Unit
      )
    }
    transaction.execute()

    if (createdUser != null) {
      val cachedXivoUser = fromXivoUser(user.entity.xivo.uuid, createdUser)
      cachedXivoUserManager.createOrUpdateForXivoUser(cachedXivoUser)
    }
    logger.info(s"User $user created")
    user.copy(id = transaction.userId)
  }
  }

  private def createInternalRouteStep(user: User, by: String): Option[Long] = {
    val internalRouteId = routeManager.createInternal(user).id
    val now = new DateTime()
    routeMetaManager.create(
      RouteMeta(None, internalRouteId.get, now, by, now, by, now, by, manual = false))
    internalRouteId
  }

  private def rollbackInternalRouteStep(internalRouteId: Option[Long], by: String): Unit = {
    internalRouteId.foreach(id => {
      routeManager.setDeleted(id)
      routeMetaManager.setDeleted(id, by)
    })
  }

  private def createUserLinkStep(user: User, xivoId: Long, externalRouteId: Option[Long], internalRouteId: Option[Long]): Option[Long] =
    userLinkManager.create(
      UserLink(
        None, user.entity.id.get, xivoId, externalRouteId, internalRouteId, user.intervalId,
        user.templateId
      )).get.id

  private def findVoicemail(number: String)(implicit connector: XivoConnector): Option[Voicemail] =
    connector.listVoicemails().find(vm => vm.getNumber == number)

  private def parseAndGetUserUsingXivo(findEntity: Long => Option[Entity]): RowParser[Option[User]] =
    get[Option[Long]]("id") ~ get[Long]("entity_id") ~ get[Int]("id_on_xivo") ~ get[Option[String]]("digits") map {
      case id ~ entityId ~ idOnXivo ~ extNum =>
        findEntity(entityId).flatMap { entity =>
          connectorPool.withConnector(entity.xivo) { c =>
            getXivoUser(c, idOnXivo, entity).map { xivoUser =>
              createUserFromXivoUser(id, xivoUser, extNum, entity)
            }
          }
        }
    }

  private def createUserFromXivoUser(id: Option[Long], xivoUser: XivoUser, extNum: Option[String], entity: Entity) = {
    val peerSipName: PeerSipNameMode.Value =
      Option(xivoUser.getLine) match {
        case Some(l) => extractLineTypeFromOptions(l)
        case None => PeerSipNameMode.Auto
      }

    val callerIdMode = CachedXivoUser.parseCallerIdMode(xivoUser)

    User(
      id,
      entity,
      Option(xivoUser.getFirstname).getOrElse(""),
      Option(xivoUser.getLastname).getOrElse(""),
      Option(xivoUser.getLine.getNumber).getOrElse(""),
      extNum,
      Option(xivoUser.getVoicemail).map(_.getEmail).filter(_ != null),
      Option(xivoUser.getUsername),
      Option(xivoUser.getPassword),
      Option(xivoUser.getLine).map(_.getprovisioningCode()).filter(_ != null),
      None,
      None,
      ringingTime = Option(xivoUser.getRingSeconds).map {
        _.toInt
      },
      peerSipName = Option(peerSipName),
      callerIdMode = callerIdMode,
      customCallerId = Option(xivoUser.getOutcallerid)
    )
  }

  private def extractLineTypeFromOptions(line: Line): PeerSipNameMode.Value = {
    if (line.getName == line.getNumber)
      PeerSipNameMode.Model
    else {
      Option(line.getOptions)
        .map(l => l.toList.map(_.toList))
        .map { outerList =>
          outerList
            .flatMap { innerList =>
              innerList match {
                case List("webrtc", "yes", _*) => Some(PeerSipNameMode.WebRTC)
                case List("webrtc", "ua", _*) => Some(PeerSipNameMode.UniqueAccount)
                case _ => None
              }
            }
            .headOption
            .getOrElse(PeerSipNameMode.Auto)
        }
    }.getOrElse(PeerSipNameMode.Auto)
  }

  private def listForXivo(xivo: XivoObj): List[XivoUser] = {
    connectorPool.withConnector(xivo)(c => {
      c.getAllUsers.toList
    })
  }

  override def getFromXivoAndCacheForEntity(entity: Entity): UserList = {
    require(entity.id.nonEmpty, "entity.id is empty")

    val xivoUsers: List[XivoUser] = getAndCacheXivoUsers(entity.xivo.uuid, entity.xivo)
    val xivoUsersForEntity: List[XivoUser] = xivoUsers.filter { xu =>
      xu.getLine != null && xu.getLine.getContext == entity.name
    }
    val usersFromXivoUsers: List[User] = xivoUsersForEntity.map { xu =>
      createUserFromXivoUser(Some(xu.getId.toLong), xu, Some(xu.getOutcallerid), entity)
    }

    val xivoIdsAlreadyPersisted: Set[Long] = userFinder.listBothUserIdsForEntity(entity.id.get).map(_._2).toSet
    usersFromXivoUsers
      .filter { xu => !xivoIdsAlreadyPersisted.contains(xu.id.get) }
      .foreach { xu => userLinkManager.create(UserLink(None, xu.entity.id.get, xu.id.get, None, None, None, None)).get }

    listForEntityFromCache(entity)
  }

  private def getCachedUsersForEntity(xivoUuid: UUID, entityName: String): Map[Int, CachedXivoUser] = {
    val cachedUsers = cachedXivoUserManager.allInEntity(xivoUuid, entityName) match {
      case Failure(e) =>
        logger.error("failed to fetch cached xivo users", e)
        Nil
      case Success(s) => s
    }

    cachedUsers.map(xu => (xu.xivoUserId, xu))(collection.breakOut)
  }

  override def listForEntityFromCache(entity: Entity): UserList =
    userFinder.listForEntityFromCache(entity, getCachedUsersForEntity(entity.xivo.uuid, entity.name))

  override def findAcrossXivos(entityInterface: EntityInterface, search: String, limit: Int = 100): List[User] =
    cachedXivoUserManager.findAcrossXivos(search, limit) match {
      case Failure(e) =>
        logger.error("failed to fetch cached xivo users", e)
        Nil
      case Success(cachedUsers) =>
        userFinder.listAcrossXivosFromCache(entityInterface, cachedUsers, limit)
    }

  override def getAndCacheXivoUsers(xivoUuid: UUID, xivo: XivoObj): List[XivoUser] = {
    val xivoUsers = listForXivo(xivo)
    xivoUsers
      .map { xu => cachedXivoUserManager.createOrUpdateForXivoUser(fromXivoUser(xivoUuid, xu)) }
      .foreach {
        case Failure(e) => logger.error("Failed to store CachedXivoUser", e)
        case _ =>
      }
    logger.info("xivo users cached " + xivoUsers.length.toString)
    xivoUsers
  }

  override def delete(userId: Long, entities: EntityInterface, by: String = applicationRouteMetaByUser, withConfiguration: Boolean = true): Unit = {
    userLinkManager.get(userId) match {
      case Failure(_) => throw new NoSuchElementException(s"L'utilisateur $userId n'existe pas")
      case Success(userLink) =>
        if (withConfiguration) {
          Try(deleteXivoUserImpl(userId, entities, by, userLink))
        } else {
          Try(deleteProxyUserImpl(userId, entities, by, userLink))
        }
    }
  }

  def deleteXivoUserImpl(userId: Long, entities: EntityInterface, by: String = applicationRouteMetaByUser, userLink: UserLink): Unit = {
    val entity = entities.getEntity(userLink.entityId).get
    connectorPool.withConnector(entity.xivo) { implicit connector => {
      logger.info(s"Starting deletion of XiVO user $userId")
      var xivoUser: XivoUser = null
      new AutoRollbackTransaction {
        val xivoUserOpt: Option[XivoUser] = getXivoUser(connector, userLink.idOnXivo.toInt, entity)
        xivoUserOpt match {
          case Some(u) =>
            xivoUser = u
            val userWithVM = getUserWithVoicemailFromXivoUser(userId, entities, Some(xivoUser))
            val internalLineNumberOpt = Option(xivoUser.getLine).map(_.getNumber)

            step(
              executeFun = userLinkManager.delete(userId).get,
              rollbackFun = userLinkManager.create(UserLink(None, userLink.entityId, xivoUser.getId.toLong,
                userLink.externalRouteId, userLink.internalRouteId, userLink.intervalId, userLink.templateId)).get
            )

            addSteps(
              userWithVM.flatMap(userVoicemailOperations.stepOnDeleteUser(xivoUser, _))
            )

            step(
              executeFun = {
                connector.deleteUser(xivoUser)
              },
              rollbackFun = {
                xivoUser = connector.createUser(xivoUser)
              }
            )

            step(
              executeFun = {
                connector.deleteLineForUser(xivoUser)
              },
              rollbackFun = {
                connector.createLineForUser(xivoUser)
              }
            )

            step(
              executeFun = {
                connector.deleteIncallForUser(xivoUser)
              },
              rollbackFun = {
                connector.createIncallForUser(xivoUser)
              }
            )

            step(
              executeFun = userLink.externalRouteId.foreach(id => {
                routeManager.setDeleted(id)
                routeMetaManager.setDeleted(id, by)
              }),
              rollbackFun = Unit)

            step(executeFun = {
              internalLineNumberOpt.foreach { internalLineNumber =>
                val route = routeManager.setDeletedByDigits(internalLineNumber)
                routeMetaManager.get(route.id.get) match {
                  case Failure(_) =>
                    val now = new DateTime()
                    routeMetaManager.create(RouteMeta(None, route.id.get, now, by, now, by, now, by, manual = false))
                  case Success(_) =>
                    routeMetaManager.setDeleted(route.id.get, by)
                }
              }
            },
              rollbackFun = Unit)

          case None =>
            userLinkManager.delete(userId).get
            userLink.externalRouteId.foreach(id => {
              routeManager.setDeleted(id)
              routeMetaManager.setDeleted(id, by)
            })
            userLink.internalRouteId.foreach(id => {
              routeManager.setDeleted(id)
              routeMetaManager.setDeleted(id, by)
            })

        }
      }.execute()

      if (xivoUser != null) {
        cachedXivoUserManager.delete(entity.xivo.uuid, xivoUser.getId)
      }
      logger.info(s"XiVO User $userId deleted")
    }
    }
  }

  def deleteProxyUserImpl(userId: Long, entities: EntityInterface, by: String = applicationRouteMetaByUser, userLink: UserLink): Unit = {
    val entity = entities.getEntity(userLink.entityId).get
    logger.info(s"Starting deletion of proxy user $userId")
    new AutoRollbackTransaction {
      userLinkManager.delete(userId).get
      userLink.externalRouteId.foreach(id => {
        routeManager.setDeleted(id)
        routeMetaManager.setDeleted(id, by)
      })
      userLink.internalRouteId.foreach(id => {
        routeManager.setDeleted(id)
        routeMetaManager.setDeleted(id, by)
      })
    }.execute()

    cachedXivoUserManager.delete(entity.xivo.uuid, userLink.idOnXivo.toInt)
    logger.info(s"Proxy user $userId deleted")
  }

  override def disconnectDevice(userId: Long, entities: EntityInterface, by: String = applicationRouteMetaByUser): Boolean =
    userLinkManager.get(userId) match {
      case Failure(_) => throw new NoSuchElementException(s"L'utilisateur $userId n'existe pas")
      case Success(userLink) =>
        val entity = entities.getEntity(userLink.entityId).get
        connectorPool.withConnector(entity.xivo)(connector => {
          getXivoUser(connector, userLink.idOnXivo.toInt, entity) match {
            case None =>
              throw new NoSuchElementException(s"L'utilisateur $userId n'existe pas")
            case Some(xivoUser) if xivoUser.getLine == null || xivoUser.getLine.getDeviceId() == null =>
              false
            case Some(xivoUser) =>
              val device = new Device(xivoUser.getLine.getDeviceId())
              logger.info(s"Resetting device ${device.getId} on xivo id=${entity.xivo.id.get} name='${entity.xivo.name}' for user id=$userId xivoId=${userLink.idOnXivo} username=${xivoUser.getUsername} to autoprov (by $by)")
              connector.resetAutoprov(device)
              connector.synchronizeDevice(device)
              true
          }
        })
    }

  override def getUserWithVoicemailFromXivo(id: Long, entities: EntityInterface): Option[UserWithVM] =
    getUserWithVoicemailFromXivoUser(id, entities, None)

  private def getUserWithVoicemailFromXivoUser(id: Long, entities: EntityInterface, xivoUserOpt: Option[XivoUser]): Option[UserWithVM] =
    userLinkManager.get(id).toOption.flatMap { userLink =>
      val extNum = userLink.externalRouteId.flatMap(rid => routeManager.getRoute(rid)).map(_.digits).getOrElse("")
      for {
        entity <- entities.getEntity(userLink.entityId)
        xivoUser <- xivoUserOpt orElse connectorPool.withConnector(entity.xivo) { c =>
          getXivoUser(c, userLink.idOnXivo.toInt, entity)
        }
        user = createUserFromXivoUser(Some(id), xivoUser, Some(extNum), entity)
      } yield UserWithVM(user, VoicemailSettings.fromXivoUser(xivoUser))
    }

  override def getUserFromCache(id: Long, entities: EntityInterface): Option[User] =
    for {
      userLink <- userLinkManager.get(id).toOption
      entity <- entities.getEntity(userLink.entityId)
      cachedUsers = getCachedUsersForEntity(entity.xivo.uuid, entity.name)
      res <- userFinder.getUserUsingCache(id, entity, cachedUsers)
    } yield res

  override def getUserWithVoicemailCombiningXivoAndCache(userId: Long, entities: EntityInterface): Option[UserWithVM] =
    getUserWithVoicemailFromXivo(userId, entities).map { fromXivo =>
      getUserFromCache(userId, entities) match {
        case None => fromXivo
        case Some(userFromCache) =>
          UserWithVM(
            fromXivo.user.copy(intervalId = userFromCache.intervalId, templateId = userFromCache.templateId),
            fromXivo.voicemail
          )
      }
    }

  private def updateRouteOnUserUpdate(oldUser: User, newUser: User, oldUserLink: UserLink, by: String): Unit = {
    val now = new DateTime()

    def updatedExtRouteMeta() = oldUserLink.externalRouteId.map(id =>
      routeMetaManager.get(id).getOrElse(
        RouteMeta(None, oldUserLink.externalRouteId.get, now, by, now, by, now, by, manual = false)
      ).copy(modifiedBy = by, modifiedDate = now))

    def updatedIntRouteMeta(routeId: Option[Long] = oldUserLink.id) = routeId.map(id =>
      routeMetaManager.get(id).getOrElse(
        RouteMeta(None, oldUserLink.internalRouteId.get, now, by, now, by, now, by, manual = false)
      ).copy(modifiedBy = by, modifiedDate = now))

    def attachUserToNewRoute(): Unit = {
      val routeId = routeManager.create(Route(None, newUser.externalNumber.get, s"(${newUser.externalNumber.get})",
        newUser.entity.xivo.contextName, newUser.internalNumber)).id
      routeId.foreach(id => routeMetaManager.create(RouteMeta(None, id, now, by, now, by, now, by, manual = false)))
      userLinkManager.update(oldUserLink.copy(externalRouteId = routeId)).get
    }

    def getOldInternalRouteId(oldUserLink: UserLink, oldUser: User): Option[Long] =
      if (oldUserLink.internalRouteId.isDefined) {
        oldUserLink.internalRouteId
      } else {
        routeManager.findRouteByDigits(oldUser.internalNumber).flatMap(_.id)
      }

    if (oldUser.externalNumber.isDefined && newUser.externalNumber.isEmpty) {
      userLinkManager.update(oldUserLink.copy(externalRouteId = None)).get
      routeManager.setDeleted(oldUserLink.externalRouteId.get)
      routeMetaManager.setDeleted(oldUserLink.externalRouteId.get, by)
    } else if (oldUser.externalNumber.isEmpty && newUser.externalNumber.isDefined) {
      attachUserToNewRoute()
    } else if (oldUser.externalNumber.isDefined && newUser.externalNumber.isDefined) {
      for {
        oldExternalNumber <- oldUser.externalNumber
        newExternalNumber <- newUser.externalNumber
        if newExternalNumber != oldExternalNumber
      } {
        oldUserLink.externalRouteId.foreach { externalRouteId =>
          routeManager.setDeleted(externalRouteId)
          routeMetaManager.setDeleted(externalRouteId, by)
        }

        routeManager.findRouteByDigits(newExternalNumber) match {
          case None =>
            attachUserToNewRoute()
          case Some(routeToReuse) =>
            val newRoute = routeToReuse.copy(
              regexp = s"($newExternalNumber)",
              context = newUser.entity.xivo.contextName,
              target = newUser.internalNumber
            )
            routeToReuse.id.map(id => routeMetaManager.get(id).map(rm =>
              routeMetaManager.update(rm.copy(modifiedDate = now, modifiedBy = by))))
            routeManager.update(newRoute)
            userLinkManager.update(oldUserLink.copy(externalRouteId = newRoute.id)).get
        }
      }
    }

    updatedExtRouteMeta().foreach(rm => routeMetaManager.update(rm))
    if (oldUser.internalNumber != newUser.internalNumber) {
      routeManager.findRouteByDigits(newUser.internalNumber) match {
        case Some(route) =>
          getOldInternalRouteId(oldUserLink, oldUser).foreach(internalRouteId => {
            routeManager.setDeleted(internalRouteId)
            routeMetaManager.setDeleted(internalRouteId, by)
          })

          routeManager.update(route.copy(context = newUser.entity.xivo.contextName))
          routeMetaManager.get(route.id.get).map(rm => routeMetaManager.update(rm.copy(modifiedBy = by, modifiedDate = now)))

          userLinkManager.get(oldUser.id.get)
            .map(ul => ul.copy(internalRouteId = route.id, intervalId = newUser.intervalId))
            .map(updatedUl => userLinkManager.update(updatedUl))
        case None =>
          routeManager.findRouteByDigits(oldUser.internalNumber) match {
            case Some(oldRoute) => {
              routeManager.update(Route(oldRoute.id, newUser.internalNumber, s"(${newUser.internalNumber})",
                newUser.entity.xivo.contextName, "\\1"))
              updatedIntRouteMeta(oldRoute.id).foreach(rm => routeMetaManager.update(rm))

              userLinkManager.get(oldUser.id.get)
                .map(ul => ul.copy(intervalId = newUser.intervalId))
                .map(updatedUl => userLinkManager.update(updatedUl))
            }
            case None => {
              val newRoute = routeManager.create(Route(None, newUser.internalNumber, s"(${newUser.internalNumber})",
                newUser.entity.xivo.contextName, "\\1"))
              newRoute.id.foreach(newRouteId => {
                val meta = RouteMeta(None, newRouteId, now, by, now, by, now, by, manual = false)
                routeMetaManager.update(meta)
                userLinkManager.get(oldUser.id.get)
                  .map(ul => ul.copy(intervalId = newUser.intervalId, internalRouteId = Some(newRouteId)))
                  .map(updatedUl => userLinkManager.update(updatedUl))
              })
            }
          }
      }
    }
  }

  override def update(old: UserWithVM, updated: UserWithVM, by: String = applicationRouteMetaByUser, withConfiguration: Boolean = true): Unit =
    if (withConfiguration) {
      Try(updateXivoUserImpl(old, updated, by))
    } else {
      Try(updateProxyUserImpl(old, updated, by))
    }

  def updateXivoUserImpl(old: UserWithVM, updated: UserWithVM, by: String = applicationRouteMetaByUser): Unit =
    connectorPool.withConnector(old.user.entity.xivo) { implicit connector => {

      logger.info(s"Updating XiVO user: $old -> $updated")

      val oldUserLink = userLinkManager.get(old.user.id.get).get

      val oldXivoUser = getXivoUser(connector, oldUserLink.idOnXivo.toInt, old.user.entity).get
      val tmpXivoUser = getXivoUser(connector, oldUserLink.idOnXivo.toInt, old.user.entity).get
      val xivoUser = xivoUserTransformer.updateXivoUser(tmpXivoUser, updated.user)

      val transaction = new AutoRollbackTransaction {
        step(
          executeFun = connector.updateUser(xivoUser, false),
          rollbackFun = connector.updateUser(oldXivoUser)
        )

        step(
          executeFun = connector.updateLineForUser(xivoUser),
          rollbackFun = connector.updateLineForUser(oldXivoUser)
        )

        step(
          executeFun = {
            xivoUser.getIncomingCall match {
              case null =>
              case incall =>
                incall.setExtensionId(xivoUser.getLine.getExtensionId)
                incall.setSda(updated.user.internalNumber)
                xivoUser.setIncomingCall(incall)
                connector.updateIncallForUser(xivoUser)
            }
          },
          rollbackFun = {
            connector.updateIncallForUser(oldXivoUser)
          }
        )

        addSteps(
          userVoicemailOperations.stepsOnUpdateUser(oldXivoUser, xivoUser, old, updated)
        )
      }
      transaction.execute()
      updateRouteOnUserUpdate(old.user, updated.user, oldUserLink, by)

      if (xivoUser.getLine != null && xivoUser.getLine.getDeviceId != null &&
        (old.user.firstName != updated.user.firstName || old.user.lastName != updated.user.lastName)
      ) {
        connector.synchronizeDevice(new Device(xivoUser.getLine.getDeviceId))
      }

      cachedXivoUserManager.createOrUpdateForXivoUser(fromXivoUser(updated.user.entity.xivo.uuid, xivoUser))
      logger.info("XiVO user update finished")
    }
    }

  def updateProxyUserImpl(old: UserWithVM, updated: UserWithVM, by: String = applicationRouteMetaByUser): Unit = {
    logger.info(s"Updating Proxy user: $old -> $updated")

    val oldUserLink = userLinkManager.get(old.user.id.get).get
    updateRouteOnUserUpdate(old.user, updated.user, oldUserLink, by)
    cachedXivoUserManager.createOrUpdateForXivoUser(CachedXivoUser.fromUser(updated.user.entity.xivo.uuid, updated.user, old.user.id.get.toInt))
    logger.info("Proxy user update finished")
  }

  private def getXivoUser(c: XivoConnector, idOnXivo: Int, entity: Entity): Option[XivoUser] =
    try {
      val xivoUser = c.getUser(idOnXivo)
      val internalNumber = c.getInternalNumberForUser(entity.name, idOnXivo)
      if (internalNumber != null) {
        xivoUser.getLine.setExtensionId(internalNumber.getExtensionId)
        xivoUser.getLine.setNumber(internalNumber.getNumber)
      }

      Some(xivoUser)
    } catch {
      case e: WebServicesException if e.getStatus == 404 => None
    }

}

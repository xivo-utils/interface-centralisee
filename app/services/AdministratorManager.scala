package services

import javax.inject.Inject

import anorm._
import com.google.inject.{ImplementedBy, Singleton}
import common.{AnormMacro, Crud, CrudMacro}
import model.UIAdministrator
import org.jasypt.util.password.StrongPasswordEncryptor
import org.slf4j.LoggerFactory
import play.api.db.{Database, _}

import scala.util.{Failure, Success, Try}

case class AdministratorDb(id: Option[Long],
                           name: String,
                           login: String,
                           hashedPassword: String,
                           superadmin: Boolean,
                           ldap: Boolean)
object AdministratorDb {
  def fromAdministrator(a: UIAdministrator) = AdministratorDb(
    a.id,
    a.name,
    a.login,
    a.password,
    a.superAdmin,
    a.ldap)
}

@ImplementedBy(classOf[AdministratorManagerImpl])
trait AdministratorManager extends Crud[AdministratorDb, Long] {
  def getByLogin(login: String): Try[AdministratorDb]
}

@Singleton
class AdministratorManagerImpl @Inject()(@NamedDatabase("default") db: Database) extends AdministratorManager {
  val logger = LoggerFactory.getLogger(getClass)
  val encryptor = new StrongPasswordEncryptor()

  val administratorParser: RowParser[AdministratorDb] =
    AnormMacro.namedParser[AdministratorDb](AnormMacro.ColumnNaming.SnakeCase)
  
  override def create(admin: AdministratorDb): Try[AdministratorDb] = {
    Try({
      db.withTransaction {
        implicit c => {
          val withHashedPassword: AdministratorDb =
            admin.copy(hashedPassword = encryptor.encryptPassword(admin.hashedPassword))

          val id = CrudMacro.query[AdministratorDb](withHashedPassword, "administrators", Set("id"), true,
            CrudMacro.SnakeCase, CrudMacro.SQLInsert).executeInsert[Option[Long]]()
          withHashedPassword.copy(id = id)
        }
      }
    })
  }

  override def get(id: Long): Try[AdministratorDb] = db.withConnection { implicit c =>
    Try( SQL("select * from administrators where id={id}").on("id" -> id).as(administratorParser.single) )
  }

  def getByLogin(login: String): Try[AdministratorDb] = db.withConnection { implicit c =>
    Try( SQL("select * from administrators where login={login}").on("login" -> login).as(administratorParser.single))
  }

  override def update(newAdmin: AdministratorDb): Try[AdministratorDb] ={
    db.withTransaction {
      implicit c => {
        get(newAdmin.id.get) match {
          case Failure(f) => Failure(new Exception("Administrator to update not found"))
          case Success(oldAdmin) =>
            val hashedPwd = getHashedPassword(oldAdmin.hashedPassword, newAdmin.hashedPassword)
            val withPassword: AdministratorDb = newAdmin.copy(hashedPassword=hashedPwd)
            val updatedCnt = CrudMacro.query[AdministratorDb](withPassword, "administrators", Set("id"), true,
              CrudMacro.SnakeCase, CrudMacro.SQLUpdate).executeUpdate()
            if (updatedCnt != 1) {
              throw new Exception("Unable to update Administrator")
            }
            return Success(withPassword)
        }
      }
    }
  }

  private def getHashedPassword(oldPassword: String, newPassword: String): String = {
    if(oldPassword == newPassword) { newPassword }
    else { encryptor.encryptPassword(newPassword) }
  }

  override def delete(id: Long): Try[AdministratorDb] = db.withConnection { implicit c =>
    val entry = get(id)
    if (entry.isFailure) entry
    else {
      val deletedCount = SQL("delete from administrators where id={id}").on("id" -> id).executeUpdate()
      if (deletedCount == 1) entry
      else Failure(new Exception("Cannot delete Administrator"))
    }
  }

  override def all: Try[List[AdministratorDb]] = db.withConnection { implicit c =>
    Try( SQL("select * from administrators").as(administratorParser.*) )
  }

}

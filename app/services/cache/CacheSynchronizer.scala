package services.cache

import akka.actor.Actor
import com.google.inject.{Inject, Singleton}
import play.api.Logger
import services.{XivoDb, XivoManager}

import scala.util.{Failure, Success}

@Singleton
class CacheSynchronizer @Inject() (userCachingService: UserCachingService, xivoManager: XivoManager) extends Actor {

  def receive = {
    case CacheScheduler.StartCachingAllXivos => cacheAll()
    case CacheScheduler.StartCachingXivo(xivoId) => cacheById(xivoId)
  }

  def cacheAll(): Unit = {
    Logger.info("Starting caching users of all Xivos")
    xivoManager.all() match {
      case Failure(f) =>
        Logger.error("Unable to get xivo list for caching")
      case Success(xivos) =>
        xivos.filter(! _.isProxy).foreach(cache)
    }
  }

  def cacheById(xivoId: Long) = xivoManager.get(xivoId) match {
    case Failure(f) =>
      Logger.error(s"Unable to get xivo.id=$xivoId for caching")
    case Success(xivo) =>
      cache(xivo)
  }

  private def cache(xivo: XivoDb): Unit =  {
    Logger.info(s"Caching for xivo: ${xivo.name} (${xivo.host})")
    userCachingService.cacheForXivo(xivo)
  }

}

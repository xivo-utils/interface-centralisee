package services.cache

import java.util.UUID
import javax.inject.Inject
import anorm.{RowParser, _}
import com.google.inject.Singleton
import common.{AnormMacro, Crud, CrudMacro, DBUtil}
import model.{CallerIdMode, Entity, PeerSipNameMode, User}
import org.slf4j.LoggerFactory
import play.api.db.{Database, _}
import play.api.libs.json.Json
import services.dbmapper.UuidDbMapper._
import xivo.restapi.model.{User => XivoUser}

import scala.annotation.switch
import scala.collection.JavaConverters.iterableAsScalaIterableConverter
import scala.util.{Failure, Try}

case class CachedXivoUser(id: Option[Long],
                          xivoUuid: UUID,
                          entityName:  Option[String],
                          xivoUserId: Int,
                          firstName: String,
                          lastName: String,                 
                          internalNumber: Option[String],
                          externalNumber: Option[String],
                          mail: Option[String],
                          ctiLogin: Option[String],
                          ctiPassword: Option[String],
                          provisioningNumber: Option[String],
                          ringingTime: Option[Int],
                          peerSipName: Option[PeerSipNameMode.Type],
                          callerIdMode: Option[CallerIdMode.Type],
                          customCallerId: Option[String]
) {

  def toUser(id: Option[Long], entity: Entity, intervalId: Option[Int] = None, templateId: Option[Int] = None): User = {
    require(internalNumber.isDefined)
    User(
      id,
      entity = entity,
      firstName = firstName,
      lastName = lastName,
      internalNumber = internalNumber.get,
      externalNumber = externalNumber,
      mail = mail,
      ctiLogin = ctiLogin,
      ctiPassword = ctiPassword,
      provisioningNumber = provisioningNumber,
      intervalId = intervalId,
      templateId = templateId,
      ringingTime = ringingTime,
      peerSipName = peerSipName,
      callerIdMode = callerIdMode,
      customCallerId = customCallerId
    )
  }
}

object CachedXivoUser {
  def parseCallerIdMode(xivoUser: XivoUser): Option[CallerIdMode.Value] = {
    Option(xivoUser.getOutcallerid) match {
      case Some("anonymous") => Some(CallerIdMode.Anonymous)
      case Some(_) => Some(CallerIdMode.Custom)
      case None => None
    }
  }
  def fromXivoUser(_xivoUuid: UUID, xivoUser: XivoUser): CachedXivoUser = {
    val userMail = Option(xivoUser.getEmail)
    val voicemailEmail = Option(xivoUser.getVoicemail).map(_.getEmail)

    val line = Option(xivoUser.getLine)
    val lineType = line.flatMap(l => Option(l.getOptions).map(o => o.get(0).get(1)))
    val peerSipName = line.map(l =>
        if (l.getName == l.getNumber) PeerSipNameMode.Model
        else lineType match {
          case Some("yes") =>  PeerSipNameMode.WebRTC
          case Some("ua") =>  PeerSipNameMode.UniqueAccount
          case _ => PeerSipNameMode.Auto
        }
    )

    val callerIdMode = parseCallerIdMode(xivoUser)

    CachedXivoUser(
      id = None,
      xivoUuid = _xivoUuid,
      entityName = Option(xivoUser.getLine).map(_.getContext),
      xivoUserId = xivoUser.getId,
      firstName = Option(xivoUser.getFirstname).getOrElse(""),
      lastName = Option(xivoUser.getLastname).getOrElse(""),
      internalNumber = Option(xivoUser.getLine).map(_.getNumber),
      externalNumber = Option(xivoUser.getOutcallerid),
      mail = if(userMail.nonEmpty) userMail else voicemailEmail,
      ctiLogin = Option(xivoUser.getUsername),
      ctiPassword = Option(xivoUser.getPassword),
      provisioningNumber = Option(xivoUser.getLine).map(_.getprovisioningCode()),
      ringingTime = Option(xivoUser.getRingSeconds).map{_.toInt},
      peerSipName = peerSipName,
      callerIdMode = callerIdMode,
      customCallerId = Option(xivoUser.getOutcallerid)
    )
  }

  def fromUser(_xivoUuid: UUID, user: User, id: Int): CachedXivoUser = {
    CachedXivoUser(
      id = None,
      xivoUuid = _xivoUuid,
      entityName = Some(user.entity.name),
      xivoUserId = id,
      firstName = user.firstName,
      lastName = user.lastName,
      internalNumber = Some(user.internalNumber),
      externalNumber = user.externalNumber,
      mail = user.mail,
      ctiLogin = user.ctiLogin,
      ctiPassword = user.ctiPassword,
      provisioningNumber = user.provisioningNumber,
      ringingTime = user.ringingTime,
      peerSipName = user.peerSipName,
      callerIdMode = user.callerIdMode,
      customCallerId = user.customCallerId
    )
  }

  implicit val jsonReads = Json.reads[CachedXivoUser]
  implicit val jsonWrites = Json.writes[CachedXivoUser]
}

@Singleton
class CachedXivoUserManager @Inject()(@NamedDatabase("default") db: Database) extends Crud[CachedXivoUser, Long] {
  val logger = LoggerFactory.getLogger(getClass)

  implicit val peerSipNameModeToStatement = DBUtil.enumToStatement(PeerSipNameMode.enumValues)
  implicit val optionPeerSipNameModeToStatement = DBUtil.enumTypeMetaData[PeerSipNameMode.Type]()
  implicit def enumToPeerSipNameMode = DBUtil.columnToEnum(PeerSipNameMode.enumValues)

  implicit val callerIdModeToStatement = DBUtil.enumToStatement(CallerIdMode.enumValues)
  implicit val optionCallerIdModeToStatement = DBUtil.enumTypeMetaData[CallerIdMode.Type]()
  implicit def enumToCallerIdMode = DBUtil.columnToEnum(CallerIdMode.enumValues)

  val cachedXivoUserParser: RowParser[CachedXivoUser] =
    AnormMacro.namedParser[CachedXivoUser](AnormMacro.ColumnNaming.SnakeCase)

  val ids = Set("id")

  override def create(cachedXivoUser: CachedXivoUser): Try[CachedXivoUser] = {
    Try({
      db.withTransaction {
        implicit c => {
          val newUId = CrudMacro.query[CachedXivoUser](cachedXivoUser, "xivo_users", ids, true, CrudMacro.SnakeCase,CrudMacro.SQLInsert)
            .executeInsert[Option[Long]]()
          cachedXivoUser.copy(id = newUId)
        }
      }
    })
  }

  def createOrUpdateForXivoUser(cachedXivoUser: CachedXivoUser): Try[CachedXivoUser] = {
    get(cachedXivoUser.xivoUuid, cachedXivoUser.xivoUserId) match {
      case scala.util.Success(found) => update(cachedXivoUser.copy(id = found.id))
      case Failure(_) => create(cachedXivoUser)
    }
  }

  def getNextIdForXivo(xivoUuid: UUID): Try[Long] = db.withConnection { implicit c =>
    Try(SQL(
      """
        SELECT COALESCE(MAX(xivo_user_id), 0) + 1
        FROM xivo_users
        WHERE xivo_uuid = {uuid}""".stripMargin)
      .on("uuid" -> xivoUuid)
      .as(SqlParser.scalar[Int].singleOpt).get)
  }

  def createOrUpdate(cachedXivoUser: CachedXivoUser): Try[CachedXivoUser] = {
    Try({
      guardIdDefined(cachedXivoUser)
      db.withTransaction {
        implicit c => {
          val updatedCnt = CrudMacro.query[CachedXivoUser](cachedXivoUser, "xivo_users", ids, false, CrudMacro.SnakeCase, CrudMacro.SQLUpsert)
            .executeUpdate()
          if (updatedCnt != 1) {
            throw new Exception("Unable to createOrUpdate CachedXivoUser")
          }
          cachedXivoUser
        }
      }
    })
  }

  override def get(id: Long): Try[CachedXivoUser] = db.withConnection { implicit c =>
    Try( SQL("SELECT * FROM xivo_users WHERE id={id}").on("id" -> id).as(cachedXivoUserParser.single) )
  }

  def countForExternalNumber(externalNumber: String, excludedId: Option[Long] = None): Try[Int] = db.withConnection { implicit c =>
    excludedId match {
      case Some(exclId) =>
        Try( SQL(
          """
            SELECT COUNT(*)
            FROM xivo_users xu
            LEFT JOIN users u ON xu.xivo_user_id = u.id_on_xivo
            WHERE xu.external_number = {externalNumber} AND u.id != {exclId}""".stripMargin)
          .on("exclId" -> exclId, "externalNumber" -> externalNumber)
          .as(SqlParser.scalar[Int].singleOpt).get)
      case None =>
        Try( SQL("SELECT COUNT(*) FROM xivo_users WHERE external_number={externalNumber}")
          .on("externalNumber" -> externalNumber)
          .as(SqlParser.scalar[Int].singleOpt).get)
    }
  }

  def countForXivo(xivoUuid: UUID): Try[Int] = db.withConnection { implicit c =>
    Try {
      SQL("SELECT COUNT(*) FROM xivo_users WHERE xivo_uuid={xivoUuid}")
        .on("xivoUuid" -> xivoUuid)
        .as(SqlParser.scalar[Int].singleOpt).get
    }
  }

  def get(xivoUuid: UUID, xivoUserId: Int): Try[CachedXivoUser] = db.withConnection { implicit c =>
    Try{
      SQL("SELECT * FROM xivo_users WHERE xivo_uuid={xivoUuid} AND xivo_user_id={xivoUserId}")
        .on("xivoUuid" -> xivoUuid, "xivoUserId" -> xivoUserId)
        .as(cachedXivoUserParser.single)
    }
  }

  override def update(cachedXivoUser: CachedXivoUser): Try[CachedXivoUser] = {
    Try({
      guardIdDefined(cachedXivoUser)
      db.withTransaction {
        implicit c => {
          val updatedCnt = CrudMacro.query[CachedXivoUser](cachedXivoUser, "xivo_users", ids, false, CrudMacro.SnakeCase, CrudMacro.SQLUpdate)
            .executeUpdate()
          if (updatedCnt != 1) {
            throw new Exception("Unable to update CachedXivoUser")
          }
        }
      }
      cachedXivoUser
    })
  }

  def delete(xivoUuid: UUID, xivoUserId: Int): Try[Boolean] = db.withConnection { implicit c =>
    Try {
      val deletedCount = SQL("DELETE FROM xivo_users WHERE xivo_uuid={xivoUuid} AND xivo_user_id={xivoUserId}")
        .on("xivoUuid" -> xivoUuid, "xivoUserId" -> xivoUserId)
        .executeUpdate()
      (deletedCount: @switch) match {
        case 0 => false
        case 1 => true
        case other => throw new Exception(s"Invalid number of deleted records: $deletedCount")
      }
    }
  }

  override def delete(id: Long): Try[CachedXivoUser] = db.withConnection { implicit c =>
    val entry = get(id)
    if (entry.isFailure) entry
    else {
      val deletedCount = SQL("DELETE FROM xivo_users WHERE id={id}").on("id" -> id).executeUpdate()
      if (deletedCount == 1) entry
      else Failure(new Exception("Cannot delete CachedXivoUser"))
    }
  }

  override def all(): Try[List[CachedXivoUser]] = db.withConnection { implicit c =>
    Try( SQL("SELECT * FROM xivo_users").as(cachedXivoUserParser.*) )
  }

  def allInEntity(xivoUuid: UUID, entityName: String): Try[List[CachedXivoUser]] = db.withConnection { implicit c =>
    Try {
      SQL("SELECT * FROM xivo_users WHERE xivo_uuid={xivoUuid} AND entity_name={entityName}")
        .on("xivoUuid" -> xivoUuid, "entityName" -> entityName)
        .as(cachedXivoUserParser.*)
    }
  }

  def findAcrossXivos(search: String, limit: Int = 100): Try[List[CachedXivoUser]] = db.withConnection { implicit c =>
    Try(
      SQL("""
        SELECT *
        FROM xivo_users
        WHERE
          LOWER(first_name) LIKE({search}) OR
          LOWER(last_name) LIKE({search}) OR
          LOWER(internal_number) LIKE({search}) OR
          LOWER(external_number) LIKE({search})
        ORDER BY first_name, last_name, id
        LIMIT {limit}
      """)
        .on("search" -> s"%${search.toLowerCase}%", "limit" -> limit)
        .as(cachedXivoUserParser.*)
    )
  }

  private def guardIdDefined(cachedXivoUser: CachedXivoUser): Unit =
    require(cachedXivoUser.id.isDefined, "id must be defined")
}

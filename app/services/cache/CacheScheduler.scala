package services.cache

import akka.actor.{ActorRef, ActorSystem}
import com.google.inject.Inject
import com.google.inject.name.Named
import model.utils.ConfigParser

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

object CacheScheduler {
  case object StartCachingAllXivos
  case class StartCachingXivo(xivoId: Long)
}

class CacheScheduler @Inject()(system: ActorSystem,
                               @Named("cache-synchronizer-actor") schedulerActor: ActorRef,
                               config: ConfigParser)(implicit ec: ExecutionContext) {
  import CacheScheduler._

  system.scheduler.schedule(10 seconds, config.cacheInterval hours, schedulerActor, StartCachingAllXivos)

  def startCachingXivo(xivoId: Long) = schedulerActor ! StartCachingXivo(xivoId)
}

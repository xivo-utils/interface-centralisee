package services.cache

import anorm.SqlParser._
import anorm._
import com.google.inject.{Inject, _}
import model.UserLink
import org.slf4j.LoggerFactory
import play.api.db._
import services.{UserLinkManager, UserManager, XivoDb}

@Singleton
class UserCachingService @Inject()(@NamedDatabase("default") db: Database,
                                   userManager: UserManager,
                                   userLinkManager: UserLinkManager) {

  val logger = LoggerFactory.getLogger(getClass)

  def cacheForXivo(xivo: XivoDb): Unit = {
    require(xivo.id.nonEmpty, "xivo.id is empty")

    val xivoUsers = userManager.getAndCacheXivoUsers(xivo.uuid, xivo)
    val xivoIdsAlreadyPersisted: Set[Long] =
      userLinkManager.findForXivo(xivo.id.get).getOrElse(Nil).map(_.idOnXivo)(collection.breakOut)
    val entityNameToId: Map[String,Int] = mapEntityNameToId(xivo.id.get)

    for {
      xivoUser <- xivoUsers
      if xivoUser.getLine != null
      if ! xivoIdsAlreadyPersisted.contains(xivoUser.getId.toLong)
      if entityNameToId.contains(xivoUser.getLine.getContext)
      entityId = entityNameToId(xivoUser.getLine.getContext)
    } userLinkManager.create(UserLink(None, entityId, xivoUser.getId.toLong, None, None, None, None)).get
  }


  def mapEntityNameToId(xivoId: Long): Map[String,Int] = db.withConnection { implicit c =>
    def toPair = get[String]("name") ~ get[Int]("id") map { case name ~ id => (name, id) }
    SQL("SELECT e.name, e.id FROM entities e WHERE e.xivo_id = {xivoId} ")
      .on('xivoId -> xivoId)
      .as(toPair.*)
      .toMap
  }

}

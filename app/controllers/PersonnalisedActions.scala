package controllers

import java.security.InvalidParameterException

import model.AdministratorInterface
import org.slf4j.LoggerFactory
import play.api.Configuration
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.json.Json
import play.api.mvc.Results._
import play.api.mvc.{ActionBuilder, AnyContent, Request, Result}
import xivo.restapi.connection.WebServicesException
import javax.inject.{Singleton, Inject}
import scala.concurrent.Future

trait AbstractJsonAction extends ActionBuilder[Request] {

  val logger = LoggerFactory.getLogger(getClass)

  def executeRequest[A](request: Request[A], block: (Request[A]) => Future[Result]) = {
    try {
      if((request.method.equalsIgnoreCase("POST") || request.method.equalsIgnoreCase("PUT")) &&
        request.asInstanceOf[Request[AnyContent]].body.asJson.isEmpty) Future(BadRequest(Json.toJson("No JSON found")))
      else block(request)
    } catch {
      case e: InvalidParameterException => Future(BadRequest(Json.toJson(e.getMessage)))
      case e: WebServicesException => Future(BadRequest(Json.toJson(e.getMessage)))
      case e: NoSuchElementException => {
        logger.error("Error : element not found", e)
        Future(NotFound(Json.toJson(e.getMessage)))
      }
      case e: Exception => {
        logger.error("An unexpected error occured", e)
        Future(InternalServerError(Json.toJson(e.getMessage)))
      }
    }
  }
}

object JsonAction extends AbstractJsonAction {
  override def invokeBlock[A](request: Request[A], block: (Request[A]) => Future[Result]) = executeRequest(request, block)
}

@Singleton
class AuthenticatedAction @Inject()(config: Configuration, admins: AdministratorInterface) extends ActionBuilder[Request] {

  val logger = LoggerFactory.getLogger(getClass)

  override def invokeBlock[A](request: Request[A], block: (Request[A]) => Future[Result]): Future[Result] = {
    request.session.get("username") match {
      case None => Future(Redirect(routes.Login.loginPage()))
      case Some(login) => if(admins.isUrlAllowed(login, request.path, request.method)) {
        try {
          block(request)
        } catch {
          case e: Exception =>
            logger.error("An unexpected error occured", e)
            Future(InternalServerError(Json.toJson(e.getMessage)))
        }
      }
        else Future(Forbidden(""))
    }
  }
}

@Singleton
class JsonAuthenticatedAction @Inject()(admins: AdministratorInterface) extends ActionBuilder[Request] with AbstractJsonAction {

  override def invokeBlock[A](request: Request[A], block: (Request[A]) => Future[Result]): Future[Result] = request.session.get("username") match {
      case None => Future(Forbidden(""))
      case Some(login) => if (admins.isUrlAllowed(login, request.path, request.method)) executeRequest(request, block)
        else Future(Forbidden(""))
    }

}

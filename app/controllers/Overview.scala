package controllers

import javax.inject.Inject
import ic.info.{BuildInfo => BI}
import play.api.{Logger, Configuration}
import play.api.mvc.{Action, Controller}

class Overview @Inject()(config: Configuration) extends Controller {
  val configDump = config.entrySet.map(item => s"${item._1}: ${item._2}\n").mkString("\n")
  val buildInfo = s"Name: ${BI.name} version: ${BI.version}"

  val logger = Logger(getClass.getName)

  def overview = Action(implicit request => {
    logger.info(s"Config: ${configDump}")
    logger.info(s"Build info: $buildInfo")
    Ok(views.html.overview(config.entrySet.toList.sortWith(_._1 < _._1).map(item => (item._1, item._2.toString)), buildInfo))
  })
}

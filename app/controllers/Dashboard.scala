package controllers

import javax.inject.Inject

import be.objectify.deadbolt.scala.ActionBuilders
import play.api.Configuration
import play.api.mvc.Controller
import model.AdministratorInterface
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class Dashboard @Inject()(config: Configuration, admins: AdministratorInterface, actionBuilder: ActionBuilders)
  extends Controller {

  def index = actionBuilder.DynamicAction("dashboard.index").defaultHandler() {
    implicit request => Future(Ok(views.html.dashboard.index(admins)))
  }
}

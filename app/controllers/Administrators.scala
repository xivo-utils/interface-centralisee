package controllers

import model.validators.AdministratorValidator
import model.AdministratorInterface
import play.api.Configuration
import play.api.mvc.Controller
import javax.inject.Inject
import be.objectify.deadbolt.scala.ActionBuilders
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class Administrators @Inject()(config: Configuration, admins: AdministratorInterface,
                               actionBuilder: ActionBuilders, validator: AdministratorValidator)
  extends Controller {

  def listPage = actionBuilder.DynamicAction("administrators.listPage").defaultHandler() {
    implicit request => Future(Ok(views.html.administrators(admins)))
  }

  def creationPage = actionBuilder.DynamicAction("administrators.creationPage").defaultHandler() {
    implicit request => Future(Ok(views.html.createAdmin(admins)))
  }

  def editionPage(id: Long) = actionBuilder.DynamicAction("administrators.editionPage").defaultHandler() {
    implicit request => {
      val adminLogin = admins.getAdmin(id).map(_.login).getOrElse("")
      Future(Ok(views.html.editAdmin(admins, id, adminLogin)))
    }
  }

}

package controllers

import javax.inject.Inject
import be.objectify.deadbolt.scala.ActionBuilders
import model.AdministratorInterface
import model.validators.AdministratorValidator
import play.api.Configuration
import play.api.mvc.Controller
import services.rights.RoleManager
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class Roles @Inject()(config: Configuration, admins: AdministratorInterface, roles: RoleManager,
                      actionBuilder: ActionBuilders, validator: AdministratorValidator)
  extends Controller {

  def listPage = actionBuilder.DynamicAction("roles.listPage").defaultHandler() {
    implicit request => Future(Ok(views.html.roleTemplate.roles(admins)))
  }

  def create = actionBuilder.DynamicAction("roles.createPage").defaultHandler() {
    implicit request =>
      Future(Ok(views.html.roleTemplate.roleAction(admins, "CREATE", views.html.roleTemplate.roleCreate())))
  }

  def edit(id: Long) = actionBuilder.DynamicAction("roles.editPage").defaultHandler() {
    implicit request =>
      Future(Ok(views.html.roleTemplate.roleAction(admins, "EDIT", views.html.roleTemplate.roleEdit())))
  }

}

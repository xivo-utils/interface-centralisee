package controllers

import be.objectify.deadbolt.scala.ActionBuilders
import controllers.helpers.LoggingAction
import model.validators.{EntityValidator, ProxyValidatorInterface, XivoValidatorInterface}
import model.{AdministratorInterface, CombinedId, Entity, EntityInterface, Interval, IntervalRoutingModes, Xivo, XivoInterface}
import play.api.Configuration
import play.api.libs.json.Json
import play.api.mvc.Controller
import security.handlers.HandlerKeys

import java.io.File
import javax.inject.Inject
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class Xivo @Inject()(configuration: Configuration, admins: AdministratorInterface, xivoManager: XivoInterface,
                     xivoValidator: XivoValidatorInterface, actionBuilder: ActionBuilders, proxyValidator: ProxyValidatorInterface, entities: EntityInterface, entityValidator: EntityValidator)
  extends Controller {

  lazy val SshPubKey = fileContent(configuration.getString("ssh.publicKey").get)

  def list = actionBuilder.DynamicAction(name = "administrators.list").key(HandlerKeys.json) { request =>
    JsonAction({
      Ok(ListResult(xivoManager.listExtended).toJson)
    }).apply(request)
  }

  def get(id: Long) = actionBuilder.DynamicAction(name = "administrators.list").key(HandlerKeys.json) { request =>
    JsonAction({
      xivoManager.getXivo(id) match {
        case None => NotFound(Json.obj("errors" -> s"Ce Xivo id: $id n'existe pas"))
        case Some(x) => Ok(Json.toJson(x))
      }
    }).apply(request)
  }

  def create = actionBuilder.DynamicAction(name = "administrators.list").key(HandlerKeys.json) { authRequest =>
    LoggingAction { toLog =>
      JsonAction {
        implicit request => request.body.asJson match {
          case None => BadRequest(Json.toJson("JSON cannot be empty"))
          case Some(json) => val xivoCreation = xivoValidator.fromJson(request.body.asJson.get)
            xivoValidator.validate(xivoCreation)
            val result = xivoManager.create(xivoCreation)
            toLog(xivoCreation)
            Created(Json.toJson(result.notices))
        }
      }
    }.apply(authRequest)
  }

  def createProxy = actionBuilder.DynamicAction(name = "administrators.list").key(HandlerKeys.json) { authRequest =>
    LoggingAction { toLog =>
      JsonAction {
        implicit request => request.body.asJson match {
          case None => BadRequest(Json.toJson("JSON cannot be empty"))
          case Some(json) => val proxyCreation = proxyValidator.fromJson(request.body.asJson.get)
            proxyValidator.validate(proxyCreation)
            val result = xivoManager.createProxy(proxyCreation)
            val entity = Entity(None, CombinedId(result.result.uuid, "default"), "default", "Default", result.result, proxyCreation.intervals, "", "default")
            try {
              entityValidator.validateCreation(entity, true)
              entities.create(entity, false)
            } catch {
              case e: Exception =>
                result.result.id.foreach(id => xivoManager.delete(id))
                throw e
            }
            toLog(proxyCreation)
            Created(Json.toJson(result.notices))
        }
      }
    }.apply(authRequest)
  }

  def listPage = actionBuilder.DynamicAction("xivo.listPage").defaultHandler() {
    implicit request => Future(Ok(views.html.xivo(admins)))
  }

  def creationPage = actionBuilder.DynamicAction("xivo.creationPage").defaultHandler() {
    implicit request => Future(Ok(views.html.createXivo(admins, SshPubKey)))
  }

  def creationPageProxy = actionBuilder.DynamicAction("xivo.creationPageProxy").defaultHandler() {
    implicit request => Future(Ok(views.html.createProxy(admins)))
  }

  private def fileContent(path: String): String = scala.io.Source.fromFile(new File(path)).mkString

  def synchronizeConfigFiles = actionBuilder.DynamicAction(name = "administrators.list").key(HandlerKeys.json) { request =>
    LoggingAction { toLog =>
      JsonAction {
        xivoManager.synchronizeConfigFiles()
        Ok("")
      }
    }.apply(request)
  }
      
}

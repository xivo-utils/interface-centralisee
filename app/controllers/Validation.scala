package controllers

import javax.inject.Inject
import be.objectify.deadbolt.scala.ActionBuilders
import model.AdministratorInterface
import play.api.mvc.Controller
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class Validation @Inject()(admins: AdministratorInterface, actionBuilder: ActionBuilders)
  extends Controller {

  def index = actionBuilder.DynamicAction("users.editionPage").defaultHandler() {
    implicit request => Future(Ok(views.html.validation(admins)))
  }
}

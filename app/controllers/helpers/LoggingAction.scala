package controllers.helpers

import be.objectify.deadbolt.scala.AuthenticatedRequest
import model._
import model.rights.{Role, UIRole}
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.{JsValue, Json}
import play.api.mvc.{Action, AnyContentAsJson, Request, Result}
import scala.collection.mutable.ListBuffer
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class LoggingAction[A](messageParts: => List[AnyRef], action: Action[A], logger: Logger) extends Action[A] {
  import LoggingAction._
  override lazy val parser = action.parser

  override def apply(request: Request[A]): Future[Result] = {
    val login = request match {
      case authRequest: AuthenticatedRequest[Any] =>
        getUsername(authRequest).map("'" + _ + "'").getOrElse(Unknown)
      case _ =>
        errorLogger.warn(s"LoggingAction used for non-authenticated request $request")
        Unknown
    }

    action(request).map { result =>
      val status = result.header.status
      val pref = if (status >= 200 && status < 300) "Ok" else "Error"
      val line = s"${request.id}: auth-user=$login ${request.method} ${request.uri} $pref[$status]" +
        messageParts.map(convertToLog).mkString(" ", " ", "")
      logger.info(line)
      request.body match {
        case AnyContentAsJson(jsValue) =>
          logger.debug(request.id + ": body:" + Json.prettyPrint(jsValue))
        case _ =>
      }

      result
    }
  }

  def log(request: Request[A], username: String): Unit = {
  }
}

object LoggingAction {
  val Unknown = "UNKNOWN"
  val actionsLoggerName = "UserActions"

  private val logger = LoggerFactory.getLogger(actionsLoggerName)
  private val errorLogger = LoggerFactory.getLogger(LoggingAction.getClass)

  def logToActionLogger(login: String, message: String): Unit = s"auth-user=$login $message"

  def getUsername(implicit request: AuthenticatedRequest[_]): Option[String] =
    request.subject.map(_.identifier)

  type ToLog = (AnyRef*) => Unit

  def apply[A](block: ToLog => Action[A]): LoggingAction[A] = {
    val parts = ListBuffer[AnyRef]()
    def addToLog(newParts: AnyRef*): Unit = parts ++= newParts
    new LoggingAction(parts.toList, block(addToLog), logger)
  }

  case object ActionFailed
  val actionFailed = "Action failed:"

  def convertToLog(item: AnyRef): String = item match {
    case s: String => s
    case xivo: Xivo => format(xivo)
    case xivo: XivoCreation => format(xivo)
    case entity: Entity => format(entity)
    case user: User => format(user)
    case admin: UIAdministrator => format(admin)
    case role: Role => format(role)
    case role: UIRole => format(role)
    case template: Template => format(template)
    case ActionFailed => actionFailed
    case other =>
      errorLogger.warn("convertToLog called on unsupported type=" + other.getClass)
      other.toString
  }

  def format(xivo: Xivo): String = s"xivo:name='${xivo.name}',host=${xivo.host}"

  def format(xivo: XivoCreation): String = s"xivo:name='${xivo.name}',host=${xivo.host}"

  def format(entity: Entity): String = s"entity:name='${entity.name}' @" + format(entity.xivo)

  def format(user: User): String = {
    val name = user.ctiLogin.map("'" + _ + "'").getOrElse(Unknown)
    s"user:login=$name @" + format(user.entity)
  }

  def format(admin: UIAdministrator): String = {
    val roles =  admin.roles.map(_.name).mkString("(", ",", ")")
    s"administrator:name='${admin.name}',superAdmin=${admin.superAdmin},roles=$roles"
  }

  def format(role: Role): String = s"role:name='${role.name}'"

  def format(role: UIRole): String = {
    val name = role.name.map("'" + _ + "'").getOrElse(Unknown)
    s"role:name=$name"
  }

  def format(template: Template): String = s"template:name='${template.properties.name}'"

}

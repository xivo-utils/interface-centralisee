package controllers

import javax.inject.Inject
import be.objectify.deadbolt.scala.ActionBuilders
import model.AdministratorInterface
import play.api.mvc.Controller
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class Templates @Inject()(admins: AdministratorInterface, actionBuilder: ActionBuilders)
  extends Controller {

  def index = actionBuilder.DynamicAction("templates.index").defaultHandler() {
    implicit request => Future(Ok(views.html.userTemplate.templates(admins)))
  }

  def create = actionBuilder.DynamicAction("templates.createPage").defaultHandler() {
    implicit request =>
      Future(Ok(views.html.userTemplate.templateAction(admins, "CREATE", views.html.userTemplate.templateCreate())))
  }

  def edit(id: Long) = actionBuilder.DynamicAction("templates.editionPage").defaultHandler() {
    implicit request => Future(
      Ok(views.html.userTemplate.templateAction(admins, "EDIT", views.html.userTemplate.templateEdit())))
  }
}

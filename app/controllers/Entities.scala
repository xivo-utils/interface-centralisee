package controllers

import javax.inject.Inject

import be.objectify.deadbolt.scala.ActionBuilders
import model.validators.EntityValidator
import model.{AdministratorInterface, CombinedId, Entity, EntityInterface}

import scala.concurrent.ExecutionContext.Implicits.global
import play.api.libs.json.{JsBoolean, JsObject, JsString, Json}
import play.api.mvc.Controller
import security.handlers.HandlerKeys
import services.UIAdministratorManager

import scala.concurrent.Future
import scala.util.{Failure, Success, Try}
import controllers.helpers.LoggingAction
import controllers.helpers.LoggingAction.ActionFailed


object Entities {
  val ErrorNoSuchEntity = "L'entité %s n'existe pas"
}

class Entities @Inject()(entities: EntityInterface, admins: AdministratorInterface, adminManager: UIAdministratorManager,
                         validator: EntityValidator, actionBuilder: ActionBuilders)
  extends Controller {

  def create = actionBuilder.DynamicAction(name = "entities.create").key(HandlerKeys.json) { request =>
    LoggingAction { toLog =>
      JsonAction {
        val entity: Entity = validator.fromJson(request.body.asJson.get)
        validator.validateCreation(entity)
        entities.create(entity, true)
        toLog(entity)
        Created("")
      }
    }.apply(request)
  }

  def list(updateFromXivo: Boolean) = actionBuilder.DynamicAction(name = "entities.list").key(HandlerKeys.json) { request =>
    adminManager.getByLogin(request.subject.get.identifier) match {
      case Success(admin) =>
        JsonAction({Ok(ListResult(entities.listForAdmin(admin, updateFromXivo)).toJson)}).apply(request)
      case Failure(f) =>
        Future(Forbidden("Administrator not found"))
    }
  }

  def listPage(xivoId: Long) = actionBuilder.DynamicAction("entities.listPage").defaultHandler() {
    implicit request => {
      val xivoName = entities.getEntity(xivoId).map(_.xivo.name).getOrElse("")
      Future(Ok(views.html.entities(admins, xivoId, xivoName)))
    }
  }

  def creationPage(xivoId: Long) = actionBuilder.DynamicAction("entities.creationPage").defaultHandler() {
    implicit request => Future(Ok(views.html.createEntity(admins, xivoId)))
  }

  def listAvailableNumbers(cId: CombinedId, interval: Option[Int] = None) =
    actionBuilder.DynamicAction(name = "entities.listAvailableNumbers").key(HandlerKeys.json) { request =>
      JsonAction({
        Ok(ListResult(entities.availableNumbers(cId, interval)).toJson)
      }).apply(request)
    }

  def externalNumber(cId: CombinedId, intNum: String) =
    actionBuilder.DynamicAction(name = "entities.externalNumber").key(HandlerKeys.json) { request =>
      JsonAction({
        Ok(
          JsObject(Seq(
            "externalNumber" -> JsString(entities.externalNumber(cId, intNum).getOrElse(""))
          ))
        )
      }).apply(request)
    }

  def delete(combinedId: CombinedId) = actionBuilder.DynamicAction(name = "entities.delete").key(HandlerKeys.json) { request =>
    LoggingAction { toLog =>
      JsonAction {
        entities.delete(combinedId, request.session.get("username").get)
        toLog(s"entity:id=$combinedId")
        NoContent
      }
    }.apply(request)
  }

  def editionPage(combinedId: CombinedId) = actionBuilder.DynamicAction("entities.editionPage").defaultHandler() {
    implicit request => Future(Ok(views.html.editEntity(admins, combinedId.toString)))
  }

  import Entities._

  def get(cId: CombinedId) = actionBuilder.DynamicAction(name = "entities.get").key(HandlerKeys.json) { request =>
    JsonAction({
      entities.getEntity(cId) match {
        case Some(entity) => Ok(Json.toJson(entity))
        case None => NotFound(Json.toJson(ErrorNoSuchEntity.format(cId)))
      }
    }).apply(request)
  }

  def update(cId: CombinedId) = actionBuilder.DynamicAction(name = "entities.update").key(HandlerKeys.json) { request =>
    LoggingAction { toLog =>
      JsonAction {
        entities.getEntity(cId) match {
          case None => NotFound(Json.toJson(ErrorNoSuchEntity.format(cId)))
          case Some(oldEntity) =>
            val newEntity = validator.fromJson(request.body.asJson.get)
            validator.validateUpdate(oldEntity, newEntity)
            val res = entities.update(oldEntity, newEntity, request.session.get("username").get, configureXiVO = !newEntity.xivo.isProxy)
            if (!res.success) {
              toLog(ActionFailed)
            }
            toLog(res.entity)
            Ok(JsObject(Seq(
              "success" -> JsBoolean(res.success),
              "errorMessage" -> JsString(res.errorMessage)
            )))
        }
      }
    }.apply(request)
  }

}

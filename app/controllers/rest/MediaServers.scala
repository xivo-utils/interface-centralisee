package controllers.rest

import com.google.inject.Inject
import model.MediaServer
import model.utils.{GlobalXivoConnectorPool, XivoConnectorPool}
import play.api.Logger
import play.api.libs.json.Json
import play.api.mvc.{Action, Controller}
import services.XivoManager

import scala.collection.JavaConverters._
import scala.util.{Failure, Success, Try}

class MediaServers @Inject()(xivoManager: XivoManager, globalXivoConnectorPool: GlobalXivoConnectorPool)
  extends Controller {

  var connectorPool: XivoConnectorPool = globalXivoConnectorPool

  def list(xivoId: Int) = Action { request =>
    val mediaServers: Try[List[MediaServer]] = xivoManager.get(xivoId).map { xivo =>
      connectorPool.withConnector(xivo)(connector => {
        connector.listMds()
          .asScala
          .toList
          .map(mds => MediaServer(mds.getId, mds.getName, mds.getDisplayName, mds.getVoipIp))
      })
    }
    mediaServers match {
      case Success(t) =>
        Ok(Json.toJson(t))
      case Failure(e) =>
        Logger.error(s"Unable to list mediaservers: ${e.getMessage}")
        InternalServerError(s"Unable to list mediaservers: ${e.getMessage}")
    }
  }

  def get(xivoId: Int, mdsName: String) = Action { request =>
    val mediaServers: Try[MediaServer] = xivoManager.get(xivoId).map { xivo =>
      connectorPool.withConnector(xivo)(connector => {
        connector.listMds()
          .asScala
          .toList
          .filter(_.getName == mdsName)
          .map(mds => MediaServer(mds.getId, mds.getName, mds.getDisplayName, mds.getVoipIp))
          .head
      })
    }
    mediaServers match {
      case Success(t) =>
        Ok(Json.toJson(t))
      case Failure(e) =>
        Logger.error(s"Unable to get mediaserver $mdsName: ${e.getMessage}")
        InternalServerError(s"Unable to get mediaserver $mdsName: ${e.getMessage}")
    }
  }
}

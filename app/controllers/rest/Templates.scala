package controllers.rest

import be.objectify.deadbolt.scala.ActionBuilders
import com.google.inject.Inject
import controllers.JsonAction
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc.Controller
import services.TemplateManager
import model.{CombinedId, Template}
import play.api.Logger
import security.handlers.HandlerKeys
import controllers.helpers.LoggingAction
import scala.util.{Failure, Success}

class Templates @Inject()(templateManager: TemplateManager, actionBuilder: ActionBuilders)
  extends Controller {

  def create = actionBuilder.DynamicAction(name = "templates.create").key(HandlerKeys.json) { request =>
    LoggingAction { toLog =>
      JsonAction {
        request.body.asJson.get.validate[Template] match {
          case JsSuccess(t, _) =>
            templateManager.create(t) match {
              case Success(template) =>
                Logger.info(s"Template created: $template")
                toLog(template)
                Created("")
              case Failure(e) =>
                Logger.error(s"Error while creating template: ${e.getMessage}")
                InternalServerError(s"Unable to create template: ${e.getMessage}")
            }
          case JsError(e) =>
            Logger.warn(s"Error while parsing template: $e")
            BadRequest(s"Unable to parse json: ${e.toString()}")
        }
      }
    }.apply(request)
  }

  def list(eId: Option[CombinedId] = None) = actionBuilder.DynamicAction(name = "templates.list")
    .key(HandlerKeys.json) { request =>
      JsonAction({
        val templates = eId.fold(templateManager.all())(eId => templateManager.forEntity(eId))
        templates match {
          case Success(t) =>
            Ok(Json.toJson(t))
          case Failure(e) =>
            Logger.error(s"Unable to list templates: ${e.getMessage}")
            InternalServerError(s"Unable to list templates: ${e.getMessage}")
        }
      }).apply(request)
    }

  def get(id: Long) = actionBuilder.DynamicAction(name = "templates.get").key(HandlerKeys.json) { request =>
    JsonAction({
      templateManager.get(id) match {
        case Success(t) =>
          Ok(Json.toJson(t))
        case Failure(e) =>
          Logger.error(s"Unable to get template id: $id because of: ${e.getMessage}")
          InternalServerError(s"Unable to get template id: $id because of: ${e.getMessage}")
      }
    }).apply(request)
  }

  def edit(id: Long) = actionBuilder.DynamicAction(name = "templates.edit").key(HandlerKeys.json) { request =>
    LoggingAction { toLog =>
      JsonAction {
        request.body.asJson.get.validate[Template] match {
          case JsSuccess(t, _) =>
            templateManager.update(t) match {
              case Success(template) =>
                Logger.info(s"Template updated: $template")
                toLog(template)
                NoContent
              case Failure(e) =>
                Logger.error(s"Error while updating template: ${e.getMessage}")
                InternalServerError(s"Unable to update template: ${e.getMessage}")
            }
          case JsError(e) =>
            Logger.warn(s"Error while parsing template: $e")
            BadRequest(s"Unable to parse json: ${e.toString()}")
        }
      }
    }.apply(request)
  }

  def delete(id: Long) = actionBuilder.DynamicAction(name = "templates.delete").key(HandlerKeys.json) { request =>
    LoggingAction { toLog =>
      JsonAction {
        templateManager.delete(id) match {
          case Success(t) =>
            toLog(t)
            NoContent
          case Failure(e) =>
            Logger.error(s"Unable to delete template id: $id because of: ${e.getMessage}")
            InternalServerError(s"Unable to delete template id: $id because of: ${e.getMessage}")
        }
      }
    }.apply(request)
  }

}

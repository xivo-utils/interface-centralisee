package controllers.rest

import javax.inject.Inject

import be.objectify.deadbolt.scala.ActionBuilders
import controllers.{JsonAction, ListResult}
import model.AdministratorInterface
import model.validators.AdministratorValidator
import org.postgresql.util.PSQLException
import play.api.{Configuration, Logger}
import play.api.libs.json.Json
import play.api.mvc.{Controller, Result}
import security.handlers.HandlerKeys
import services.UIAdministratorManager
import scala.util.{Failure, Success}
import controllers.helpers.LoggingAction

class Administrators @Inject()(config: Configuration, administrators: AdministratorInterface,
                               adminManager: UIAdministratorManager, actionBuilder: ActionBuilders,
                               validator: AdministratorValidator) extends Controller {

  val logger = Logger(getClass.getName)

  def list = actionBuilder.DynamicAction(name = "administrators.list").key(HandlerKeys.json) { request =>
    JsonAction({
      adminManager.all() match {
        case Success(admins) => Ok(ListResult(admins).toJson)
        case Failure(f) => internalFailure("Unable to retrieve administrators", f)
      }
    }).apply(request)
  }

  def create = actionBuilder.DynamicAction(name = "administrators.create").key(HandlerKeys.json) { request =>
    LoggingAction { toLog =>
      JsonAction {
        val admin = validator.fromJson(request.body.asJson.get)
        validator.validate(admin)
        adminManager.create(admin) match {
          case Success(administrator) =>
            toLog(administrator)
            Created("")
          case Failure(e) =>
            responseOnException("Administrator creation failed", e)
        }
      }
    }.apply(request)
  }

  def get(id: Long) = actionBuilder.DynamicAction(name = "administrators.get").key(HandlerKeys.json) { request =>
    JsonAction({
      adminManager.get(id) match {
        case Failure(f) => NotFound(Json.toJson("Cet administrateur n'existe pas"))
        case Success(admin) => Ok(Json.toJson(admin))
      }
    }).apply(request)
  }

  def edit(id: Long) = actionBuilder.DynamicAction(name = "administrators.edit").key(HandlerKeys.json) { request =>
    LoggingAction { toLog =>
      JsonAction {
        val oldAdmin = adminManager.get(id).get
        val newAdmin = validator.fromJson(request.body.asJson.get)
        validator.validateUpdate(oldAdmin, newAdmin)
        adminManager.update(newAdmin) match {
          case Success(admin) =>
            toLog(admin)
            NoContent
          case Failure(e) => responseOnException("Administrator update failed", e)
        }
      }
    }.apply(request)
  }

  def delete(id: Long) = actionBuilder.DynamicAction(name = "administrators.delete").key(HandlerKeys.json) { request =>
    LoggingAction { toLog =>
      JsonAction {
        if (administrators.isLastSuperAdmin(id)) {
          BadRequest(Json.toJson("Il est interdit de supprimer le dernier superadmin"))
        } else {
          adminManager.delete(id)
          toLog(s"administrator:id=$id")
          NoContent
        }
      }
    }.apply(request)
  }

  private def responseOnException(message: String, e: Throwable ) : Result = e match {
    case e: PSQLException =>
      if (e.getMessage.contains("violates unique constraint \"administrators_name_unique\""))
        BadRequest("Name is already in use, please choose different one.")
      else
        internalFailure(message, e)
    case other => internalFailure("Administrator update failed", other)
  }

  private def internalFailure(message: String, e: Throwable): Result = {
    logger.error("Administrator update failed", e)
    InternalServerError("Unable to update administrator")
  }

}

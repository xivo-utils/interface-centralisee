package controllers.rest

import be.objectify.deadbolt.scala.ActionBuilders
import com.google.inject.Inject
import controllers.JsonAction
import play.api.mvc.Controller
import security.handlers.HandlerKeys
import services.UserRouteCoherencyInterface


class Validation @Inject()(userRoute: UserRouteCoherencyInterface, actionBuilder: ActionBuilders)
  extends Controller {

  def missingRoutes = actionBuilder.DynamicAction(name = "users.deleteStaleUsers").key(HandlerKeys.json) { request =>
    JsonAction(
      Ok(userRoute.getIncoherentUsersAsCsv())
        .as("text/csv")
        .withHeaders(CONTENT_DISPOSITION -> "attachment;filename=\"usersWithMissingRoutes.csv\"")
    ).apply(request)
  }

}

package controllers.rest

import be.objectify.deadbolt.scala.ActionBuilders
import com.google.inject.Inject
import controllers.JsonAction
import model.rights._
import play.api.Logger
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc.Controller
import security.handlers.HandlerKeys
import services.UIAdministratorManager
import services.rights.EntityRightsInterface
import controllers.helpers.LoggingAction

import scala.util.{Failure, Success}

class Roles @Inject()(entityRightsManager: EntityRightsInterface, adminManager: UIAdministratorManager,
                      actionBuilder: ActionBuilders)
  extends Controller {

  def create = actionBuilder.DynamicAction(name = "roles.create").key(HandlerKeys.json) { request =>
    LoggingAction { toLog =>
      JsonAction {
        request.body.asJson.get.validate[UIRole] match {
          case JsSuccess(t, _) =>
            entityRightsManager.create(t) match {
              case Success(role) =>
                Logger.info(s"Role created: $role")
                toLog(role)
                Created("")
              case Failure(e) =>
                Logger.error(s"Error while creating role: ${e.getMessage}")
                InternalServerError(s"Unable to create role: ${e.getMessage}")
            }
          case JsError(e) =>
            Logger.warn(s"Error while parsing role: $e")
            BadRequest(s"Unable to parse json: ${e.toString()}")
        }
      }
    }.apply(request)
  }

  def list() = actionBuilder.DynamicAction(name = "roles.list")
    .key(HandlerKeys.json) { request =>
      JsonAction({
        val roles = entityRightsManager.all()
        roles match {
          case Success(t) =>
            Ok(Json.toJson(t))
          case Failure(e) =>
            Logger.error(s"Unable to list roles: ${e.getMessage}")
            InternalServerError(s"Unable to list roles: ${e.getMessage}")
        }
      }).apply(request)
    }

  def get(id: Long) = actionBuilder.DynamicAction(name = "roles.get").key(HandlerKeys.json) { request =>
    JsonAction({
      entityRightsManager.get(id) match {
        case Success(t) =>
          Ok(Json.toJson(t))
        case Failure(e) =>
          Logger.error(s"Unable to get role id: $id because of: ${e.getMessage}\n${e.printStackTrace()}")
          InternalServerError(s"Unable to get role id: $id because of: ${e.getMessage}")
      }
    }).apply(request)
  }

  def myself() = actionBuilder.DynamicAction(name = "roles.myself").key(HandlerKeys.json) { request =>
    JsonAction({
      adminManager.getByLogin(request.subject.get.identifier) match {
        case Success(admin) =>
          val role: UIRole =
            if(admin.superAdmin) {
              entityRightsManager.getSuperadminRole()
            } else {
              val uiRoles: List[UIRole] = admin.roles.map(role => entityRightsManager.get(role.id.get).get)
              UIRole(None, None, UIObjectsHelper.aggregateUIRoles(uiRoles))
            }
          Ok(Json.toJson(role))
        case Failure(e) =>
          Logger.error(s"Unable to get admin ${request.subject.get.identifier} because of: ${e.getMessage}\n${e.printStackTrace()}")
          InternalServerError(s"Unable to get admin ${request.subject.get.identifier} because of: ${e.getMessage}")
      }
    }).apply(request)
  }

  def empty() = actionBuilder.DynamicAction(name = "roles.empty").key(HandlerKeys.json) { request =>
    JsonAction({
      val role = entityRightsManager.getEmptyRole()
      val json = Json.toJson(role)
      Ok(json)
    }).apply(request)
  }

  def edit(id: Long) = actionBuilder.DynamicAction(name = "roles.edit").key(HandlerKeys.json) { request =>
    LoggingAction { toLog =>
      JsonAction {
        request.body.asJson.get.validate[UIRole] match {
          case JsSuccess(t, _) =>
            entityRightsManager.update(t) match {
              case Success(role) =>
                Logger.info(s"Role updated: $role")
                toLog(role)
                NoContent
              case Failure(e) =>
                Logger.error(s"Error while updating role: ${e.getMessage}")
                InternalServerError(s"Unable to update role: ${e.getMessage}")
            }
          case JsError(e) =>
            Logger.warn(s"Error while parsing role: $e")
            BadRequest(s"Unable to parse json: ${e.toString()}")
        }
      }
    }.apply(request)
  }

  def delete(id: Long) = actionBuilder.DynamicAction(name = "roles.delete").key(HandlerKeys.json) { request =>
    LoggingAction { toLog =>
      JsonAction {
        entityRightsManager.delete(id) match {
          case Success(role) =>
            toLog(role)
            NoContent
          case Failure(e) =>
            Logger.error(s"Unable to delete role id: $id because of: ${e.getMessage}")
            InternalServerError(s"Unable to delete role id: $id because of: ${e.getMessage}")
        }
      }
    }.apply(request)
  }

}

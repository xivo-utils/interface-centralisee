package controllers

import configuration.AuthConfig
import model.AdministratorInterface
import model.utils.{AuthMethod, ConfigParser}
import models.authentication.AuthenticationProviderImpl
import models.AuthenticateUser
import play.api.db.DBApi
import play.api.libs.ws.WSClient
import play.api.mvc.{Action, Controller, Result}
import play.api.libs.json.Json
import play.api.{Configuration, Logger}
import javax.inject.Inject
import controllers.helpers.LoggingAction.logToActionLogger

class Login @Inject()(admins: AdministratorInterface, configuration: Configuration, configParser: ConfigParser,
                      wsClient: WSClient, dbApi: DBApi) extends Controller {
  val log = Logger(getClass.getName)
  val defaultRootAdmin = "admin"
  val authenticateUser = new AuthenticateUser(new AuthenticationProviderImpl(new AuthConfig(configuration), wsClient))

  def loginPage = Action(implicit request => Ok(views.html.login()))

  def login = JsonAction(implicit request => {
    val json = request.body.asJson.get
    val login = (json \ "login").asOpt[String]
    val pwd = (json \ "password").asOpt[String]
    if (login.isEmpty || pwd.isEmpty)
      BadRequest(Json.toJson("Le nom d'utilisateur et le mot de passe sont obligatoires"))
    else {
      processLogin(login.get, pwd.get)
    }
  })

  private def processLogin(login: String, pwd: String): Result = {
    if (admins.exists(login)) {
      configParser.authMethod match {
        case AuthMethod.`ldap` =>
          if (login.equals(defaultRootAdmin)) {
            processVerifyResult(login, verifyLocalLogin(login, pwd))
          } else {
            processVerifyResult(login, (verifyLdapLogin(login, pwd) || verifyLocalLogin(login, pwd)))
          }
        case AuthMethod.`login` => processVerifyResult(login, verifyLocalLogin(login, pwd))
      }
    } else {
      Forbidden(Json.toJson("Nom d'utilisateur ou mot de passe incorrect"))
    }
  }

  private def processVerifyResult(login: String, authentication: Boolean): Result = {
    if (authentication) {
      Ok(Json.toJson("")).withSession("username" -> login)
    } else {
      Forbidden(Json.toJson("Nom d'utilisateur ou mot de passe incorrect"))
    }
  }

  private def verifyLdapLogin(login: String, pwd: String): Boolean = {
    authenticateUser.authenticate(login, pwd) match {
      case Some(user) =>
        log.debug(s"$login authenticated with LDAP")
        logToActionLogger(login, "authenticated via LDAP")
        true
      case None =>
        log.debug(s"LDAP authentication failed for $login")
        logToActionLogger(login, "failed to authenticated via LDAP")
        false
    }
  }

  private def verifyLocalLogin(login: String, pwd: String): Boolean = {
    val hashedPwd = admins.getHashedPassword(login)
    if (hashedPwd.isEmpty || !admins.passwordMatches(pwd, hashedPwd.get)) {
      log.debug(s"Database authentication failed for $login")
      logToActionLogger(login, "authenticated via database")
      false
    } else {
      log.debug(s"$login authenticated with database")
      logToActionLogger(login, "failed to authenticated via database")
      true
    }
  }

  def logout = Action(Redirect(routes.Login.loginPage()).withNewSession)
}

package controllers

import play.api.libs.json.{Writes, Json}

case class ListResult[T, U](items: List[T], fields: Map[String, U]=Map[String, String]()) {
  def toJson(implicit w: Writes[T], w2: Writes[U]) = {
    var res = Json.obj("items" -> Json.toJson(items))
    for((k,v) <- fields)
      res = res + (k -> Json.toJson(v))
    res
  }
}

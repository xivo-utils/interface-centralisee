package controllers

import java.security.InvalidParameterException
import javax.inject.Inject

import com.google.inject.Singleton
import be.objectify.deadbolt.scala.{ActionBuilders, AuthenticatedRequest}
import model.validators.UserValidatorFactory
import model._
import org.slf4j.LoggerFactory
import play.api.libs.json.{JsBoolean, JsObject, JsString, Json}
import play.api.mvc._
import security.handlers.HandlerKeys
import services.{UserLinkManager, UserManager}
import controllers.helpers.LoggingAction
import controllers.helpers.LoggingAction.{ToLog, getUsername}
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext

object Users {
  val LineSeparator = "\\r?\n"
  val CsvSeparator = '|'
  val CsvEncoding = "UTF-8"
}

@Singleton
class Users @Inject()(userManager: UserManager, entities: EntityInterface, admins: AdministratorInterface,
                      validatorFactory: UserValidatorFactory, actionBuilder: ActionBuilders, userLinkManager: UserLinkManager)
  extends Controller {

  import Users._
  val logger = LoggerFactory.getLogger(getClass)

  def create(withConfiguration: Boolean) = actionBuilder.DynamicAction(name = "users.create").key(HandlerKeys.json) { request =>
    LoggingAction { toLog =>
      JsonAction {
        val validator = validatorFactory.forAdmin(request.session.get("username").get)
        val user: User = if (withConfiguration) {
          validator.fromJson(request.body.asJson.get)
        } else {
          validator.fromProxyUserJson(request.body.asJson.get)
        }
        validator.validateCreation(user, withConfiguration)

        userManager.create(user, request.session.get("username").get, withConfiguration).get
        toLog(user)
        Created("")
      }
    }.apply(request)
  }

  def fromCsv = actionBuilder.DynamicAction("users.editionPage").defaultHandler() { implicit authRequest =>
    LoggingAction { toLog =>
      Action { request =>
        request.body.asText match {
          case Some(text) => text.split(LineSeparator).toList match {
            case head :: tail => createFromCsv(head, tail, toLog)
            case Nil => BadRequest(Json.toJson("Le fichier est vide"))
          }
          case None => BadRequest(Json.toJson("Impossible de lire le contenu."))
        }
      }
    }.apply(authRequest)
  }

  private def createFromCsv(head: String, lines: List[String], toLog: ToLog)(implicit request: AuthenticatedRequest[AnyContent]): Result = {
    val validator = validatorFactory.forAdmin(request.session.get("username").get)
    var errors = List[String]()
    var number = 1
    val header = head.split(CsvSeparator)
    for {line <- lines} {
      val userMap = header.zip(line.split(CsvSeparator)).toMap

      try {
        val user = validator.fromMap(userMap)
        validator.validateCreation(user, true)
        userManager.create(user, request.session.get("username").get, true).get
        toLog(user)
      } catch {
        case e: InvalidParameterException => errors = errors ++ List(s"Ligne $number: ${e.getMessage}")
          logger.info("Paramètres invalides", e)
        case e: Exception => errors = errors ++ List(s"Ligne $number: ${e.getMessage}")
          logger.info("Erreur inattendue", e)
      }
      number += 1
    }

    if(errors.isEmpty) {
      Created("")
    } else {
      BadRequest(Json.obj("errors" -> errors))
    }
  }

  def delete(id: Long, withConfiguration: Boolean) = actionBuilder.DynamicAction(name = "users.delete").key(HandlerKeys.json) { implicit request =>
    LoggingAction { toLog =>
      JsonAction {
        userManager.getUserFromCache(id, entities) match {
          case None =>
            NotFound(Json.obj("errors" -> s"L'utilisateur id: $id n'existe pas"))
          case Some(_) =>
            userManager.delete(id, entities, getUsername.getOrElse(LoggingAction.Unknown), withConfiguration)
            toLog(s"user:id=$id")
            NoContent
        }
      }
    }.apply(request)
  }

  def disconnectDevice(id: Long) = actionBuilder.DynamicAction(name = "users.disconnectDevice").key(HandlerKeys.json) { request =>
    LoggingAction { toLog =>
      JsonAction {
        userManager.getUserWithVoicemailFromXivo(id, entities) match {
          case None =>
            NotFound(Json.obj("errors" -> s"L'utilisateur id: $id n'existe pas"))
          case Some(UserWithVM(foundUser, _)) =>
            val deviceFound = userManager.disconnectDevice(id, entities, request.session.get("username").get)
            toLog(foundUser, s"deviceFound=$deviceFound")
            Ok(
              JsObject(Map(
                "deviceFound" -> JsBoolean(deviceFound)
              ))
            )
        }
      }
    }.apply(request)
  }

  def listForEntity(cId: CombinedId, updateFromXivo: Boolean) = actionBuilder.DynamicAction(name = "users.listForEntity").key(HandlerKeys.json) { request =>
    JsonAction({
      entities.getEntity(cId) match {
        case None =>
          NotFound(Json.obj("errors" -> "Entity not found"))
        case Some(entity) =>
          val res = userManager.listForEntityFromCache(entity)
          Ok(ListResult(res.users, Map("ghostIds" -> res.ghostIds)).toJson)
      }
    }).apply(request)
  }

  def search(query: String, limit: Int) = actionBuilder.DynamicAction(name = "users.search").key(HandlerKeys.json) { request =>
    JsonAction({
      val users =
        if (query == "") Nil
        else userManager.findAcrossXivos(entities, query, limit)
      Ok(ListResult(users).toJson)
    }).apply(request)
  }

  def searchPage() = actionBuilder.DynamicAction("users.listPage").defaultHandler() {
    implicit request => Future(Ok(views.html.usersSearch(admins)))
  }

  def get(id: Long) = actionBuilder.DynamicAction(name = "users.get").key(HandlerKeys.json) { request =>
    JsonAction({
      userManager.getUserWithVoicemailCombiningXivoAndCache(id, entities) match {
        case None => NotFound(Json.obj("errors" -> s"L'utilisateur id: $id n'existe pas"))
        case Some(userWithVM) =>
          Ok(Json.toJson(userWithVM))
      }
    }).apply(request)
  }

  def update(id: Long, withConfiguration: Boolean) = actionBuilder.DynamicAction(name = "users.update").key(HandlerKeys.json) { authRequest =>
    LoggingAction { toLog =>
      JsonAction {
        implicit request =>
          request.body.asJson match {
            case None => NoContent
            case Some(json) =>
              val old = userManager.getUserWithVoicemailCombiningXivoAndCache(id, entities).get
              val validator = validatorFactory.forAdmin(request.session.get("username").get)
              val (updatedUser, updatedVoicemail) = validator.fromJsonForUpdate(json, old.user)
              val updated = UserWithVM(updatedUser, updatedVoicemail.getOrElse(old.voicemail))
              validator.validateUpdate(old.user, updated.user)
              userManager.update(old, updated, request.session.get("username").get, withConfiguration)
              toLog(updatedUser)
              NoContent
          }
      }
    }
      .apply(authRequest)
  }


  def deleteStaleUsers(entityCId: CombinedId) = actionBuilder.DynamicAction(name = "users.deleteStaleUsers").key(HandlerKeys.json) { request =>
    LoggingAction { toLog =>
      JsonAction {
        val entity = entities.getEntity(entityCId).get
        toLog(entity)
        userManager.getFromXivoAndCacheForEntity(entity).ghostIds.foreach { id =>
          toLog(s"user.id=$id")
          userManager.delete(id, entities, request.session.get("username").get, withConfiguration = true)
        }
        NoContent
      }
    }.apply(request)
  }

  def checkExternalNumber(entityCId: model.CombinedId, intervalId: Int, extNum: String, userId: Option[Long]) =
    actionBuilder.DynamicAction(name = "entities.checkExternalNumber").key(HandlerKeys.json) { request =>
      JsonAction({
        val validator = validatorFactory.forAdmin(request.session.get("username").get)
        val entity = entities.getEntity(entityCId).get
        val excludedRouteId = userId.flatMap(uid => userLinkManager.get(uid).toOption).flatMap(_.externalRouteId)
        val res = validator.checkExternalNumber(Some(intervalId), entity.intervals, extNum, userId, excludedRouteId)
        Ok(
          JsObject(Seq(
            "available" -> JsBoolean(res.isEmpty),
            "message" -> JsString(res.getOrElse(""))
          ))
        )
      }).apply(request)
    }

  def listPage(entityId: CombinedId) = actionBuilder.DynamicAction("users.listPage").defaultHandler() {
    implicit request => {
      val entityName = entities.getEntity(entityId).map(_.name).getOrElse("")
      Future(Ok(views.html.users(admins, entityId, entityName)))
    }
  }

  def creationPage(entityId: CombinedId) = actionBuilder.DynamicAction("users.creationPage").defaultHandler() {
    implicit request => Future(Ok(views.html.createUser(admins, entityId)))
  }

  def editionPage(id: Long) = actionBuilder.DynamicAction("users.editionPage").defaultHandler() {
    implicit request => Future(
      userManager.getUserWithVoicemailFromXivo(id, entities) match {
        case None => NotFound(views.html.noSuchUser())
        case Some(result) => Ok(views.html.editUser(admins, id, result.user.firstName + ' ' + result.user.lastName))

      }
    )
  }

}

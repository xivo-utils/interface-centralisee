package model

import org.joda.time.DateTime

case class RouteMeta(
                      id: Option[Long],
                      routeRef: Long,
                      createdDate: DateTime,
                      createdBy: String,
                      modifiedDate: DateTime,
                      modifiedBy: String,
                      releasedDate: DateTime,
                      releasedBy: String,
                      manual: Boolean = true
                    )

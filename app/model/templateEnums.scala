package model

import play.api.libs.json.{JsError, _}

object PeerSipNameMode extends Enumeration {
  type Type = Value

  val Auto, Model, WebRTC, UniqueAccount = Value

  val enumValues: Map[PeerSipNameMode.Type, String] = Map(
    PeerSipNameMode.Auto -> "auto",
    PeerSipNameMode.Model -> "model",
    PeerSipNameMode.WebRTC -> "webrtc",
    PeerSipNameMode.UniqueAccount -> "ua")

  implicit val reads = new Reads[PeerSipNameMode.Type] {
    def reads(string: JsValue) = string match {
      case s: JsString => JsSuccess(enumValues.filter(e => e._2 equals s.value).head._1)
      case _ => JsError("Expected one of auto/model/webrtc/ua")
    }
  }

  implicit val writes = new Writes[PeerSipNameMode.Type] {
    def writes(peerSipName: PeerSipNameMode.Type) = JsString(enumValues(peerSipName))
  }
}

object CallerIdMode extends Enumeration {
  type Type = Value

  val IncomingNo, Anonymous, Custom = Value

  val enumValues: Map[CallerIdMode.Type, String] = Map(
    CallerIdMode.IncomingNo -> "incomingNo",
    CallerIdMode.Anonymous -> "anonymous",
    CallerIdMode.Custom -> "custom"
  )

  implicit val reads = new Reads[CallerIdMode.Type] {
    def reads(string: JsValue) = string match {
      case s: JsString => JsSuccess(enumValues.filter(e => e._2 equals s.value).head._1)
      case _ => JsError("Expected one of incomingNo/anonymous/custom")
    }
  }

  implicit val writes = new Writes[CallerIdMode.Type] {
    def writes(callerIdMode: CallerIdMode.Type) = JsString(enumValues(callerIdMode))
  }
}

object VoiceMailNumberMode extends Enumeration {
  type Type = Value

  val ShortNumber, Custom = Value

  val enumValues: Map[VoiceMailNumberMode.Type, String] = Map(
    VoiceMailNumberMode.ShortNumber -> "short_number",
    VoiceMailNumberMode.Custom -> "custom"
  )

  implicit val reads = new Reads[VoiceMailNumberMode.Type] {
    def reads(string: JsValue) = string match {
      case s: JsString => JsSuccess(enumValues.filter(e => e._2 equals s.value).head._1)
      case _ => JsError("Expected one of ShortNumber/Custom")
    }
  }

  implicit val writes = new Writes[VoiceMailNumberMode.Type] {
    def writes(vmNoMode: VoiceMailNumberMode.Type) = JsString(enumValues(vmNoMode))
  }
}

package model

import play.api.libs.json.{JsObject, JsValue, Json, Writes}

case class UserWithVM(user: User, voicemail: VoicemailSettings)

object UserWithVM {

  implicit val writes = new Writes[UserWithVM] {
    override def writes(userWithVM: UserWithVM): JsValue =
      Json.toJson(userWithVM.user) match {
        case userJson: JsObject =>
          import model.VoicemailSettings.jsonWrites
          userJson + ("voicemail" -> Json.toJson(userWithVM.voicemail))
        case _ =>
          throw new Exception("JSON representation of User is expected to be JsObject")
      }
  }

}

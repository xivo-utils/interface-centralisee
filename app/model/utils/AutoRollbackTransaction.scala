package model.utils

import org.slf4j.LoggerFactory

import scala.collection.TraversableOnce
import scala.collection.mutable.ListBuffer

trait Step {
  def execute
  def rollback
}

object Step {
  def apply(executeFun: => Unit, rollbackFun: => Unit) = new Step {
    override def execute = executeFun
    override def rollback = rollbackFun
  }
}

class AutoRollbackTransaction(initialSteps: TraversableOnce[Step] = Nil) {
  private val logger = LoggerFactory.getLogger(getClass)
  private val steps = ListBuffer[Step]()
  addSteps(initialSteps)

  def execute(): Unit = {
    logger.info(s"Starting a new transaction with ${steps.length} steps")
    val jSteps = scala.collection.JavaConversions.seqAsJavaList(steps)
    val it = jSteps.listIterator()
    var i = 1
    while(it.hasNext) {
      try {
        logger.info(s"Executing step number $i")
        it.next().execute
        i += 1
      } catch {
        case e: Exception => logger.warn("An exception was thrown, rolling back transaction", e)
          it.previous()
          while(it.hasPrevious) {
            i -= 1
            logger.info(s"Rolling back step $i")
            it.previous().rollback
          }
          logger.info("Transaction rolled back")
          throw e
      }
    }
    logger.info("Transaction executed correctly")
  }

  def addStep(step: Step): Unit = steps += step
  def addSteps(stepsToAdd: TraversableOnce[Step]): Unit = steps ++= stepsToAdd
  def step(executeFun: => Unit, rollbackFun: => Unit): Unit = addStep(Step(executeFun, rollbackFun))
}

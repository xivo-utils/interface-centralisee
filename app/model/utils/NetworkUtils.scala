package model.utils

import java.net.{InetAddress, Socket}

class NetworkUtils {
  def getExternalIp(remoteIp: String): String = new Socket(remoteIp, 22).getLocalAddress.getHostAddress

  def isHostReachable(host: String): Boolean = InetAddress.getByName(host).isReachable(500)
}

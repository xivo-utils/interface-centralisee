package model.utils

object MathUtils {
  def paddWithZeros(number: Int, size: Int): String = {
    val strNum = number.toString
    return "0" * (size - strNum.length) + strNum
  }
}

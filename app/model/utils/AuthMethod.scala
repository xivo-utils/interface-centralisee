package model.utils

object AuthMethod extends Enumeration {
  type AuthMethod = Value
  val ldap, login = Value
}

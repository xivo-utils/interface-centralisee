package model.utils

import java.io.File
import org.slf4j.LoggerFactory

import scala.io.Source

case class RemoteConfigFile(localPath: String, remotePath: String, owner: String, octalPermissions: String)

object RemoteConfigFile {

  val CommentPattern = "^\\s*#(.*)".r
  val LinePattern = """^\s*([^\s]+)\s+([^\s]+)\s+([^\s]+)\s+([0-7]{3})""".r
  val logger = LoggerFactory.getLogger(getClass)

  def all(fileMapper: File, xivoConfDir: String): List[RemoteConfigFile] = fromFileMapper(fileMapper).map(f => f.copy(localPath=xivoConfDir + "/" + f.localPath))

  private def fromFileMapper(fileMapper: File): List[RemoteConfigFile] = Source.fromFile(fileMapper).getLines().filterNot(_.trim.isEmpty).flatMap({
    case CommentPattern(m) => None
    case LinePattern(localPath, remotePath, owner, octalPermissions) => Some(RemoteConfigFile(localPath, remotePath, owner, octalPermissions))
    case line => logger.error(s"Ignoring invalid line in file mapper : $line")
      None
  }).toList
}
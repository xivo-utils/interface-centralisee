package model.utils

import org.slf4j.LoggerFactory
import play.api.Play
import play.api.Play.current

import scala.sys.process._

class SshManager {

  val logger = LoggerFactory.getLogger(getClass())
  lazy val KeyFile = Play.application.configuration.getString("ssh.privateKey").get

  def executeRemoteCommand(host: String, cmd: String): Int = {
    logger.debug(s"Executing '$cmd' on remote host $host")
    (s"ssh -i $KeyFile -o PasswordAuthentication=No -o StrictHostKeyChecking=no root@$host $cmd" !)
  }

  def getRemoteCommandOutput(host: String, cmd: String): Option[String] = {
    logger.debug(s"Executing '$cmd' on remote host '$host'")
    try {
      val s = (s"ssh -i $KeyFile -o PasswordAuthentication=No -o StrictHostKeyChecking=no root@$host $cmd" !!)
      logger.debug(s"Command '$cmd' on '$host' returned: '$s'")
      Some(s)
    } catch {
      case e: Exception =>
        logger.warn(s"Error while executing '$cmd' on host '$host': ${e.getMessage}")
        None
    }
  }

  def copyFile(host: String, file: RemoteConfigFile): Int = {
    val SUCCESS = 0
    logger.debug(s"Copiyng file ${file.localPath} to ${file.remotePath}")
    def copy(): Int = s"/usr/bin/scp -i $KeyFile -o PasswordAuthentication=No -o StrictHostKeyChecking=no ${file.localPath} root@$host:${file.remotePath}" !
    def chown(): Int = executeRemoteCommand(host,  s"chown ${file.owner} ${file.remotePath}")
    def chmod(): Int = executeRemoteCommand(host,  s"chmod ${file.octalPermissions} ${file.remotePath}")
    val commands: Array[() => Int] = Array(copy, chown, chmod)
    def execute(commands: Iterator[()=>Int]): Int = {
      if (commands.hasNext) {
        val command = commands.next()
        val result = command()
        if (result == SUCCESS) {
          execute(commands)
        }
        else {
          logger.warn(s"Command returned error code: $result")
          result
        }
      }
      else {
        logger.debug(s"Copied file ${file.localPath} to ${file.remotePath} with owner ${file.owner} and perm. ${file.octalPermissions} on remote host $host")
        SUCCESS
      }
    }
    execute(commands.iterator)
  }
}

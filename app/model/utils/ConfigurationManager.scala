package model.utils

import model.{Xivo, XivoCreation}
import xivo.ldap.xivoconnection.XivoConnector
import xivo.restapi.connection.WebServicesException

import scala.collection.mutable.ListBuffer

class ConfigurationManager(configFiles: List[RemoteConfigFile], defaultXivoConnectorPool: XivoConnectorPool, config: ConfigParser) {

  var connectorPool = defaultXivoConnectorPool
  var ipResolver = new NetworkUtils()
  var ssh = new SshManager
  val VersionFile = "/usr/share/xivo/XIVO-VERSION"
  val DefaultPostgresqlVersion = "9.4"
  val XivoVersionWithAddedACL = "16.01"

  val agiBinError = "erreur lors de la création du répertoire agi-bin"
  val syncError = "erreur lors de synchronisation des fichiers"
  val postgresCfgError = "échec lors du màj de configuration de postgresql"
  val restartError = "erreur lors du redémarrage des services"

  def configure(xivo: XivoCreation): List[String] = {
    var result = new ListBuffer[String]()
    result ++= getResult(ssh.executeRemoteCommand(xivo.host, "mkdir -p /usr/share/asterisk/agi-bin"), agiBinError)
    result ++= getResult(synchronizeFiles(xivo.host), syncError)
    val postgresVersion = getPostgresqlVersion(xivo.host).getOrElse(DefaultPostgresqlVersion)
    if (postgresVersion == "9.4") {
      result ++= getResult(ssh.executeRemoteCommand(xivo.host, s"echo listen_addresses=\\'*\\' >> /etc/postgresql/$postgresVersion/main/postgresql.conf"), postgresCfgError)
      result ++= getResult(ssh.executeRemoteCommand(xivo.host, s"echo host asterisk asterisk ${ipResolver.getExternalIp(xivo.host)}/32 md5 >> /etc/postgresql/$postgresVersion/main/pg_hba.conf"), postgresCfgError)
      result ++= getResult(ssh.executeRemoteCommand(xivo.host, "/usr/bin/xivo-service restart all"), restartError)
    }
    else {
      result ++= getResult(ssh.executeRemoteCommand(xivo.host, s"echo host asterisk asterisk ${ipResolver.getExternalIp(xivo.host)}/32 md5 >> /var/lib/postgresql/$postgresVersion/main/pg_hba.conf"), postgresCfgError)
      result ++= getResult(ssh.executeRemoteCommand(xivo.host, s"bash --login -c '/usr/bin/xivo-service restart all'"), restartError)
    }
    connectorPool.withConnector(xivo)(connector => {
      try {
        connector.getUsersNumber
      } catch {
        case e: WebServicesException =>
          createWebServicesAccess(connector, xivo)
          connector.getUsersNumber
      }
    })
    result.toList
  }

  private[utils] def createWebServicesAccess(connector: XivoConnector, xivo: XivoCreation): Unit = {
    getXivoVersion(xivo.host) match {
      case Some(version) =>
        if (version >= XivoVersionWithAddedACL) {
          connector.createWebServicesUserWithAcl(xivo.name, "", ipResolver.getExternalIp(xivo.host), "{}")
        } else {
          connector.createWebServicesUser(xivo.name, "", ipResolver.getExternalIp(xivo.host))
        }
      case None => throw new WebServicesException("Unable to create webservices access", 403)
    }

  }

  private def getResult(result: Int, msg:String): List[String] = {
    if (result != 0) {
      List(msg)
    }
    else {
      Nil
    }
  }

  private[utils] def getPostgresqlVersion(host: String): Option[String] = {
    getXivoVersion(host) match {
      case Some(version) if version >= "2019.05.00" => Some("11")
      case Some(version) if version >= "15.20" => Some("9.4")
      case None => None
    }
  }

  private def getXivoVersion(host: String): Option[String] = {
    try {
      ssh.getRemoteCommandOutput(host, s"cat $VersionFile").map(s => s.filterNot(c => c.isControl))
    } catch {
      case _: Exception =>
        None
    }
  }

  private def synchronizeFiles(host: String): Int = {
    var result = 0
    configFiles.foreach(result += ssh.copyFile(host, _))
    result
  }

  def synchronizeFiles(xivo: Xivo): Unit = synchronizeFiles(xivo.host)

  def getPlayAuthToken(host: String): String = {
    try {
      ssh.getRemoteCommandOutput(host, s"cat /etc/docker/xivo/custom.env | grep -oP -m 1 \'PLAY_AUTH_TOKEN=\\K.*\'")
        .map(_.trim).getOrElse(config.defaultPlayAuthToken)
    } catch {
    case _: Exception =>
      config.defaultPlayAuthToken
    }
  }
}

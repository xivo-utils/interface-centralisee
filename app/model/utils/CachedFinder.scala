package model.utils

import scala.collection.concurrent.TrieMap

class CachedFinder[Key,Result](find: Key => Option[Result]) extends (Key => Option[Result])
{
  private val cache = TrieMap[Key, Result]()

  override def apply(key: Key): Option[Result] =
    cache.get(key).orElse {
        find(key).map { result =>
          cache(key) = result
          result
        }
      }
}

object CachedFinder {
  def apply[Key,Result](find: Key => Option[Result]) = new CachedFinder(find)
}

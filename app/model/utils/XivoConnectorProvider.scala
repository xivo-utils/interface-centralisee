package model.utils

import java.util.concurrent.ConcurrentHashMap
import javax.inject.Inject

import com.google.inject.Singleton
import model.XivoObj
import nf.fr.eraasoft.pool.{ObjectPool, PoolSettings, PoolableObjectBase}
import org.slf4j.LoggerFactory
import xivo.ldap.xivoconnection.{XivoConnector, XivoConnectorFactory}
import xivo.restapi.connection.DefaultRemoteRestApiConfig
import xivo.restapi.connection.RestapiConfig

class RemoteRestApiConfig(host: String, incallContext: String, token: String) extends DefaultRemoteRestApiConfig(host) {
  override def get(name: String): String = {
    name match {
      case RestapiConfig.EXTERNAL_CONTEXT_NAME => incallContext
      case RestapiConfig.CONFIGMGT_TOKEN => token
      case name => super.get(name)
    }
  }
}

trait XivoConnectorProvider {
  def get(host: String, incallContext: String, token: String): XivoConnector =
    new XivoConnectorFactory(host, new RemoteRestApiConfig(host, incallContext, token)).getRealXivoConnector
}

class XivoConnectorPool(config: ConfigParser) {
  this: XivoConnectorProvider =>
  val poolMap = new ConcurrentHashMap[String, ObjectPool[XivoConnector]]()
  val logger = LoggerFactory.getLogger(getClass)
  val incallContext = config.incallContext

  def withConnector[T](xivo: XivoObj)(f: XivoConnector => T): T = {
    val playAuthToken = xivo.playAuthToken.getOrElse(config.defaultPlayAuthToken)
    val connector = synchronized {
      poolMap.get(xivo.host) match {
        case pool if pool != null => pool
        case null => val poolSettings = new PoolSettings[XivoConnector](
          new PoolableObjectBase[XivoConnector]() {
            override def make(): XivoConnector =  get(xivo.host, incallContext, playAuthToken)

            override def activate(t: XivoConnector) = ()
          }).min(0).max(10)
          val pool = poolSettings.pool()
          poolMap.put(xivo.host, pool)
          pool
      }
    }.getObj
    try {
      val res = f(connector)
      synchronized(poolMap.get(xivo.host).returnObj(connector))
      res
    } catch {
      case e: Exception => logger.error("Error when executing function " + f, e)
        synchronized(poolMap.get(xivo.host).returnObj(connector))
        throw e
    }
  }
}

@Singleton
class GlobalXivoConnectorPool @Inject()(config: ConfigParser) extends XivoConnectorPool(config) with XivoConnectorProvider
package model.utils

import javax.inject.Inject
import AuthMethod.AuthMethod
import play.api.Logger
import play.api.Configuration

class ConfigParser @Inject()(config: Configuration) {
  val logger = Logger.logger

  val defaultAuthMethod = AuthMethod.login
  val defaultPlayAuthToken: String = config.getString("playAuthToken").getOrElse("u@pf#41[gYHJm<]9N[a0iWDQQ7`e9k")
  def authMethod: AuthMethod = {
    try {
      config.getString("authentication.method") match {
        case Some(method) =>
          AuthMethod.withName(method)
        case None =>
          defaultAuthMethod
      }
    }
    catch {
      case e: NoSuchElementException =>
        logger.warn("Incorrect value for auth method, please check your configuration.")
        defaultAuthMethod
    }
  }

  def allowIntervalOverlap: Boolean = config.getBoolean("allowIntervalOverlap").getOrElse(false)
  def anonymousCallerId: String = config.getString("anonymousCallerId").getOrElse("anonymous")
  val emptyIfMailAsCtiLogin = ""
  def incallContext: String = config.getString("incallContext").getOrElse("from-extern")

  def cacheInterval: Int = config.getInt("cacheInterval").getOrElse(12)

  def disableOutcallerIdProcessing: Boolean = config.getBoolean("disableOutcallerIdProcessing").getOrElse(false)
}

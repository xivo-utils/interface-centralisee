package model.utils

import java.io.File
import play.api.Play
import play.api.Play.current

class ConfigurationManagerFactory {
  lazy val fileMapper = Play.application.configuration.getString("xivo.fileMapper").get
  lazy val xivoConfDir = Play.application.configuration.getString("xivo.configurationDir").get
  lazy val configFiles = RemoteConfigFile.all(new File(fileMapper), xivoConfDir)

  def get(connectorPool: XivoConnectorPool, config: ConfigParser): ConfigurationManager = new ConfigurationManager(configFiles, connectorPool, config)
}

package model

import javax.inject.Inject

import anorm.SqlParser._
import anorm._
import com.google.inject.{ImplementedBy, Singleton}
import org.slf4j.LoggerFactory
import play.api.db.{Database, _}

case class Route(
                  id: Option[Long],
                  digits: String,
                  regexp: String,
                  context: String,
                  target: String="\\1",
                  prefix: Boolean = false
                )
object Route {
  val route: RowParser[Route] = Macro.namedParser[Route]
  val targetDisabled = "to_pit_stop"
}

@ImplementedBy(classOf[RouteImpl])
trait RouteInterface {
  def routeExists(digits: String): Boolean
  def numberUsed(digits: String): Boolean
  def getRoute(id: Long): Option[Route]
  def findRouteByDigits(digits: String): Option[Route]
  def create(route: Route): Route
  def update(route: Route): Unit
  def setDeleted(id: Long): Unit
  def setDeletedByDigits(digits: String): Route
  def createExternal(u: User): Route
  def createInternal(u: User): Route
}

@Singleton
class RouteImpl @Inject()(@NamedDatabase("default") db: Database) extends RouteInterface {

  val logger = LoggerFactory.getLogger(getClass)

  val SelectRoute = "SELECT id, digits, prefix, regexp, context, target FROM route "

  override def routeExists(digits: String): Boolean = findRouteByDigits(digits).nonEmpty

  override def numberUsed(digits: String): Boolean =
    findRouteByDigits(digits).exists(_.context != Route.targetDisabled)

  override def create(route: Route): Route = {
    findRouteByDigits(route.digits) match {
      case None =>
        val created = createNew(route)
        logger.debug(s"A new route was created: $created")
        created
      case Some(r) =>
        val updatedRoute = route.copy(id = r.id)
        update(updatedRoute)
        logger.debug(s"An existing route was found, updating instead of creating: $route")
        updatedRoute
    }
  }

  override def findRouteByDigits(digits: String): Option[Route] = db.withConnection(implicit c =>
    SQL(SelectRoute + " WHERE digits = {digits}")
      .on('digits -> digits).as(Route.route *).headOption)

  private def createNew(route: Route): Route = {
    val genId: Option[Long] = db.withConnection(implicit c =>
      SQL("INSERT INTO route(digits, prefix, regexp, context, target) VALUES ({digits}, {prefix}, {regexp}, {context}, {target})")
        .on('digits -> route.digits, 'prefix -> route.prefix, 'regexp -> route.regexp, 'context -> route.context,
          'target -> route.target)
        .executeInsert(get[Long]("id") *).headOption)
    route.copy(id = genId)
  }

  override def getRoute(id: Long): Option[Route] = db.withConnection(implicit c =>
    SQL(SelectRoute + " WHERE id = {id}").on('id -> id).as(Route.route *).headOption)

  override def update(route: Route): Unit = db.withConnection(implicit c => {
    SQL("UPDATE route SET digits={digits}, prefix={prefix}, regexp={regexp}, context={context}, target={target} " +
      "WHERE id = {id}")
      .on('id -> route.id, 'digits -> route.digits, 'prefix -> route.prefix, 'regexp -> route.regexp, 'context -> route.context,
        'target -> route.target)
      .executeUpdate()
    logger.debug(s"Updated route: $route")
  })

  override def setDeleted(id: Long): Unit = {
    getRoute(id) match {
      case Some(route) =>
        update(route.copy(target = "\\1", context = Route.targetDisabled))
      case None =>
    }
  }

  override def setDeletedByDigits(digits: String): Route = {
    val route = Route(None, digits, s"($digits)", Route.targetDisabled, "\\1")
    create(route)
  }

  def createExternal(user: User): Route = {
    val extRoute = Route(None, user.externalNumber.get, s"(${user.externalNumber.get})", user.entity.xivo.contextName,
      user.internalNumber)
    create(extRoute)
  }

  def createInternal(user: User): Route = {
    val intRoute = Route(None, user.internalNumber, s"(${user.internalNumber})", user.entity.xivo.contextName)
    create(intRoute)
  }

}

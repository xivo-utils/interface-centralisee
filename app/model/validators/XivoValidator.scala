package model.validators

import java.security.InvalidParameterException
import com.google.inject.{ImplementedBy, Inject, Singleton}
import model._
import model.utils.{NetworkUtils, SshManager}
import play.api.libs.json.JsValue

object XivoValidator {
  val ErrorUnreachable = "Le Xivo n'est pas joignable sur le réseau"
  val ErrorNoSuchXivo = "Il n'existe pas de XiVO d'id %s"
  val ErrorUniqueValues = "Le nom ou l'adresse existent déjà"
  val ErrorSshKey = "Impossible de se connecter au XiVO avec cette clé ssh"
  val ErrorWrongContextName= "Le nom du trunk ne doit pas contenir que lettres sans accents, chiffres et - et _"
}

@ImplementedBy(classOf[XivoValidator])
trait XivoValidatorInterface extends Validator {
  def fromJson(json: JsValue): XivoCreation
  def validate(xivo: XivoCreation)
  var sshManager: SshManager = new SshManager
  var netUtils: NetworkUtils = new NetworkUtils
}

@Singleton
class XivoValidator @Inject()(xivoManager: XivoInterface) extends XivoValidatorInterface {

  import XivoValidator._
  override def fromJson(json: JsValue): XivoCreation =
    ((json \ "name").asOpt[String], (json \ "host").asOpt[String], (json \ "linkedXivo").asOpt[List[Long]],
      (json \ "configure").asOpt[Boolean], (json \ "contextName").asOpt[String]) match {
      case (Some(name), Some(host), Some(linkedXivo), cfg: Option[Boolean], Some(contextName)) =>
        XivoCreation(name, host, linkedXivo, contextName, cfg.getOrElse(true))
      case _ => throw new InvalidParameterException(ErrorRequiredFields("name", "host", "linkedXivo", "contextName"))
    }

  private def validateSshKey(xivo: XivoCreation) = {
    val res = sshManager.executeRemoteCommand(xivo.host, "exit")
    if( res != 0)
      throw new InvalidParameterException(ErrorSshKey)
  }

  override def validate(xivo: XivoCreation): Unit = {
    validateUniqueValues(xivo)
    validateXivoIds(xivo)
    validateXivoIsReachable(xivo)
    validateSshKey(xivo)
    validateContextName(xivo.contextName)
  }

  private def validateUniqueValues(xivo: XivoCreation): Unit = {
    for(x <- xivoManager.list) {
      if(x.name.equals(xivo.name) || x.host.equals(xivo.host))
        throw new InvalidParameterException(ErrorUniqueValues)
    }
  }

  private def validateXivoIds(xivo: XivoCreation): Unit = {
    for(id <- xivo.linkedXivo) {
      if(xivoManager.getXivo(id).isEmpty) throw new InvalidParameterException(ErrorNoSuchXivo.format(id))
    }
  }

  private def validateXivoIsReachable(xivo: XivoCreation): Unit = {
    if(!netUtils.isHostReachable(xivo.host)) throw new InvalidParameterException(ErrorUnreachable)
  }

  private def validateContextName(name: String): Unit = {
    if (! name.matches("[a-zA-Z0-9-_]*")) {
      throw new InvalidParameterException(ErrorWrongContextName)
    }
  }

}

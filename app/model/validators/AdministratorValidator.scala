package model.validators

import java.security.InvalidParameterException
import javax.inject.{Inject, Singleton}

import model._
import model.rights.Role
import play.api.libs.json.JsValue
import services.{AdministratorManager, UIAdministratorManager}
import services.rights.RoleManager

import scala.util.{Failure, Success}

trait AdministratorValidatorInterface {
  def validateUpdate(oldAdmin: UIAdministrator, newAdmin: UIAdministrator)
  def fromJson(json: JsValue): UIAdministrator
  def validate(admin: UIAdministrator)
}

object AdministratorValidator extends Validator {
  val ErrorLoginExists = "Le login %s existe déjà"
}

@Singleton
class AdministratorValidator @Inject()(roles: RoleManager, admins: AdministratorInterface,
                                       adminManager: UIAdministratorManager) extends AdministratorValidatorInterface {

  import AdministratorValidator._

  override def validate(admin: UIAdministrator): Unit = validateUniqueLogin(admin)

  def validateUniqueLogin(admin: UIAdministrator) = {
    adminManager.all() match {
      case Success(admins) =>
        if (admins.map(_.login).contains(admin.login)) {
          throw new InvalidParameterException(ErrorLoginExists.format(admin.login))
        }
      case Failure(f) =>
        throw new Exception("Unable to validate login")
    }
  }

  override def fromJson(value: JsValue): UIAdministrator = (
    (value \ "id").asOpt[Long],
    (value \ "login").asOpt[String],
    (value \ "name").asOpt[String],
    (value \ "password").asOpt[String],
    (value \ "superAdmin").asOpt[Boolean],
    (value \ "ldap").asOpt[Boolean],
    (value \ "roleIds").asOpt[List[Long]]) match {
      case (id, Some(login), Some(name), Some(password), Some(superAdmin), Some(ldap), Some(roleIds)) =>
        UIAdministrator(id, name, login, password, superAdmin, ldap, roleIds.map(id => roles.get(id) match {
          case Success(role) => role
          case Failure(f) => throw new InvalidParameterException(ErrorNoSuchRole.format(id))
        }))
      case (id, Some(login), Some(name), Some(password), Some(superAdmin), Some(ldap), None) =>
        UIAdministrator(id, name, login, password, superAdmin, ldap, List())
      case _ => throw new InvalidParameterException(ErrorRequiredFields("login", "name", "password", "superAdmin", "ldap"))
  }

  override def validateUpdate(oldAdmin: UIAdministrator, newAdmin: UIAdministrator) =
    if(!oldAdmin.login.equals(newAdmin.login)) validateUniqueLogin(newAdmin)
}

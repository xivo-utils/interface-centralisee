package model.validators

import java.security.InvalidParameterException
import com.google.inject.{ImplementedBy, Inject, Singleton}
import model._
import play.api.libs.json.JsValue

object ProxyValidator {
  val ErrorUniqueValues = "Le nom ou l'adresse existent déjà"
  val ErrorWrongContextName= "Le nom du trunk ne doit pas contenir que lettres sans accents, chiffres et - et _"
}

@ImplementedBy(classOf[ProxyValidator])
trait ProxyValidatorInterface extends Validator {
  def fromJson(json: JsValue): ProxyCreation
  def validate(xivo: ProxyCreation)
}

@Singleton
class ProxyValidator @Inject()(xivoManager: XivoInterface) extends ProxyValidatorInterface {

  import XivoValidator._
  override def fromJson(json: JsValue): ProxyCreation =
    ((json \ "name").asOpt[String], (json \ "host").asOpt[String],(json \ "contextName").asOpt[String], (json \ "intervals").asOpt[List[Interval]]) match {
      case (Some(name), Some(host), Some(contextName), Some(intervals)) =>
        ProxyCreation(name, host, contextName, intervals)
      case _ => throw new InvalidParameterException(ErrorRequiredFields("name", "host", "contextName", "intervals"))
    }

  override def validate(xivo: ProxyCreation): Unit = {
    validateUniqueValues(xivo)
    validateContextName(xivo.contextName)
  }

  private def validateUniqueValues(xivo: ProxyCreation): Unit = {
    for(x <- xivoManager.list) {
      if(x.name.equals(xivo.name) || x.host.equals(xivo.host))
        throw new InvalidParameterException(ErrorUniqueValues)
    }
  }

  private def validateContextName(name: String): Unit = {
    if (! name.matches("[a-zA-Z0-9-_]*")) {
      throw new InvalidParameterException(ErrorWrongContextName)
    }
  }

}


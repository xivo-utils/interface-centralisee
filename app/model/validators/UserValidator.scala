package model.validators

import java.security.InvalidParameterException
import javax.inject.{Inject, Singleton}

import model._
import model.utils.{GlobalXivoConnectorPool, XivoConnectorPool, XivoConnectorProvider}
import org.slf4j.LoggerFactory
import play.api.libs.json.JsValue
import services.cache.{CachedXivoUser, CachedXivoUserManager}
import services.{TemplateManager, UIAdministratorManager}
import model.Interval.extNumInRangeOfIntervalRoutedWithCustDirNum
import model.validators.UserValidator.computeExtNumber

import scala.util.{Failure, Success}

trait UserValidatorFactoryInterface {
  def forAdmin(adminLogin: String): UserValidator
}

@Singleton
class UserValidatorFactory @Inject()(admins: AdministratorInterface, entity: EntityInterface, route: RouteInterface,
                                     templateManager: TemplateManager, adminManager: UIAdministratorManager,
                                     cachedXivoUserManager: CachedXivoUserManager, globalXivoConnectorPool: GlobalXivoConnectorPool)
  extends UserValidatorFactoryInterface {

  def forAdmin(adminLogin: String): UserValidator =
    adminManager.getByLogin(adminLogin) match {
      case Success(admin) =>
        new UserValidator(entity, entity.listForAdmin(admin), route, templateManager, adminManager, cachedXivoUserManager, globalXivoConnectorPool)
      case Failure(f) =>
        throw new Exception("Unable to get admin details")
    }
}

class UserValidator(entities: EntityInterface, validEntities: List[Entity], routes: RouteInterface,
                    templates: TemplateManager, adminManager: UIAdministratorManager,
                    cachedXivoUserManager: CachedXivoUserManager, defaultlXivoConnectorPool: XivoConnectorPool) {
  import UserValidator._

  var connectorPool = defaultlXivoConnectorPool
  val logger = LoggerFactory.getLogger(getClass)

  private def convertToInt(o: Option[String], typed: String, fieldName: String) = {
    try {
      o.map(e=>e.toInt)
    } catch {
      case e: java.lang.NumberFormatException =>
        throw new InvalidParameterException(ErrorWrongFieldFormat(fieldName, typed, o))
    }
  }

  def fromMap(map: Map[String, String]): User = (
    map.get("entity.displayName"),
    map.get("firstName"),
    map.get("lastName"),
    map.get("internalNumber"),
    map.get("externalNumber"),
    map.get("mail"),
    map.get("ctiLogin"),
    map.get("ctiPassword"),
    map.get("intervalId"),
    map.get("templateId"),
    map.get("ringingTime"),
    map.get("peerSipName"),
    map.get("callerIdMode"),
    map.get("customCallerId")) match {
      case (Some(displayName), Some(firstName), Some(lastName), Some(internalNum), extNum, mail, ctiLogin, ctiPwd,
            intervalId, Some(templateId), Some(ringingTime), Some(peerSipName), Some(callerIdMode), Some(customCallerId)) => entities.getByDisplayName(displayName)
        match {
          case None =>
            throw new InvalidParameterException(ErrorNoSuchEntity.format(displayName))
          case Some(e) =>
            User(None,
              e, firstName, lastName, internalNum, extNum, mail.filter(_.nonEmpty), ctiLogin, ctiPwd, None,
              convertToInt(intervalId, "integer" , "intervalId"), convertToInt(Some(templateId), "integer" , "templateId"),
              convertToInt(Some(ringingTime), "integer" , "ringingTime"), PeerSipNameMode.enumValues.find(_._2 == peerSipName).map(_._1),
              CallerIdMode.enumValues.find(_._2 == callerIdMode).map(_._1), Option(customCallerId))
        }

    case other =>
      throw new InvalidParameterException(ErrorMissingParameters("entity.displayName", "firstName",
        "lastName", "internalNumber", "templateId"))

  }

  def fromJson(json: JsValue): User =
    parseFromJson(json) match {
      case (id, Some(entityCId), Some(firstName), Some(lastName), Some(internalNumber), externalNumber,
      mail, ctiLogin, ctiPassword, Some(intervalId), Some(templateId), voicemail, Some(ringingTime),Some(peerSipName),
      Some(callerIdMode), customCallerId) =>

        val entity = entities.getEntity(entityCId).get
        templates.get(templateId).getOrElse(ErrorNoSuchTemplate.format(templateId))
        val extNumber = entity.intervals.find(_.id.contains(intervalId)).flatMap { interval =>
          if (interval.routingMode == IntervalRoutingModes.RoutedWithCustomDirectNumber) {
            externalNumber
          } else {
            computeExtNumber(interval, internalNumber)
          }
        }

        User(id, entity, firstName, lastName, internalNumber, extNumber, mail.filter(_.nonEmpty), ctiLogin, ctiPassword, None,
          Some(intervalId), Some(templateId), Some(ringingTime), Some(peerSipName),
              Some(callerIdMode), customCallerId)

      case _ =>
        throw new InvalidParameterException(
          UserValidator.ErrorMissingParameters("entityCId", "firstName", "lastName", "internalNumber", "intervalId",
            "templateId", "ringingTime", "peerSipName", "callerIdMode"))
  }

  def fromProxyUserJson(json: JsValue): User =
    parseFromJson(json) match {
      case (id, Some(entityCId), Some(firstName), Some(lastName), Some(internalNumber), externalNumber,
      mail, ctiLogin, ctiPassword, Some(intervalId), templateId, voicemail, ringingTime,peerSipName,
      callerIdMode, customCallerId) =>

        val entity = entities.getEntity(entityCId).get
        val extNumber = entity.intervals.find(_.id.contains(intervalId)).flatMap { interval =>
          if (interval.routingMode == IntervalRoutingModes.RoutedWithCustomDirectNumber) {
            externalNumber
          } else {
            computeExtNumber(interval, internalNumber)
          }
        }

        User(id, entity, firstName, lastName, internalNumber, extNumber, mail.filter(_.nonEmpty), ctiLogin, ctiPassword, None,
          Some(intervalId), templateId, ringingTime, peerSipName,
          callerIdMode, customCallerId)

      case _ =>
        throw new InvalidParameterException(
          UserValidator.ErrorMissingParameters("entityCId", "firstName", "lastName", "internalNumber", "intervalId",
            "templateId", "ringingTime", "peerSipName", "callerIdMode"))
    }

  def fromJsonForUpdate(json: JsValue, oldUser: User): (User, Option[VoicemailSettings]) =
    parseFromJson(json) match {

      case (oldUser.id, Some(oldUser.entity.combinedId), Some(firstName), Some(lastName), Some(intNum), extNumOpt,
      mail, ctiLogin, ctiPwd, intervalId, templateId, voicemail, ringingTime, peerSipName, callerIdMode, customCallerId) =>

        val extNumber = for {
          intervalId <- intervalId
          interval <- oldUser.entity.intervals.find(_.id.contains(intervalId))
          extNum <- extNumOpt
          computedExtNumber <- computeExtNumber(interval, intNum)
        } yield {
          if (interval.routingMode == IntervalRoutingModes.RoutedWithCustomDirectNumber) {
            extNum
          } else {
            computedExtNumber
          }
        }

        val extNumber2 = if(extNumber.isDefined) extNumber else extNumOpt

        (
          oldUser.copy(firstName = firstName, lastName = lastName, ctiLogin = ctiLogin, ctiPassword = ctiPwd,
          externalNumber = extNumber2, mail = mail.filter(_.nonEmpty), ringingTime = ringingTime,
          peerSipName = peerSipName, callerIdMode = callerIdMode, customCallerId = customCallerId, internalNumber = intNum,
            intervalId = intervalId),
          voicemail
        )

      case _ =>
        throw new InvalidParameterException(UserValidator.ErrorMissingParameters("id", "entityId", "firstName"))
    }

  private def parseFromJson(json: JsValue) = (
    (json \ "id").asOpt[Long],
    (json \ "entityCId").asOpt[CombinedId],
    (json \ "firstName").asOpt[String],
    (json \ "lastName").asOpt[String],
    (json \ "internalNumber").asOpt[String],
    (json \ "externalNumber").asOpt[String],
    (json \ "mail").asOpt[String],
    (json \ "ctiLogin").asOpt[String].filter(_.trim.nonEmpty),
    (json \ "ctiPassword").asOpt[String].filter(_.trim.nonEmpty),
    (json \ "intervalId").asOpt[Int],
    (json \ "templateId").asOpt[Int],
    (json \ "voicemail").asOpt[VoicemailSettings],
    (json \ "ringingTime").asOpt[Int],
    (json \ "peerSipName").asOpt[PeerSipNameMode.Value],
    (json \ "callerIdMode").asOpt[CallerIdMode.Value],
    (json \ "customCallerId").asOpt[String].filter(_.trim.nonEmpty)
  )

  def validateCreation(user: User, checkInXivo: Boolean) {
    validateInterval(user)
    if (checkInXivo) {
      validateExtensionNotUsed(user)
      validateSlots(user)
    }
    validateRights(user)
    validateExternalNumber(user)
  }

  private def validateInterval(user: User) {
    val eligibleIntervals = user.entity.intervals.filter(i => i.start.length == user.internalNumber.length)
    val matchingIntervals = eligibleIntervals.filter(i => i.start <= user.internalNumber && i.end >= user.internalNumber)
    if(matchingIntervals.isEmpty)
      throw new InvalidParameterException(UserValidator.ErrorNumberOutOfIntervals(user.entity.intervals))
  }

  private def validateExtensionNotUsed(user: User) {
    connectorPool.withConnector(user.entity.xivo)(connector => {
      if (connector.extensionExists(user.internalNumber, user.entity.name))
        throw new InvalidParameterException(UserValidator.ErrorExtensionExists.format(user.internalNumber, user.entity.name))
    })
  }

  private def validateSlots(user: User) = user.entity.xivo.remainingSlots.foreach(n => if(n <= 0) throw new InvalidParameterException(UserValidator.ErrorNoSlot))

  private def validateRights(user: User) = {
    if(!validEntities.map(entity => entity.combinedId).contains(user.entity.combinedId)) throw new InvalidParameterException(ErrorInsufficientRights)
  }

  def validateUpdate(oldUser: User, newUser: User) {
    validateIdNotChanged(oldUser, newUser)
    validateEntityNotChanged(oldUser, newUser)
    if(!oldUser.internalNumber.equals(newUser.internalNumber)) {
      validateInterval(newUser)
      validateExtensionNotUsed(newUser)
    }
    if(oldUser.externalNumber != newUser.externalNumber) {
      validateExternalNumber(newUser)
    }
  }

  private def validateIdNotChanged(oldUser: User, newUser: User) = if(!oldUser.id.equals(newUser.id)) throw new InvalidParameterException(ErrorIdModification)

  private def validateEntityNotChanged(oldUser: User, newUser: User) = if(!oldUser.entity.id.equals(newUser.entity.id))
    throw new InvalidParameterException(ErrorEntityModification)

  private def validateExternalNumber(user: User): Unit =
    for {
      extNum <- user.externalNumber
      errMsg <- checkExternalNumber(user.intervalId, user.entity.intervals, extNum, user.id, None)
    } {
      throw new InvalidParameterException(errMsg)
    }

  def checkExternalNumber(intervalIdOpt: Option[Int], intervals: List[Interval], externalNumber: String,
                          userIdToIgnore: Option[Long], routeIdToIgnore: Option[Long]): Option[String] = {
    val conflictingRoute = routes.findRouteByDigits(externalNumber)
      .filter(_.context != Route.targetDisabled)
      .filter(r => routeIdToIgnore.isEmpty || r.id.isEmpty || r.id != routeIdToIgnore)

    if (conflictingRoute.nonEmpty) {
      Some(ExternalNumberIsAlreadyUsed(externalNumber))
    } else {
      val errorMsg = for {
        intervalId <- intervalIdOpt
        interval <- intervals.find(_.id.contains(intervalId))
        if interval.routingMode == IntervalRoutingModes.RoutedWithCustomDirectNumber
        countExtNum <- cachedXivoUserManager.countForExternalNumber(externalNumber, userIdToIgnore).toOption
      } yield if (countExtNum > 0) {
        ExternalNumberIsAlreadyUsed(externalNumber)
      } else if (!extNumInRangeOfIntervalRoutedWithCustDirNum(externalNumber, interval)) {
        ExternalNumberOutOfRange(externalNumber, interval)
      } else {
        ""
      }

      errorMsg.filterNot(_.isEmpty)
    }
  }

}

object UserValidator {
  val ErrorExtNumExists = "Le numéro externe %s est déjà utilisé"
  val ErrorInsufficientRights = "Vous ne disposez pas des droits nécessaires pour créer cet utilisateur"
  val ErrorNoSlot = "Il n'y a plus de place sur le xivo"
  val ErrorEntityModification = "Il est interdit de modifier l'entité"
  val ErrorIdModification = "Il est interdit de modifier l'id"
  val ErrorNoSuchEntity = "L'entité %s n'existe pas"
  val ErrorNoSuchTemplate = "Le template avec ID %s n'existe pas"
  def ErrorMissingParameters(p: String *) = s"Les paramètres ${p.mkString(",")} sont obligatoires"
  def ErrorNumberOutOfIntervals(intervals: List[Interval]) =
    "Le numéro interne doit être compris dans un des intervales suivants : " +
    intervals.map(i => i.start + " - " + i.end).mkString(",")
  val ErrorExtensionExists = "Le numéro %s existe déjà dans l'entité %s"
  def ErrorWrongFieldFormat(field: String, typed: String, value: Any) =
    s"La value($value) dans le champ $field doit être de type $typed"
  def ExternalNumberOutOfRange(extNum: String, i: Interval) =
    s"""External number $extNum is not in range ${i.startExtNum.getOrElse("")} - ${i.endExtNum.getOrElse("")}"""
  def ExternalNumberIsAlreadyUsed(extNum: String) = s"External number $extNum is already used"


  def computeExtNumber(interval: Interval, internalNumber: String): Option[String] = interval.routingMode match {
    case IntervalRoutingModes.Routed => Some(internalNumber)
    case IntervalRoutingModes.RoutedWithDirectNumber =>
      interval.directNumber.map(_ + internalNumber)
    case IntervalRoutingModes.RoutedWithCustomDirectNumber =>
      interval.directNumber.map { dn =>
        val directNumber = if (dn == "") 0L else dn.toLong
        val num = directNumber + internalNumber.toLong - interval.start.toLong
        ("%0" + dn.length + "d").format(num)
      }
  }
}

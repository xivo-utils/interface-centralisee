package model.validators

import java.security.InvalidParameterException
import javax.inject.{Inject, Singleton}

import model._
import model.utils.{ConfigParser, GlobalXivoConnectorPool, XivoConnectorPool, XivoConnectorProvider}
import play.api.{Configuration, Logger}
import play.api.libs.json.JsValue

trait EntityValidatorInterface {
  def validateUpdate(oldEntity: Entity, newEntity: Entity)
  def fromJson(json: JsValue): Entity
  def validateCreation(e: Entity, forProxy: Boolean)
}

object EntityValidator extends Validator {
  val ErrorNoInterval = "Renseigner au moins un intervalle"
  val ErrorIntervalInclusion = "Le nouvel intervalle doit inclure l'ancien"
  val ErrorNameChanged = "Il est interdit de modifier le nom de l'entité"
  val ErrorXivoChanged = "Il est interdit de modifier le xivo de rattachement"
  val ErrorContextExists = "Il existe déjà un contexte %s"
  val ErrorIntervalOverlap = "L'intervalle de numéros recouvre un intervalle existant"
  val ErrorNameRegexp = "Le nom de l'entité doit être composé uniquement de lettres minuscules et de chiffres, et ne doit pas dépasser 30 caractères"
  val ErrorUniqueNames = "Le nom ou le nom affiché existent déjà"
  val ErrorNoSuchXivo = "Le xivo %s n'existe pas"
}

@Singleton
class EntityValidator @Inject()(entities: EntityInterface, xivoManager: XivoInterface, config: ConfigParser, connectorPool: GlobalXivoConnectorPool)
  extends EntityValidatorInterface {

  import EntityValidator._

  override def fromJson(value: JsValue): Entity = ((value \ "id").asOpt[Long],
    (value \ "name").asOpt[String],
    (value \ "displayName").asOpt[String],
    (value \ "xivoId").asOpt[Long],
    (value \ "intervals").asOpt[List[Interval]],
    (value \ "presentedNumber").asOpt[String],
    (value \ "mdsName").asOpt[String]) match {
    case (id, Some(name), Some(dName), Some(xId), Some(intervals), Some(pNum), Some(mdsName)) => xivoManager.getXivo(xId) match {
      case Some(theXivo) => Entity(id, CombinedId(theXivo.uuid, name), name, dName, theXivo, intervals, pNum, mdsName)
      case None => throw new InvalidParameterException(ErrorNoSuchXivo.format(xId))
    }
    case _ => throw new InvalidParameterException(ErrorRequiredFields("name", "displayName", "xivoId", "intervals", "presentedNumber", "mdsName"))
  }

  override def validateCreation(entity: Entity, forProxy: Boolean = false) {
    validateOverlap(entity, entities.listFromDb.flatMap(_.intervals))
    if (!forProxy) {
      validateNamesUniqueness(entity)
      validateContext(entity)
    }
  }

  def validateOverlap(newEntity: Entity, otherIntervals: List[Interval]) = {
    if(!config.allowIntervalOverlap) {
      validateIntervalsOverlap(newEntity.intervals)
      for(i <- newEntity.intervals) validateIntervalOverlap(i, otherIntervals)
    }
  }

  def validateIntervalsOverlap(intervals: List[Interval]): Unit = {
    Logger.debug(s"Validating intervals overlap: $intervals")
    intervals match {
      case List() =>
      case head::tail =>
        for(i <- tail) {
          if(head.overlaps(i)) {
            throw new InvalidParameterException(ErrorIntervalOverlap)
          }
        }
        Logger.debug("overlap")
        if (tail.length > 0) { validateIntervalsOverlap(tail) }
    }
  }

  def validateIntervalOverlap(i: Interval, otherIntervals: List[Interval]): Unit = {
    Logger.error(new Exception().getStackTrace.mkString("\n"))
    Logger.debug("overlap" + i + ", " + otherIntervals)
    for(otherInterval <- otherIntervals) {
      if(i.overlaps(otherInterval))
        throw new InvalidParameterException(ErrorIntervalOverlap)
    }
  }

  def validateNamesUniqueness(e: Entity) {
    for(otherEntity <- entities.listFromDb) {
      if(otherEntity.name.equals(e.name) || otherEntity.displayName.equals(e.displayName))
        throw new InvalidParameterException(ErrorUniqueNames)
    }
  }

  def validateContext(e: Entity): Unit = {
    connectorPool.withConnector(e.xivo)(c => if(c.contextExists(e.name)) throw new InvalidParameterException(ErrorContextExists.format(e.name)))
  }

  override def validateUpdate(oldEntity: Entity, newEntity: Entity): Unit = {
    Logger.debug("Validating update:" + oldEntity + newEntity)
    validateParametersNotChanged(oldEntity, newEntity)
    validateOverlap(newEntity, entities.allBut(oldEntity.combinedId).flatMap(_.intervals))
    validateIntervalInclusion(oldEntity, newEntity)
    validateDisplayNameUniqueness(oldEntity, newEntity)
  }

  def validateParametersNotChanged(oldEntity: Entity, newEntity: Entity): Unit = {
    if(oldEntity.xivo.id.get != newEntity.xivo.id.get) {
      Logger.warn("Rejecting entity update because of xivo changed")
      throw new InvalidParameterException(ErrorXivoChanged)
    }
    if(!oldEntity.name.equals(newEntity.name)) {
      Logger.warn("Rejecting entity update because of name changed")
      throw new InvalidParameterException(ErrorNameChanged)
    }
  }

  def validateIntervalInclusion(oldEntity: Entity, newEntity: Entity): Unit = {
    var isOk = true
    for(oldI <- oldEntity.intervals) {
      isOk &= newEntity.intervals.filter(newI => newI.includes(oldI)).nonEmpty
    }
    if(!isOk) {
      Logger.warn("Rejecting entity update because of ErrorIntervalInclusion")
      throw new InvalidParameterException(ErrorIntervalInclusion)
    }
  }

  def validateDisplayNameUniqueness(oldEntity: Entity, newEntity: Entity): Unit = {
    if(!oldEntity.displayName.equals(newEntity.displayName) && entities.getByDisplayName(newEntity.displayName).isDefined)
      throw new InvalidParameterException(ErrorUniqueNames)
  }
}

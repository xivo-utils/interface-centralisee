package model.validators

trait Validator {
  def ErrorRequiredFields(fields: String *) = s"Les champs ${fields.mkString(",")} sont obligatoires"
  val ErrorNoSuchRole = "Le role %s n'existe pas"

}

package model

import play.api.libs.functional.syntax._
import play.api.libs.json.Reads.maxLength
import play.api.libs.json._

case class TemplateProperties(
                     id: Option[Long],
                     name: String,
                     peerSipName: PeerSipNameMode.Type,
                     callerIdMode: CallerIdMode.Type,
                     customCallerId: Option[String],
                     ringingTime: Int,
                     voiceMailEnabled: Boolean,
                     voiceMailNumberMode: Option[VoiceMailNumberMode.Type],
                     voiceMailCustomNumber: Option[String],
                     voiceMailSendEmail: Boolean,
                     voiceMailDeleteAfterNotif: Boolean
                   ) {
  import TemplateProperties._
  require(name.length <= NameMaxLength, "name too long")
  require(customCallerId.forall(_.length <= CustomCallerIdMaxLength), "customCallerId too long")
  require(voiceMailCustomNumber.forall(_.length <= VoiceMailCustomNumberMaxLength), "voiceMailCustomNumber too long")

  def voicemailSettings: VoicemailSettings =
    if (!voiceMailEnabled) VoicemailSettings.disabled
    else VoicemailSettings(voiceMailEnabled, voiceMailNumberMode, voiceMailCustomNumber, Some(voiceMailSendEmail), Some(voiceMailDeleteAfterNotif))
}

object TemplateProperties {
  val NameMaxLength = 50
  val CustomCallerIdMaxLength = 20
  val VoiceMailCustomNumberMaxLength = 20

  implicit val writes = Json.writes[TemplateProperties]
}

abstract class TemplateElementWithId[T](templateId: Option[Long]) {
  def getElementId: T
  def getTemplateId: Option[Long] = templateId
}

case class TemplateXivo(xivoId: Long, templateId: Option[Long]) extends TemplateElementWithId[Long](templateId) {
  def getElementId: Long = xivoId
}

case class TemplateEntity(entityCombinedId: CombinedId, templateId: Option[Long]) extends TemplateElementWithId[CombinedId](templateId) {
  def getElementId: CombinedId = entityCombinedId
}

case class TemplateInterval(intervalId: Int, templateId: Option[Long]) extends TemplateElementWithId[Int](templateId) {
  def getElementId: Int = intervalId
}

case class Template(properties: TemplateProperties, xivos: List[TemplateXivo], entities: List[TemplateEntity], intervals: List[TemplateInterval])

object Template {
  import TemplateProperties._

  implicit val reads: Reads[Template] = (
    (JsPath \ "id").readNullable[Long] and
    (JsPath \ "name").read[String](maxLength[String](NameMaxLength)) and
    (JsPath \ "peerSipName").read[PeerSipNameMode.Type] and
    (JsPath \ "callerIdMode").read[CallerIdMode.Type] and
    (JsPath \ "customCallerId").readNullable[String](maxLength[String](CustomCallerIdMaxLength)) and
    (JsPath \ "ringingTime").read[Int] and
    (JsPath \ "voiceMailEnabled").read[Boolean] and
    (JsPath \ "voiceMailNumberMode").readNullable[VoiceMailNumberMode.Type] and
    (JsPath \ "voiceMailCustomNumber").readNullable[String](maxLength[String](VoiceMailCustomNumberMaxLength)) and
    (JsPath \ "voiceMailSendEmail").readNullable[Boolean].map(_.getOrElse(false)) and
    (JsPath \ "voiceMailDeleteAfterNotif").readNullable[Boolean].map(_.getOrElse(false)) and
    (JsPath \ "xivos").read[List[Long]].map(xivosList => xivosList.map(xivoId => TemplateXivo(xivoId, None))) and
    (JsPath \ "entities").read[List[CombinedId]].map(combinedIdList => combinedIdList.map(combinedId => TemplateEntity(combinedId, None))) and
    (JsPath \ "intervals").read[List[Int]].map(intervalIdList => intervalIdList.map(intervalId => TemplateInterval(intervalId, None)))
  )(Template.fromJson _)

  def fromJson(id: Option[Long],
               name: String,
               peerSipName: PeerSipNameMode.Type,
               callerIdMode: CallerIdMode.Type,
               customCallerId: Option[String],
               ringingTime: Int,
               voiceMailEnabled: Boolean,
               voiceMailNumberMode: Option[VoiceMailNumberMode.Type],
               voiceMailCustomNumber: Option[String],
               voiceMailSendEmail: Boolean,
               voiceMailDeleteAfterNotif: Boolean,
               xivos: List[TemplateXivo],
               entities: List[TemplateEntity],
               intervals: List[TemplateInterval]): Template = {
    def getVoiceMailSendEmail = if (voiceMailEnabled) voiceMailSendEmail else false
    def getVoiceMailDeleteAfterNotif = if (getVoiceMailSendEmail) voiceMailDeleteAfterNotif else false

    Template(TemplateProperties(id, name, peerSipName, callerIdMode, customCallerId, ringingTime,
      voiceMailEnabled, voiceMailNumberMode, voiceMailCustomNumber, getVoiceMailSendEmail, getVoiceMailDeleteAfterNotif),
      xivos.map(x => x.copy(templateId = id)),
      entities.map(e => e.copy(templateId = id)),
      intervals.map(i => i.copy(templateId = id))
    )
  }

  implicit val locationWrites: Writes[Template] = new Writes[Template] {
    def writes(tpl: Template) =
      TemplateProperties.writes.writes(tpl.properties) ++
      Json.obj(
        "xivos" -> JsArray(tpl.xivos.map {
          case TemplateXivo(xivoId, _) => JsNumber(xivoId)
        }),
        "entities" -> JsArray(tpl.entities.map {
          case TemplateEntity(entityCombinedId, _) => JsString(entityCombinedId.toString())
        }),
        "intervals" -> JsArray(tpl.intervals.map {
          case TemplateInterval(entityIntervalId, _) => JsNumber(entityIntervalId)
        })
      )
  }
}

package model

import play.api.libs.json.{JsObject, Json, Writes}

case class MediaServer(id: Long, name: String, displayName: String, voipIp: String)


object MediaServer {
  implicit val mediaServerWrites: Writes[MediaServer] = new Writes[MediaServer] {
    def writes(mds: MediaServer): JsObject = Json.obj(
      "id" -> mds.id,
      "name"  -> mds.name,
      "displayName"  -> mds.displayName,
      "voipIp"  -> mds.voipIp
    )
  }

}

package model

import java.util.UUID

import play.api.libs.json._
import play.api.mvc.{PathBindable, QueryStringBindable}
import anorm._
import java.sql.PreparedStatement

import anorm.ToStatement


case class CombinedId(xivoUuid: UUID, contextName: String) {
  override def toString: String = {
    s"""$contextName@${xivoUuid.toString()}"""
  }
}

object CombinedId {
  implicit val combinedIdWrites = new Writes[CombinedId] {
    def writes(combinedId: CombinedId) = JsString(combinedId.toString())
  }
  implicit val combinedIdReads = new Reads[CombinedId] {
    def reads(string: JsValue) = string match {
      case s: JsString => JsSuccess(extractCombinedId(s.value))
      case _ => JsError("Expected string")
    }
  }

  implicit def rowToCombinedId: Column[CombinedId] = Column.nonNull { (value, meta) =>
    val MetaDataItem(qualified, nullable, clazz) = meta
    value match {
      case s: String => {
        try {
          Right(extractCombinedId(s))
        } catch {
          case e: Exception =>
            Left(TypeDoesNotMatch("Cannot convert " + value + ":" + value.asInstanceOf[AnyRef].getClass + " to CombinedId for column " + qualified))
        }
      }
      case _ => Left(TypeDoesNotMatch("Cannot convert " + value + ":" + value.asInstanceOf[AnyRef].getClass + " to CombinedId for column " + qualified))
    }
  }

  implicit def combinedIdToStatement: ToStatement[CombinedId] = new ToStatement[CombinedId] {
    def set(s: PreparedStatement, i: Int, cid: CombinedId): Unit =
      s.setObject(i, cid.toString, java.sql.Types.VARCHAR)
  }

  implicit def pathBinder(implicit stringBinder: PathBindable[String]) = new PathBindable[CombinedId] {
    override def bind(key: String, value: String): Either[String, CombinedId] = {
      for {
        s <- stringBinder.bind(key, value).right
      } yield extractCombinedId(s)
    }
    override def unbind(key: String, value: CombinedId): String = value.toString()
  }

  implicit def queryStringBindable(implicit stringBinder: QueryStringBindable[String]) = new QueryStringBindable[CombinedId] {
    override def bind(key: String, params: Map[String, Seq[String]]): Option[Either[String, CombinedId]] = {
      for {
        s <- stringBinder.bind("entityId", params)
      } yield {
        s match {
          case Right(s) => Right(extractCombinedId(s))
          case _ => Left("Unable to bind CombinedId")
        }
      }
    }
    override def unbind(key: String, value: CombinedId): String = stringBinder.unbind("entityId", value.toString())
  }

  private[model] def extractCombinedId(s: String): CombinedId = {
    val atSignPosition = s.indexOf("@")
    val contextName = s.substring(0, atSignPosition)
    val xivo = s.substring(atSignPosition + 1, s.length)
    CombinedId(UUID.fromString(xivo), contextName)

  }
}

package model

import java.security.InvalidParameterException
import java.sql.Connection
import javax.inject.Inject

import com.google.inject.{ImplementedBy, Singleton}
import anorm.SqlParser._
import model.utils._
import anorm._
import model.validators.{EntityValidator, UserValidator}
import org.slf4j.LoggerFactory
import play.api.db._
import play.api.libs.functional.syntax._
import play.api.libs.json._
import services.{AvailableNumbersService, UserFinder, UserManager}
import services.rights.{OperationManager, PermissionManager, RolePermissionManager}
import xivo.restapi.model.{Context, ContextInterval}
import EntityInterface._

import scala.collection.JavaConversions._
import scala.collection.mutable.ListBuffer
import scala.concurrent.Future
import scala.util.{Failure, Success, Try}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.control.NonFatal

case class Entity(id: Option[Long], combinedId: CombinedId, name: String, displayName: String, xivo: Xivo, intervals: List[Interval], presentedNumber: String, mdsName: String) {

  import Entity._

  require(name.matches(NameRegex), EntityValidator.ErrorNameRegexp)
  require(displayName.length <= DisplayNameMaxLength, "displayName too long")
  require(presentedNumber.length <= PresentedNumberMaxLength, "presentedNumber too long")
  require(intervals.nonEmpty, EntityValidator.ErrorNoInterval)
}

object Entity {
  val NameMinLength = 1
  val NameMaxLength = 30
  val NameRegex = s"[a-z0-9-_]{$NameMinLength,$NameMaxLength}"
  val DisplayNameMaxLength = 128
  val PresentedNumberMaxLength = 40

  val ErrorNoSuchEntity = "L'entité %s n'existe pas"

  implicit val writes = ((__ \ "id").write[Option[Long]] and
    (__ \ "combinedId").write[CombinedId] and
    (__ \ "name").write[String] and
    (__ \ "displayName").write[String] and
    (__ \ "xivo").write[Xivo] and
    (__ \ "intervals").write[List[Interval]] and
    (__ \ "presentedNumber").write[String] and
    (__ \ "mdsName").write[String]) (unlift(Entity.unapply))
}

case class XivoContext(xivo: Xivo, context: Context)

@ImplementedBy(classOf[EntityImpl])
trait EntityInterface {
  val defaultByValue = "__application"

  def allBut(cId: CombinedId): List[Entity]

  def update(oldEntity: Entity, newEntity: Entity, by: String = defaultByValue, configureXiVO: Boolean): UpdateResult

  def delete(cId: CombinedId, by: String = defaultByValue)

  def create(e: Entity, configureXiVO: Boolean): Entity

  def list: List[Entity]

  def listFromDb: List[Entity]

  def getEntity(id: Long): Option[Entity]

  def getEntity(cId: CombinedId): Option[Entity]

  def getByDisplayName(displayName: String): Option[Entity]

  def availableNumbers(entityCId: CombinedId, intervalId: Option[Int]): List[String]

  def externalNumber(cId: CombinedId, intNum: String): Option[String]

  def listForAdmin(admin: UIAdministrator, updateFromXivo: Boolean = true): List[Entity]
}

object EntityInterface {
  final case class UpdateResult(entity: Entity, success: Boolean, errorMessage: String = "")
}

@Singleton
class EntityImpl @Inject()(users: UserManager, routes: RouteInterface, xivoManager: XivoInterface,
                           rolePermissionManager: RolePermissionManager, permissionManager: PermissionManager,
                           operationManager: OperationManager, @NamedDatabase("default") db: Database,
                           availableNumbersService: AvailableNumbersService, globalXivoConnectorPool: GlobalXivoConnectorPool,
                           userFinder: UserFinder)
  extends EntityInterface {
  val logger = LoggerFactory.getLogger(getClass)
  var connectorPool: XivoConnectorPool = globalXivoConnectorPool

  val SelectEntity =
    """
      |SELECT
      |  e.id,
      |  e.combined_id,
      |  e.name,
      |  e.display_name,
      |  e.xivo_id,
      |  e.mds_name,
      |  array_agg(ei.id) as interval_id,
      |  array_agg(ei.start) AS starts,
      |  array_agg(ei.end) AS ends,
      |  array_agg(ei.interval_routing_mode) as interval_routing_mode,
      |  array_agg(ei.direct_number) as direct_number,
      |  array_agg(ei.label) as label,
      |  e.presented_number
      |FROM entities e
      |JOIN entity_intervals ei ON e.id = ei.entity_id
      |""".stripMargin

  val GroupByEntity = " GROUP BY e.id, e.combined_id, e.name, e.display_name, e.xivo_id, e.presented_number "

  override def getByDisplayName(displayName: String): Option[Entity] = db.withConnection(implicit c => {
    SQL(SelectEntity + "WHERE e.display_name = {dName}" + GroupByEntity)
      .on('dName -> displayName).as(simple *).headOption
  })

  override def getEntity(id: Long): Option[Entity] = db.withConnection(implicit c => {
    SQL(SelectEntity + "WHERE e.id = {id} " + GroupByEntity)
      .on('id -> id).as(simple *).headOption
  })

  override def getEntity(cId: CombinedId): Option[Entity] = db.withConnection(implicit c => {
    SQL(SelectEntity + "WHERE e.combined_id = {combinedId} " + GroupByEntity)
      .on('combinedId -> cId).as(simple *).headOption
  })

  val simple =
    get[Option[Long]]("id") ~
      get[CombinedId]("combined_id") ~
      get[String]("name") ~
      get[String]("display_name") ~
      get[String]("mds_name") ~
      get[List[Option[Int]]]("interval_id") ~
      get[List[Option[String]]]("starts") ~
      get[List[Option[String]]]("ends") ~
      get[List[Option[String]]]("interval_routing_mode") ~
      get[List[Option[String]]]("direct_number") ~
      get[List[Option[String]]]("label") ~
      get[Long]("xivo_id") ~
      get[String]("presented_number") map {
      case id ~ combinedId ~ name ~ displayName ~ mdsName ~ interval_id ~ starts ~ ends ~ routingMode ~ prefix ~ label ~
        xivoId ~ presentedNumber =>
        Entity(id, combinedId, name, displayName, xivoManager.getXivo(xivoId).get,
          toInterval(interval_id, starts, ends, routingMode, prefix, label), presentedNumber, mdsName)
    }

  def toInterval(id: List[Option[Int]], start: List[Option[String]], end: List[Option[String]], routingMode: List[Option[String]],
                 prefix: List[Option[String]], label: List[Option[String]]): List[Interval] = {
    ((0 until id.length) map { i: Int =>
      Interval(id(i), start(i).get, end(i).get,
        IntervalRoutingModes.fromString(routingMode(i).get).getOrElse(IntervalRoutingModes.Routed), prefix(i),
        label(i).getOrElse(""))
    }).toList
  }

  override def create(e: Entity, configureXivo: Boolean = true): Entity = db.withConnection(implicit c => {
    logger.info(s"Starting creation of entity $e")
    val combinedId = CombinedId(e.xivo.uuid, e.name)
    val transaction = new AutoRollbackTransaction {
      var genId: Option[Long] = None
      var intervalsWithIds: ListBuffer[Interval] = new ListBuffer()

      step(executeFun = genId = SQL("INSERT INTO entities(combined_id, name, display_name, xivo_id, presented_number, mds_name) VALUES ({combinedId}, {name}, {displayName}, {xivoId}, {presNum}, {mds_name})")
        .on('combinedId -> combinedId.toString, 'name -> e.name, 'displayName -> e.displayName, 'xivoId -> e.xivo.id, 'presNum -> e.presentedNumber, 'mds_name -> e.mdsName).executeInsert(),
        rollbackFun = SQL("DELETE FROM entities WHERE id = {id}").on('id -> genId).executeUpdate())

      step(executeFun = e.intervals.foreach(i => {
        val intervalId = SQL("INSERT INTO entity_intervals(entity_id, start, \"end\", interval_routing_mode, " +
          "direct_number, label) VALUES ({eId}, {start}, {end}, {routing}::interval_routing_modes, " +
          "{prefix}, {label})")
          .on('eId -> genId, 'start -> i.start, 'end -> i.end, 'routing -> IntervalRoutingModes.enumValues(i.routingMode),
            'prefix -> i.directNumber, 'label -> i.label).executeInsert()
        intervalsWithIds += i.copy(id = intervalId.map(_.toInt))
      }),
        rollbackFun = SQL("DELETE FROM entity_intervals WHERE entity_id = {eId}").on('eId -> genId).executeUpdate()
      )
      if (configureXivo) {
        step(executeFun = createContextForEntity(e),
          rollbackFun = connectorPool.withConnector(e.xivo)(c => c.deleteContext(e.name)))
      }
    }
    transaction.execute()
    val entityWithIds = e.copy(id = transaction.genId, intervals = transaction.intervalsWithIds.toList)
    logger.info("Entity creation done")
    logger.debug(s"Entity created: $entityWithIds")
    entityWithIds
  })

  private def createContextForEntity(e: Entity): Unit = {
    connectorPool.withConnector(e.xivo)(connector => {
      connector.createContext(entityToContext(e))
      connector.createContextInclusion("default", e.name, 0)
      connector.createContextInclusion(e.name, "default", 0)
    })
  }

  override def list: List[Entity] = {
    var contexts: ListBuffer[XivoContext] = ListBuffer.empty
    xivoManager.list.filter(! _.isProxy).foreach(x => {
      connectorPool.withConnector(x)(connector => {
        connector.listUserContexts().toList.foreach(context => {
          contexts += XivoContext(x, context)
        })
      })
    })
    val entities = convertContexts(contexts)
    logger.debug("Found entities: " + entities)
    persistEntities(entities)
    listFromDb
  }

  override def listFromDb: List[Entity] = db.withConnection(implicit c =>
    SQL(SelectEntity + GroupByEntity + "ORDER  BY e.name ASC")
      .as(simple.*)
  )

  private def persistEntities(entities: List[Entity]): Unit = {
    entities.foreach(e => {
      db.withConnection(implicit c => {
        if (!findInDb(e.combinedId)) {
          c.setAutoCommit(false)
          try {
            val genId = SQL("INSERT INTO entities(combined_id, name, display_name, xivo_id, presented_number, mds_name) VALUES ({combinedId}, {name}, {displayName}, {xivoId}, {presNum}, {mdsName})")
              .on('combinedId -> e.combinedId.toString, 'name -> e.name, 'displayName -> e.displayName, 'xivoId -> e.xivo.id, 'presNum -> e.presentedNumber, 'mdsName -> e.mdsName).executeInsert()

            e.intervals.foreach(i => SQL("INSERT INTO entity_intervals(entity_id, start, \"end\") VALUES ({eId}, {start}, {end})")
              .on('eId -> genId, 'start -> i.start, 'end -> i.end).executeUpdate())

            c.commit()
            c.setAutoCommit(true)
          }
          catch {
            case e: Exception => c.rollback()
              c.setAutoCommit(true)
              throw e
          }
        }
      })
    })
  }

  private def findInDb(cId: CombinedId)(implicit c: Connection): Boolean = {
    SQL("SELECT combined_id FROM entities WHERE combined_id = {id}")
      .on('id -> cId.toString)
      .as(SqlParser.get[CombinedId]("combined_id") *)
      .headOption.isDefined
  }

  private def convertContexts(contexts: ListBuffer[XivoContext]): List[Entity] = {
    contexts.map(xc =>
      Try(Entity(None, CombinedId(xc.xivo.uuid, xc.context.getName), xc.context.getName, xc.context.getName,
        xc.xivo, convertIntervals(xc.context.getIntervals.toList), "inbNo", "default")).toOption
    ).toList.flatten
  }

  private def convertIntervals(from: List[ContextInterval]): List[Interval] = {
    from.map(i => Interval(None, i.start, i.end))
  }

  override def availableNumbers(entityCId: CombinedId, intervalId: Option[Int]): List[String] = getEntity(entityCId) match {
    case None => throw new NoSuchElementException("Cette entité n'existe pas")
    case Some(e) =>
      val intervals = intervalId match {
        case None => e.intervals
        case Some(id) => e.intervals.filter(i => i.id.get == id)
      }
      availableNumbersService.findAvailableNumbers(intervals)
  }

  override def externalNumber(entityCId: CombinedId, internalNum: String): Option[String] =
    getEntity(entityCId) match {
      case None =>
        throw new NoSuchElementException("Cette entité n'existe pas")
      case Some(e) =>
        val intNumLong = internalNum.toLong
        val res = e.intervals
          .find(i => i.start.toLong <= intNumLong && i.end.toLong >= intNumLong)
          .flatMap(UserValidator.computeExtNumber(_, internalNum))
        res
    }

  override def listForAdmin(admin: UIAdministrator, updateFromXivo: Boolean = true): List[Entity] = {
    def listEntities =
      if (updateFromXivo) Try(list).getOrElse(listFromDb)
      else listFromDb

    if (admin.superAdmin) {
      listEntities
    } else {
      val permissions = admin.roles.flatMap(role => rolePermissionManager.getForRole(role.id.get).get)
        .map(permission => permissionManager.get(permission.permissionId).get)
        .filter(permission => operationManager.get(permission.operationId).get.reading)
      listEntities.filter(e => permissions.exists(p => p.entityId == e.id.get))
    }
  }

  override def delete(cId: CombinedId, by: String = defaultByValue): Unit = {
    getEntity(cId) match {
      case None => throw new InvalidParameterException(Entity.ErrorNoSuchEntity.format(cId))
      case Some(entity) =>
        logger.info(s"Deleting entity $entity")
        entity.id.foreach(entityId => {
          for (userId <- userFinder.listUserIdsForEntity(entityId)) {
            users.delete(userId, this, by, withConfiguration = true)
          }
          connectorPool.withConnector(entity.xivo)(c => c.deleteContext(entity.name))
          db.withConnection(implicit c => {
            SQL("DELETE FROM administrator_entity WHERE entity_id = {id}").on('id -> entityId).executeUpdate()
            SQL("DELETE FROM entity_intervals WHERE entity_id = {id}").on('id -> entityId).executeUpdate()
            SQL("DELETE FROM entities WHERE id = {id}").on('id -> entityId).executeUpdate()
          })
          logger.info("Entity deletion finished")
        })
    }

  }

  override def update(oldEntity: Entity, newEntity: Entity, by: String = defaultByValue, configureXivo: Boolean = true): UpdateResult = {

    val transaction = new AutoRollbackTransaction {
      var intervalsWithIds: ListBuffer[Interval] = new ListBuffer()

      if (configureXivo) {
        step(
          executeFun = {
            connectorPool.withConnector(oldEntity.xivo)(_.disableLiveReload())
          },
          rollbackFun = {}
        )
      }

      step(
        executeFun = {
          logger.info(s"Starting update of entity $oldEntity with $newEntity")

          db.withTransaction(implicit c => {
            SQL("UPDATE entities SET display_name = {dName}, presented_number = {presNum} WHERE id = {id}")
              .on('id -> oldEntity.id, 'dName -> newEntity.displayName, 'presNum -> newEntity.presentedNumber).executeUpdate()
            newEntity.intervals.foreach(i =>
              i.id match {
                case None =>
                  val intervalId = SQL("INSERT INTO entity_intervals(entity_id, start, \"end\", interval_routing_mode, " +
                    "direct_number, label) VALUES ({eId}, {start}, {end}, {routing}::interval_routing_modes, {prefix}, {label})")
                    .on('eId -> newEntity.id, 'start -> i.start, 'end -> i.end, 'routing -> IntervalRoutingModes.enumValues(i.routingMode),
                      'prefix -> i.directNumber, 'label -> i.label).executeInsert()
                  intervalsWithIds += i.copy(id = intervalId.map(_.toInt))
                case Some(id) =>
                  SQL("UPDATE entity_intervals SET start = {start}, \"end\" = {end}, interval_routing_mode = {routing}::interval_routing_modes, " +
                    "direct_number = {prefix}, label = {label} WHERE id = {id}").on(
                    'id -> id, 'start -> i.start, 'end -> i.end, 'routing -> IntervalRoutingModes.enumValues(i.routingMode),
                    'prefix -> i.directNumber, 'label -> i.label).executeUpdate()
                  intervalsWithIds += i
              }
            )
            logger.debug("update entity ok")
          })
        },
        rollbackFun = {
          val oldIntervalIds: Set[Int] = oldEntity.intervals.flatMap(_.id).toSet
          val newIntervalIds: Set[Int] = intervalsWithIds.flatMap(_.id).toSet

          db.withTransaction(implicit c => {

            (newIntervalIds diff oldIntervalIds).foreach { id =>
              SQL("DELETE FROM entity_intervals WHERE id = {id}").on('id -> id).executeUpdate()
            }

            SQL("UPDATE entities SET display_name = {dName}, presented_number = {presNum} WHERE id = {id}")
              .on('id -> oldEntity.id, 'dName -> oldEntity.displayName, 'presNum -> oldEntity.presentedNumber).executeUpdate()

            oldEntity.intervals.foreach(i =>
              i.id.map { id =>
                SQL("UPDATE entity_intervals SET start = {start}, \"end\" = {end}, interval_routing_mode = {routing}::interval_routing_modes, " +
                  "direct_number = {prefix}, label = {label} WHERE id = {id}").on(
                  'id -> id, 'start -> i.start, 'end -> i.end, 'routing -> IntervalRoutingModes.enumValues(i.routingMode),
                  'prefix -> i.directNumber, 'label -> i.label).executeUpdate()
              }
            )

          })

          if (configureXivo) {
            db.withConnection(implicit c => {
              connectorPool.withConnector(oldEntity.xivo) { c =>
                try {
                  c.updateContext(entityToContext(oldEntity))
                } catch {
                  case NonFatal(e) =>
                    logger.error("Exception during rollback of entity update: update context", e)
                }
              }
            })

          }
        }
      )

      step(
        executeFun = {
          if (configureXivo) {
            connectorPool.withConnector(newEntity.xivo)(c => c.updateContext(entityToContext(newEntity)))
          }
          logger.info("Entity update finished")
        },
        rollbackFun = {}
      )

    }

    val result = Try {
      transaction.execute()
    } match {
      case Success(s) =>
        logger.debug(s"Entity updated from $oldEntity, to $newEntity")
        UpdateResult(newEntity.copy(intervals = transaction.intervalsWithIds.toList), success = true)
      case Failure(e) =>
        logger.error(s"Failure - Entity update from $oldEntity, to $newEntity failed, rollback was attempted", e)
        UpdateResult(oldEntity, success = false, e.getMessage)
    }

    if (configureXivo) {
      Try {
        connectorPool.withConnector(oldEntity.xivo)(_.enableLiveReload())
      } match {
        case s: Success[_] =>
          result
        case Failure(e1) =>
          val errMsg = s"Enabling live reload for xivo=${oldEntity.xivo.uuid} failed"
          logger.warn(errMsg + ", will retry", e1)
          val newErrMsg = if (result.errorMessage.isEmpty) errMsg else result.errorMessage + " " + errMsg
          Future {
            connectorPool.withConnector(oldEntity.xivo)(_.enableLiveReload())
          }.onFailure { case e2 =>
            logger.error(s"Enabling live reload for xivo=${oldEntity.xivo.uuid} failed second time", e2)
          }
          result.copy(success = false, errorMessage = newErrMsg)
      }
    } else result
  }

  private def entityToContext(e: Entity): Context = {
    new Context(e.name, e.intervals.map(i => new ContextInterval(i.start, i.end, 0)))
  }

  override def allBut(cId: CombinedId): List[Entity] = {
    listFromDb.filterNot(e => e.combinedId == cId)
  }
}

package model.rights

import play.api.libs.json.Json

case class Role(id: Option[Long], name: String)

object Role {
  implicit val reads = Json.reads[Role]
  implicit val writes = Json.writes[Role]
}


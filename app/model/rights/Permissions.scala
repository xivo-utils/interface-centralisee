package model.rights

case class Permissions(
                        id: Option[Long],
                        entityId: Long,
                        operationId: Long,
                        name: String
                      )


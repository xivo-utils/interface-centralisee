package model.rights

import play.api.libs.json.{JsPath, Json, Reads}
import play.api.libs.json.Reads.{maxLength, minLength}
import play.api.libs.functional.syntax._

case class UIOperations(reading: Boolean = false, creating: Boolean= false, editing: Boolean= false,
                        deleting: Boolean= false)
object UIOperations {
  implicit val reads = Json.reads[UIOperations]
  implicit val writes = Json.writes[UIOperations]
}

case class UIEntityPermission(entityId: Long, entityName: String, operations: UIOperations)
object UIEntityPermission {
  implicit val reads = Json.reads[UIEntityPermission]
  implicit val writes = Json.writes[UIEntityPermission]
}

case class UIXivoPermission(xivoId: Long, xivoName: String, entities: List[UIEntityPermission])
object UIXivoPermission {
  implicit val reads = Json.reads[UIXivoPermission]
  implicit val writes = Json.writes[UIXivoPermission]
}

case class UIRole(id: Option[Long], name: Option[String], xivo: List[UIXivoPermission]) {
  require(name.forall(_.length <= UIRole.NameMaxLength), "name too long")
}

object UIRole {
  val NameMaxLength = 100

  implicit val reads: Reads[UIRole] = (
    (JsPath \ "id").readNullable[Long] and
    (JsPath \ "name").readNullable[String](maxLength[String](NameMaxLength)) and
    (JsPath \ "xivo").lazyRead(Reads.list[UIXivoPermission](UIXivoPermission.reads))
  ) (UIRole.apply _)

  implicit val writes = Json.writes[UIRole]
}
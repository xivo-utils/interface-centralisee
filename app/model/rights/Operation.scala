package model.rights

case class Operation(
                      id: Option[Long],
                      name: String,
                      reading: Boolean = false,
                      creating: Boolean = false,
                      editing: Boolean = false,
                      deleting: Boolean = false
                    )


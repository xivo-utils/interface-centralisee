package model.rights

case class RolePermission(
                           roleId: Long,
                           permissionId: Long
                         )


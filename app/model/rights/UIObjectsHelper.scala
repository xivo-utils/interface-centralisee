package model.rights

object UIObjectsHelper {

  def aggregateUIRoles(uiRoles: List[UIRole]): List[UIXivoPermission] = {
    val permissions: List[UIXivoPermission] = uiRoles.flatMap(_.xivo)
    val byXivo: Map[(Long, String), List[UIXivoPermission]] = permissions.groupBy(p => (p.xivoId, p.xivoName))
    byXivo.map {
      case ((xivoId: Long, xivoName: String), xivoPermissions: List[UIXivoPermission]) =>
        val entityPermissions: List[UIEntityPermission] = xivoPermissions.flatMap(_.entities)
        UIXivoPermission(xivoId, xivoName, aggregateEntityPermissions(entityPermissions))
    }.toList
  }

  def aggregateEntityPermissions(permissions: List[UIEntityPermission]): List[UIEntityPermission] = {
    val byEntity: Map[(Long, String), List[UIEntityPermission]] = permissions.groupBy(p => (p.entityId, p.entityName))
    byEntity.map {
      case ((entityId: Long, entityName: String), permissions: List[UIEntityPermission]) =>
        UIEntityPermission(entityId, entityName, permissions.foldLeft(UIOperations()) {
          (o: UIOperations, p: UIEntityPermission) => combineOperations(o, p.operations)
        })
    }.toList
  }

  def combineOperations(a: UIOperations, b: UIOperations): UIOperations = UIOperations(
    reading = a.reading || b.reading,
    creating = a.creating || b.creating,
    editing = a.editing || b.editing,
    deleting = a.deleting || b.deleting)

}


package model

import play.api.libs.json._
import xivo.restapi.model.{Device, IncomingCall, Voicemail, User => XivoUser}

case class VoicemailSettings(
  enabled: Boolean,
  numberMode: Option[VoiceMailNumberMode.Type],
  customNumber: Option[String],
  sendEmail: Option[Boolean],
  deleteAfterNotif : Option[Boolean]
) {
  def disabled = !enabled

  import VoicemailSettings._
  require(enabled == numberMode.nonEmpty, "numberMode must be non-empty if and only if voicemail is enabled")
  require(numberMode.forall(_ != VoiceMailNumberMode.Custom || customNumber.nonEmpty), "customNumber is required for Custom voice number mode")
  require(customNumber.forall(_.length <= CustomNumberMaxLength), "customNumber too long")
  require(enabled == sendEmail.nonEmpty, "sendEmail must be non-empty if and only if voicemail is enabled")
  require(deleteAfterNotif.nonEmpty == sendEmail.nonEmpty, "deleteAfterNotif must be non-empty if and only if voicemail and sendEmail are enabled")
}

object VoicemailSettings {
  val CustomNumberMaxLength = 20
  val disabled = VoicemailSettings(enabled = false, None, None, None, None)

  implicit val jsonReads = Reads[VoicemailSettings] {
    case jsObject: JsObject =>
      if ((jsObject \ "enabled").asOpt[Boolean].contains(false)) {
        JsSuccess(disabled)
      } else if ((jsObject \ "sendEmail").asOpt[Boolean].isEmpty) {
        Json.reads[VoicemailSettings].reads(jsObject + ("sendEmail" -> JsBoolean(false)) + ("deleteAfterNotif" -> JsBoolean(false)))
      } else {
        Json.reads[VoicemailSettings].reads(jsObject)
      }
    case jsValue: JsValue =>
      Json.reads[VoicemailSettings].reads(jsValue)
  }

  implicit val jsonWrites = Json.writes[VoicemailSettings]

  def fromXivoUser(xivoUser: XivoUser): VoicemailSettings =
    if (xivoUser.getVoicemail == null) {
      disabled
    } else {
      val voicemail: Voicemail = xivoUser.getVoicemail
      val voicemailNumber = Option(voicemail.getNumber).getOrElse("")
      val userNumber = Option(xivoUser.getLine).map(_.getNumber).filter(_.nonEmpty)
      val sendEmail = Some(Option(voicemail.getAttach).exists(_.booleanValue))
      val deleteAfterNotif = Some(Option(voicemail.getDeleteMessages).exists(_.booleanValue) &&
        Option(voicemail.getAttach).exists(_.booleanValue))

      if (userNumber.contains(voicemailNumber)) {
        VoicemailSettings(enabled = true, Some(VoiceMailNumberMode.ShortNumber), None, sendEmail, deleteAfterNotif)
      } else {
        VoicemailSettings(enabled = true, Some(VoiceMailNumberMode.Custom), Some(voicemailNumber), sendEmail, deleteAfterNotif)
      }
    }
}
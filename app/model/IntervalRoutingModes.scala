package model

import play.api.libs.json.{Writes, _}

object IntervalRoutingModes extends Enumeration {
  type Type = Value

  val Routed, RoutedWithDirectNumber, RoutedWithCustomDirectNumber = Value

  val enumValues: Map[IntervalRoutingModes.Type, String] = Map(
    IntervalRoutingModes.Routed -> "routed",
    IntervalRoutingModes.RoutedWithDirectNumber -> "with_direct_number",
    IntervalRoutingModes.RoutedWithCustomDirectNumber -> "with_customized_direct_number")

  lazy val listEnumValues = enumValues.values.mkString("/")

  def fromString(value: String): Option[IntervalRoutingModes.Type] = {
    enumValues.find(v => v._2 == value).map(v => v._1)
  }

  implicit val reads = new Reads[IntervalRoutingModes.Type] {
    def reads(string: JsValue) = string match {
      case s: JsString =>
        try {
          JsSuccess(fromString(s.value).get)
        } catch {
          case e: NoSuchElementException => JsError("Expected one of " + listEnumValues)
          case e: Exception => JsError(s"Unexpected error: ${e.getMessage}")
        }
      case _ => JsError("Expected one of " + listEnumValues)
    }
  }

  implicit val writes = new Writes[IntervalRoutingModes.Type] {
    def writes(intervalRoutingMode: IntervalRoutingModes.Type) = JsString(enumValues(intervalRoutingMode))
  }

}

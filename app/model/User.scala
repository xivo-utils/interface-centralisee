package model

import play.api.libs.functional.syntax._
import play.api.libs.json._

case class UserLink(
                     id: Option[Long],
                     entityId: Long,
                     idOnXivo: Long,
                     externalRouteId: Option[Long],
                     internalRouteId: Option[Long],
                     intervalId: Option[Int] = None,
                     templateId: Option[Int] = None)

case class User(id: Option[Long],
                entity: Entity,
                firstName: String,
                lastName: String,
                internalNumber: String,
                externalNumber: Option[String],
                mail: Option[String],
                ctiLogin: Option[String],
                ctiPassword: Option[String],
                provisioningNumber: Option[String] = None,
                intervalId: Option[Int] = None,
                templateId: Option[Int] = None,
                ringingTime: Option[Int] = None,
                peerSipName: Option[PeerSipNameMode.Type] = None,
                callerIdMode: Option[CallerIdMode.Type] = None,
                customCallerId: Option[String] = None) {
  import User._
  require(firstName.nonEmpty, errorFieldEmpty("firstName"))
  require(firstName.length <= FirstNameMaxLength, errorFieldTooLong("firstName"))
  require(lastName.length <= LastNameMaxLength, errorFieldTooLong("lastName"))
  require(mail.forall(_.length <= MailMaxLength), errorFieldTooLong("mail"))
  require(internalNumber.length <= InternalNumberMaxLength, errorFieldTooLong("internalNumber"))
  require(internalNumber.nonEmpty, errorFieldEmpty("internalNumber"))
  require(externalNumber.forall(_.length <= ExternalNumberMaxLength), errorFieldTooLong("externalNumber"))
  require(ctiLogin.forall(_.length <= CtiLoginMaxLength), errorFieldTooLong("ctiLogin"))
  require(ctiPassword.forall(_.length <= CtiPasswordMaxLength), errorFieldTooLong("ctiPassword"))
  require(ctiLogin.getOrElse("").isEmpty == ctiPassword.getOrElse("").isEmpty, ErrCtiCredentials)
  require(customCallerId.forall(_.length <= CustomCallerIdMaxLength), errorFieldTooLong("customCallerId"))
}

object User {
  implicit val writesUser: Writes[User] = (
    (__ \ "id").write[Option[Long]] and
    (__ \ "entity").write[Entity] and
    (__ \ "firstName").write[String] and
    (__ \ "lastName").write[String] and
    (__ \ "internalNumber").write[String] and
    (__ \ "externalNumber").write[Option[String]] and
    (__ \ "mail").write[Option[String]] and
    (__ \ "ctiLogin").write[Option[String]] and
    (__ \ "ctiPassword").write[Option[String]] and
    (__ \ "provisioningNumber").write[Option[String]] and
    (__ \ "intervalId").writeNullable[Int] and
    (__ \ "templateId").writeNullable[Int] and
    (__ \ "ringingTime").writeNullable[Int] and
    (__ \ "peerSipName").writeNullable[PeerSipNameMode.Type] and
    (__ \ "callerIdMode").writeNullable[CallerIdMode.Type] and
    (__ \ "customCallerId").writeNullable[String]
    )(unlift(User.unapply))

  val FirstNameMaxLength = 128
  val LastNameMaxLength = 128
  val MailMaxLength = 128
  val InternalNumberMaxLength = 40
  val ExternalNumberMaxLength = 80
  val CtiLoginMaxLength = 64
  val CtiPasswordMaxLength = 64
  val CustomCallerIdMaxLength = 20

  def errorFieldEmpty(field: String) = s"$field is empty"
  def errorFieldTooLong(field: String) = s"$field is too long"
  val ErrCtiCredentials = "ctiLogin and ctiPassword must be either both empty or both non-empty"
}

case class UserList(users: List[User], ghostIds: List[Long])

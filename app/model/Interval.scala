package model

import model.validators.EntityValidator
import model.validators.UserValidator
import play.api.data.validation.ValidationError
import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._

case class Interval(id: Option[Int] = None, start: String, end: String, routingMode: IntervalRoutingModes.Type = IntervalRoutingModes.Routed,
  directNumber: Option[String] = None, label: String = "") {

  def includes(interval: Interval): Boolean = start <= interval.start && end >= interval.end

  def overlaps(i: Interval): Boolean = {
    if (i.start.length == start.length) {
      return !(i.end < start || i.start > end)
    }
    false
  }

  import Interval._
  require(start.matches(IntervalRegex), errDoesNotMatch("start", IntervalRegex))
  require(end.matches(IntervalRegex), errDoesNotMatch("end", IntervalRegex))
  require(start.length == end.length, ErrorStartEndSameLength)
  require(end.toInt >= start.toInt, ErrorStartEndOrder)
  require(directNumber.forall(_.matches(DirectNumberRegex)), errDoesNotMatch("directNumber", DirectNumberRegex))
  require(label.length <= LabelMaxLength, errorFieldTooLong("label"))

  def startExtNum = UserValidator.computeExtNumber(this, start)
  def endExtNum = UserValidator.computeExtNumber(this, end)
}

object Interval {
  val greaterThanStart = ValidationError("error.endMustBeGreaterOrEqualThanStart")

  implicit val writes: Writes[Interval] = (
    (__ \ "id").writeNullable[Int] and
    (__ \ "start").write[String] and
    (__ \ "end").write[String] and
    (__ \ "routing_mode").write[IntervalRoutingModes.Type] and
    (__ \ "direct_number").writeNullable[String] and
    (__ \ "label").write[String] and
    (__ \ "start_ext_num").writeNullable[String] and
    (__ \ "end_ext_num").writeNullable[String]
  )(unliftWithExtNumBounds _)

  def unliftWithExtNumBounds(i: Interval) = (i.id, i.start, i.end, i.routingMode, i.directNumber, i.label,
    i.startExtNum, i.endExtNum)

  val IntervalMinLength = 1
  val IntervalMaxLength = 10
  val IntervalRegex = s"\\d{$IntervalMinLength,$IntervalMaxLength}"
  val IntervalMaxLengthForUI = 6
  val IntervalRegexForUI = s"\\d{$IntervalMinLength,$IntervalMaxLengthForUI}"

  val DirectNumberMaxLength = 20
  val DirectNumberRegex = s"\\d{0,$DirectNumberMaxLength}"

  val LabelMaxLength = 50

  val allDigitsErr = ValidationError("error.allDigitsErr")

  implicit val reads: Reads[Interval] = (
    (JsPath \ "id").readNullable[Int] and
    (JsPath \ "start").read[String](minLength[String](IntervalMinLength) keepAnd maxLength[String](IntervalMaxLength))
      .filter(allDigitsErr)(_.matches(IntervalRegex)) and
    (JsPath \ "start").read[String].flatMap[String] { start =>
      (JsPath \ "end").read[String](minLength[String](IntervalMinLength) keepAnd maxLength[String](IntervalMaxLength))
        .filter(allDigitsErr)(_.matches(IntervalRegex))
        .filter(greaterThanStart)(_.toLong >= start.toLong)
    } and
    (JsPath \ "routing_mode").read[IntervalRoutingModes.Type] and
    (JsPath \ "routing_mode").read[IntervalRoutingModes.Type].flatMap[Option[String]] { routingMode =>
      if (routingMode == IntervalRoutingModes.RoutedWithDirectNumber || routingMode == IntervalRoutingModes.RoutedWithCustomDirectNumber) {
        (JsPath \ "direct_number").read[String](maxLength[String](DirectNumberMaxLength))
          .filter(allDigitsErr)(_.matches(DirectNumberRegex)).map(Some.apply)
      } else {
        (JsPath \ "direct_number").readNullable[String](maxLength[String](DirectNumberMaxLength))
          .filter(allDigitsErr)(_.forall(_.matches(DirectNumberRegex))).map(_ => None)
      }
    } and
    (JsPath \ "label").readNullable[String](maxLength[String](LabelMaxLength)).map(_.getOrElse(""))
  )(Interval.apply _)

  def extNumInRangeOfIntervalRoutedWithCustDirNum(extNum: String, interval: Interval): Boolean = {
    require(interval.routingMode == IntervalRoutingModes.RoutedWithCustomDirectNumber)
    val inRange = for {
      start <- interval.startExtNum
      end <- interval.endExtNum
      if extNum.matches("\\d+")
      if extNum.length == start.length
      extNumLong = extNum.toLong
    } yield start.toLong <= extNumLong && extNumLong <= end.toLong
    inRange.getOrElse(false)
  }

  def errDoesNotMatch(field: String, regex: String) = s"$field does not match regex $regex"
  val ErrorStartEndOrder = "start must be less then or equal to an end"
  val ErrorStartEndSameLength = "start and end must have the same length"
  def errorFieldTooLong(field: String) = s"$field is too long"
}

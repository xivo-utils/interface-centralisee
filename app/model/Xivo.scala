package model

import com.google.inject.{ImplementedBy, Inject, Singleton}
import model.utils._
import org.slf4j.LoggerFactory
import play.api.Configuration
import play.api.db.{Database, NamedDatabase}
import play.api.libs.json._
import services.cache.{CacheScheduler, CachedXivoUserManager}
import services.{XivoDb, XivoManager}
import xivo.ldap.xivoconnection.XivoConnector
import xivo.restapi.model.Context

import java.util.UUID
import scala.collection.JavaConversions._
import scala.util.{Failure, Success, Try}

trait XivoObj {
  val host: String
  val playAuthToken: Option[String]
  val isProxy: Boolean
}

case class Xivo(id: Option[Long], uuid: UUID, name: String, host: String, contextName: String, remainingSlots: Option[Int] = None, playAuthToken: Option[String], isProxy: Boolean = false) extends XivoObj {
  override def equals(o: Any): Boolean = o match {
    case Xivo(_id, _uuid, _name, _host, _contextName, _, _, _) =>
      id.equals(_id) && uuid.equals(_uuid) && name.equals(_name) && contextName.equals(_contextName) && _host.equals(host)
    case _ => false
  }

  import Xivo._
  require(name.length <= NameMaxLength, "name too long")
  require(host.length <= HostMaxLength, "host too long")
  require(contextName.length <= ContextNameMaxLength, "contextName too long")
}

object Xivo {
  val NameMaxLength = 30
  val HostMaxLength = 100
  val ContextNameMaxLength = 100

  implicit val writes: Writes[Xivo] = new Writes[Xivo] {
    def writes(xivo: Xivo): JsObject = Json.obj(
      "id"  -> xivo.id,
      "uuid"  -> xivo.uuid,
      "name"  -> xivo.name,
      "host"  ->xivo.host,
      "contextName"  ->xivo.contextName,
      "remainingSlots"  -> xivo.remainingSlots,
      "isProxy" -> xivo.isProxy
    )
  }

  def fromXivoDb(xDb: XivoDb, remainingSlots: Option[Int]) =
    Xivo(xDb.id, xDb.uuid, xDb.name, xDb.host, xDb.contextName, remainingSlots, xDb.playAuthToken, xDb.isProxy)
}

case class XivoCreation(name: String, host: String, linkedXivo: List[Long], contextName: String, configure: Boolean = true, playAuthToken: Option[String] = None, isProxy: Boolean = false) extends XivoObj {
  require(name.length <= Xivo.NameMaxLength, "name too long")
  require(host.length <= Xivo.HostMaxLength, "host too long")
  require(contextName.length <= Xivo.ContextNameMaxLength, "contextName too long")
}

case class ProxyCreation(name: String, host: String, contextName: String, intervals: List[Interval], playAuthToken: Option[String] = None, isProxy: Boolean = true) extends XivoObj {
  require(name.length <= Xivo.NameMaxLength, "name too long")
  require(host.length <= Xivo.HostMaxLength, "host too long")
  require(contextName.length <= Xivo.ContextNameMaxLength, "contextName too long")
}

class CreationResult(var result: Xivo = null, var notices: List[String] = List()) {
  def setResult(x: Xivo): Unit = result = x
  def accumulate(notice: String) = { notices = notices ++ List(notice) }
  def accumulate(notice: List[String]) = { notices = notices ++ notice }
}

case class XivoExtended(xivo: Xivo, isReachable: Boolean)
object XivoExtended {
  implicit val writes = Writes[XivoExtended] { xivoExt =>
    Xivo.writes.writes(xivoExt.xivo).as[JsObject] + ("isReachable" -> JsBoolean(xivoExt.isReachable))
  }
  def fromXivoDb(xDb: XivoDb, remainingSlots: Option[Int], isReachable: Boolean) =
    XivoExtended(Xivo.fromXivoDb(xDb, remainingSlots), isReachable)
}

@ImplementedBy(classOf[XivoImpl])
trait XivoInterface {
  def synchronizeConfigFiles()
  def getXivo(id: Long): Option[Xivo]
  def getXivo(uuid: UUID): Option[Xivo]
  def list: List[Xivo]
  def listExtended: List[XivoExtended]
  def create(xivo: XivoCreation): CreationResult
  def createProxy(proxy: ProxyCreation): CreationResult
  def createContextOrNotice(connector: XivoConnector, name: String): Option[String]
  def delete(xivoId: Long)

  var connectorPool: XivoConnectorPool
  var configManagerFactory = new ConfigurationManagerFactory

  val NoticeContextExists = "Le contexte %s existe déjà"
  def WarningUnreachableXivo(x: XivoDb) = s"Le XiVO ${x.name} (${x.host}) n'est pas joignable"
}

@Singleton
class XivoImpl @Inject()(@NamedDatabase("default") db: Database, configuration: Configuration, xivoManager: XivoManager,
                         cacheScheduler: CacheScheduler, cachedXivoUserManager: CachedXivoUserManager,
                         globalXivoConnectorPool: GlobalXivoConnectorPool, config: ConfigParser) extends XivoInterface {

  val logger = LoggerFactory.getLogger(getClass)
  lazy val xivoCapacity = configuration.getInt("xivoCapacity").get
  var connectorPool: XivoConnectorPool = globalXivoConnectorPool

  override def getXivo(id: Long): Option[Xivo] = db.withConnection(implicit c => {
    xivoManager.get(id) match {
      case Success(x) => Some(Xivo.fromXivoDb(x, getRemainingSlots(x.uuid)))
      case Failure(e) =>
        logger.info(s"Unable to get xivo with id $id, error: $e")
        None
    }
  })

  override def getXivo(uuid: UUID): Option[Xivo] =
    xivoManager.get(uuid) match {
      case Success(x) => Some(Xivo.fromXivoDb(x, getRemainingSlots(uuid)))
      case Failure(e) =>
        logger.info(s"Unable to get xivo with id $uuid, error: $e")
        None
    }

  override def list: List[Xivo] =
    xivoManager.all() match {
      case Success(l) => l.map(e => Xivo.fromXivoDb(e, getRemainingSlots(e.uuid)))
      case Failure(e) =>
        logger.error(s"Unable to get list of xivos, error: $e")
        Nil
    }

  private def isXivoReachable(xivo: XivoObj): Boolean = {
      xivo.isProxy ||
      Try {
        connectorPool.withConnector(xivo) {
          _.getXivoInfo
        }
      }.isSuccess
  }

  override def listExtended: List[XivoExtended] =
    xivoManager.all() match {
      case Success(l) =>
        logger.info(s"Received list of Xivos $l")
        l.map { xivo =>
          XivoExtended.fromXivoDb(xivo, getRemainingSlots(xivo.uuid), isXivoReachable(xivo))
        }
      case Failure(e) =>
        logger.error(s"Unable to get list of xivos, error: $e")
        Nil
    }

  override def create(xivo: XivoCreation): CreationResult = {
    logger.info(s"Starting creation of xivo $xivo")
    val result = new CreationResult()
    if (xivo.configure) {
      result.accumulate(configureXivo(xivo).notices)
    }
    val uuid = connectorPool.withConnector(xivo)(connector => {
      connector.getXivoInfo().getUuid
    })
    val playAuthToken: String = configManagerFactory.get(connectorPool, config).getPlayAuthToken(xivo.host)
    val xivoDb = xivoManager.create(XivoDb(None, uuid, xivo.name, xivo.host, xivo.contextName, Some(playAuthToken)))
    xivoDb match {
      case Success(x) =>
        result.setResult(Xivo.fromXivoDb(x, None))
        logger.info(s"Xivo creation finished with result ${result.result}")
        result.result.id.foreach(cacheScheduler.startCachingXivo)
      case Failure(e) =>
        result.accumulate(s"Unable to create xivo: $e")
        logger.warn(s"Xivo creation failed: $e")
    }
    logger.info(s"Xivo creation finished with result $result")
    result
  }

  override def createProxy(proxy: ProxyCreation): CreationResult = {
    logger.info(s"Starting creation of proxy $proxy")
    val result = new CreationResult()
    val xivoDb = xivoManager.create(XivoDb(None, UUID.randomUUID(), proxy.name, proxy.host, proxy.contextName, None, true))
    xivoDb match {
      case Success(x) =>
        result.setResult(Xivo.fromXivoDb(x, None))
        logger.info(s"Proxy creation finished with result ${result.result}")
      case Failure(e) =>
        result.accumulate(s"Unable to create proxy: $e")
        logger.warn(s"Proxy creation failed: $e")
    }
    logger.info(s"Proxy creation finished with result $result")
    result
  }

  private def configureXivo(xivo: XivoCreation): CreationResult = {
    val result = new CreationResult()
    val configManager = configManagerFactory.get(connectorPool, config)
    configManager.configure(xivo).foreach(s => result.accumulate(s))
    connectorPool.withConnector(xivo)(connector => {
      for (x <- xivo.linkedXivo.map(id => getXivo(id).get)) {
        createContextOrNotice(connector, x.contextName).foreach(result.accumulate)
        connectorPool.withConnector(x)(c => createContextOrNotice(c, xivo.contextName).foreach(result.accumulate))
      }
      if (connector.contextExists("routage"))
        result.accumulate(NoticeContextExists.format("routage"))
      else {
        connector.createContext(new Context("routage", List()))
        connector.createContextInclusion("default", "routage", 255)
      }
      connector.deleteContextInclusion("default", "to-extern")
    })
    result
  }

  override def createContextOrNotice(connector: XivoConnector, name: String): Option[String] = {
    if(connector.contextExists(name))
      Some(NoticeContextExists.format(name))
    else {
      connector.createContext(new Context(name, List()))
      None
    }
  }

  def getRemainingSlots(xivoUuid: UUID): Option[Int] =
    cachedXivoUserManager.countForXivo(xivoUuid) match {
      case Success(userCount) =>
        Some(0 max xivoCapacity - userCount)
      case Failure(e) =>
        logger.error(s"Counting cached users for xivo=$xivoUuid failed", e)
        None
    }

  override def synchronizeConfigFiles(): Unit = list.foreach(configManagerFactory.get(connectorPool, config).synchronizeFiles)

  override def delete(xivoId: Long): Unit = xivoManager.delete(xivoId)

}

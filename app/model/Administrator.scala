package model

import anorm.SqlParser._
import anorm._
import com.google.inject.{ImplementedBy, Singleton}
import org.jasypt.util.password.StrongPasswordEncryptor
import org.slf4j.LoggerFactory
import play.api.db.{Database, NamedDatabase}
import play.api.libs.functional.syntax._
import play.api.libs.json._
import javax.naming.ConfigurationException
import scala.collection.JavaConversions._
import javax.inject.Inject
import model.rights.Role
import play.api.Configuration
import services.AdministratorDb

case class UIAdministrator(id: Option[Long],
                           name: String,
                           login: String,
                           password: String,
                           superAdmin: Boolean,
                           ldap: Boolean,
                           roles: List[Role]) {
  import Administrator._
  require(login.length <= LoginMaxLength, "login too long")
  require(name.length <= NameMaxLength, "name too long")
}

object UIAdministrator {
  implicit val writes = ((__ \ "id").write[Option[Long]] and
    (__ \ "name").write[String] and
    (__ \ "login").write[String] and
    (__ \ "password").write[String] and
    (__ \ "superAdmin").write[Boolean] and
    (__ \ "ldap").write[Boolean] and
    (__ \ "roles").write[List[Role]])(unlift(UIAdministrator.unapply))

  def apply(adminDb: AdministratorDb, roles: List[Role]): UIAdministrator =
    UIAdministrator(adminDb.id, adminDb.name, adminDb.login, adminDb.hashedPassword, adminDb.superadmin, adminDb.ldap, roles)
}

case class Administrator(id: Option[Long],
                         login: String,
                         name: String,
                         password: String,
                         superAdmin: Boolean,
                         ldap: Boolean,
                         entities: List[Entity]) {

  import Administrator._
  require(login.length <= LoginMaxLength, "login too long")
  require(name.length <= NameMaxLength, "name too long")

}
object Administrator {

  val LoginMaxLength = 40
  val NameMaxLength = 100

  implicit val writes = ((__ \ "id").write[Option[Long]] and
    (__ \ "login").write[String] and
    (__ \ "name").write[String] and
    (__ \ "password").write[String] and
    (__ \ "superAdmin").write[Boolean] and
    (__ \ "ldap").write[Boolean] and
    (__ \ "entities").write[List[Entity]])(unlift(Administrator.unapply))
}

@ImplementedBy(classOf[AdministratorImpl])
trait AdministratorInterface {
  def delete(id: Long)
  def update(oldAdmin: Administrator, newAdmin: Administrator)
  def passwordMatches(password: String, hash: String): Boolean
  def getHashedPassword(login: String): Option[String]
  def create(admin: Administrator): Administrator
  def list: List[Administrator]
  def isUrlAllowed(login: String, url: String, method: String): Boolean
  def getAdmin(id: Long): Option[Administrator]
  def exists(login: String): Boolean
  def isSuperAdmin(login: String): Boolean
  def isLastSuperAdmin(adminId: Long): Boolean
}

class UrlRule(url: String, _methods: List[String]) {
  val methods = _methods.map(_.toLowerCase)
  def matches(otherUrl: String, method: String) = otherUrl.startsWith(url) && (methods.isEmpty || methods.contains(method.toLowerCase))
}

@Singleton
class AdministratorImpl @Inject()(configuration: Configuration, @NamedDatabase("default") db: Database, entities: EntityInterface) extends AdministratorInterface {
  val logger = LoggerFactory.getLogger(getClass)
  val encryptor = new StrongPasswordEncryptor()
  def urlRules = getUrlRulesFromConfig

  private def getUrlRulesFromConfig: Iterable[UrlRule] = configuration.getStringList("rights.restrictedUrls") match {
    case None => List()
    case Some(rules) => rules.map(str => {
      str.split(",").toList match {
        case url :: methods => new UrlRule(url, methods.filterNot(_.isEmpty))
        case List() => throw new ConfigurationException(s"Invalid URL specification : $str")
      }
    })
  }

  val simple = get[Option[Long]]("id") ~
    get[String]("login") ~
    get[String]("name") ~
    get[String]("hashed_password") ~
    get[Boolean]("superadmin") ~
    get[Boolean]("ldap") ~
    get[List[Option[Long]]]("entity_ids") map {
      case id ~ login ~ name ~ hashedPwd ~ superAdmin ~ ldap ~ entityIds => Administrator(id, login, name, hashedPwd, superAdmin, ldap, entityIds.flatten.map(id => entities.getEntity(id).get))
    }

  override def list: List[Administrator] = db.withConnection(implicit c =>
  SQL("SELECT a.id, a.login, a.name, a.hashed_password, a.superadmin, a.ldap, array_agg(ae.entity_id) AS entity_ids FROM administrators a " +
  "LEFT JOIN administrator_entity ae ON a.id = ae.administrator_id GROUP BY a.id, a.login, a.superadmin, a.ldap ORDER BY login ASC").as(simple *))

  override def create(admin: Administrator): Administrator = db.withConnection(implicit c => {
    logger.info(s"Starting creation of admin ${admin.copy(password="****")}")
    var res = admin
    val hash = encryptor.encryptPassword(admin.password)
    c.setAutoCommit(false)
    try {
      val genId: Option[Long] = SQL("INSERT INTO administrators(login, name, hashed_password, superadmin, ldap) VALUES ({login}, {name}, {hash}, {superAdmin}, {ldap})").on(
      'login -> admin.login, 'name -> admin.name, 'hash -> hash, 'superAdmin -> admin.superAdmin, 'ldap -> admin.ldap).executeInsert()
      for(e <- admin.entities) {
        SQL("INSERT INTO administrator_entity(administrator_id, entity_id) VALUES ({adminId}, {entityId})").on('adminId -> genId.get, 'entityId -> e.id).executeUpdate()
      }
      c.commit()
      res = admin.copy(id = genId, password = hash)
      c.setAutoCommit(true)
    } catch {
      case e: Exception => c.rollback()
        c.setAutoCommit(true)
        throw e
    }
    logger.info(s"Creation of admin ${admin.copy(password="****")} finished")
    res
  })

  override def passwordMatches(password: String, hash: String): Boolean = encryptor.checkPassword(password, hash)

  override def getHashedPassword(login: String): Option[String] = db.withConnection(implicit c =>
  SQL("SELECT hashed_password FROM administrators WHERE login = {login}").on('login -> login).as(get[String]("hashed_password") *).headOption)

  def isSuperAdmin(login: String): Boolean = db.withConnection(implicit c =>
    SQL("SELECT superadmin FROM administrators WHERE login = {login}").on('login -> login).as(get[Boolean]("superadmin") *).headOption.getOrElse(false))

  override def isUrlAllowed(login: String, url: String, method: String): Boolean = {
    if(isSuperAdmin(login)) true
    else urlRules.find(_.matches(url, method)).isEmpty
  }

  override def getAdmin(id: Long): Option[Administrator] = db.withConnection(implicit c =>
    SQL("SELECT a.id, a.login, a.name, a.hashed_password, a.superadmin, a.ldap, array_agg(ae.entity_id) AS entity_ids FROM administrators a " +
      "LEFT JOIN administrator_entity ae ON a.id = ae.administrator_id WHERE id = {id} GROUP BY a.id, a.login, a.superadmin, a.ldap ORDER BY login ASC")
      .on('id -> id).as(simple *).headOption)

  override def exists(login: String): Boolean = db.withConnection(implicit c =>
    SQL("SELECT login FROM administrators WHERE login = {login}").on('login -> login).as((get[String]("login")).singleOpt).isDefined )

  override def update(oldAdmin: Administrator, newAdmin: Administrator): Unit = db.withConnection(implicit c => {
    val hash = if(oldAdmin.password.equals(newAdmin.password)) oldAdmin.password else encryptor.encryptPassword(newAdmin.password)
    c.setAutoCommit(false)
    try {
      SQL("DELETE FROM administrator_entity WHERE administrator_id = {id}").on('id -> oldAdmin.id).executeUpdate()
      SQL("UPDATE administrators SET login = {login}, hashed_password = {hash}, name = {name}, superadmin = {superAdmin}, ldap = {ldap} WHERE id = {id}").on(
        'login -> newAdmin.login, 'name -> newAdmin.name, 'hash -> hash, 'superAdmin -> newAdmin.superAdmin, 'ldap -> newAdmin.ldap, 'id -> oldAdmin.id).executeUpdate()
      for(e <- newAdmin.entities) {
        SQL("INSERT INTO administrator_entity(administrator_id, entity_id) VALUES ({adminId}, {entityId})").on('adminId -> oldAdmin.id, 'entityId -> e.id).executeUpdate()
      }
      c.commit()
      c.setAutoCommit(true)
    } catch {
      case e: Exception => c.rollback()
        c.setAutoCommit(true)
        throw e
    }
  })

  override def delete(id: Long): Unit = db.withConnection(implicit c => {
    c.setAutoCommit(false)
    try {
      SQL("DELETE FROM administrator_entity WHERE administrator_id = {id}").on('id -> id).executeUpdate()
      SQL("DELETE FROM administrators WHERE id = {id}").on('id -> id).executeUpdate()
      c.commit()
      c.setAutoCommit(true)
    } catch {
      case e: Exception => c.rollback()
        c.setAutoCommit(true)
        throw e
    }
  })

  override def isLastSuperAdmin(adminId: Long): Boolean = db.withConnection(implicit c =>
    SQL("SELECT id FROM administrators WHERE superadmin = true AND id != {id}").on('id -> adminId).as(get[Long]("id") *).isEmpty)
}

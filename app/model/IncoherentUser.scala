package model

import java.io.{PrintWriter, StringWriter}

import purecsv.safe._

case class IncoherentUser(firstName: String, lastname: String, internalNo: String, externalNo: Option[String], xivoName: String, entityName: String)

object IncoherentUser {
  def apply(u: User): IncoherentUser =
    new IncoherentUser(
      u.firstName, u.lastName, u.internalNumber, removeQuotes(u.externalNumber), u.entity.xivo.name, u.entity.displayName
    )

  type CSV = String
  def convertToCsv(u: List[IncoherentUser]): CSV = {
    val sep = "|"
    val header = Seq("Firstname", "Lastname", "internalNumber", "externalNumber", "XivoName", "EntityName")
    val writer = new StringWriter()
    u.writeCSVTo(new PrintWriter(writer), sep, Some(header))
    writer.toString
  }

  private def removeQuotes(s: Option[String]): Option[String] = {
    s match {
      case None => None
      case Some(s) => Some(s.replace("\"", ""))
    }
  }
}

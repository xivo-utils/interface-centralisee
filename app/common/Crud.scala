package common

import anorm.{Macro, RowParser}

import scala.util.Try

/**
  * Standardize CRUD operation on persisted classes
  *
  * @tparam T The type of the class to persist
  * @tparam I The type of the id (PK) of the class to persist
  */
trait Crud[T, I] {
  /**
    * Persist the object
    *
    * @param t the object to persist
    * @return the persisted object with the updated id or an exception upon failure
    */
  def create(t: T): Try[T]

  /**
    * Update the persisted data of the given object
 *
    * @param t the object to persist
    * @return the persisted object or an exception upon failure
    */
  def update(t: T): Try[T]

  /**
    * Get an object given its id
    *
    * @param id the id of the object to get
    * @return the corresponding object or an exception upon failure
    */
  def get(id: I): Try[T]

  /**
    * Delete an object given its id
    * @param id the delete object or an exception upon failure
    * @return
    */
  def delete(id: I): Try[T]

  /**
    * Get all persisted object
    * @return a list of all persisted or an exception upon failure
    */
  def all(): Try[List[T]]
}

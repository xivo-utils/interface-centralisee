package common

import anorm.{Column, ParameterMetaData, ToStatement, TypeDoesNotMatch}

/**
  * Collection of DB Helper methods
  */
object DBUtil {

  /**
    * helper to create a converter between Enum and custom db type
    * @param dbValues a map defining the string representation of each enum value
    * @tparam T the enum type
    */
  def enumToStatement[T](dbValues: Map[T, String] ) = new ToStatement[T] {

    def set(s: java.sql.PreparedStatement, index: Int, aValue: T): Unit = {
      s.setObject(index, dbValues(aValue), java.sql.Types.OTHER)
    }
  }

  /**
    * Helper to create a converter between a database column value and it's enum counterpart
    * @param dbValues a map defining the string representation of each enum value
    * @tparam T the enum type
    */
  def columnToEnum[T](dbValues: Map[T, String] )(implicit m: Manifest[T]): Column[T] = Column {
    (value, meta) =>
      try {
        val s = value.asInstanceOf[String]
        dbValues.filter(a => a._2 == s).map(_._1).headOption match {
          case Some(v) => Right(v)
          case None => Left(TypeDoesNotMatch("No matching type value for " + value))
        }

      } catch {
        case e: Exception =>Left(TypeDoesNotMatch("Cannot convert " + value + " to UserMeta"))
      }
  }

  /**
    * Helper to create metadata information for the given custom enum
    * @tparam T
    * @return
    */
  def enumTypeMetaData[T]() = new ParameterMetaData[T] {
    override def sqlType: String = java.sql.JDBCType.OTHER.getName

    override def jdbcType: Int = java.sql.Types.OTHER
  }
}

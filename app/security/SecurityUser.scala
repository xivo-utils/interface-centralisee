package security

import be.objectify.deadbolt.scala.models.{Permission, Role, Subject}

class SecurityUser(val userName: String) extends Subject {
  override def roles: List[Role] = List()

  override def permissions: List[Permission] = List()

  override def identifier: String = userName
}

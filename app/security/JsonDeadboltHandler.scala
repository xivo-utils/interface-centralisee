package security

import be.objectify.deadbolt.scala.{AuthenticatedRequest, DynamicResourceHandler}
import play.api.mvc.{Request, Result}
import play.api.mvc.Results._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class JsonDeadboltHandler(dynamicResourceHandler: Option[DynamicResourceHandler] = None) extends UrlDeadboltHandler {

  override def getDynamicResourceHandler[A](request: Request[A]): Future[Option[DynamicResourceHandler]] = {
    Future(dynamicResourceHandler)
  }

  override def onAuthFailure[A](request: AuthenticatedRequest[A]): Future[Result] = {
    Future(Forbidden(""))
  }
}

package security.handlers

import be.objectify.deadbolt.scala.HandlerKey

object HandlerKeys {

  val json = Key("json")
  val web = Key("web")

  case class Key(name: String) extends HandlerKey

}

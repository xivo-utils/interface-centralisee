package security.handlers

import be.objectify.deadbolt.scala.{AuthenticatedRequest, DeadboltHandler, DynamicResourceHandler}
import model.AdministratorInterface
import play.api.Logger

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class UrlDynamicResourceHandler(admins: AdministratorInterface) extends DynamicResourceHandler {
  override def isAllowed[A](name: String, meta: Option[Any], deadboltHandler: DeadboltHandler,
                            request: AuthenticatedRequest[A]): Future[Boolean] = {
    request.subject match {
      case None =>
        Logger.warn(s"Tried to get ${request.path} from ${request.remoteAddress} without being authenticated")
        Future(false)
      case Some(securityUser) =>
        if (admins.isUrlAllowed(securityUser.identifier, request.path, request.method)) {
          Future(true)
        }
        else {
          Logger.info(s"Refusing ${securityUser.identifier} to access ${request.path}")
          Future(false)
        }
    }
  }

  override def checkPermission[A](permissionValue: String, meta: Option[Any] = None,
                                  deadboltHandler: DeadboltHandler,
                                  request: AuthenticatedRequest[A]): Future[Boolean] = Future(false)
}

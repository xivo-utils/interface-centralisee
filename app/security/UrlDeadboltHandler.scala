package security

import be.objectify.deadbolt.scala.models.Subject
import be.objectify.deadbolt.scala.{AuthenticatedRequest, DeadboltHandler}
import play.api.mvc.Request
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

trait UrlDeadboltHandler extends DeadboltHandler {
  override def beforeAuthCheck[A](request: Request[A]) = Future(None)

  override def getSubject[A](request: AuthenticatedRequest[A]): Future[Option[Subject]] = {
    Future(request.session.get("username").map(new SecurityUser(_)))
  }
}

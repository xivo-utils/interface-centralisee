package security

import be.objectify.deadbolt.scala.{AuthenticatedRequest, DynamicResourceHandler}
import controllers.routes
import play.api.mvc.Results._
import play.api.mvc.{Request, Result}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class WebDeadboltHandler(dynamicResourceHandler: Option[DynamicResourceHandler] = None) extends UrlDeadboltHandler {

  override def getDynamicResourceHandler[A](request: Request[A]): Future[Option[DynamicResourceHandler]] = {
    Future(dynamicResourceHandler)
  }

  override def onAuthFailure[A](request: AuthenticatedRequest[A]): Future[Result] = {
    Future(Redirect(routes.Login.loginPage()))
  }
}

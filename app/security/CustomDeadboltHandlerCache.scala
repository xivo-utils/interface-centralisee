package security

import javax.inject.{Inject, Singleton}

import be.objectify.deadbolt.scala.{DeadboltHandler, HandlerKey}
import be.objectify.deadbolt.scala.cache.HandlerCache
import model.AdministratorInterface
import security.handlers.{HandlerKeys, UrlDynamicResourceHandler}

import scala.collection.immutable.Map

@Singleton
class CustomDeadboltHandlerCache @Inject()(admins: AdministratorInterface) extends HandlerCache {

  private val webHandler = new WebDeadboltHandler(Some(new UrlDynamicResourceHandler(admins)))
  private val jsonHandler = new JsonDeadboltHandler(Some(new UrlDynamicResourceHandler(admins)))

  private val handlers: Map[HandlerKey, DeadboltHandler] =
    Map(
      HandlerKeys.json -> jsonHandler,
      HandlerKeys.web -> webHandler
    )

  override def apply(): DeadboltHandler = webHandler
  override def apply(handlerKey: HandlerKey): DeadboltHandler = handlers(handlerKey)
}

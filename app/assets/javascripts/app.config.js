(function() {
    'use strict';

    angular.module('ecu').config(config);

    config.$inject = ['$translateProvider', '$translatePartialLoaderProvider'];

    function config($translateProvider, $translatePartialLoaderProvider) {
        $translatePartialLoaderProvider.addPart('main');
        $translatePartialLoaderProvider.addPart('form');
        $translateProvider.useLoader('$translatePartialLoader', {
            urlTemplate: '/assets/i18n/{part}-{lang}.json'
        });
        $translateProvider.registerAvailableLanguageKeys(['en','fr'], {
               'en_*': 'en',
               'fr_*': 'fr'
        });
        $translateProvider.determinePreferredLanguage();
        $translateProvider.fallbackLanguage(['fr']);
    }
})();

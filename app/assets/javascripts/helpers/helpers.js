var HelpersFunctions = HelpersFunctions || {};

HelpersFunctions.helpers = {
    skipNulls: function(object) {
        for(var key in object) {
            if(object[key] === '')
                delete object[key];
        }
        return object;
    }
};

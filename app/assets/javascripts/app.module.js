(function(){
    'use strict';
    angular.module('ecu', [
        'pascalprecht.translate',
        'ngMessages',
        'admin',
        'xivo',
        'entities',
        'users',
        'templates',
        'roles',
        'dashboard',
        'validation'
     ]);
})();

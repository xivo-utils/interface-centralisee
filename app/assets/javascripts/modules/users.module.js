angular.module('users', [
    'pascalprecht.translate',
    'ui.select',
    'ui.bootstrap',
    'entities',
    'templates'
])
.config(function ($translateProvider, $translatePartialLoaderProvider) {
    $translatePartialLoaderProvider.addPart('users');
    $translatePartialLoaderProvider.addPart('templates');
});

(function(){
    'use strict';

    angular
    .module('ecu')
    .run(run);

    run.$inject =['$rootScope', 'XivoFactory', 'EntityFactory'];

    function run ($rootScope, XivoFactory, EntityFactory) {
        $rootScope.sidebarXivosPending = true;
        XivoFactory.listXivos().then(function(response) {
            $rootScope.sidebarXivosPending = false;
            $rootScope.sidebarXivosList = response.items;
        });
        EntityFactory.listEntities().then(function(response) {
            $rootScope.sidebarEntitiesList = response.items;
            EntityFactory.listEntitiesWithXivoUpdate().then(function(response) {
                $rootScope.sidebarEntitiesList = response.items;
            });
        });
    }
})();

(function() {
    'use strict';

    angular.module('templates').factory('TemplateFactory', TemplateFactory);

    TemplateFactory.$inject = [ '$http', '$rootScope', '$window' ];

    function TemplateFactory($http, $rootScope, $window) {

        var getBaseUrl = function() {
            return $window.location.origin + '/api/1.0/templates';
        };

        var httpGet = function(url) {
            return $http.get(url).then(
                function(response) {
                    return response.data;
                });
        };

        var _create = function(template) {
            return $http.post(getBaseUrl(), template).then(
                function(response) {
                    return response.status;
                });
        };

        var _get = function(id) {
            var url = getBaseUrl() + '/' + id;
            return httpGet(url);
        };

        var _list = function() {
            return httpGet(getBaseUrl());
        };

        var _listForEntity = function(eId) {
            return $http.get(getBaseUrl(), {
                params: {
                    entityId: eId
                }
            }).then(
                function(response) {
                    return response.data;
                });
        };

        var _update = function(template) {
            return $http.put(getBaseUrl() + '/' + template.id, template).then(
                function(response) {
                    return response.status;
                });
        };

        var _delete = function(id) {
            return $http.delete(getBaseUrl() + '/' + id).then(
                function(response) {
                    return response.status;
                });
        };

        return {
            createTemplate : _create,
            getTemplate : _get,
            listTemplates: _list,
            listForEntity: _listForEntity,
            updateTemplate: _update,
            deleteTemplate: _delete
        };

    }
})();

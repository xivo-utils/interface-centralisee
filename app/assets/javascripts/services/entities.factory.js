(function() {
    'use strict';

    angular.module('entities').factory('EntityFactory', EntityFactory);

    EntityFactory.$inject = [ '$http', '$rootScope', '$window' ];

    function EntityFactory($http, $rootScope, $window) {

        var getBaseUrl = function() {
            return $window.location.origin + '/api/1.0/entities';
        };

        var getMediaServerUrl = function() {
            return $window.location.origin + '/api/1.0/mediaservers/xivo';
        };

        var httpGet = function(url) {
            return $http.get(url).then(
                function(response) {
                    return response.data;
                });
        };

        var _getMdsListForSelectedXivo = function(id) {
            var url = getMediaServerUrl() + '/' + id;
            return httpGet(url);
        };

        var _create = function(entity) {
            return $http.post(getBaseUrl(), entity)
            .then(
                function successCallback(response) {
                    return response.status;
                },
                function errorCallback(response) {
                    return response;
                }
             );
        };

        var _get = function(id) {
            var url = getBaseUrl() + '/' + id;
            return httpGet(url);
        };

        var _list = function() {
            return httpGet(getBaseUrl());
        };

        var _list_with_xivo_update = function() {
            return httpGet(getBaseUrl() + '?updateFromXivo=true');
        };

        var _edit = function(id, entity) {

            return $http.put(getBaseUrl() + '/' + id, entity)
            .then(
                function successCallback(response) {
                    return response;
                },
                function errorCallback(response) {
                    return response;
                }
            );
        };

        var _delete = function(id) {
            return $http.delete(getBaseUrl() + '/' + id).then(
                function(response) {
                    return response.status;
                });
        };

        var _available_numbers = function(id) {
            var url = getBaseUrl() + '/' + id + '/available_numbers';
            return httpGet(url);
        };

        var _available_numbers_for_interval = function(eId, iId) {
            var url = getBaseUrl() + '/' + eId + '/available_numbers?interval=' + iId;
            return httpGet(url);
        };

        var _external_number_for_internal_number = function(eId, internalNumber) {
            var url = getBaseUrl() + '/' + eId + '/external_number/' + internalNumber;
            return httpGet(url);
        };

        var _check_external_number = function(eId, intervalId, externalNumber, userId) {
            var url = getBaseUrl() + '/' + eId + '/check_external_number/' + intervalId + '/' + externalNumber;
            if (userId === parseInt(userId, 10)) {
                url += '?userId=' + userId;
            }
            return httpGet(url);
        };

        var _cached_entity_users = function(id) {
            var url = getBaseUrl() + '/' + id + '/users';
            return httpGet(url);
        };

        var _updated_entity_users = function(id) {
            var url = getBaseUrl() + '/' + id + '/users?updateFromXivo=true';
            return httpGet(url);
        };

        var _deleteStaleUsers = function(entityId) {
            var url = getBaseUrl() + '/' + entityId + '/delete_stale_users';
            return $http.get(url).then(
                function(response) {
                    return response;
                });
        };

        var _routingModes = [
            {code: 'routed', name: 'ROUTED'},
            {code: 'with_direct_number', name: 'ROUTED_WITH_DIRECT_NUMBER'},
            {code: 'with_customized_direct_number', name: 'ROUTED_WITH_CUSTOMIZED_DIRECT_NUMBER'}
        ];
        var _getRoutingModes = function() { return _routingModes; };
        var _getDefaultRoutingMode = function() { return _routingModes[0]; };
        var _getRoutingModeWithDirNo = function() { return _routingModes[1]; };
        var _getRoutingModeWithCustomizedDirectNo = function() { return _routingModes[2]; };

        return {
            createEntity: _create,
            getMdsListForSelectedXivo: _getMdsListForSelectedXivo,
            getEntity: _get,
            listEntities: _list,
            listEntitiesWithXivoUpdate: _list_with_xivo_update,
            editEntity: _edit,
            deleteEntity: _delete,
            getEntityAvailableNumbers: _available_numbers,
            getIntervalAvailableNumbers: _available_numbers_for_interval,
            getExternalNumberForInternalNumber: _external_number_for_internal_number,
            checkExternalNumber: _check_external_number,
            getCachedEntityUsers: _cached_entity_users,
            getUpdatedEntityUsers: _updated_entity_users,
            deleteStaleUsers: _deleteStaleUsers,
            getRoutingModes: _getRoutingModes,
            getDefaultRoutingMode: _getDefaultRoutingMode,
            getRoutingModeWithDirectNo: _getRoutingModeWithDirNo,
            getRoutingModeWithCustomizedDirectNo: _getRoutingModeWithCustomizedDirectNo
        };
    }
})();

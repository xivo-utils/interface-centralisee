(function() {
    'use strict';

    angular.module('users').factory('UserFactory', UserFactory);

    UserFactory.$inject = [ '$http', '$rootScope', '$window', '$translate' ];

    function UserFactory($http, $rootScope, $window, $translate) {

        var getBaseUrl = function() {
            return $window.location.origin + '/api/1.0/users';
        };

        var getMdsDisplayNameUrl = function() {
            return $window.location.origin + '/api/1.0/mediaservers/xivo';
        };

        var httpGet = function(url) {
            return $http.get(url).then(
                function(response) {
                    return response.data;
                });
        };

        var _create = function(user, withConfiguration) {
            var configuration = !withConfiguration?'?withConfiguration=false': '';
            var url = getBaseUrl() + configuration;
            
            return $http.post(url, user)
            .then(
                function successCallback(response) {
                    return response.status;
                },
                function errorCallback(response) {
                    return response;
                }
            );
        };

        var _get = function(id) {
            var url = getBaseUrl() + '/' + id;
            return httpGet(url);
        };

        var _edit = function(id, user, withConfiguration) {
            var configuration = !withConfiguration?'?withConfiguration=false': '';
            return $http.put(getBaseUrl() + '/' + id + configuration, user)
            .then(
                function successCallback(response) {
                    return response.status;
                },
                function errorCallback(response) {
                    return response;
                }
            );
        };

        var _delete = function(id, withConfiguration) { 
            var configuration = !withConfiguration?'?withConfiguration=false': '';
            return $http.delete(getBaseUrl() + '/' + id + configuration).then(
                function(response) {
                    return response.status;
                });
        };

        var _getMdsDisplayName = function(id, mdsName) {
            var url = getMdsDisplayNameUrl() + '/' + id +  '/' + mdsName ;
            return httpGet(url);
        };

        var _disconnectDevice = function(id) {
            return $http.post(getBaseUrl() + '/' + id + '/disconnect-device', {});
        };

        var _import_csv = function(csv) {
            return $http.post(
                getBaseUrl() + "/csv",
                csv,
                {
                    headers: {'Content-Type': "text/plain;charset=utf-8"}
                }
            )
            .then(
                function successCallback(response) {
                    return response.status;
                },
                function errorCallback(response) {
                    return response.errors;
                }
             );
        };

        var _search = function(query) {
            var url = getBaseUrl() + '/search?query=' + encodeURIComponent(query);
            return httpGet(url);
        };

        var _getUsernamePolicy =  function() {
          return /^([a-zA-Z0-9-\._~\!\$&\'\(\)\*\+,;=%]{4,64})?$/;
        };

        var _getPasswordPolicy =  function() {
          return _getUsernamePolicy();
        };

        var _generatePassword = function(length) {
          var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOP123456789._~!$&'()*+,;=%";
          var pwd = "";
          for (var x = 0; x < length; x++) {
              var i = Math.floor(Math.random() * chars.length);
              pwd += chars.charAt(i);
          }
          return pwd;
        };

        var getTimeLabel = function(time) {
            if (time > 0) {
                return $translate('SECONDS').then(function (translation) {
                    return time + ' ' + translation;
                });
            } else {
                return $translate('UNLIMITED').then(function (translation) {
                    return translation;
                });
            }
        };

        return {
            getMdsDisplayName: _getMdsDisplayName,
            createUser : _create,
            getUser : _get,
            editUser: _edit,
            deleteUser: _delete,
            disconnectDevice: _disconnectDevice,
            importUsersFromCSV: _import_csv,
            search: _search,
            generatePassword: _generatePassword,
            getUsernamePolicy: _getUsernamePolicy,
            getPasswordPolicy: _getPasswordPolicy,
            getTimeLabel: getTimeLabel
        };

    }
})();

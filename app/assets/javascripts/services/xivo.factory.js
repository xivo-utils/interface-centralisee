(function() {
    'use strict';

    angular.module('xivo').factory('XivoFactory', XivoFactory);

    XivoFactory.$inject = [ '$http', '$rootScope', '$window' ];

    function XivoFactory($http, $rootScope, $window) {

        var getBaseUrl = function() {
            return $window.location.origin + '/api/1.0/xivo';
        };

        var getProxyBaseUrl = function() {
            return $window.location.origin + '/api/1.0/proxy';
        };

        var httpGet = function(url) {
            return $http.get(url).then(
                function(response) {
                    return response.data;
                });
        };

        var _create = function(template) {
            return $http.post(getBaseUrl(), template);
        };

        var _createProxy = function(template) {
            return $http.post(getProxyBaseUrl(), template);
        };

        var _get = function(id) {
            var url = getBaseUrl() + '/' + id;
            return httpGet(url);
        };

        var _list = function() {
            return httpGet(getBaseUrl());
        };

        var _synchronizeConfigFiles = function() {
            return httpGet(getBaseUrl() + '/synchronize_config_files');
        };

        return {
            createXivo : _create,
            createProxy: _createProxy,
            getXivo : _get,
            listXivos: _list,
            synchronizeConfigFiles: _synchronizeConfigFiles
        };
    }
})();

(function() {
    'use strict';

    angular.module('admin').factory('AdministratorFactory', AdministratorFactory);

    AdministratorFactory.$inject = [ '$http', '$rootScope', '$window' ];

    function AdministratorFactory($http, $rootScope, $window) {

        var getBaseUrl = function() {
            return $window.location.origin + '/api/1.0/administrators';
        };

        var httpGet = function(url) {
            return $http.get(url).then(
                function(response) {
                    return response.data;
                });
        };

        var _create = function(administrator) {
            return $http.post(getBaseUrl(), administrator)
            .then(
                function successCallback(response) {
                    return response.status;
                },
                function errorCallback(response) {
                    return response;
                }
             );
        };

        var _get = function(id) {
            var url = getBaseUrl() + '/' + id;
            return httpGet(url);
        };

        var _list = function() {
            return httpGet(getBaseUrl());
        };

        var _edit = function(id, administrator) {
            return $http.put(getBaseUrl() + '/' + id, administrator)
            .then(
                function successCallback(response) {
                    return response.status;
                },
                function errorCallback(response) {
                    return response;
                }
            );
        };

        var _delete = function(id) {
            return $http.delete(getBaseUrl() + '/' + id).then(
                function(response) {
                    return response.status;
                });
        };

        return {
            createAdministrator : _create,
            getAdministrator : _get,
            listAdministrators: _list,
            editAdministrator: _edit,
            deleteAdministrator: _delete
        };

    }
})();

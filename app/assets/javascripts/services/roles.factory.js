(function() {
    'use strict';

    angular.module('roles').factory('RoleFactory', RoleFactory);

    RoleFactory.$inject = [ '$http', '$rootScope', '$window' ];

    function RoleFactory($http, $rootScope, $window) {

        var getBaseUrl = function() {
            return $window.location.origin + '/api/1.0/roles';
        };

        var getEmptyRoleUrl = function() {
            return $window.location.origin + "/api/1.0/emptyRole";
        };

        var getMyselfUrl = function() {
            return $window.location.origin + "/api/1.0/myself";
        };

        var httpGet = function(url) {
            return $http.get(url).then(
                function(response) {
                    return response.data;
                });
        };

        var _create = function(role) {
            return $http.post(getBaseUrl(), role).then(
                function(response) {
                    return response.status;
                });
        };

        var _get = function(id) {
            var url = getBaseUrl() + '/' + id;
            return httpGet(url);
        };

        var _getEmpty = function() {
            var url = getEmptyRoleUrl();
            return httpGet(url);
        };

        var _myself = function() {
            var url = getMyselfUrl();
            return httpGet(url);
        };

        var _list = function() {
            return httpGet(getBaseUrl());
        };

        var _update = function(role) {
            return $http.put(getBaseUrl() + '/' + role.id, role).then(
                function(response) {
                    return response.status;
                });
        };

        var _remove = function(id) {
            return $http.delete(getBaseUrl() + '/' + id).then(
                function(response) {
                    return response.status;
                });
        };

        return {
            create : _create,
            get: _get,
            getEmpty: _getEmpty,
            myself: _myself,
            list: _list,
            update: _update,
            remove: _remove
        };

    }
})();

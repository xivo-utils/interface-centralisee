angular.module('roles')
.controller('RoleAction', function($scope, $translate, $window, RoleFactory) {

    $scope.role = {};

    $scope.getEmptyRole = function() {
        RoleFactory.getEmpty().then(function(response) { $scope.processRole(response); });
    };

    $scope.initCreate = function() {
        console.log('Init create');
        $scope.getEmptyRole();
    };

    $scope.initEdit = function() {
        RoleFactory.get($window.location.pathname.split('/').pop())
            .then(function(response) { $scope.processRole(response); });
    };

    $scope.init = function(name) {
        $scope[name]();
    };

    $scope.processRole = function(response) {
        $scope.role = response;
        console.log($scope.role);
        _.forEach($scope.role.xivo, function(xivo) {
            _.forEach(['reading', 'creating', 'editing', 'deleting'], function(operation) {
                $scope.updateXivoCheck(xivo, operation);
            });
        });
    };

    $scope.toggleXivoCheck = function(xivo, operation){
        xivo[operation] = !xivo[operation];
        $scope.propagateToEntity(xivo, operation);
    };

    $scope.propagateToEntity = function(xivo, operation) {
        _.forEach(xivo.entities, function(entity) {
            entity.operations[operation] = xivo[operation];
        });
    };

    $scope.toggleEntityCheck = function(xivo, entity, operation) {
        entity.operations[operation] = !entity.operations[operation];
        $scope.updateXivoCheck(xivo, operation);
    };

    $scope.updateXivoCheck = function(xivo, operation) {
        if (_.every(xivo.entities, function(e) { return e.operations[operation] === true; })) { xivo[operation] = true; }
        else { if (_.every(xivo.entities, function(e) { return e.operations[operation] === false; })) {xivo[operation] = false;}
        else { _.unset(xivo, operation); } }
    };

    $scope.createRole = function(isValid) {
        if (!isValid) {
            return;
        }
        console.log("Creating role: ", $scope.role);
        RoleFactory.create($scope.role).then(function(response) {
            console.log(response);
            window.location.replace("/roles");
        });
    };

    $scope.updateRole = function(isValid) {
        if (!isValid) {
            return;
        }
        console.log("Updating role: ", $scope.role);
        RoleFactory.update($scope.role).then(function(response) {
            console.log(response);
            window.location.replace("/roles");
        });
    };

   $scope.toggle = function(scope) {
      scope.toggle();
    };

});

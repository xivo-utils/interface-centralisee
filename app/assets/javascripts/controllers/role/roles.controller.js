angular.module('roles')
.constant('_', window._)
.config(function ($translateProvider, $translatePartialLoaderProvider) {
    $translatePartialLoaderProvider.addPart('form');
    $translatePartialLoaderProvider.addPart('roles');
    $translateProvider.useLoader('$translatePartialLoader', {
        urlTemplate: '/assets/i18n/{part}-{lang}.json'
    });
    $translateProvider.registerAvailableLanguageKeys(['en','fr'], {
        'en_*': 'en',
        'fr_*': 'fr'
    });
    $translateProvider.preferredLanguage('fr');
    $translateProvider.fallbackLanguage(['fr']);
})
.controller('Role', function($scope, $translate, RoleFactory) {

    $scope.roles = [];

    $scope.refreshRoles = function() {
        RoleFactory.list().then(function(response) {
            $scope.roles = response;
        });
    };

    $scope.removeRole = function(id) {
        RoleFactory.remove(id).then(function(response) {
            $scope.refreshRoles();
        });
    };

    $scope.refreshRoles();

});

angular.module('users')
.controller('UsersSearchController',
    function($scope, $http, $window, $translate, $timeout, UserFactory, RoleFactory) {

    $scope.processErrors = function(response) {
        if(response.responseJSON === undefined)
            return;
        if(response.responseJSON.errors === undefined)
            $scope.error = response.responseJSON;
        else
            $scope.errors = response.responseJSON.errors;
        $scope.$apply();
    };

    $scope.updateUsers = function(searchPattern) {

        UserFactory.search(searchPattern).then(function(response) {
            $scope.users = response.items;
        });
    };

    $scope.init = function(searchPattern) {
        $scope.searchPattern = searchPattern;
        $scope.updateUsers(searchPattern);
         
        RoleFactory.myself().then(function(response) {
            $scope.myself = response;
        });
    };
     
    $scope.editButtonIsEnabled = function(user) {
        return !user.entity.xivo.isProxy && $scope.canEdit;
    };

    $scope.getEditRoute = function(user) {
        if (!user.entity.xivo.isProxy && user.id) {
            return "/users/edit/" + user.id;
        } else {
            return "#";
        }
    };

    $scope.getOperations = function(entity) {
        var roleXivo = _.find($scope.myself.xivo, function(x) { return x.xivoId === entity.xivo.id; });
        var roleEntity = _.find(roleXivo.entities, function(e) { return e.entityId === entity.id; });
        $scope.canEdit = roleEntity.operations.editing;
        return roleEntity.operations;
    };

    $scope.deleteUser = function(user) {
        if (!user.confirmDeletion) {
            user.confirmDeletion = true;
            $timeout(
                function() {
                    user.confirmDeletion = false;
                },
                5000
            );
            return;
        }

        user.inDeletion = true;
        var isXivo = !user.entity.xivo.isProxy;
        UserFactory.deleteUser(user.id, isXivo).then(function(response) {
            if (response == 204) {
                $scope.updateUsers($scope.entityId);
            }
            else {
                $scope.error = response;
            }
        });
    };

    $scope.disconnectDevice = function(user) {
        if (!user.confirmDisconnectDevice) {
            user.confirmDisconnectDevice = true;
            $timeout(
                function() {
                    user.confirmDisconnectDevice = false;
                },
                5000
            );
            return;
        }

        user.inDisconnectDevice = true;
        UserFactory.disconnectDevice(user.id).then(function(response) {
            user.confirmDisconnectDevice = false;
            user.inDisconnectDevice = false;
            if (response.status !== 200) {
                $scope.error = response;
            } else if (!response.data.deviceFound) {
                $translate('DISASSOCIATE_DEVICE_FAILED').then(function (translation) {
                  $scope.error = translation;
                });
            }
        });
    };
});
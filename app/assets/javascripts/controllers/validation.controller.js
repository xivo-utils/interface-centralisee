angular.module('validation')
.config(function ($translateProvider, $translatePartialLoaderProvider) {
    $translatePartialLoaderProvider.addPart('validation');
    $translateProvider.useLoader('$translatePartialLoader', {
        urlTemplate: '/assets/i18n/{part}-{lang}.json'
    });
    $translateProvider.registerAvailableLanguageKeys(['en','fr'], {
           'en_*': 'en',
           'fr_*': 'fr'
    });
    $translateProvider.preferredLanguage('fr');
    $translateProvider.fallbackLanguage(['fr']);
})
.controller('ValidationController', function($scope, $translate) {

});

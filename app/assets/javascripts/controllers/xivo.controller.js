angular.module('xivo')
.config(function ($translateProvider, $translatePartialLoaderProvider) {
    $translatePartialLoaderProvider.addPart('xivo');
})
.controller('XivoController', function($scope, XivoFactory) {
    XivoFactory.listXivos().then(function(response) {
        $scope.xivoList = response.items;
    });

    $scope.synchronizeConfigFiles = function() {
        XivoFactory.synchronizeConfigFiles();
    };
})
.controller('XivoCreationController', function($scope, $window, $modal, XivoFactory) {

    $scope.submitXivoCreation = function() {
        if ($scope.xivo.configure)
        {
            $scope.confirmModal().result.then(function() {
                $scope.createXivo($scope.xivo);
            });
        }
        else
        {
            $scope.createXivo($scope.xivo);
        }
    };

    $scope.confirmModal = function() {
        return $modal.open({
            templateUrl: 'confirmModal',
            windowClass: 'modal-warning',
            controller: function ($scope, $modalInstance) {
                $scope.ok = function () {
                    $modalInstance.close();
                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });
    };

    var submitWithConfigBtn = angular.element(document.querySelector("button#create-and-config-xivo"));
    var submitWithoutConfigBtn = angular.element(document.querySelector("button#create-xivo-without-config"));
    var getSubmitBtn = function () {
        if ($scope.xivo.configure) {
            return submitWithConfigBtn;
        } else {
            return submitWithoutConfigBtn;
        }
    };

    $scope.createXivo = function(xivo) {
        getSubmitBtn().button("loading");
        XivoFactory.createXivo(JSON.stringify(xivo)).then(
            function(json) {
                if(json.length > 0) {
                    var msg = "Notes :\n";
                    for(var i=0; i<json.length; i++) {
                        msg += "- " + json[i] + "\n";
                    }
                    $window.alert(msg);
                }

                $window.location.replace("/xivo");
             },
            function(response) {
                $scope.error = response.data;
                getSubmitBtn().button("reset");
            }
        );
    };

    $scope.updateXivoContextName = function() {
        $scope.xivo.contextName = $scope.contextPrefix +
        _.chain($scope.xivo.name).toLower().snakeCase().deburr().value();   
        };


    $scope.xivo = {
        linkedXivo: [],
        configure: true
    };

    $scope.contextPrefix = 'to_';
})

.controller('ProxyCreationController', function($scope, $window, XivoFactory, EntityFactory) {
    $scope.createProxy = function(proxy) {
        XivoFactory.createProxy(JSON.stringify(proxy)).then(
            function() {
                $window.location.replace("/xivo");
             },
            function(response) {
                $scope.error = response.data;
            }
        );
    };

    $scope.submitProxyCreation = function() {
        $scope.createProxy($scope.proxy);
    };

    $scope.updateProxyContextName = function() {
        $scope.proxy.contextName = $scope.contextPrefix +
        _.chain($scope.proxy.name).toLower().snakeCase().deburr().value();
    };

    $scope.contextPrefix = 'to_';

    $scope.proxy = {
        intervals: [{routing_mode: EntityFactory.getDefaultRoutingMode().code}]
    };

    $scope.addInterval = function() {
        $scope.proxy.intervals.push({routing_mode: EntityFactory.getDefaultRoutingMode().code});
    };

    $scope.dropInterval = function(index) {
        $scope.proxy.intervals.splice(index, 1);
    };


    $scope.updateMode = function(interval, mode) {
        interval.routing_mode = mode.code;
    };

    $scope.showDirectNumber = function(interval) {
        return interval.routing_mode === EntityFactory.getRoutingModeWithDirectNo().code ||
            interval.routing_mode === EntityFactory.getRoutingModeWithCustomizedDirectNo().code;
    };

    $scope.showLastDirectNumber = function(interval) {
        return interval.routing_mode === EntityFactory.getRoutingModeWithCustomizedDirectNo().code;
    };

    var isNumber = function(str) {
        return str !== "" && !isNaN(str);
    };

    $scope.lastDirectNumber = function(interval) {
        if (!$scope.showLastDirectNumber(interval) || !isNumber(interval.start) || !isNumber(interval.end) ||
            !isNumber(interval.direct_number)) {
            return "";
        }

        var num = parseInt(interval.direct_number, 10) + parseInt(interval.end, 10) - parseInt(interval.start, 10) + "";
        while (interval.direct_number.length > num.length) {
            num = "0" + num;
        }
        return num;
    };

    $scope.getModeName = function(code) {
        var index = _.findIndex($scope.routingModes, ['code', code]);
        return $scope.routingModes[index].name;
    };


    $scope.init = function() {
        $scope.routingModes = EntityFactory.getRoutingModes();
    };

    $scope.init();
});

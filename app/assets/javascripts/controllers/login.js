angular.module('login', [])
.controller('LoginController', function($scope) {
    $scope.doLogin = function() {
        $.ajax({
            type : "POST",
            contentType: "application/json",
            data: JSON.stringify({
                login: $scope.login,
                password: $scope.password
            }),
            url : window.location.origin + '/api/1.0/login',
            success: function(response) {
                         window.location.replace("/");
                     },
            error: function(response) {
                        $scope.error = response.responseJSON;
                        $scope.$apply();
                    },
        });
    };
});

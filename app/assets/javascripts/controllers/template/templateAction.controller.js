angular.module('templates')
.controller('TemplateAction', function($scope, $translate, $window, TemplateFactory, XivoFactory, EntityFactory) {

    $scope.refreshEntities = function() {
        EntityFactory.listEntities().then(function(response) {
            $scope.entitiesList = _.filter(response.items, function(entity) {
                return _.map($scope.template.xivos).indexOf(entity.xivo.id) !== -1;
            });
            $scope.template.entities = _.intersection($scope.template.entities, _.map($scope.entitiesList, 'combinedId'));
            $scope.refreshSelectedEntities();
            $scope.template.intervals =$scope.dispatchIntervals($scope.template.intervals, $scope.selectedEntities);
        });
    };

    $scope.refreshSelectedEntities = function() {
        $scope.selectedEntities = _.intersectionWith($scope.entitiesList, $scope.template.entities,
            function(arrVal, othVal) {
                return arrVal.combinedId === othVal;
            });
    };

    $scope.dispatchIntervals = function(selectedIntervalsIds, selectedEntities) {
        var result = {};
        _.forEach(selectedEntities, function(value) {
            var intervalsSelectedInEntity = _.filter(value.intervals,
                function(interval) {
                    return _.includes(selectedIntervalsIds, interval.id);
                });
            if (intervalsSelectedInEntity.length > 0) {
                result[value.combinedId] = _.map(intervalsSelectedInEntity, 'id');
            }
        });
        return result;
    };

    $scope.selectRingingTime = function(time) {
        $scope.template.ringingTime = time;
        if ($scope.template.ringingTime > 0) {
            $translate('SECONDS').then(function (translation) {
              $scope.ringingTimeLabel = $scope.template.ringingTime + ' ' + translation;
            });
        } else {
            $translate('UNLIMITED').then(function (translation) {
                $scope.ringingTimeLabel = translation;
            });
        }
    };

    $scope.createTemplate = function() {
        $scope.template.intervals = $scope.mergeIntervals($scope.template.intervals);
        console.log("Creating template: ", $scope.template);
        TemplateFactory.createTemplate($scope.template).then(function(response) {
            console.log(response);
            window.location.replace("/templates");
        });
    };

    $scope.updateTemplate = function() {
        $scope.template.intervals = $scope.mergeIntervals($scope.template.intervals);
        console.log("Updating template: ", $scope.template);
        TemplateFactory.updateTemplate($scope.template).then(function(response) {
            console.log(response);
            window.location.replace("/templates");
        });
    };

    $scope.mergeIntervals = function(intervals) {
        console.log('Intervals: ', intervals);
        return _.chain(intervals).values().flatten().value();
    };

    $scope.getId = function(location) {
        var pathElems = location.pathname.split('/');
        return pathElems[pathElems.length-1];
    };

    $scope.initForm = function() {
        $scope.ringTimes = [5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60];
        $scope.entitiesList = [];
        $scope.selectedEntities = [];
        $scope.template = {};
    };

    $scope.initCreate = function() {
        console.log('Init create');
        $scope.initForm();
        $scope.template = {
            name: "",
            peerSipName: "auto",
            callerIdMode: "incomingNo",
            voiceMailEnabled: false,
            voiceMailNumberMode: "short_number",
            xivos: [],
            entities: []
        };
        XivoFactory.listXivos().then(function(response) {
            $scope.xivosList = response.items;
            $scope.isXivo = $scope.xivosList.filter(function(elem) {return !elem.isProxy;}); 
        });
        $scope.selectRingingTime(20);
    };

    $scope.initEdit = function() {
        console.log('Init edit');
        $scope.initForm();
        XivoFactory.listXivos().then(function(response) {
            $scope.xivosList = response.items;
            $scope.isXivo = $scope.xivosList.filter(function(elem) {return !elem.isProxy;}); 
            TemplateFactory.getTemplate($scope.getId($window.location)).then(function(response) {
                console.log('Template to edit: ', response);
                $scope.template = response;
                $scope.refreshEntities();
                $scope.selectRingingTime(response.ringingTime);
            });
        });
    };

    $scope.init = function(name) {
        $scope[name]();
    };

});

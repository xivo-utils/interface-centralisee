angular.module('templates')
.constant('_', window._)
.config(function ($translateProvider, $translatePartialLoaderProvider) {
    $translatePartialLoaderProvider.addPart('form');
    $translatePartialLoaderProvider.addPart('templates');
    $translateProvider.useLoader('$translatePartialLoader', {
        urlTemplate: '/assets/i18n/{part}-{lang}.json'
    });
    $translateProvider.registerAvailableLanguageKeys(['en','fr'], {
        'en_*': 'en',
        'fr_*': 'fr'
    });
    $translateProvider.preferredLanguage('fr');
    $translateProvider.fallbackLanguage(['fr']);
})
.controller('Template', function($scope, $translate, TemplateFactory) {

    $scope.templates = [];

    $scope.refreshTemplates = function() {
        TemplateFactory.listTemplates().then(function(response) {
            $scope.templates = response;
        });
    };

    $scope.deleteTemplate = function(id) {
        TemplateFactory.deleteTemplate(id).then(function(response) {
            $scope.refreshTemplates();
        }, function(error) {
          $scope.error = $translate.instant('TEMPLATE_DELETE_FAILED');
        });
    };

    $scope.refreshTemplates();

});

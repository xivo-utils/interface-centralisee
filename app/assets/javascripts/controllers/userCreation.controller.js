angular.module('users')
.controller('UserCreationController', function($scope, EntityFactory, UserFactory, TemplateFactory, XivoFactory) {

    $scope.externalNumberError = false;

    var submitBtn = angular.element(document.querySelector("button#create-user"));

    $scope.createUser = function() {
        $scope.error = undefined;
        submitBtn.button("loading");
        UserFactory.createUser(JSON.stringify(_($scope.user).omit(_.isNull)), $scope.entity.isXivoEntity).then(function(response) {
            if (response == 201) {
                window.location.replace("/users?entityId="+$scope.user.entityCId+"&intnb=" + $scope.user.internalNumber);
            }
            else {
                $scope.error = response.data;
                submitBtn.button("reset");
            }
        });
    };

    $scope.updateFromModel = function() {
        $scope.template = _.find($scope.templates, function(t) {
            return t.id === $scope.user.templateId;
        });
        $scope.updateIntervals($scope.entity.id);
        $scope.user.externalNumber = $scope.template.customCallerId;
        $scope.user.ringingTime = $scope.template.ringingTime;
        $scope.user.peerSipName = $scope.template.peerSipName;
        UserFactory.getTimeLabel($scope.user.ringingTime).then(function(timeLabel) {
            $scope.ringingTimeLabel = timeLabel;
        });
        $scope.user.callerIdMode = $scope.template.callerIdMode;
    };

    $scope.intervals = [];
    $scope.updateIntervals = function(entityId) {
        $scope.intervals = _.filter($scope.entity.intervals, function(i) {
            return (_.findIndex($scope.template.intervals, function(o) { return o === i.id; }) > -1);
        });
    };

    $scope.updateAvailableNumbers = function(resetNumber) {
        EntityFactory.getIntervalAvailableNumbers($scope.entity.combinedId, $scope.user.intervalId).then(function(response) {
            $scope.container.availableNumbers = response.items;
            if (resetNumber) $scope.user.internalNumber = null;
        });
    };

    $scope.mailRequired = function() {
        return $scope.template !== undefined && $scope.template.voiceMailSendEmail;
    };

    $scope.ctiRequired = function() {
        return $scope.user !== undefined && ($scope.user.peerSipName === "webrtc" || $scope.user.peerSipName === "ua");
    };

    $scope.init = function(entityId) {
        $scope.user = {};
        $scope.container = {};
        $scope.intervals = [];
        EntityFactory.getEntity(entityId).then(function(response) {
            $scope.entity = response;
            $scope.entity.xivoId = $scope.entity.xivo.id;
            $scope.entity.isXivoEntity = !$scope.entity.xivo.isProxy;
            $scope.user.entityCId = $scope.entity.combinedId;
            if($scope.entity.isXivoEntity) {
                UserFactory.getMdsDisplayName($scope.entity.xivoId, $scope.entity.mdsName).then(function(response) {
                    $scope.mds = response;
                });
            }
        });
        TemplateFactory.listForEntity(entityId).then(function(response) {
            $scope.templates = response;
        });
    };
});

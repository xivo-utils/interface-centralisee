angular.module('users')
.controller('UsersController',
    function($scope, $http, $window, $translate, $timeout, UserFactory, EntityFactory, RoleFactory) {
    
    $scope.processErrors = function(response) {
        if(response.responseJSON === undefined)
            return;
        if(response.responseJSON.errors === undefined)
            $scope.error = response.responseJSON;
        else
            $scope.errors = response.responseJSON.errors;
        $scope.$apply();
    };

    $scope.updateUsers = function(entityId) {
        EntityFactory.getCachedEntityUsers(entityId).then(function(response) {
            $scope.users = response.items;
            $scope.ghostIds = response.ghostIds;

            EntityFactory.getUpdatedEntityUsers(entityId).then(function(response) {
                $scope.users = response.items;
                $scope.ghostIds = response.ghostIds;

                var urlParams = new URLSearchParams($window.location.search);
                var internalNumber = urlParams.get('intnb');
                if (internalNumber) {
                    var newlyCreatedUser = _.find($scope.users, function(u) {
                        return u.internalNumber == internalNumber;
                    });
                    if (newlyCreatedUser) {
                        $scope.provisioning = {
                            user: newlyCreatedUser.firstName+" "+newlyCreatedUser.lastName,
                            number: newlyCreatedUser.provisioningNumber
                        };
                    }
                }
            });
        });
    };

    $scope.init = function(entityId) {
        $scope.entityId = entityId;
        $scope.updateUsers(entityId);
        EntityFactory.getEntity(entityId).then(function(response) {
            $scope.entity = response;
            $scope.xivo = response.xivo;
            $scope.entity.isXivoEntity = !$scope.entity.xivo.isProxy;
            RoleFactory.myself().then(function(response) {
                $scope.myself = response;
                $scope.canCreate = $scope.getCanCreate();
                $scope.canEdit = $scope.getCanEdit();
                $scope.canDelete = $scope.getCanDelete();
            });
        });   
    };


    $scope.editButtonIsEnabled = function(user) {
        return !user.entity.xivo.isProxy && $scope.canEdit;
    };

    $scope.getEditRoute = function(user) {
        if (!user.entity.xivo.isProxy && user.id) {
            return "/users/edit/" + user.id;
        } else {
            return "#";
        }
    };

    $scope.getOperations = function() {
        var roleXivo = _.find($scope.myself.xivo, function(x) { return x.xivoId === $scope.entity.xivo.id; });
        var roleEntity = _.find(roleXivo.entities, function(e) { return e.entityId === $scope.entity.id; });
        return roleEntity.operations;
    };

    $scope.getCanCreate = function() {
        return $scope.getOperations().creating;
    };
    $scope.getCanEdit = function() {
        return $scope.getOperations().editing;
    };

    $scope.getCanDelete = function(entity) {
        return $scope.getOperations().deleting;
    };

    $scope.upload = function() {
        if ($('#files')[0].files.length === 0) {
            return;
        }
        var file = $('#files')[0].files[0];
        var fileReader = new FileReader();

        fileReader.onload = function(e) {
          UserFactory.importUsersFromCSV(fileReader.result).then(function(response) {
              if (response == 201) {
                  window.location.replace("/administrators");
              }
              else {
                  $scope.error = response;
              }
          });
        };
        fileReader.readAsText(file);
    };

    $scope.deleteUser = function(user) {
        if (!user.confirmDeletion) {
            user.confirmDeletion = true;
            $timeout(
                function() {
                    user.confirmDeletion = false;
                },
                5000
            );
            return;
        }

        user.inDeletion = true;
        var isXivo = !user.entity.xivo.isProxy;
        UserFactory.deleteUser(user.id, isXivo).then(function(response) {
            if (response == 204) {
                $scope.updateUsers($scope.entityId);
            }
            else {
                $scope.error = response;
            }
        });
    };

    $scope.disconnectDevice = function(user) {
        if (!user.confirmDisconnectDevice) {
            user.confirmDisconnectDevice = true;
            $timeout(
                function() {
                    user.confirmDisconnectDevice = false;
                },
                5000
            );
            return;
        }

        user.inDisconnectDevice = true;
        UserFactory.disconnectDevice(user.id).then(function(response) {
            user.confirmDisconnectDevice = false;
            user.inDisconnectDevice = false;
            if (response.status !== 200) {
                $scope.error = response;
            } else if (!response.data.deviceFound) {
                $translate('DISASSOCIATE_DEVICE_FAILED').then(function (translation) {
                  $scope.error = translation;
                });
            }
        });
    };

    $scope.deleteStaleUsers = function() {
        TemplateFactory.deleteStaleUsers().then(function(response) {
            $scope.usersForEntity();
        });
    };
});

function getAvailableNumbers($scope, EntityFactory, replaceNum) {
    if(replaceNum === undefined)
        replaceNum = true;
    if(typeof($scope.user) === 'undefined' || typeof($scope.user.entityCId) === 'undefined')
        return;
    else {
        EntityFactory.getEntityAvailableNumbers($scope.user.entityCId).then(function(response) {
            $scope.container.availableNumbers = response.items;
            if(replaceNum)
                $scope.user.internalNumber = $scope.container.availableNumbers[0];
        });
    }
}

angular.module('admin')
.config(function ($translateProvider, $translatePartialLoaderProvider) {
    $translatePartialLoaderProvider.addPart('administrators');
    $translatePartialLoaderProvider.addPart('roles');
})
.controller('AdminController', function($scope, $translate, AdministratorFactory) {
    $scope.loadAdministratorsList = function()
    {
        AdministratorFactory.listAdministrators().then(function(response) {
            $scope.admins = response.items;
        });
    };
    $scope.loadAdministratorsList();

    $scope.boolText = function(bool) {
        if(bool) return 'Oui';
        else return 'Non';
    };

    $scope.commaSeparatedNames = function(entities) {
        var res = "";
        for(var i=0; i<entities.length; i++) {
            res += entities[i].name + ", ";
        }
        return res.substring(0, res.length - 2);
    };

    $scope.deleteAdmin = function(id) {
        AdministratorFactory.deleteAdministrator(id).then(function(response) {
            $scope.loadAdministratorsList();
        });
    };
})
.controller('AdminCreationController', function ($scope, AdministratorFactory) {

    $scope.createAdmin = function(isValid) {
        if(isValid) {
            console.log('Form valid, creating admin', $scope.admin);
            AdministratorFactory.createAdministrator(JSON.stringify($scope.admin)).then(function(response) {
                if (response == 201) {
                    window.location.replace("/administrators");
                }
                else {
                    $scope.error = response.data;
                }
            });
        } else {
            console.log('Form is not valid');
        }
    };

    $scope.emptyPasswordFields = function() {
        $scope.admin.password = "";
        $scope.admin.confirmPassword = "";
    };

    $scope.admin = {
        superAdmin: false,
        ldap: false,
        entityIds: []
    };

    $scope.configHandler = $scope.$apply;
})
.controller('AdminEditionController', function ($scope, $timeout, AdministratorFactory) {
    $scope.hideMyLogin = {value: true};

    function setRoleIds(admin) {
        admin.roleIds = [];
        for(var i=0; i<admin.roles.length; i++) {
            admin.roleIds.push(admin.roles[i].id);
        }
    }

    $scope.init = function(adminId) {
        AdministratorFactory.getAdministrator(adminId).then(function(response)
        {
            $scope.admin = response;
            setRoleIds($scope.admin);
            $scope.admin.confirmPassword = response.password;
        });
    };

    $scope.editAdmin = function(isValid) {
        if(! isValid) {
            console.log("Invalid form values");

            return;
        }
        AdministratorFactory.editAdministrator($scope.admin.id, JSON.stringify($scope.admin)).then(function(response) {
            console.log("Updating administrator");
            if (response == 204) {
                window.location.replace("/administrators");
            }
            else {
                $scope.error = response.data;
            }
        });
    };

    $scope.configHandler = function() {
        $timeout(function(){$scope.hideMyLogin.value=false;}, 10);
    };
})
.controller('AdminFormController', function($scope, RoleFactory) {
    $scope.listRoles = function() {
        RoleFactory.list().then(function(response) {
            $scope.roles = response;
            $scope.configHandler();
        });
    };

    $scope.listRoles();
});

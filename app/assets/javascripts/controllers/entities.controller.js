angular.module('entities')
.config(function ($translateProvider, $translatePartialLoaderProvider) {
    $translatePartialLoaderProvider.addPart('entities');
})
.controller('EntitiesController', function($scope, $window, EntityFactory, RoleFactory) {

    $scope.init = function(xivoId) {
        EntityFactory.listEntities().then(function(response) {
            $scope.entities = _.filter(response.items, {'xivo': {'id': _.toInteger(xivoId)}});
            $scope.xivo = $scope.entities[0].xivo;
        });
        RoleFactory.myself().then(function(response) {
            $scope.myself = response;
        });
    };

    $scope.getOperation = function(entity) {
        var roleXivo = _.find($scope.myself.xivo, function(x) { return x.xivoId === entity.xivo.id; });
        var roleEntity = _.find(roleXivo.entities, function(e) { return e.entityId === entity.id; });
        return roleEntity.operations;
    };

    $scope.canEdit = function(entity) {
        return $scope.getOperation(entity).editing;
    };

    $scope.canDelete = function(entity) {
        return $scope.getOperation(entity).deleting;
    };

    $scope.deleteEntity = function(combinedId) {
        var doIt = $window.confirm("Ceci supprimera tous les utilisateurs appartenant à l'entité. Etes-vous sûr de vouloir continuer ?");
        if(!doIt)
            return;

        EntityFactory.deleteEntity(combinedId).then(function(response) {
            if (response == 204) {
                $scope.loadEntitiesList();
            }
            else {
                $scope.error = response.data;
            }
        });
    };
})

.controller('EntityCreationController', function($scope, $window, $http, EntityFactory, XivoFactory) {
    
    $scope.entity = {
        intervals: [{routing_mode: EntityFactory.getDefaultRoutingMode().code}]
    };

    $scope.configHandler = function() { return undefined; };

    var submitBtn = angular.element(document.querySelector("button#submit-entity"));

    $scope.createEntity = function(isValid) {
        if (!isValid) {
            return;
        }
        submitBtn.button("loading");
        $scope.error = undefined;
        EntityFactory.createEntity(JSON.stringify($scope.entity)).then(function(response) {
            if (response == 201) {
                window.location.replace("/entities?xivoId=" + $scope.entity.xivoId);
            }
            else {
                $scope.error = response.data;
                submitBtn.button("reset");
            }
        });
    };

    $scope.init = function(xivoId) {
        XivoFactory.getXivo(xivoId).then(function(response) {
            $scope.xivo = response;
            $scope.entity.xivoId = $scope.xivo.id;
            $scope.isXivoEntity = !response.isProxy;
            if ($scope.isXivoEntity) {
                EntityFactory.getMdsListForSelectedXivo(xivoId).then(function(response) {
                    $scope.mdsList = response;
                    $scope.preSelectedMds = $scope.getPreSelectedMds($scope.mdsList);
                    $scope.entity.mdsName=$scope.preSelectedMds.name;
                });
            }
        });
    };

    $scope.getPreSelectedMds = function(mdsList) {
        return _.find(mdsList, function(mds) { return mds.name === 'default'; });
    };
    
    $scope.setPreselectedMds = function (mds) {
        $scope.preSelectedMds = mds;
    };
})

.controller('EntityEditionController', function($scope, $window, EntityFactory, $translate) {
    
    $scope.init = function(entityCombinedId) {
        EntityFactory.getEntity(entityCombinedId).then(function(response) {
            $scope.entity = response;
            $scope.entity.xivoId = $scope.entity.xivo.id;
            $scope.xivo = $scope.entity.xivo;
            $scope.isXivoEntity = !$scope.entity.xivo.isProxy;

            if ($scope.isXivoEntity) {
                EntityFactory.getMdsListForSelectedXivo($scope.entity.xivoId).then(function(response) {
                    $scope.mdsList = response;
                    $scope.preSelectedMds = $scope.getPreSelectedMds($scope.mdsList);
                });
            }
        });
    };
    
    $scope.getPreSelectedMds = function(mdsList) {
        return _.find(mdsList, function(mds) { return mds.name === $scope.entity.mdsName; });
    };

    $scope.setPreselectedMds = function (mds) {
      $scope.preSelectedMds = mds;
    };

    var submitBtn = angular.element(document.querySelector("button#submit-entity"));

    $scope.editEntity = function(isValid) {
        if (!isValid) {
            return;
        }
        submitBtn.button("loading");
        $scope.error = undefined;
        EntityFactory.editEntity($scope.entity.combinedId, JSON.stringify($scope.entity)).then(function(response) {
            if (response.status == 200) {
                if (response.data.success === true) {
                   window.location.replace("/entities?xivoId=" + $scope.entity.xivoId);
                } else {
                    $translate('ENTITY_UPDATE_FAILED').then(function (translation) {
                      $scope.error = translation.replace("ERROR", response.data.errorMessage);
                    });
                    submitBtn.button("reset");
                }
            }
            else {
                $scope.error = response.data;
            }
        });
    };
})

.controller('EntityFormController', function($scope, $window, $http, EntityFactory) {

    $scope.init = function() {
        $scope.listXivo();
        $scope.routingModes = EntityFactory.getRoutingModes();
    };

    $scope.updateMode = function(interval, mode) {
        interval.routing_mode = mode.code;
    };

    $scope.showDirectNumber = function(interval) {
        return interval.routing_mode === EntityFactory.getRoutingModeWithDirectNo().code ||
            interval.routing_mode === EntityFactory.getRoutingModeWithCustomizedDirectNo().code;
    };

    $scope.showLastDirectNumber = function(interval) {
        return interval.routing_mode === EntityFactory.getRoutingModeWithCustomizedDirectNo().code;
    };

    var isNumber = function(str) {
        return str !== "" && !isNaN(str);
    };

    $scope.lastDirectNumber = function(interval) {
        if (!$scope.showLastDirectNumber(interval) || !isNumber(interval.start) || !isNumber(interval.end) ||
            !isNumber(interval.direct_number)) {
            return "";
        }

        var num = parseInt(interval.direct_number, 10) + parseInt(interval.end, 10) - parseInt(interval.start, 10) + "";
        while (interval.direct_number.length > num.length) {
            num = "0" + num;
        }
        return num;
    };

    $scope.getModeName = function(code) {
        var index = _.findIndex($scope.routingModes, ['code', code]);
        return $scope.routingModes[index].name;
    };

    $scope.listXivo = function() {
        $http.get($window.location.origin + '/api/1.0/xivo').then(function(response) {
            $scope.xivoList = response.data.items;
        });
    };
    
    $scope.addInterval = function() {
        $scope.entity.intervals.push({routing_mode: EntityFactory.getDefaultRoutingMode().code});
    };

    $scope.dropInterval = function(index) {
        $scope.entity.intervals.splice(index, 1);
    };

    $scope.init();
});

angular.module('users')
.controller('UserEditionController', function($scope, $window, UserFactory, EntityFactory, TemplateFactory) {
    $scope.intervals = [];
    $scope.externalNumberError = false;

    var initTemplate = function() {
        if ($scope.user.templateId === parseInt($scope.user.templateId, 10)) {
            TemplateFactory.getTemplate($scope.user.templateId).then(function(response) {
                $scope.template = response;
                filterIntervalsFromTemplate();
            });
        } else {
            $scope.template = {voiceMailSendEmail: false, voiceMailDeleteAfterNotif: false};
        }
    };

    $scope.init = function(id) {
        UserFactory.getUser(id).then(function(response) {
            $scope.user = response;
            $scope.selectRingingTime(response.ringingTime);
            $scope.entity = response.entity;
            $scope.entity.isXivoEntity = !$scope.entity.xivo.isProxy;
            $scope.intervals = $scope.entity.intervals;
            $scope.user.entityCId = $scope.entity.combinedId;
            $scope.updateAvailableNumbers(false);
            initTemplate();
        });
    };

    var submitBtn = angular.element(document.querySelector("button#submit-user"));

    $scope.editUser = function() {
        $scope.error = undefined;
        submitBtn.button("loading");
        UserFactory.editUser($scope.user.id, JSON.stringify(_($scope.user).omit(_.isNull)), $scope.entity.isXivoEntity).then(function(response) {
            if (response == 204) {
                window.location.replace("/users?entityId="+$scope.user.entityCId);
            }
            else {
                $scope.error = response.data;
                submitBtn.button("reset");
            }
        });
    };

    $scope.selectRingingTime = function(time) {
        $scope.user.ringingTime = time;
        UserFactory.getTimeLabel(time).then(function(timeLabel) {
            $scope.ringingTimeLabel = timeLabel;
        });
    };

    $scope.ctiRequired = function() {
        return $scope.user !== undefined && ($scope.user.peerSipName === "webrtc" || $scope.user.peerSipName === "ua");
    };

    $scope.mailRequired = function() {
        return $scope.user !== undefined && $scope.user.voicemail.enabled && $scope.user.voicemail.sendEmail;
    };

    $scope.numberModeRequired = function() {
        return $scope.user !== undefined && $scope.user.voicemail.enabled && !$scope.user.voicemail.numberMode;
    };

    $scope.customNumberRequired = function() {
        return $scope.user !== undefined && $scope.user.voicemail.enabled && $scope.user.voicemail.numberMode === 'custom';
    };

    $scope.configHandler = function() {
        $scope.getUser(userId);
    };

    $scope.updateAvailableNumbers = function(resetNumber) {
        EntityFactory.getIntervalAvailableNumbers($scope.entity.combinedId, $scope.user.intervalId).then(function(response) {
            $scope.container.availableNumbers = response.items;
            if (resetNumber) $scope.user.internalNumber = null;
        });
    };
    
    function filterIntervalsFromTemplate() {
        $scope.intervals = $scope.intervals.filter(function (interval) {
            return $scope.template.intervals.includes(interval.id);
        });
    }
});

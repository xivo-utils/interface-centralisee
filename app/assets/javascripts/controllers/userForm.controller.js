angular.module('users')
.controller('UserFormController', function($scope, EntityFactory, UserFactory) {

    $scope.updateExternalNumberForInternalNumber = function() {
    
        if ($scope.user.internalNumber.length > 0 && $scope.userForm && $scope.userForm.externalNumber.$pristine) {
            EntityFactory.getExternalNumberForInternalNumber($scope.entity.combinedId, $scope.user.internalNumber).then(function(response) {
                $scope.user.externalNumber = response.externalNumber;
            });
        }
        
    };

    var getIntervalForUser = function() {
        if (!$scope.user) {
            return undefined;
        } else {
            return  _.find($scope.intervals, function(i) { return i.id === $scope.user.intervalId; } );
        }
    };

    $scope.externalNumberFromTo = function () {
        var interval =  getIntervalForUser();
        if (interval !== undefined && interval.start_ext_num !== undefined && interval.end_ext_num !== undefined) {
            return '(' + interval.start_ext_num + ' - ' + interval.end_ext_num + ')';
        } else {
            return "";
        }
    };

    $scope.showInternalNumber = function () {
        if($scope.entity !== undefined) {
            $scope.intervals = $scope.entity.intervals;
        }
        var interval =  getIntervalForUser();

        return interval !== undefined;
    };

    $scope.showExternalNumber = function () {
        var interval =  getIntervalForUser();
        return interval !== undefined && interval.routing_mode === 'with_customized_direct_number';
    };

    $scope.templateIsDefined = function() {
        return $scope.user !== undefined && $scope.user.templateId !== undefined;
    };

    $scope.externalNumberErrMsg = "";

    $scope.checkExternalNumber = function () {
        $scope.externalNumberError = false;
        $scope.externalNumberErrMsg = "";
        if (!$scope.externalNumberRegexObj.test($scope.user.externalNumber)) {
            return;
        }

        var entityCId =  ($scope.entity !== undefined) ? $scope.entity.combinedId : $scope.user.entity.combinedId;

        EntityFactory.checkExternalNumber(entityCId, $scope.user.intervalId,
            $scope.user.externalNumber, $scope.user.id).then(function(response) {
            $scope.externalNumberError = !response.available;
            $scope.externalNumberErrMsg = response.message;
        });
    };

    $scope.renewPassword = function () {
        $scope.user.ctiPassword = UserFactory.generatePassword(8);
    };

    $scope.selectRingingTime = function(time) {
        $scope.user.ringingTime = time;
        UserFactory.getTimeLabel(time).then(function(timeLabel) {
            $scope.ringingTimeLabel = timeLabel;
        });
    };

    $scope.init = function() {
        $scope.ringTimes = [5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60];
        $scope.$parent.container = {};
        EntityFactory.listEntities().then(function(response) {
            $scope.entities = response.items;
        });
    };

    $scope.init();

    $scope.externalNumberRegex = /^\d+$/;
    $scope.externalNumberRegexObj = new RegExp($scope.externalNumberRegex);
    $scope.handleExternalNumberPattern = (function() {
        return {
          test: function(value) { return !$scope.showExternalNumber() || $scope.externalNumberRegexObj.test(value); }
        };
    })();

    $scope.ctiLoginRegex = UserFactory.getUsernamePolicy();
    $scope.ctiPasswordRegex = UserFactory.getPasswordPolicy();
});

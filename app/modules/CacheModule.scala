package modules

import com.google.inject.AbstractModule
import services.cache.{CacheScheduler, CacheSynchronizer}
import play.api.libs.concurrent.AkkaGuiceSupport

class CacheModule extends AbstractModule with AkkaGuiceSupport {
  def configure() = {
    bindActor[CacheSynchronizer]("cache-synchronizer-actor")
    bind(classOf[CacheScheduler]).asEagerSingleton()
  }
}

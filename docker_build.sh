#!/usr/bin/env bash
set -e

POSTGRES_95_PORT=$(pg_lsclusters | grep 9.5 | awk '{print $3}')
if ! [[ "$POSTGRES_95_PORT" =~ ^[0-9]*$ ]]; then
	echo "Unable to get port of Postgresql version 9.5, exiting"
	exit 1
fi

export TEST_DB_DEFAULT_URL="jdbc:postgresql://localhost:${POSTGRES_95_PORT}/icx_test"
echo "Exported variable TEST_DB_DEFAULT_URL with value '$TEST_DB_DEFAULT_URL'"

export TEST_LDAP_PORT=3389
echo "Exported variable TEST_LDAP_PORT with value '$TEST_LDAP_PORT'"

sbt clean test docker:publishLocal

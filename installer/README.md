# GCU installer package `gcu-installer`

## Directory content

| directory      | description                                                                            |
|----------------|----------------------------------------------------------------------------------------|
| debian         | sources for debian package `gcu-installer`                                             |
| docker-compose | docker-compose file for GCU, packaged into `gcu-installer`                             |
| icdu_nginx     | sources for building GCU `nginx` docker container                                      |
| routing-server | configuration for GCU docker container `routing_server`, packaged into `gcu-installer` |

## How to manually build `gcu-installer` package

Before building, you must check you have the necessaries built tools: 

``` bash
sudo apt-get install devscripts debhelper
```

Then to manually build the package, you need to issue the following command:

``` bash
cd ./debian
debuild -us -uc
```

It will generate the deb file in the parent folder of the current working directory. 
On the test environment, you can then install the package manually:

``` bash
dpkg -i gcu-installer_0.0.1_all.deb
```

## How to build nginx docker container

To build and publish dev version:

``` bash
docker build -t xivoxc/icdu_nginx:latestdev installer/icdu_nginx/docker
docker push xivoxc/icdu_nginx:latestdev
```

To build and publish production version:

``` bash
docker build -t xivoxc/icdu_nginx:latest installer/icdu_nginx/docker
docker push xivoxc/icdu_nginx:latest
```
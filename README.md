# Gestion Centralisée des Utilisateurs (GCU) (old: Interface Centralisée)

## Dev environment 
### Database

Ensure you will have uuid-ossp for new databases:
```sql
CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\";
```

Create two postgresql databases:
```sql
CREATE USER icx WITH PASSWORD 'icx';
CREATE DATABASE icx WITH OWNER icx;
CREATE DATABASE icx_test WITH OWNER icx;
```


You can specify the database URL for tests by exporting an environment variable TEST_DB_DEFAULT_URL, e.g.:
export TEST_DB_DEFAULT_URL="jdbc:postgresql://localhost:5436/icx_test"
You can also specify the LDAP port for tests by exporting an environment variable TEST_LDAP_PORT, e.g.:
export TEST_LDAP_PORT=3389

### SSH Key

You also need a ssh key:
ssh-keygen -t rsa -f ./conf/ssh_key

### Other prerequisites

1. installed docker

    - see https://docs.docker.com/engine/installation/linux/ubuntulinux/
    - verify by running: docker run hello-world

2. installed package ldapscripts


### Dependencies

- xivo-lib-restapi
- xivo-lib-ldap
- play-authentication (checkout tag `2.4-rc1`)


## Default password


The default login/password is admin/superpass. It is recommended to change it right after the installation.

## Massive import


To use the massive import feature, create CSV file with the separator "|" and the following header :
entity.displayName|firstName|lastName|internalNumber|externalNumber|mail|ctiLogin|ctiPassword

## Needed upgrade

- Angularjs 
- Upgrade play to play 2.8 ? Play auth is based on play 2.4

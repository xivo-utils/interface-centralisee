#!/bin/bash
set -e

function check_is_root() {
    if [ "$(id -u)" -ne "0" ]; then
        echo "Ce script doit être exécuté en root"
        exit -1
    fi;
}

cd /tmp
check_is_root
docker pull xivoxc/interface-centralisee
mkdir -p /etc/docker/interface-centralisee/xivo_config
wget https://gitlab.com/xivo-utils/interface-centralisee/raw/master/conf/application.conf
wget https://gitlab.com/xivo-utils/interface-centralisee/raw/master/conf/logback.xml
wget https://gitlab.com/xivo-utils/interface-centralisee/raw/master/conf/file_mapper
wget https://gitlab.com/xivo-utils/interface-centralisee/raw/master/conf/xivo_config/xivo_routage_agi.py
wget https://gitlab.com/xivo-utils/interface-centralisee/raw/master/conf/xivo_config/xivo_routage.conf
wget https://gitlab.com/xivo-utils/interface-centralisee/raw/master/conf/xivo_config/routage.conf
mv application.conf logback.xml file_mapper /etc/docker/interface-centralisee
mv xivo_routage_agi.py xivo_routage.conf routage.conf /etc/docker/interface-centralisee/xivo_config
apt-get update
apt-get install -y postgresql-9.1
sudo -u postgres psql -c "CREATE USER icx WITH PASSWORD 'icx'"
sudo -u postgres psql -c "CREATE DATABASE icx WITH OWNER icx"
mkdir -p /var/log/interface-centralisee
chown daemon:daemon /var/log/interface-centralisee
echo "Installation terminée. N'oubliez pas de créer une clef ssh sans mot de passe pour l'application, et de l'installer dans /etc/docker/interface-centralisee/ssh_key[.pub].
echo "Vous pouvez genérer la clef par la commande suivante: ssh-keygen -t rsa -f ~/.ssh/id_rsa"
Puis lancez l'application avec la commande suivante :
docker run -t -d --name=interface-centralisee --net host -v /etc/docker/interface-centralisee:/conf/ -v /var/log/interface-centralisee:/var/log/interface-centralisee xivo/interface-centralisee -Dconfig.file=/conf/application.conf -Dlogger.file=/conf/logback.xml -DapplyEvolutions.default=true"

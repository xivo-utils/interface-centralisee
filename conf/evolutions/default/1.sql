# --- !Ups
CREATE TABLE route (
    id SERIAL PRIMARY KEY,
    digits character varying(50) NOT NULL,
    prefix boolean DEFAULT false NOT NULL,
    regexp character varying(50) DEFAULT '(.*)$'::character varying NOT NULL,
    target character varying(50) DEFAULT '\1'::character varying NOT NULL,
    subroutine character varying(50) DEFAULT NULL::character varying,
    context character varying(50) DEFAULT 'to_pit_stop'::character varying NOT NULL,
    comment character varying(1000),
    UNIQUE(digits, prefix)
);

CREATE TABLE xivo (
    id SERIAL PRIMARY KEY,
    name VARCHAR(30) NOT NULL UNIQUE,
    host VARCHAR(100) NOT NULL UNIQUE
);

CREATE TABLE entities (
    id SERIAL PRIMARY KEY,
    name VARCHAR(30) NOT NULL UNIQUE,
    display_name VARCHAR(128) NOT NULL DEFAULT '',
    xivo_id INTEGER NOT NULL REFERENCES xivo(id),
    number_start VARCHAR(10) NOT NULL,
    number_end VARCHAR(10) NOT NULL,
    presented_number VARCHAR(40) NOT NULL
);

CREATE TABLE entities_routes (
    entity_id INTEGER NOT NULL REFERENCES entities(id),
    route_id INTEGER NOT NULL REFERENCES route(id),
    PRIMARY KEY(entity_id, route_id)
);

CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    entity_id INTEGER REFERENCES entities(id),
    id_on_xivo INTEGER NOT NULL,
    external_route_id INTEGER REFERENCES route(id)
);

CREATE TABLE administrators (
    id SERIAL PRIMARY KEY,
    login VARCHAR(40) UNIQUE NOT NULL,
    hashed_password VARCHAR(128) NOT NULL,
    superadmin BOOLEAN DEFAULT FALSE
);

CREATE TABLE administrator_entity (
    administrator_id INTEGER REFERENCES administrators(id),
    entity_id INTEGER REFERENCES entities(id),
    PRIMARY KEY (administrator_id, entity_id)
);

INSERT INTO administrators(login, hashed_password, superadmin) VALUES ('admin', '+//rIncoyp/Ai/8l3xSEeSY+P+x4uNle7cHkL6rpPS3ucgr2EAJIqnQbsIpSGwHj', true);

# --- !Downs
DROP TABLE IF EXISTS administrator_entity;
DROP TABLE IF EXISTS users CASCADE;
DROP TABLE IF EXISTS entities_routes CASCADE;
DROP TABLE IF EXISTS administrators CASCADE;
DROP TABLE IF EXISTS entities CASCADE;
DROP TABLE IF EXISTS xivo CASCADE;
DROP TABLE IF EXISTS route;

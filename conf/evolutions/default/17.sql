# --- !Ups

CREATE TABLE xivo_users (
  id SERIAL PRIMARY KEY,
  entity_name VARCHAR(30),
  first_name VARCHAR(128) NOT NULL,
  last_name VARCHAR(128) NOT NULL,
  internal_number VARCHAR(40),
  external_number VARCHAR(40),
  mail VARCHAR(128),
  cti_login VARCHAR(40),
  cti_password VARCHAR(128),
  provisioning_number VARCHAR(40)
);

# --- !Downs

DROP TABLE IF EXISTS xivo_users CASCADE;

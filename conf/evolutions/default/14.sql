# --- !Ups

ALTER TABLE users ADD COLUMN template_id integer REFERENCES template(id) DEFAULT NULL;
ALTER TABLE users ADD COLUMN interval_id integer REFERENCES entity_intervals(id) DEFAULT NULL;


# --- !Downs

ALTER TABLE users DROP COLUMN template_id;
ALTER TABLE users DROP COLUMN interval_id;

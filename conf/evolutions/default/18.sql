# --- !Ups

ALTER TABLE route_metadata ALTER COLUMN released_by SET DATA TYPE varchar(100);

# --- !Downs

ALTER TABLE route_metadata ALTER COLUMN released_by SET DATA TYPE TIMESTAMP WITHOUT TIME ZONE
    USING timestamp with time zone 'epoch';

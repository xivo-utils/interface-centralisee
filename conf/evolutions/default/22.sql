# --- !Ups

DROP TABLE IF EXISTS xivo_users CASCADE;

CREATE TABLE xivo_users (
  id SERIAL PRIMARY KEY,
  xivo_uuid UUID NOT NULL,
  entity_name VARCHAR(30),
  xivo_user_id INTEGER NOT NULL,
  first_name VARCHAR(128) NOT NULL,
  last_name VARCHAR(128) NOT NULL,
  internal_number VARCHAR(40),
  external_number VARCHAR(40),
  mail VARCHAR(128),
  cti_login VARCHAR(40),
  cti_password VARCHAR(128),
  provisioning_number VARCHAR(40)
);

ALTER TABLE xivo_users ADD CONSTRAINT xivo_users__unique UNIQUE(xivo_uuid, xivo_user_id);

# --- !Downs

DROP TABLE IF EXISTS xivo_users CASCADE;

CREATE TABLE xivo_users (
  id SERIAL PRIMARY KEY,
  entity_name VARCHAR(30),
  first_name VARCHAR(128) NOT NULL,
  last_name VARCHAR(128) NOT NULL,
  internal_number VARCHAR(40),
  external_number VARCHAR(40),
  mail VARCHAR(128),
  cti_login VARCHAR(40),
  cti_password VARCHAR(128),
  provisioning_number VARCHAR(40)
);

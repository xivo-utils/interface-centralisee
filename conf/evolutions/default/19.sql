# --- !Ups

ALTER TABLE entities DROP CONSTRAINT entities_name_key;

# --- !Downs

ALTER TABLE entities ADD CONSTRAINT entities_name_key UNIQUE(name);

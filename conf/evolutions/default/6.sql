# --- !Ups

ALTER TABLE administrators ADD COLUMN name VARCHAR(100) NOT NULL DEFAULT '';

# --- !Downs

ALTER TABLE administrators DROP COLUMN name;


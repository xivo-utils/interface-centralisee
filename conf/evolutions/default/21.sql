# --- !Ups

ALTER TABLE xivo ADD CONSTRAINT xivo_name_unique UNIQUE(name);
ALTER TABLE xivo RENAME COLUMN trunk_name TO context_name;
ALTER TABLE xivo RENAME CONSTRAINT xivo_trunk_name_unique TO xivo_context_name_unique;

# --- !Downs

ALTER TABLE xivo RENAME COLUMN context_name TO trunk_name;
ALTER TABLE xivo RENAME CONSTRAINT xivo_context_name_unique TO xivo_trunk_name_unique;
ALTER TABLE xivo DROP CONSTRAINT xivo_name_unique;


# --- !Ups

ALTER TABLE xivo ADD COLUMN play_auth_token CHARACTER VARYING(200) NOT NULL DEFAULT 'u@pf#41[gYHJm<]9N[a0iWDQQ7`e9k';

# --- !Downs

ALTER TABLE xivo DROP COLUMN play_auth_token;

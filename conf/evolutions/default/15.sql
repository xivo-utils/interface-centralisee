# --- !Ups

ALTER TABLE xivo ADD COLUMN trunk_name character varying(100);
UPDATE xivo set trunk_name=replace(name, ' ', '_');
ALTER TABLE xivo ALTER COLUMN trunk_name SET NOT NULL;
ALTER TABLE xivo ADD CONSTRAINT xivo_trunk_name_unique UNIQUE(trunk_name);

# --- !Downs

ALTER TABLE xivo DROP COLUMN trunk_name;

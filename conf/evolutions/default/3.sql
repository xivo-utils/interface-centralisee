# --- !Ups

ALTER TABLE administrators ADD COLUMN ldap BOOLEAN DEFAULT FALSE;
ALTER TABLE administrators ALTER COLUMN hashed_password DROP NOT NULL;
ALTER TABLE administrators ADD CONSTRAINT ldap_or_password CHECK (ldap = true OR hashed_password IS NOT NULL);
ALTER TABLE users DROP CONSTRAINT IF EXISTS users_external_route_id_fkey;

# --- !Downs

ALTER TABLE administrators DROP COLUMN ldap;
ALTER TABLE administrators ALTER COLUMN hashed_password NOT NULL;
ALTER TABLE administrators DROP CONSTRAINT ldap_or_password;
ALTER TABLE users ADD CONSTRAINT users_external_route_id_fkey FOREIGN KEY (external_route_id) REFERENCES route(id);


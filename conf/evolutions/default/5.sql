# --- !Ups

CREATE TABLE route_metadata (
    id SERIAL PRIMARY KEY,
    route_ref INTEGER REFERENCES route(id),
    created_date TIMESTAMP WITHOUT TIME ZONE,
    created_by CHARACTER VARYING(100),
    modified_date TIMESTAMP WITHOUT TIME ZONE,
    modified_by CHARACTER VARYING(100),
    released_date TIMESTAMP WITHOUT TIME ZONE,
    released_by TIMESTAMP WITHOUT TIME ZONE,
    manual BOOLEAN DEFAULT TRUE NOT NULL
);

# --- !Downs

DROP TABLE IF EXISTS route_metadata;

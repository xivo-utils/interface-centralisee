# --- !Ups

CREATE TABLE template_intervals (
    template_id INTEGER NOT NULL REFERENCES template(id) ON DELETE CASCADE,
    interval_id INTEGER NOT NULL REFERENCES entity_intervals(id) ON DELETE CASCADE,
    PRIMARY KEY(template_id, interval_id)
);

# --- !Downs

DROP TABLE IF EXISTS template_intervals CASCADE;

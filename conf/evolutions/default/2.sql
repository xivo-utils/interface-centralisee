# --- !Ups

CREATE TABLE entity_intervals (
    id SERIAL PRIMARY KEY,
    entity_id INTEGER REFERENCES entities(id),
    start VARCHAR(10) NOT NULL,
    "end" VARCHAR(10) NOT NULL
);

INSERT INTO entity_intervals(entity_id, start, "end") (SELECT id, number_start, number_end FROM entities);

ALTER TABLE entities DROP COLUMN number_start;
ALTER TABLE entities DROP COLUMN number_end;

# --- !Downs

ALTER TABLE entities ADD COLUMN number_start VARCHAR(10) NOT NULL;
ALTER TABLE entities ADD COLUMN number_end VARCHAR(10) NOT NULL;

UPDATE entities e SET number_start = ei.start, number_end = ei.end FROM entity_intervals ei WHERE e.id = ei.entity_id;

DROP TABLE entity_intervals;
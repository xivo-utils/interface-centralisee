# --- !Ups

DROP TABLE IF EXISTS entities_routes CASCADE;

# --- !Downs

CREATE TABLE entities_routes (
    entity_id INTEGER NOT NULL REFERENCES entities(id),
    route_id INTEGER NOT NULL REFERENCES route(id),
    PRIMARY KEY(entity_id, route_id)
);

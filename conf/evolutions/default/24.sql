# --- !Ups

ALTER TYPE peer_sip_name_modes ADD VALUE IF NOT EXISTS 'webrtc';
ALTER TYPE peer_sip_name_modes ADD VALUE IF NOT EXISTS 'ua';

# --- !Downs

-- No down for new enum value

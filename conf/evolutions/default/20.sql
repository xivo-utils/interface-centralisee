# --- !Ups

UPDATE administrators set name = login WHERE name = '';

-- for non-unique name appending suffix ' 2', ' 3' etc. (first which makes name unique)
UPDATE administrators a1
SET name =
  (
    SELECT CONCAT(a1.name, ' ', num) new_name
    FROM generate_series(2,1000) num
    WHERE NOT EXISTS(
        SELECT a3.id
        FROM administrators a3
        WHERE a3.name = CONCAT(a1.name, ' ', num)
    )
    LIMIT 1

  )
FROM administrators a2
WHERE a1.name = a2.name
AND a1.id > a2.id;

ALTER TABLE administrators ADD CONSTRAINT administrators_name_unique UNIQUE(name);

# --- !Downs

ALTER TABLE administrators DROP CONSTRAINT administrators_name_unique;



# --- !Ups

ALTER TYPE interval_routing_modes ADD VALUE IF NOT EXISTS 'with_customized_direct_number' AFTER 'with_direct_number';

ALTER TABLE entity_intervals DROP CONSTRAINT direct_number_prefix_filled_when_with_direct_number;

ALTER TABLE entity_intervals RENAME direct_number_prefix TO direct_number;

ALTER TABLE entity_intervals ADD CONSTRAINT direct_number_filled_when_with_direct_number CHECK (
    (direct_number IS NOT NULL AND (interval_routing_mode='with_direct_number' OR interval_routing_mode='with_customized_direct_number')) OR
    (direct_number IS NULL AND interval_routing_mode<>'with_direct_number' AND interval_routing_mode<>'with_customized_direct_number') );

# --- !Downs

-- No down for new enum value, because deleting value from enum is really tricky

ALTER TABLE entity_intervals DROP CONSTRAINT direct_number_filled_when_with_direct_number;

ALTER TABLE entity_intervals RENAME direct_number TO direct_number_prefix;

ALTER TABLE entity_intervals ADD CONSTRAINT direct_number_prefix_filled_when_with_direct_number CHECK (
    (direct_number_prefix IS NOT NULL AND interval_routing_mode='with_direct_number') or
    (direct_number_prefix IS NULL AND interval_routing_mode<>'with_direct_number') );

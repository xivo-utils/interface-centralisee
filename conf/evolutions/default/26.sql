# --- !Ups

ALTER TABLE entities ADD COLUMN mds_name CHARACTER VARYING(128) NOT NULL DEFAULT 'default';

# --- !Downs

ALTER TABLE entities DROP COLUMN mds_name;

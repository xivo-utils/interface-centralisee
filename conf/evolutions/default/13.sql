# --- !Ups

ALTER TABLE users ADD COLUMN internal_route_id integer;

# --- !Downs

ALTER TABLE users DROP COLUMN internal_route_id;

# --- !Ups

CREATE TYPE interval_routing_modes AS ENUM ('routed', 'not_routed', 'with_direct_number');
ALTER TABLE entity_intervals ADD COLUMN interval_routing_mode interval_routing_modes NOT NULL DEFAULT 'routed';
ALTER TABLE entity_intervals ADD COLUMN direct_number_prefix VARCHAR(20);
ALTER TABLE entity_intervals ADD CONSTRAINT direct_number_prefix_filled_when_with_direct_number CHECK (
    (direct_number_prefix IS NOT NULL AND interval_routing_mode='with_direct_number') or
    (direct_number_prefix IS NULL AND interval_routing_mode<>'with_direct_number') );
ALTER TABLE entity_intervals ADD COLUMN label VARCHAR(50);

# --- !Downs

ALTER TABLE entity_intervals DROP COLUMN interval_routing_mode;
ALTER TABLE entity_intervals DROP COLUMN direct_number_prefix;
ALTER TABLE entity_intervals DROP COLUMN label;

DROP TYPE IF EXISTS interval_routing_modes;

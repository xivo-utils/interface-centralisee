# --- !Ups

UPDATE template SET voice_mail_send_email = false WHERE voice_mail_send_email is NULL;
ALTER TABLE template ALTER COLUMN voice_mail_send_email SET DEFAULT false;
ALTER TABLE template ALTER COLUMN voice_mail_send_email SET NOT NULL;

# --- !Downs

ALTER TABLE template ALTER COLUMN voice_mail_send_email DROP NOT NULL;
UPDATE template SET voice_mail_send_email = null WHERE voice_mail_send_email is false;




# --- !Ups

ALTER TABLE xivo ADD COLUMN is_proxy boolean DEFAULT false;
ALTER TABLE xivo ALTER COLUMN play_auth_token DROP NOT NULL;

# --- !Downs

UPDATE xivo SET play_auth_token = 'u@pf#41[gYHJm<]9N[a0iWDQQ7`e9k' WHERE play_auth_token is NULL;
ALTER TABLE xivo DROP COLUMN is_proxy;
ALTER TABLE xivo ALTER COLUMN play_auth_token SET NOT NULL;
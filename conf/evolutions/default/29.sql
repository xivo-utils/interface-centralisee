# --- !Ups

ALTER TABLE xivo_users ADD COLUMN peer_sip_name peer_sip_name_modes;
ALTER TABLE xivo_users ADD COLUMN caller_id_mode caller_id_modes;
ALTER TABLE xivo_users ADD COLUMN custom_caller_id VARCHAR(20);
ALTER TABLE xivo_users ADD COLUMN ringing_time SMALLINT;

# --- !Downs

ALTER TABLE xivo_users DROP COLUMN peer_sip_name;
ALTER TABLE xivo_users DROP COLUMN caller_id_mode;
ALTER TABLE xivo_users DROP COLUMN custom_caller_id;
ALTER TABLE xivo_users DROP COLUMN ringing_time;
# --- !Ups

CREATE TYPE peer_sip_name_modes AS ENUM ('auto', 'model');
CREATE TYPE caller_id_modes AS ENUM ('incomingNo', 'anonymous', 'custom');
CREATE TYPE voice_mail_number_modes AS ENUM ('short_number', 'custom');
CREATE TABLE template (
    id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    peer_sip_name peer_sip_name_modes NOT NULL,
    routed_inbound boolean DEFAULT true NOT NULL,
    routed_inbound_prefix VARCHAR(20),
    caller_id_mode caller_id_modes NOT NULL,
    custom_caller_id VARCHAR(20),
    ringing_time SMALLINT NOT NULL,
    voice_mail_enabled boolean NOT NULL,
    voice_mail_number_mode voice_mail_number_modes,
    voice_mail_custom_number VARCHAR(20),
    voice_mail_send_email boolean
);

CREATE TABLE template_xivo (
    template_id INTEGER NOT NULL REFERENCES template(id) ON DELETE CASCADE,
    xivo_id INTEGER NOT NULL REFERENCES xivo(id) ON DELETE CASCADE,
    PRIMARY KEY(template_id, xivo_id)
);

CREATE TABLE template_entities (
    template_id INTEGER NOT NULL REFERENCES template(id) ON DELETE CASCADE,
    entity_combined_id CHARACTER VARYING(200) NOT NULL REFERENCES entities(combined_id) ON DELETE CASCADE,
    PRIMARY KEY(template_id, entity_combined_id)
);

# --- !Downs

DROP TABLE IF EXISTS template CASCADE;
DROP TABLE IF EXISTS template_xivo CASCADE;
DROP TABLE IF EXISTS template_entities CASCADE;
DROP TYPE IF EXISTS peer_sip_name_modes;
DROP TYPE IF EXISTS caller_id_modes;
DROP TYPE IF EXISTS voice_mail_number_modes;

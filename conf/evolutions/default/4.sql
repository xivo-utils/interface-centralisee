# --- !Ups

ALTER TABLE route ADD COLUMN length INTEGER;
CREATE OR REPLACE FUNCTION route_insert_prefix_length() RETURNS TRIGGER AS $route_calculate_length$
BEGIN
    NEW.length = length(NEW.digits);;
    RETURN NEW;;
END;;
$route_calculate_length$ LANGUAGE plpgsql;

CREATE TRIGGER route_insert_prefix_length BEFORE INSERT OR UPDATE ON route FOR EACH ROW EXECUTE PROCEDURE route_insert_prefix_length();


# --- !Downs

ALTER TABLE route DROP COLUMN length;
DROP TRIGGER IF EXISTS route_insert_prefix_length ON route;
DROP FUNCTION IF EXISTS route_insert_prefix_length();


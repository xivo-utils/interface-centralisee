# --- !Ups

ALTER TABLE template ADD COLUMN voice_mail_delete_after_notif BOOLEAN DEFAULT FALSE NOT NULL;

# --- !Downs

ALTER TABLE template DROP COLUMN voice_mail_delete_after_notif;



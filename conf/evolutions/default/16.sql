# --- !Ups

CREATE TABLE roles (
    id SERIAL PRIMARY KEY,
    name CHARACTER VARYING(100) NOT NULL
);

CREATE TABLE administrator_role (
    administrator_id INTEGER NOT NULL REFERENCES administrators(id),
    role_id INTEGER NOT NULL REFERENCES roles(id),
    PRIMARY KEY (administrator_id, role_id)
);

CREATE TABLE operations (
    id SERIAL PRIMARY KEY,
    name CHARACTER VARYING(100)  NOT NULL,
    creating BOOLEAN DEFAULT FALSE,
    reading BOOLEAN DEFAULT FALSE,
    editing BOOLEAN DEFAULT FALSE,
    deleting BOOLEAN DEFAULT FALSE
);

CREATE TABLE permissions (
    id SERIAL UNIQUE,
    entity_id INTEGER NOT NULL REFERENCES entities(id),
    operation_id INTEGER NOT NULL REFERENCES operations(id),
    name CHARACTER VARYING(100) NOT NULL,
    PRIMARY KEY (id, entity_id, operation_id)
);


CREATE TABLE role_permission (
    role_id INTEGER  NOT NULL REFERENCES roles(id),
    permission_id INTEGER  NOT NULL REFERENCES permissions(id),
    PRIMARY KEY (role_id, permission_id)
);

# --- !Downs

DROP TABLE IF EXISTS role_permission;
DROP TABLE IF EXISTS permissions;
DROP TABLE IF EXISTS operations;
DROP TABLE IF EXISTS administrator_role;
DROP TABLE IF EXISTS roles;

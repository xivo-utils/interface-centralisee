# --- !Ups

ALTER TABLE template DROP COLUMN routed_inbound;
ALTER TABLE template DROP COLUMN routed_inbound_prefix;

# --- !Downs

ALTER TABLE template ADD COLUMN routed_inbound BOOLEAN DEFAULT TRUE NOT NULL;
ALTER TABLE template ADD COLUMN routed_inbound_prefix VARCHAR(20);

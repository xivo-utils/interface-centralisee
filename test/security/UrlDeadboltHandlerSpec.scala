package security

import be.objectify.deadbolt.scala.models.Subject
import be.objectify.deadbolt.scala.{AuthenticatedRequest, DynamicResourceHandler}
import org.scalatest._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerTest
import play.api.http.HttpVerbs
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.mvc.{Request, Result}
import play.api.test.FakeRequest
import testutils.DbTest

import scala.concurrent.Future

class UrlDeadboltHandlerSpec extends WordSpec with ShouldMatchers with MockitoSugar with ScalaFutures with HttpVerbs
  with OneAppPerTest with DbTest {

  override def newAppForTest(testData: TestData) = new GuiceApplicationBuilder()
    .configure(testConfig)
    .build()

  val handler = new UrlDeadboltHandler {
    def getDynamicResourceHandler[A](request: Request[A]): Future[Option[DynamicResourceHandler]] = ???
    def onAuthFailure[A](request: AuthenticatedRequest[A]): Future[Result] = ???
  }

  "The UrlDeadboltHandler" should {
    "disable before auth check" in {
      whenReady(handler.beforeAuthCheck(mock[Request[Any]])) { result =>
        result shouldEqual None
      }
    }

    "return SecurityRole from the session username as subject" in {
      val request = FakeRequest(GET, "test").withSession("username" -> "dupont")
      val aRequest = AuthenticatedRequest.apply(request, None)
      whenReady(handler.getSubject(aRequest)) { result: Option[Subject] =>
        result.get.identifier shouldEqual "dupont"
      }
    }

  }
}

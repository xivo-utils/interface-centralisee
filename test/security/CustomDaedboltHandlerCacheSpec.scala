package security

import model.AdministratorInterface
import org.scalatest.mock.MockitoSugar
import org.scalatest.{ShouldMatchers, WordSpec}
import security.handlers.HandlerKeys

class CustomDeadboltHandlerCacheSpec extends WordSpec with ShouldMatchers with MockitoSugar {

  val cache = new CustomDeadboltHandlerCache(mock[AdministratorInterface])

  "The CustomDeadboltHandlerCache" should {
    "return a WebDeadboltHandler as default handler" in {
      cache.apply().getClass() shouldEqual(classOf[WebDeadboltHandler])
    }

    "return an WebDeadboltHandler for web key" in {
      cache.apply(HandlerKeys.web).getClass() shouldEqual(classOf[WebDeadboltHandler])
    }

    "return an JsonDeadboltHandler for json key" in {
      cache.apply(HandlerKeys.json).getClass() shouldEqual(classOf[JsonDeadboltHandler])
    }

  }
}

package security

import testutils.PlayShouldSpec

class SecurityUserSpec extends PlayShouldSpec {
  "SecurityUser" should {
    "return its login" in {
      val su = new SecurityUser("testLogin")
      su.identifier shouldEqual "testLogin"
    }

    "return empty permissions" in {
      val su = new SecurityUser("test")
      su.permissions shouldEqual List()
    }

    "return empty roles" in {
      val su = new SecurityUser("test")
      su.roles shouldEqual List()
    }

  }
}

package security

import be.objectify.deadbolt.scala.{AuthenticatedRequest, DynamicResourceHandler}
import org.scalatest._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mock.MockitoSugar
import play.api.mvc.Request
import play.api.mvc.Results.Forbidden

class JsonDeadboltHandlerSpec extends WordSpec with ShouldMatchers with MockitoSugar with ScalaFutures {
    val resourceHandler = mock[DynamicResourceHandler]
    val handler = new JsonDeadboltHandler(Some(resourceHandler))

    "The JsonDeadboltHandler" should {
      "return dynamic resource handler" in {
        whenReady(handler.getDynamicResourceHandler(mock[Request[Any]])) { result =>
          result shouldEqual Some(resourceHandler)
        }
      }

      "return forbidden on authorization failure" in {
        whenReady(handler.onAuthFailure(mock[AuthenticatedRequest[Any]])) {result =>
          result shouldEqual Forbidden("")
        }
      }

    }
  }

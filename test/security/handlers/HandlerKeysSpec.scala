package security.handlers

import org.scalatest._

class HandlerKeysSpec extends WordSpec with ShouldMatchers {

  "The Handler keys" should {
    "return json key with \"json\" name" in {
      HandlerKeys.json.name shouldEqual("json")
    }
    "return web key with \"web\" name" in {
      HandlerKeys.web.name shouldEqual("web")
    }

  }
}

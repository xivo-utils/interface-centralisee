package security.handlers

import be.objectify.deadbolt.scala.models.Subject
import be.objectify.deadbolt.scala.{AuthenticatedRequest, DeadboltHandler}
import model.AdministratorInterface
import org.scalatest._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mock.MockitoSugar
import org.mockito.Mockito.stub
import org.scalatestplus.play.OneAppPerTest
import play.api.http.HttpVerbs
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.test.FakeRequest
import security.SecurityUser
import testutils.DbTest

class UrlDynamicResourceHandlerSpec extends WordSpec with OneAppPerTest with DbTest with ShouldMatchers with MockitoSugar
  with ScalaFutures with HttpVerbs {

  override def newAppForTest(testData: TestData) = new GuiceApplicationBuilder()
    .configure(testConfig)
    .build()

  class Helper() {
    val admins = mock[AdministratorInterface]
    val handler = new UrlDynamicResourceHandler(admins)

    def getRequest(method: String = GET, path: String = "/", subject: Option[Subject] = None): AuthenticatedRequest[Any] = {
      val request = FakeRequest(GET, "/test")
      AuthenticatedRequest.apply(request, subject)
    }
  }

  "The UrlDynamicResourceHander" should {
    "refuse request if no session is active" in new Helper() {
      whenReady(handler.isAllowed("test", None, mock[DeadboltHandler], getRequest())) {
        result =>
          result shouldEqual false
      }
    }

    "refuse request if the user is not allowed to access this URL" in new Helper() {
      val request = getRequest(GET, "test", Some(new SecurityUser("dupond")))
      stub(admins.isUrlAllowed("dupond", "/test", GET)).toReturn(false)
      whenReady(handler.isAllowed("test", None, mock[DeadboltHandler], request)) {
      result =>
        result shouldEqual false
      }
    }

    "accept the request if the user is authorized" in new Helper() {
      val request = getRequest(GET, "test", Some(new SecurityUser("dupond")))
      stub(admins.isUrlAllowed("dupond", "/test", GET)).toReturn(true)
      whenReady(handler.isAllowed("test", None, mock[DeadboltHandler], request)) {
        result =>
          result shouldEqual true
      }
    }


    "return false on checkPermission" in new Helper() {
      whenReady(handler.checkPermission("name", None, mock[DeadboltHandler], mock[AuthenticatedRequest[Any]])) {
        result =>
          result shouldEqual false
      }
    }
  }
}

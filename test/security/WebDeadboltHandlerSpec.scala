package security

import be.objectify.deadbolt.scala.{AuthenticatedRequest, DynamicResourceHandler}
import controllers.routes
import org.scalatest._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mock.MockitoSugar
import play.api.mvc.Request
import play.api.mvc.Results.Redirect

class WebDeadboltHandlerSpec extends WordSpec with ShouldMatchers with MockitoSugar with ScalaFutures {
  val resourceHandler = mock[DynamicResourceHandler]
  val handler = new WebDeadboltHandler(Some(resourceHandler))

  "The WebDeadboltHandler" should {
    "return dynamic resource handler" in {
      whenReady(handler.getDynamicResourceHandler(mock[Request[Any]])) { result =>
        result shouldEqual Some(resourceHandler)
      }
    }

    "redirect to the login page on authorization failure" in {
      whenReady(handler.onAuthFailure(mock[AuthenticatedRequest[Any]])) {result =>
        result shouldEqual Redirect(routes.Login.loginPage.url)
      }
    }

  }
}

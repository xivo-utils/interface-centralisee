package controllers

import java.util.UUID
import model._
import model.validators.{EntityValidator, ProxyValidatorInterface, XivoValidatorInterface}
import org.mockito.Matchers._
import org.scalatest.mock.MockitoSugar
import play.api.Application
import play.api.libs.json.Json
import play.api.test.FakeRequest
import org.mockito.Mockito._
import org.scalatest.TestData
import org.scalatestplus.play.OneAppPerTest
import play.api.inject._
import play.api.inject.guice.GuiceApplicationBuilder
import testutils.{PlayShouldSpec, TestConfig}

class XivoSpec extends PlayShouldSpec with OneAppPerTest with TestConfig with MockitoSugar {

  override def newAppForTest(testData: TestData): Application = new GuiceApplicationBuilder()
    .configure(testConfig)
    .overrides(bind[XivoInterface].toInstance(mock[XivoInterface]))
    .overrides(bind[XivoValidatorInterface].toInstance(mock[XivoValidatorInterface]))
    .overrides(bind[ProxyValidatorInterface].toInstance(mock[ProxyValidatorInterface]))
    .overrides(bind[EntityInterface].toInstance(mock[EntityInterface]))
    .overrides(bind[AdministratorInterface].toInstance(mock[AdministratorInterface]))
    .overrides(bind[EntityValidator].toInstance(mock[EntityValidator]))
    .build

  class Helper() {
    val configuration = app.configuration
    val admins = app.injector.instanceOf(classOf[AdministratorInterface])
    val xivoValidator = app.injector.instanceOf(classOf[XivoValidatorInterface])
    val proxyValidator = app.injector.instanceOf(classOf[ProxyValidatorInterface])
    val entityManager = app.injector.instanceOf(classOf[EntityInterface])
    val xivoManager = app.injector.instanceOf(classOf[XivoInterface])
    val entityValidator = app.injector.instanceOf(classOf[EntityValidator])
    stub(admins.isUrlAllowed(anyString, anyString, anyString)).toReturn(true)
  }

  "The Xivo controller" should {
    "list existing xivo" in new Helper() {
      val xivo1 = Xivo(None, UUID.fromString("37af25bd-bb98-4cbd-bc2a-7530b89c2907"), "xivo1", "192.168.2.3", "xivo1-trunk", Some(xivoCapacity), Some("token"))
      val xivo2 = Xivo(None, UUID.fromString("59298e06-4e6f-4353-9f88-834cc9bd999a"), "xivo2", "192.168.2.4", "xivo1-trunk", Some(xivoCapacity), Some("token"))
      val expected = List(XivoExtended(xivo1, isReachable = true), XivoExtended(xivo2, isReachable = false))
      stub(xivoManager.listExtended).toReturn(expected)

      val res = route(app, FakeRequest(GET, "/api/1.0/xivo").withSession("username" -> "test")).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.obj("items" -> expected)
    }

    "list existing xivo with proxies" in new Helper() {
      val xivo1 = Xivo(None, UUID.fromString("37af25bd-bb98-4cbd-bc2a-7530b89c2907"), "xivo1", "192.168.2.3", "xivo1-trunk", Some(xivoCapacity), Some("token"))
      val proxy1 = Xivo(None, UUID.fromString("59298e06-4e6f-4353-9f88-834cc9bd999a"), "proxy1", "192.168.10.1", "proxy1-trunk", Some(xivoCapacity), Some("token"), true)
      val expected = List(XivoExtended(xivo1, isReachable = true), XivoExtended(proxy1, isReachable = true))
      stub(xivoManager.listExtended).toReturn(expected)

      val res = route(app, FakeRequest(GET, "/api/1.0/xivo").withSession("username" -> "test")).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.obj("items" -> expected)
    }

    "get a xivo by id" in new Helper() {
      val x1 = model.Xivo(Some(2), UUID.randomUUID(), "xivo1", "10.0.0.1", "to_xivo1", None, Some("token"))
      stub(xivoManager.getXivo(2)).toReturn(Some(x1))

      val res = route(app, FakeRequest(GET, "/api/1.0/xivo/2").withSession("username" -> "test")).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.toJson(x1)
    }

    "create a xivo from its JSON representation and return the notices" in new Helper() {
      val xc = XivoCreation("xivo1", "192.168.1.2", List(2, 4), "to_xivo1", true, Some("token"))
      val xivo = model.Xivo(Some(3), UUID.randomUUID(), "xivo1", "192.168.1.2", "to_xivo1", None, Some("token"))
      val notices = List("notice 1", "notice 2")
      val result = new CreationResult(xivo, notices)
      val json = Json.obj(
        "name" -> "xivo1",
        "host" -> "192.168.1.2",
        "linkedXivo" -> List(2, 4)
      )
      stub(xivoValidator.fromJson(json)).toReturn(xc)
      stub(xivoManager.create(xc)).toReturn(result)

      val res = route(app, FakeRequest(POST, "/api/1.0/xivo").withJsonBody(json).withSession("username" -> "test")).get

      status(res) shouldEqual CREATED
      contentAsJson(res) shouldEqual Json.toJson(notices)
      verify(xivoValidator).fromJson(json)
      verify(xivoValidator).validate(xc)
      verify(xivoManager).create(xc)
    }

    "create a proxy from its JSON representation and return the notices" in new Helper() {
      val intervals = List(Interval(None, "1000", "1099", IntervalRoutingModes.Routed), Interval(None, "200", "209", label = "label"))
      val pc = ProxyCreation("my proxy", "172.16.0.2", "to_my_proxy", intervals)
      val xivo = model.Xivo(Some(3), UUID.randomUUID(), "my proxy", "172.16.0.2", "to_my_proxy", None, Some("token"), isProxy = true)
      val entity = Entity(None, CombinedId(xivo.uuid, "default"), "default", "Default", xivo, intervals, "", "default")
      val notices = List("notice 1", "notice 2")
      val result = new CreationResult(xivo, notices)
      val json = Json.obj(
        "name" -> "my proxy",
        "host" -> "172.16.0.2",
        "contextName" -> "to_my_proxy",
        "intervals" -> intervals
      )
      stub(proxyValidator.fromJson(json)).toReturn(pc)
      stub(xivoManager.createProxy(pc)).toReturn(result)
      stub(entityManager.create(entity, configureXiVO = false)).toReturn(entity)
      val res = route(app, FakeRequest(POST, "/api/1.0/proxy").withJsonBody(json).withSession("username" -> "test")).get
      status(res) shouldEqual CREATED
      contentAsJson(res) shouldEqual Json.toJson(notices)
      verify(proxyValidator).fromJson(json)
      verify(proxyValidator).validate(pc)
      verify(entityValidator).validateCreation(entity, true)
      verify(xivoManager).createProxy(pc)
      verify(entityManager).create(entity, false)
    }

    "synchronize config files for all xivo" in new Helper() {
      val res = route(app, FakeRequest(GET, "/api/1.0/xivo/synchronize_config_files").withSession("username" -> "test")).get

      status(res) shouldEqual OK
      verify(xivoManager).synchronizeConfigFiles()
    }
  }
}

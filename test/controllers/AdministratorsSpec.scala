package controllers

import model._
import org.mockito.Matchers._
import org.scalatest.mock.MockitoSugar
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.inject.bind
import play.api.test.FakeRequest
import org.mockito.Mockito.stub
import org.scalatest.TestData
import org.scalatestplus.play.OneAppPerTest
import play.api.Application
import testutils.{PlayShouldSpec, TestConfig}

class AdministratorsSpec extends PlayShouldSpec with OneAppPerTest with TestConfig with MockitoSugar {

  override def newAppForTest(testData: TestData): Application = new GuiceApplicationBuilder()
    .configure(testConfig)
    .overrides(bind[AdministratorInterface].toInstance(mock[AdministratorInterface]))
    .build

  class Helper() {
    val admins = app.injector.instanceOf(classOf[AdministratorInterface])
    stub(admins.isUrlAllowed(anyString, anyString, anyString)).toReturn(true)
  }

  "The Administrators controller" should {
    "render the list page" in new Helper() {
      implicit val request = FakeRequest(GET, "/administrators").withSession("username" -> "test")
      val res = route(app, request).get

      status(res) shouldEqual OK
      contentAsString(res) shouldEqual views.html.administrators(admins).toString()
    }

    "render the creation page" in new Helper() {
      implicit val request = FakeRequest(GET, "/administrators/create").withSession("username" -> "test")
      val res = route(app, request).get

      status(res) shouldEqual OK
      contentAsString(res) shouldEqual views.html.createAdmin(admins).toString()
    }

    "render the edition page" in new Helper() {
      stub(admins.getAdmin(1)).toReturn(Some(Administrator(Some(1), "admin", "admin", "password", superAdmin = false, ldap = false, List())))
      implicit val request = FakeRequest(GET, "/administrators/edit/1").withSession("username" -> "test")
      val res = route(app, request).get

      status(res) shouldEqual OK
      contentAsString(res) shouldEqual views.html.editAdmin(admins, 1, "admin").toString()
    }

  }
}

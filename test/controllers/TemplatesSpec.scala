package controllers

import org.scalatest.TestData
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerTest
import play.api.Application
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.test.FakeRequest
import testutils.{PlayShouldSpec, TestConfig}

class TemplatesSpec extends PlayShouldSpec with OneAppPerTest with TestConfig with MockitoSugar {

  override def newAppForTest(testData: TestData): Application = new GuiceApplicationBuilder()
    .configure(testConfig)
    .build

  "The Templates controller" should {
    "render the index page" in {
      val res = route(app, FakeRequest(GET, "/templates").withSession("username" -> "test")).get

      status(res) shouldEqual OK
    }

    "render the creation page" in {
      val res = route(app, FakeRequest(GET, "/templates/create").withSession("username" -> "test")).get

      status(res) shouldEqual OK
    }

    "render the edition page" in {
      val res = route(app, FakeRequest(GET, "/templates/edit/1").withSession("username" -> "test")).get

      status(res) shouldEqual OK
    }


  }
}

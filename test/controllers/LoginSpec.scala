package controllers

import model.AdministratorInterface
import org.mockito.Mockito.{stub, verify}
import org.scalatest.TestData
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerTest
import play.api.inject._
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.Application
import play.api.db.DBApi
import play.api.libs.json.Json
import play.api.libs.ws.WSClient
import play.api.test.FakeRequest
import testutils.{PlayShouldSpec, TestConfig}

class LoginSpec extends PlayShouldSpec with OneAppPerTest with TestConfig with MockitoSugar {

  override def newAppForTest(testData: TestData): Application = new GuiceApplicationBuilder()
    .configure(testConfig)
    .overrides(bind[AdministratorInterface].toInstance(mock[AdministratorInterface]))
    .build

  class Helper() {
    val config = app.configuration
    val wsClient = app.injector.instanceOf(classOf[WSClient])
    val dbApi = app.injector.instanceOf(classOf[DBApi])
    val admins = app.injector.instanceOf(classOf[AdministratorInterface])
  }

  "The login controller with local method" should {
    "return OK with a session if the login is successful" in new Helper() {
      val json = Json.obj(
        "login" -> "dupond",
        "password" -> "toto"
      )
      stub(admins.getHashedPassword("dupond")).toReturn(Some("my_hash"))
      stub(admins.exists("dupond")).toReturn(true)
      stub(admins.passwordMatches("toto", "my_hash")).toReturn(true)
      val res = route(app, FakeRequest(POST, "/api/1.0/login").withJsonBody(json)).get

      status(res) shouldEqual OK
      session(res).get("username") shouldEqual Some("dupond")
      verify(admins).getHashedPassword("dupond")
      verify(admins).passwordMatches("toto", "my_hash")
      verify(admins).exists("dupond")
    }

    "return 400 if values are missing in the JSON" in new Helper() {
      val json = Json.obj(
        "login" -> "dupond"
      )

      val res = route(app, FakeRequest(POST, "/api/1.0/login").withJsonBody(json)).get

      status(res) shouldEqual BAD_REQUEST
      session(res).get("username") shouldEqual None
    }

    "return Forbidden if the login is not correct" in new Helper() {
      val json = Json.obj(
        "login" -> "dupond",
        "password" -> "toto"
      )
      stub(admins.getHashedPassword("dupond")).toReturn(None)
      stub(admins.exists("dupond")).toReturn(true)

      val res = route(app, FakeRequest(POST, "/api/1.0/login").withJsonBody(json)).get

      status(res) shouldEqual FORBIDDEN
      session(res).get("username") shouldEqual None
      verify(admins).getHashedPassword("dupond")
      verify(admins).exists("dupond")
    }

    "return Forbidden if the password is not correct" in new Helper() {
      val json = Json.obj(
        "login" -> "notcorrect",
        "password" -> "nonplus"
      )
      stub(admins.getHashedPassword("notcorrect")).toReturn(Some("my_hash"))
      stub(admins.passwordMatches("nonplus", "my_hash")).toReturn(false)
      stub(admins.exists("notcorrect")).toReturn(true)

      val res = route(app, FakeRequest(POST, "/api/1.0/login").withJsonBody(json)).get

      status(res) shouldEqual FORBIDDEN
      session(res).get("username") shouldEqual None
      verify(admins).getHashedPassword("notcorrect")
      verify(admins).passwordMatches("nonplus", "my_hash")
      verify(admins).exists("notcorrect")
    }

    "flush the session and redirect to the login page on logout" in new Helper() {
      val res = route(app, FakeRequest(GET, "/logout").withSession("username" -> "dupond")).get

      status(res) shouldEqual SEE_OTHER
      redirectLocation(res) shouldEqual Some("/login")
      session(res).get("username") shouldEqual None
    }
  }
}

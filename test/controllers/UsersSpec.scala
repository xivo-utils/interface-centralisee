package controllers

import java.security.InvalidParameterException
import java.util.UUID

import model._
import model.validators.{UserValidator, UserValidatorFactory}
import org.mockito.Matchers.anyString
import org.mockito.Mockito.{stub, verify, when}
import org.scalatest.TestData
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerTest
import play.api.Application
import play.api.inject._
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json._
import play.api.test.FakeRequest
import services.UserManager
import testutils.{PlayShouldSpec, TestConfig}

import scala.util.{Failure, Success}

class UsersSpec extends PlayShouldSpec with OneAppPerTest with TestConfig with MockitoSugar {

  override def newAppForTest(testData: TestData): Application = new GuiceApplicationBuilder()
    .configure(testConfig)
    .overrides(bind[UserManager].toInstance(mock[UserManager]))
    .overrides(bind[UserValidatorFactory].toInstance(mock[UserValidatorFactory]))
    .overrides(bind[EntityInterface].toInstance(mock[EntityInterface]))
    .overrides(bind[AdministratorInterface].toInstance(mock[AdministratorInterface]))
    .build

  class Helper {
    val uuid = UUID.randomUUID()
    val xivo = model.Xivo(Some(2), uuid, "xivo1", "192.168.1.48", "to_xivo1", None, Some("token"))
    val myEntity = model.Entity(Some(3.toLong), CombinedId(uuid, "pontorson"), "pontorson", "Pontorson", xivo, List(Interval(Some(5), "2000", "2099")), "52000", "default")
    val validator = mock[UserValidator]
    val usersManager = app.injector.instanceOf(classOf[UserManager])
    val validatorFactory = app.injector.instanceOf(classOf[UserValidatorFactory])
    val entities = app.injector.instanceOf(classOf[EntityInterface])
    val admins = app.injector.instanceOf(classOf[AdministratorInterface])
    when(validatorFactory.forAdmin(anyString())).thenReturn(validator)
    stub(admins.isUrlAllowed(anyString, anyString, anyString)).toReturn(true)
    val voicemail = VoicemailSettings(enabled = true, Some(VoiceMailNumberMode.ShortNumber), None, Some(true), Some(true))
  }

  "The Users controller" should {
    "create a user from its JSON representation" in new Helper() {
      val json = Json.obj(
        "firstName" -> "Martin",
        "lastName" -> "Dupond",
        "entityId" -> myEntity.id.get,
        "internalNumber" -> "2000"
      )
      val expectedUser = User(None, myEntity, "Martin", "Dupond", "2000", None, None, None, None)
      stub(validator.fromJson(json)).toReturn(expectedUser)
      stub(usersManager.create(expectedUser, "test", true)).toReturn(Success(expectedUser.copy(id = Some(123))))

      val res = route(app, FakeRequest(POST, "/api/1.0/users").withJsonBody(json).withSession("username" -> "test")).get

      status(res) shouldEqual CREATED
      verify(validator).validateCreation(expectedUser, true)
      verify(usersManager).create(expectedUser, "test", true)
    }

    "create a user from its JSON representation without configuration" in new Helper() {
      val json = Json.obj(
        "firstName" -> "Martin",
        "lastName" -> "Dupond",
        "entityId" -> myEntity.id.get,
        "internalNumber" -> "2000"
      )
      val expectedUser = User(None, myEntity, "Martin", "Dupond", "2000", None, None, None, None)
      stub(validator.fromProxyUserJson(json)).toReturn(expectedUser)
      stub(usersManager.create(expectedUser, "test", withConfiguration = false)).toReturn(Success(expectedUser.copy(id = Some(123))))

      val res = route(app, FakeRequest(POST, "/api/1.0/users?withConfiguration=false").withJsonBody(json).withSession("username" -> "test")).get

      status(res) shouldEqual CREATED
      verify(validator).validateCreation(expectedUser, false)
      verify(usersManager).create(expectedUser, "test", false)
    }

    "return the eventual validation error" in new Helper() {
      val json = Json.obj(
        "firstName" -> "Martin",
        "lastName" -> "Dupond",
        "entityId" -> myEntity.id.get,
        "internalNumber" -> "2000"
      )
      val expectedUser = User(None, myEntity, "Martin", "Dupond", "2000", None, None, None, None)
      stub(validator.fromJson(json)).toReturn(expectedUser)
      stub(validator.validateCreation(expectedUser, true)).toThrow(new InvalidParameterException("foo bar baz"))

      val res = route(app, FakeRequest(POST, "/api/1.0/users").withJsonBody(json).withSession("username" -> "test")).get

      status(res) shouldEqual BAD_REQUEST
      contentAsJson(res) shouldEqual Json.toJson("foo bar baz")
      verify(validator).validateCreation(expectedUser, true)
    }

    "list users for an entity using Xivo users cache" in new Helper() {
      val u1 = User(Some(1.toLong), myEntity, "Pierre", "Dupond", "2000", None, None, None, None)
      val u2 = User(Some(2.toLong), myEntity, "Jean", "Durand", "2000", None, None, None, None)
      stub(entities.getEntity(myEntity.combinedId)).toReturn(Some(myEntity))
      stub(usersManager.listForEntityFromCache(myEntity)).toReturn(UserList(List(u1, u2), List(3, 4)))

      val res = route(app, FakeRequest(GET, "/api/1.0/entities/" + myEntity.combinedId.toString + "/users")
        .withSession("username" -> "test")).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.obj("items" -> List(u1, u2), "ghostIds" -> List(3, 4))
    }

    "create a set of users from CSV" in new Helper() {
      val csvHeader = List("firstName", "lastName", "entity.displayName", "internalNumber").mkString(Users.CsvSeparator.toString)
      val csvValues = List(
        List("Pierre", "Dupond", "Pontorson", "2000").mkString(Users.CsvSeparator.toString),
        List("Marc", "Durand", "Pontorson", "2001").mkString(Users.CsvSeparator.toString)
      )
      val csv = (csvHeader :: csvValues).mkString("\r\n")
      val expectedUser1 = User(None, myEntity, "Pierre", "Dupond", "2000", None, None, None, None)
      val expectedUser2 = User(None, myEntity, "Marc", "Durand", "2001", None, None, None, None)
      stub(validator.fromMap(Map("firstName" -> "Pierre", "lastName" -> "Dupond", "entity.displayName" -> "Pontorson", "internalNumber" -> "2000"))).toReturn(expectedUser1)
      stub(validator.fromMap(Map("firstName" -> "Marc", "lastName" -> "Durand", "entity.displayName" -> "Pontorson", "internalNumber" -> "2001"))).toReturn(expectedUser2)
      stub(usersManager.create(expectedUser1, "test", true)).toReturn(Success(expectedUser1))
      stub(usersManager.create(expectedUser2, "test", true)).toReturn(Success(expectedUser2))

      val res = route(app, FakeRequest(POST, "/api/1.0/users/csv").withHeaders("Content-Type" -> "text/plain;charset=UTF-8").withBody(csv).withSession("username" -> "test")).get

      status(res) shouldEqual CREATED
      verify(validator).validateCreation(expectedUser1, true)
      verify(usersManager).create(expectedUser1, "test", true)
      verify(validator).validateCreation(expectedUser2, true)
      verify(usersManager).create(expectedUser2, "test", true)
    }

    "return creation errors with their position in the CSV" in new Helper() {
      val csvHeader = List("firstName", "lastName", "entity.displayName", "internalNumber").mkString(Users.CsvSeparator.toString)
      val csvValues = List(
        List("Pierre", "Dupond", "Pontorson", "2000").mkString(Users.CsvSeparator.toString),
        List("Marc", "Durand", "Pontorson", "2001").mkString(Users.CsvSeparator.toString)
      )
      val csv = (csvHeader :: csvValues).mkString("\r\n")
      val expectedUser1 = User(None, myEntity, "Pierre", "Dupond", "2000", None, None, None, None)
      val expectedUser2 = User(None, myEntity, "Marc", "Durand", "2001", None, None, None, None)
      stub(validator.fromMap(Map("firstName" -> "Pierre", "lastName" -> "Dupond", "entity.displayName" -> "Pontorson", "internalNumber" -> "2000"))).toReturn(expectedUser1)
      stub(validator.fromMap(Map("firstName" -> "Marc", "lastName" -> "Durand", "entity.displayName" -> "Pontorson", "internalNumber" -> "2001"))).toReturn(expectedUser2)

      stub(usersManager.create(expectedUser1, "test", true)).toReturn(Failure(new InvalidParameterException("error 1")))
      stub(usersManager.create(expectedUser2, "test", true)).toReturn(Failure(new InvalidParameterException("error 2")))

      val res = route(app, FakeRequest(POST, "/api/1.0/users/csv").withHeaders("Content-Type" -> "text/plain").withBody(csv).withSession("username" -> "test")).get

      status(res) shouldEqual BAD_REQUEST
      contentAsJson(res) shouldEqual Json.obj("errors" -> List("Ligne 1: error 1", "Ligne 2: error 2"))
      verify(validator).validateCreation(expectedUser1, true)
      verify(usersManager).create(expectedUser1, "test", true)
      verify(validator).validateCreation(expectedUser2, true)
      verify(usersManager).create(expectedUser2, "test", true)
    }

    "delete a user by id" in new Helper() {
      val userId = 5
      stub(usersManager.getUserFromCache(5, entities)).toReturn(Some(mock[User]))

      val res = route(app, FakeRequest(DELETE, "/api/1.0/users/" + userId).withSession("username" -> "test")).get

      status(res) shouldEqual NO_CONTENT
      verify(usersManager).delete(userId, entities, "test", withConfiguration = true)
    }

    "return 404 if the user to delete does not exist" in new Helper() {
      val userId = 3
      stub(usersManager.getUserFromCache(3, entities)).toReturn(None)

      val res = route(app, FakeRequest(DELETE, "/api/1.0/users/" + userId).withSession("username" -> "test")).get

      status(res) shouldEqual NOT_FOUND
      contentAsJson(res) shouldEqual Json.obj("errors" -> "L'utilisateur id: 3 n'existe pas")
    }

    "reset device to autoprov by user id" in new Helper() {
      val userId = 5
      val user = User(None, myEntity, "Martin", "Dupond", "2000", None, None, None, None)
      stub(usersManager.getUserWithVoicemailFromXivo(5, entities)).toReturn(Some(UserWithVM(user, voicemail)))

      val res = route(app, FakeRequest(POST, "/api/1.0/users/" + userId + "/disconnect-device")
        .withJsonBody(JsObject(Map.empty[String, JsValue]))
        .withSession("username" -> "test")).get

      status(res) shouldEqual OK
      verify(usersManager).disconnectDevice(userId, entities, "test")
    }

    "return 404 if the user whos device reset to autoprov does not exist" in new Helper() {
      val userId = 3
      stub(usersManager.getUserWithVoicemailFromXivo(3, entities)).toReturn(None)

      val res = route(app, FakeRequest(POST, "/api/1.0/users/" + userId + "/disconnect-device")
        .withJsonBody(JsObject(Map.empty[String, JsValue]))
        .withSession("username" -> "test")).get

      status(res) shouldEqual NOT_FOUND
      contentAsJson(res) shouldEqual Json.obj("errors" -> "L'utilisateur id: 3 n'existe pas")
    }

    "get a user by id" in new Helper() {
      val user = User(Some(5L), myEntity, "Jean", "Dupond", "2000", None, None, None, None)

      private val userWithVM = UserWithVM(user, voicemail)
      stub(usersManager.getUserWithVoicemailCombiningXivoAndCache(user.id.get, entities)).toReturn(Some(userWithVM))

      val res = route(app, FakeRequest(GET, "/api/1.0/users/" + user.id.get).withSession("username" -> "test")).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.toJson(userWithVM)
      verify(usersManager).getUserWithVoicemailCombiningXivoAndCache(user.id.get, entities)
    }

    "return 404 if the requested user does not exist" in new Helper() {
      stub(usersManager.getUserWithVoicemailCombiningXivoAndCache(5, entities)).toReturn(None)
      val res = route(app, FakeRequest(GET, "/api/1.0/users/5").withSession("username" -> "test")).get

      status(res) shouldEqual NOT_FOUND
      contentAsJson(res) shouldEqual Json.obj("errors" -> "L'utilisateur id: 5 n'existe pas")
    }

    "update a user" in new Helper() {
      val oldUser = User(Some(5L), myEntity, "Jean", "Dupond", "2000", None, None, None, None)
      val newUser = User(Some(5L), myEntity, "Martin", "Durand", "2000", None, None, None, None)
      val json = Json.obj(
        "id" -> newUser.id.get,
        "entityId" -> myEntity.id.get,
        "firstName" -> newUser.firstName,
        "lastName" -> newUser.lastName,
        "internalNumber" -> newUser.internalNumber,
        "voicemail" -> Json.toJson(VoicemailSettings.disabled)
      )

      stub(usersManager.getUserWithVoicemailCombiningXivoAndCache(oldUser.id.get, entities)).toReturn(Some(UserWithVM(oldUser, voicemail)))
      stub(validator.fromJsonForUpdate(json, oldUser)).toReturn((newUser, Some(VoicemailSettings.disabled)))

      val res = route(app, FakeRequest(PUT, "/api/1.0/users/" + oldUser.id.get).withJsonBody(json).withSession("username" -> "test")).get

      status(res) shouldEqual NO_CONTENT
      verify(usersManager).getUserWithVoicemailCombiningXivoAndCache(oldUser.id.get, entities)
      verify(validator).fromJsonForUpdate(json, oldUser)
      verify(validator).validateUpdate(oldUser, newUser)
      verify(usersManager).update(UserWithVM(oldUser, voicemail),  UserWithVM(newUser, VoicemailSettings.disabled), "test", withConfiguration = true)
    }

    "delete stale users for a given entity" in new Helper() {
      val user = User(Some(1), myEntity, "test", "test", "1000", None, None, None, None)
      val id1 = 5
      val id2 = 7
      stub(usersManager.getFromXivoAndCacheForEntity(myEntity)).toReturn(UserList(List(user), List(id1, id2)))
      stub(usersManager.getUserWithVoicemailFromXivo(5, entities)).toReturn(Some(mock[UserWithVM]))
      stub(usersManager.getUserWithVoicemailFromXivo(7, entities)).toReturn(Some(mock[UserWithVM]))
      stub(entities.getEntity(myEntity.combinedId)).toReturn(Some(myEntity))
      val res = route(app, FakeRequest(GET, "/api/1.0/entities/" + myEntity.combinedId + "/delete_stale_users").withSession("username" -> "test")).get

      status(res) shouldEqual NO_CONTENT
      verify(usersManager).getFromXivoAndCacheForEntity(myEntity)
      verify(usersManager).delete(id1, entities, "test", withConfiguration = true)
      verify(usersManager).delete(id2, entities, "test", withConfiguration = true)
    }

    "check external number availability when number is not available" in new Helper() {
      stub(entities.getEntity(myEntity.combinedId)).toReturn(Some(myEntity))
      val intervalId = myEntity.intervals.head.id.get
      val extNum = "00123"
      val errMsg = "Number not nice enough"
      stub(validator.checkExternalNumber(Some(intervalId), myEntity.intervals, extNum, None, None)).toReturn(Some(errMsg))

      val res = route(app, FakeRequest(GET, "/api/1.0/entities/" + myEntity.combinedId + "/check_external_number/" + intervalId + "/" + extNum).withSession("username" -> "test")).get
      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual JsObject(Seq("available" -> JsBoolean(false), "message" -> JsString(errMsg)))
    }

    "check external number availability when number is available" in new Helper() {
      stub(entities.getEntity(myEntity.combinedId)).toReturn(Some(myEntity))
      val intervalId = myEntity.intervals.head.id.get
      val extNum = "00123"
      val errMsg = "Number not nice enough"
      stub(validator.checkExternalNumber(Some(intervalId), myEntity.intervals, extNum, None, None)).toReturn(None)

      val res = route(app, FakeRequest(GET, "/api/1.0/entities/" + myEntity.combinedId + "/check_external_number/" + intervalId + "/" + extNum).withSession("username" -> "test")).get
      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual JsObject(Seq("available" -> JsBoolean(true), "message" -> JsString("")))
    }

    "search users across XiVOs" in new Helper() {
      val u1 = User(Some(1.toLong), myEntity, "Pierre", "Dupond", "2000", None, None, None, None)
      val u2 = User(Some(2.toLong), myEntity, "Jean", "Durand", "2000", None, None, None, None)
      val query = "someQuery"
      val limit = 100

      stub(entities.getEntity(myEntity.combinedId)).toReturn(Some(myEntity))
      stub(usersManager.findAcrossXivos(entities, query, limit)).toReturn(List(u1, u2))

      val res = route(app, FakeRequest(GET, s"/api/1.0/users/search?query=$query&limit=$limit")
        .withSession("username" -> "test")).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.obj("items" -> List(u1, u2))
    }

    "search users across XiVOs with empty query" in new Helper() {
      val u1 = User(Some(1.toLong), myEntity, "Pierre", "Dupond", "2000", None, None, None, None)
      val u2 = User(Some(2.toLong), myEntity, "Jean", "Durand", "2000", None, None, None, None)
      val query = ""
      val limit = 100

      stub(entities.getEntity(myEntity.combinedId)).toReturn(Some(myEntity))
      stub(usersManager.findAcrossXivos(entities, query, limit)).toReturn(List(u1, u2))

      val res = route(app, FakeRequest(GET, s"/api/1.0/users/search?query=$query&limit=$limit")
        .withSession("username" -> "test")).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.obj("items" -> List[User]())
    }

  }

}

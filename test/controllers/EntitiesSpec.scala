package controllers

import java.security.InvalidParameterException
import java.util.UUID

import model._
import model.validators.EntityValidator
import org.mockito.Matchers.{any, anyString}
import org.mockito.Mockito.{never, stub, verify}
import org.scalatest.TestData
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerTest
import play.api.Application
import play.api.inject._
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.{JsBoolean, JsObject, JsString, Json}
import testutils.{PlayShouldSpec, TestConfig}
import play.api.test.FakeRequest
import services.UIAdministratorManager
import model.EntityInterface
import scala.util.Success

class EntitiesSpec extends PlayShouldSpec with TestConfig with OneAppPerTest with MockitoSugar  {

  val uuid = UUID.randomUUID()
  val xivo = model.Xivo(Some(2), uuid, "xivo1", "192.168.1.48", "to_xivo1", None, Some("token"))

  override def newAppForTest(testData: TestData): Application = new GuiceApplicationBuilder()
    .configure(testConfig)
    .overrides(bind[EntityInterface].toInstance(mock[EntityInterface]))
    .overrides(bind[AdministratorInterface].toInstance(mock[AdministratorInterface]))
    .overrides(bind[UIAdministratorManager].toInstance(mock[UIAdministratorManager]))
    .overrides(bind[EntityValidator].toInstance(mock[EntityValidator]))
    .build

  class Helper() {
    val entitiesManager = app.injector.instanceOf(classOf[EntityInterface])
    val admins = app.injector.instanceOf(classOf[AdministratorInterface])
    val adminManager = app.injector.instanceOf(classOf[UIAdministratorManager])
    val validator = app.injector.instanceOf(classOf[EntityValidator])
    stub(admins.isUrlAllowed(anyString, anyString, anyString)).toReturn(true)
  }
  
  "The Entities controller" should {
    "create an entity from its JSON representation" in new Helper() {
      val json = Json.obj(
        "name" -> "avranches",
        "displayName" -> "Avranches",
        "xivoId" -> xivo.id.get,
        "intervals" -> List(Json.obj("start" -> "1000", "end" -> "1099")),
        "mdsName" -> "mds0"
      )
      val entity = Entity(None, mock[CombinedId], "avranches", "Avranches", xivo, List(Interval(None, "1000", "1099")), "52000", "mds0")
      stub(validator.fromJson(json)).toReturn(entity)
      stub(entitiesManager.create(entity, true)).toReturn(entity)

      val res = route(app, FakeRequest(POST, "/api/1.0/entities").withJsonBody(json).withSession("username" -> "test")).get

      status(res) shouldEqual CREATED
      verify(validator).fromJson(json)
      verify(validator).validateCreation(entity)
      verify(entitiesManager).create(entity, true)
    }

    "display the eventual validation error validated in Entity constructor" in new Helper() {
      val json = Json.obj(
        "name" -> "avranches",
        "displayName" -> "Avranches",
        "xivoId" -> xivo.id.get,
        "intervals" -> List(Json.obj("start" -> "1000", "end" -> "1099")),
        "mdsName" -> "mds0"
      )

      stub(validator.fromJson(json)).toThrow(new InvalidParameterException("foo bar baz"))

      val res = route(app, FakeRequest(POST, "/api/1.0/entities").withJsonBody(json).withSession("username" -> "test")).get

      status(res) shouldEqual BAD_REQUEST
      contentAsJson(res) shouldEqual Json.toJson("foo bar baz")
      verify(validator).fromJson(json)
      verify(entitiesManager, never()).create(any(), any())
    }

    "display the eventual validation error validated in EntityValidator" in new Helper() {
      val json = Json.obj(
        "name" -> "avranches",
        "displayName" -> "Avranches",
        "xivoId" -> xivo.id.get,
        "intervals" -> List(Json.obj("start" -> "1000", "end" -> "1999")),
        "mdsName" -> "mds0"
      )
      val entity = Entity(None, mock[CombinedId], "avranches", "Avranches", xivo, List(Interval(None, "1000", "1999")), "52000", "mds0")
      stub(validator.fromJson(json)).toReturn(entity)
      stub(validator.validateCreation(entity, false)).toThrow(new InvalidParameterException(EntityValidator.ErrorIntervalOverlap))

      val res = route(app, FakeRequest(POST, "/api/1.0/entities").withJsonBody(json).withSession("username" -> "test")).get

      status(res) shouldEqual BAD_REQUEST
      contentAsJson(res) shouldEqual Json.toJson(EntityValidator.ErrorIntervalOverlap)
      verify(validator).fromJson(json)
      verify(validator).validateCreation(entity, false)
      verify(entitiesManager, never()).create(entity, true)
    }

    "list existing entities for the current user" in new Helper() {
      
      val e1 = Entity(None, mock[CombinedId], "avranches", "Avranches", xivo, List(Interval(None, "1000", "1099")), "52000", "mds0")
      val e2 = Entity(None, mock[CombinedId], "cherbourg", "Cherbourg", xivo, List(Interval(None, "2000", "2099")), "52001", "mds1")
      val userName = "test"
      val admin = mock[UIAdministrator]
      stub(adminManager.getByLogin(userName)).toReturn(Success(admin))
      stub(entitiesManager.listForAdmin(admin, true)).toReturn(List(e1, e2))

      val res = route(app, FakeRequest(GET, "/api/1.0/entities").withSession("username" -> userName)).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.obj("items" -> List(e1, e2))
      verify(entitiesManager).listForAdmin(admin, true)
    }

    "list the available numbers" in new Helper() {
      val cId = CombinedId(UUID.randomUUID(), "testName")
      stub(entitiesManager.availableNumbers(cId, None)).toReturn(List("2003", "2004", "2005"))

      val res = route(app, FakeRequest(GET, s"/api/1.0/entities/${cId.toString}/available_numbers").withSession("username" -> "test")).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.obj("items" -> List("2003", "2004", "2005"))
    }

    "get external number for internal number available numbers" in new Helper() {
      val cId = CombinedId(UUID.randomUUID(), "testName")

      stub(entitiesManager.externalNumber(cId, "2004")).toReturn(Some("1234567890"))
      val resF = route(app, FakeRequest(GET, s"/api/1.0/entities/${cId.toString}/external_number/2004").withSession("username" -> "test")).get

      status(resF) shouldEqual OK
      contentAsJson(resF) shouldEqual Json.obj("externalNumber" -> "1234567890")
    }

    "delete an entity" in new Helper() {
      val cId = CombinedId(UUID.randomUUID(), "testName")
      val res = route(app, FakeRequest(DELETE, s"/api/1.0/entities/$cId").withSession("username" -> "test")).get

      status(res) shouldEqual NO_CONTENT
      verify(entitiesManager).delete(cId, "test")
    }

    "retrieve a single entity" in new Helper() {

      val cId = CombinedId(uuid, "test")
      val entity = Entity(Some(2), cId, "test", "Test", xivo, List(Interval(None, "2000", "2099")), "0123456789", "mds0")
      stub(entitiesManager.getEntity(cId)).toReturn(Some(entity))

      val res = route(app, FakeRequest(GET, "/api/1.0/entities/" + cId.toString ).withSession("username" -> "test")).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.toJson(entity)
    }

    "return 404 if the entity does not exist" in new Helper() {

      val cId = CombinedId(uuid, "t")
      stub(entitiesManager.getEntity(cId)).toReturn(None)

      val res = route(app, FakeRequest(GET, "/api/1.0/entities/" + cId.toString).withSession("username" -> "test")).get

      status(res) shouldEqual NOT_FOUND
      contentAsJson(res) shouldEqual Json.toJson(s"L'entité ${cId.toString} n'existe pas")
    }

    "update an entity for a XiVO" in new Helper() {

      val cId = CombinedId(uuid, "test")
      val oldEntity = Entity(Some(2), cId, "test", "Test", xivo, List(Interval(None, "2000", "2009")), "0123456789", "default")
      val newEntity = Entity(Some(2), cId, "test", "Test2", xivo, List(Interval(None, "2000", "2019")), "0789456123", "default")
      val json = Json.obj(
        "combinedId" -> cId.toString,
        "name" -> "test",
        "displayName" -> "Test2",
        "xivoId" -> xivo.id,
        "numberStart" -> "2000",
        "numberEnd" -> "2019",
        "presentedNumber" -> "0789456123",
        "mdsName" -> "default"

      )
      stub(validator.fromJson(json)).toReturn(newEntity)
      stub(entitiesManager.getEntity(cId)).toReturn(Some(oldEntity))
      stub(entitiesManager.update(oldEntity, newEntity, "test", configureXiVO = true)).toReturn(EntityInterface.UpdateResult(newEntity, success = true))

      val res = route(app, FakeRequest(PUT, "/api/1.0/entities/" + cId.toString ).withJsonBody(json).withSession("username" -> "test")).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual JsObject(Seq("success" -> JsBoolean(true), "errorMessage" -> JsString("")))
      verify(validator).validateUpdate(oldEntity, newEntity)
    }

    "update an entity for a proxy" in new Helper() {

      val cId = CombinedId(uuid, "test")
      val oldEntity = Entity(Some(2), cId, "test", "Test", xivo.copy(isProxy = true), List(Interval(None, "2000", "2009")), "0123456789", "default")
      val newEntity = Entity(Some(2), cId, "test", "Test2", xivo.copy(isProxy = true), List(Interval(None, "2000", "2019")), "0789456123", "default")
      val json = Json.obj(
        "combinedId" -> cId.toString,
        "name" -> "test",
        "displayName" -> "Test2",
        "xivoId" -> xivo.id,
        "numberStart" -> "2000",
        "numberEnd" -> "2019",
        "presentedNumber" -> "0789456123",
        "mdsName" -> "default"

      )
      stub(validator.fromJson(json)).toReturn(newEntity)
      stub(entitiesManager.getEntity(cId)).toReturn(Some(oldEntity))
      stub(entitiesManager.update(oldEntity, newEntity, "test", configureXiVO = false)).toReturn(EntityInterface.UpdateResult(newEntity, success = true))

      val res = route(app, FakeRequest(PUT, "/api/1.0/entities/" + cId.toString ).withJsonBody(json).withSession("username" -> "test")).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual JsObject(Seq("success" -> JsBoolean(true), "errorMessage" -> JsString("")))
      verify(validator).validateUpdate(oldEntity, newEntity)
    }

    "report problem when entity update fails" in new Helper() {

      val cId = CombinedId(uuid, "test")
      val oldEntity = Entity(Some(2), cId, "test", "Test", xivo, List(Interval(None, "2000", "2009")), "0123456789", "default")
      val newEntity = Entity(Some(2), cId, "test", "Test2", xivo, List(Interval(None, "2000", "2019")), "0789456123", "mds1")
      val json = Json.obj(
        "combinedId" -> cId.toString,
        "name" -> "test",
        "displayName" -> "Test2",
        "xivoId" -> xivo.id,
        "numberStart" -> "2000",
        "numberEnd" -> "2019",
        "presentedNumber" -> "0789456123",
        "mdsName" -> "mds1"
      )
      stub(validator.fromJson(json)).toReturn(newEntity)
      stub(entitiesManager.getEntity(cId)).toReturn(Some(oldEntity))
      stub(entitiesManager.update(oldEntity, newEntity, "test", configureXiVO = true)).toReturn(EntityInterface.UpdateResult(oldEntity, success = false, errorMessage = "FOO BAR"))

      val res = route(app, FakeRequest(PUT, "/api/1.0/entities/" + cId.toString ).withJsonBody(json).withSession("username" -> "test")).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual JsObject(Seq("success" -> JsBoolean(false), "errorMessage" -> JsString("FOO BAR")))
      verify(validator).validateUpdate(oldEntity, newEntity)
    }

    "return 404 if the entity does not exist2" in new Helper() {

      val cId = CombinedId(uuid, "test")
      val json = Json.obj(
        "combinedId" -> cId.toString,
        "name" -> "test",
        "displayName" -> "Test2",
        "xivoId" -> xivo.id,
        "numberStart" -> "2000",
        "numberEnd" -> "2019",
        "presentedNumber" -> "0789456123"
      )
      stub(entitiesManager.getEntity(cId)).toReturn(None)

      val res = route(app, FakeRequest(PUT, "/api/1.0/entities/" + cId.toString ).withJsonBody(json).withSession("username" -> "test")).get

      status(res) shouldEqual NOT_FOUND
      contentAsJson(res) shouldEqual Json.toJson(s"L'entité ${cId.toString} n'existe pas")
    }
  }

}

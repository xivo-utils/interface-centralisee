package controllers

import org.scalatest.TestData
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerTest
import play.api.Application
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.test.FakeRequest
import testutils.{PlayShouldSpec, TestConfig}

class ValidationSpec extends PlayShouldSpec with OneAppPerTest with TestConfig with MockitoSugar {

  override def newAppForTest(testData: TestData): Application = new GuiceApplicationBuilder()
    .configure(testConfig)
    .build

  "The Validation controller" should {
    "render the validation page if logged in" in {
      val res = route(app, FakeRequest(GET, "/validation").withSession("username" -> "test")).get

      status(res) shouldEqual OK
    }

    "return the login page if not logged in" in {
      val res = route(app, FakeRequest(GET, "/validation")).get

      status(res) shouldEqual SEE_OTHER
      redirectLocation(res) shouldEqual(Some("/login"))

    }

  }
}

package controllers.helpers

import java.util.UUID
import be.objectify.deadbolt.scala.AuthenticatedRequest
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerTest
import testutils.{PlayShouldSpec, TestConfig}
import org.mockito.Mockito._
import org.slf4j.Logger
import be.objectify.deadbolt.scala.models.{Permission, Role, Subject}
import model._
import model.rights.{UIEntityPermission, UIRole, Role => ModelRole}
import play.api.libs.json.Json
import play.api.mvc._
import play.api.mvc.Results._
import play.api.mvc.BodyParsers.parse
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import play.api.test.FakeRequest

class LoggingActionSpec extends PlayShouldSpec with TestConfig with MockitoSugar {

  trait Helper {
    val duration = 100.millis
    val logger = mock[Logger]
    val request = mock[AuthenticatedRequest[AnyContent]]
    val subject = new Subject {
      override def identifier: String = "john-doe"
      override def roles: List[Role] = Nil
      override def permissions: List[Permission] = Nil
    }
    stub(request.subject).toReturn(Some(subject))
    stub(request.uri).toReturn("/some/uri?x=y")
    stub(request.method).toReturn("POST")

    def dummyAction(result: Result) = new Action[AnyContent] {
      override def parser: BodyParser[AnyContent] = parse.anyContent
      override def apply(request: Request[AnyContent]) = Future.successful(result)
    }

    def getAction(action: Action[AnyContent], messageParts: List[AnyRef]) = new LoggingAction(messageParts, action, logger)

    val uuid = UUID.randomUUID()
    val xivo = model.Xivo(Some(2), uuid, "xivo1", "192.168.1.48", "to_xivo1", None, Some("token"))
    val myEntity = model.Entity(Some(3.toLong), CombinedId(uuid, "pontorson"), "pontorson", "Pontorson", xivo, List(Interval(None, "2000", "2099")), "52000", "default")
  }

  "LoggingAction" should {

    "log successful authorized request as INFO" in new Helper {
      val action = getAction(dummyAction(Ok), List("foo", "bar", "baz"))
      Await.ready(action.apply(request), duration)
      verify(logger).info(s"${request.id}: auth-user='john-doe' POST /some/uri?x=y Ok[200] foo bar baz")
    }

    "log unsuccessful authorized request as INFO" in new Helper {
      val action = getAction(dummyAction(Forbidden), List("foo", "bar", "baz"))
      Await.ready(action.apply(request), duration)
      verify(logger).info(s"${request.id}: auth-user='john-doe' POST /some/uri?x=y Error[403] foo bar baz")
    }

    "log unauthorized request as INFO" in new Helper {
      val action = getAction(dummyAction(Forbidden), List("foo", "bar", "baz"))
      val fakeRequest = FakeRequest()
      Await.ready(action.apply(fakeRequest), duration)
      verify(logger).info(s"${fakeRequest.id}: auth-user=UNKNOWN GET / Error[403] foo bar baz")
    }

    "log successful authorized request with json body as INFO with body as DEBUG" in new Helper {
      val action = getAction(dummyAction(Ok), List("foo", "bar", "baz"))
      val body = Json.parse("""{"test":"Ok"}""")
      stub(request.body).toReturn(AnyContentAsJson(body))
      Await.ready(action.apply(request), duration)
      verify(logger).info(s"${request.id}: auth-user='john-doe' POST /some/uri?x=y Ok[200] foo bar baz")
      verify(logger).debug(s"""${request.id}: body:${Json.prettyPrint(body)}""")
    }

    "log unsuccessful authorized request with json body as INFO with body as DEBUG" in new Helper {
      val action = getAction(dummyAction(InternalServerError), List("foo", "bar", "baz"))
      stub(request.body).toReturn(AnyContentAsText("text"))
      Await.ready(action.apply(request), duration)
      verify(logger).info(s"${request.id}: auth-user='john-doe' POST /some/uri?x=y Error[500] foo bar baz")
      verifyNoMoreInteractions(logger)
    }
  }

  "ActionsLogger.convertToLog" should {

    "format Xivo" in new Helper {
      LoggingAction.convertToLog(xivo) shouldEqual "xivo:name='xivo1',host=192.168.1.48"
    }

    "format Entity" in new Helper {
      LoggingAction.convertToLog(myEntity) shouldEqual "entity:name='pontorson' @xivo:name='xivo1',host=192.168.1.48"
    }

    "format User" in new Helper {
      val user = User(None, myEntity, "Martin", "Dupond", "2000", None, None, Some("The Test"), Some("password"))
      LoggingAction.convertToLog(user) shouldEqual "user:login='The Test' @entity:name='pontorson' @xivo:name='xivo1',host=192.168.1.48"
    }

    "format XivoCreation" in new Helper {
      val xivoCreation = XivoCreation("xivo1", "192.168.1.48", Nil, "", true, Some("token"))
      LoggingAction.convertToLog(xivoCreation) shouldEqual "xivo:name='xivo1',host=192.168.1.48"
    }

    "format UIAdministrator" in new Helper {
      val roles = List(ModelRole(None, "foo"), ModelRole(None, "bar"))
      val admin = UIAdministrator(None, "Jane Doe", "janedoe", "", superAdmin = true, ldap = false, roles)
      LoggingAction.convertToLog(admin) shouldEqual "administrator:name='Jane Doe',superAdmin=true,roles=(foo,bar)"
    }

    "format Role" in new Helper {
      LoggingAction.convertToLog(ModelRole(None, "foo")) shouldEqual "role:name='foo'"
    }

    "format UIRole" in new Helper {
      LoggingAction.convertToLog(UIRole(None, Some("foo"), Nil)) shouldEqual "role:name='foo'"
    }

    "format Template" in new Helper {
      val template = Template(
        TemplateProperties(Some(1234), "Secondary ABC", PeerSipNameMode.Auto, CallerIdMode.IncomingNo, None, 45,
          voiceMailEnabled = true, Some(VoiceMailNumberMode.ShortNumber), Some("1000"), false, false),
        Nil, Nil, Nil
      )
      LoggingAction.convertToLog(template) shouldEqual "template:name='Secondary ABC'"
    }

    "format ActionFailed" in new Helper {
      LoggingAction.convertToLog(LoggingAction.ActionFailed) shouldEqual LoggingAction.actionFailed
    }

    "format unsupported AnyRef" in new Helper {
      case class Unsupported(name: String)
      LoggingAction.convertToLog(Unsupported("foo")) shouldEqual "Unsupported(foo)"
    }
  }

  "ActionsLogger.getUsername" should {

    "return username" in new Helper {
      LoggingAction.getUsername(request) shouldEqual Some(subject.identifier)
    }

  }

}

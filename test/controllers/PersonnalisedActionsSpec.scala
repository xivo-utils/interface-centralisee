package controllers

import java.security.InvalidParameterException

import model._
import org.mockito.Mockito.{never, stub, verify}
import org.mockito.invocation.InvocationOnMock
import org.mockito.stubbing.Answer
import org.scalatest.mock.MockitoSugar
import play.api.{Configuration, Application}
import play.api.libs.json.Json
import play.api.mvc.{Request, Result}
import play.api.test.{WithApplication, PlaySpecification, FakeRequest}
import testutils.TestConfig
import xivo.restapi.connection.WebServicesException

import scala.concurrent.Future

class PersonnalisedActionsSpec extends PlaySpecification with MockitoSugar with TestConfig {

  case class Helper(authenticatedAction: AuthenticatedAction, block: (Request[_]) => Future[Result],
                    admins: AdministratorInterface, jsonAuthenticatedAction: JsonAuthenticatedAction,
                    config: Configuration)
  class HelperApp(app: Application) extends WithApplication(app) {
    def get(): Helper = {
      val block = mock[(Request[_]) => Future[Result]]
      val admins = mock[AdministratorInterface]
      val jsonAuthenticatedAction = new JsonAuthenticatedAction(admins)
      val config = app.configuration
      val authenticatedAction = new AuthenticatedAction(config, admins)
      Helper(authenticatedAction, block, admins, jsonAuthenticatedAction, config)
    }
  }

  "The JsonAction" should {
    "call the real action if there is no problem" in new HelperApp(getApp) {
      val helper = get()
      val request = FakeRequest(GET, "/test")
      JsonAction.invokeBlock(request, helper.block)

      verify(helper.block).apply(request)
      success
    }

    "return a BadRequest if the expected body is empty" in new HelperApp(getApp) {
      val helper = get()
      val request = FakeRequest(POST, "/test")
      val res = JsonAction.invokeBlock(request, helper.block)

      verify(helper.block, never()).apply(request)
      status(res) shouldEqual BAD_REQUEST
    }

    "return a BadRequest after an InvalidParameterException" in new HelperApp(getApp) {
      val helper = get()
      val request = FakeRequest(GET, "/test")
      stub(helper.block.apply(request)).toThrow(new InvalidParameterException(""))
      val res = JsonAction.invokeBlock(request, helper.block)

      verify(helper.block).apply(request)
      status(res) shouldEqual BAD_REQUEST
    }

    "return a BadRequest after a WebServicesException" in new HelperApp(getApp) {
      val helper = get()
      val request = FakeRequest(GET, "/test")
      stub(helper.block.apply(request)).toAnswer(new Answer[Nothing] {
        override def answer(invocationOnMock: InvocationOnMock): Nothing = throw new WebServicesException("", 404)
      })
      val res = JsonAction.invokeBlock(request, helper.block)

      verify(helper.block).apply(request)
      status(res) shouldEqual BAD_REQUEST
    }

    "return a BadRequest after another exception" in new HelperApp(getApp) {
      val helper = get()
      val request = FakeRequest(GET, "/test")
      stub(helper.block.apply(request)).toThrow(new RuntimeException(""))
      val res = JsonAction.invokeBlock(request, helper.block)

      verify(helper.block).apply(request)
      status(res) shouldEqual INTERNAL_SERVER_ERROR
    }
  }

  "The AuthenticatedAction" should {
    "redirect to the login page if no session is active" in new HelperApp(getApp) {
      val helper = get()
      val request = FakeRequest(GET, "/test")

      val res = helper.authenticatedAction.invokeBlock(request, helper.block)

      verify(helper.block, never()).apply(request)
      status(res) shouldEqual SEE_OTHER
      redirectLocation(res) shouldEqual Some(routes.Login.loginPage.url)
    }

    "return Forbidden if the user is not allowed to access this URL" in new HelperApp(getApp) {
      val helper = get()
      val request = FakeRequest(PUT, "/test").withJsonBody(Json.obj()).withSession("username" -> "dupond")
      stub(helper.admins.isUrlAllowed("dupond", "/test", PUT)).toReturn(false)

      val res = helper.authenticatedAction.invokeBlock(request, helper.block)

      verify(helper.block, never()).apply(request)
      status(res) shouldEqual FORBIDDEN
    }

    "call the real action if the user is authorized" in new HelperApp(getApp) {
      val helper = get()
      val request = FakeRequest(DELETE, "/test").withSession("username" -> "dupond")
      stub(helper.admins.isUrlAllowed("dupond", "/test", DELETE)).toReturn(true)

      val res = helper.authenticatedAction.invokeBlock(request, helper.block)

      verify(helper.block).apply(request)
      success
    }
  }

  "The JsonAuthenticatedAction" should {
    "return Forbidden if no session is active" in new HelperApp(getApp) {
      val helper = get()
      val request = FakeRequest(GET, "/test")

      val res = helper.jsonAuthenticatedAction.invokeBlock(request, helper.block)

      verify(helper.block, never()).apply(request)
      status(res) shouldEqual FORBIDDEN
    }

    "return a BadRequest if the expected body is empty" in new HelperApp(getApp) {
      val helper = get()
      val request = FakeRequest(POST, "/test")
      val res = JsonAction.invokeBlock(request, helper.block)

      verify(helper.block, never()).apply(request)
      status(res) shouldEqual BAD_REQUEST
    }

    "return Forbidden if the user is not allowed to access this URL" in new HelperApp(getApp) {
      val helper = get()
      val request = FakeRequest(GET, "/test").withSession("username" -> "dupond")
      stub(helper.admins.isUrlAllowed("dupond", "/test", "GET")).toReturn(false)

      val res = helper.jsonAuthenticatedAction.invokeBlock(request, helper.block)

      verify(helper.block, never()).apply(request)
      status(res) shouldEqual FORBIDDEN
    }

    "call the real action if the user is authorized" in new HelperApp(getApp) {
      val helper = get()
      val request = FakeRequest(PUT, "/test").withJsonBody(Json.obj()).withSession("username" -> "dupond")
      stub(helper.admins.isUrlAllowed("dupond", "/test", PUT)).toReturn(true)

      val res = helper.jsonAuthenticatedAction.invokeBlock(request, helper.block)

      verify(helper.block).apply(request)
      success
    }
  }
}


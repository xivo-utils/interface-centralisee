package controllers.rest

import java.util.UUID

import model._
import model.rights.Role
import model.validators.AdministratorValidator
import org.mockito.Mockito.{never, stub, verify}
import org.scalatest.TestData
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerTest
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.test.FakeRequest
import services.UIAdministratorManager
import testutils.{PlayShouldSpec, TestConfig}

import scala.util.{Failure, Success}

class AdministratorsSpec extends PlayShouldSpec with OneAppPerTest with TestConfig with MockitoSugar {

  val role = Role(Some(1), "rName")

  override def newAppForTest(testData: TestData): Application = new GuiceApplicationBuilder()
    .configure(testConfig)
    .overrides(bind[AdministratorInterface].toInstance(mock[AdministratorInterface]))
    .overrides(bind[UIAdministratorManager].toInstance(mock[UIAdministratorManager]))
    .overrides(bind[AdministratorValidator].toInstance(mock[AdministratorValidator]))
    .build

  class Helper() {
    val admins = app.injector.instanceOf(classOf[AdministratorInterface])
    val adminManager = app.injector.instanceOf(classOf[UIAdministratorManager])
    val validator = app.injector.instanceOf(classOf[AdministratorValidator])
  }

  "The Administrators controller" should {
    "list the administrators" in new Helper() {
      val adminList = List(UIAdministrator(Some(1), "admin 01", "admin01", "test", false, false, List(role)),
        UIAdministrator(Some(2), "admin 02", "admin02", "test", true, false, List(role)))
      stub(admins.isUrlAllowed("test", "/api/1.0/administrators", "GET")).toReturn(true)
      stub(adminManager.all()).toReturn(Success(adminList))

      val res = route(app, FakeRequest(GET, "/api/1.0/administrators").withSession("username" -> "test")).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.obj("items" -> adminList)
      verify(adminManager).all()
    }

    "create an administrator" in new Helper() {
      val admin = UIAdministrator(None, "test", "test name", "test", false, false, List(role))
      val json = Json.obj(
        "login" -> "test",
        "password" -> "test",
        "superAdmin" -> false,
        "roleIds" -> List(role.id)
      )
      stub(admins.isUrlAllowed("test", "/api/1.0/administrators", "POST")).toReturn(true)
      stub(validator.fromJson(json)).toReturn(admin)
      stub(adminManager.create(admin)).toReturn(Success(admin.copy(id = Some(222))))

      val res = route(app, FakeRequest(POST, "/api/1.0/administrators").withJsonBody(json).withSession("username" -> "test")).get

      status(res) shouldEqual CREATED
      verify(validator).fromJson(json)
      verify(validator).validate(admin)
    }

    "retrieve an administrator" in new Helper() {
      val admin = UIAdministrator(Some(1), "admin 01", "admin01", "test", false, false, List(role))
      stub(admins.isUrlAllowed("test", "/api/1.0/administrators/1", "GET")).toReturn(true)
      stub(adminManager.get(1)).toReturn(Success(admin))

      val res = route(app, FakeRequest(GET, "/api/1.0/administrators/1").withSession("username" -> "test")).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.toJson(admin)
      verify(adminManager).get(1)
    }

    "return 404 if the admin does not exist" in new Helper() {
      stub(admins.isUrlAllowed("test", "/api/1.0/administrators/1", "GET")).toReturn(true)
      stub(adminManager.get(1)).toReturn(Failure(new Exception("Not found")))

      val res = route(app, FakeRequest(GET, "/api/1.0/administrators/1").withSession("username" -> "test")).get

      status(res) shouldEqual NOT_FOUND
      contentAsJson(res) shouldEqual Json.toJson("Cet administrateur n'existe pas")
    }

    "edit an administrator" in new Helper() {
      val oldAdmin = UIAdministrator(Some(2), "test", "test name", "test", false, false, List(role))
      val json = Json.obj(
        "login" -> "test2",
        "password" -> "test2",
        "superAdmin" -> true,
        "roles" -> List(role.id)
      )
      val newAdmin = UIAdministrator(None, "test2", "test 2", "test2", true, false, List(role))
      stub(validator.fromJson(json)).toReturn(newAdmin)
      stub(admins.isUrlAllowed("test", "/api/1.0/administrators/2", "PUT")).toReturn(true)
      stub(adminManager.get(2)).toReturn(Success(oldAdmin))
      stub(adminManager.update(newAdmin)).toReturn(Success(newAdmin))

      val res = route(app, FakeRequest(PUT, "/api/1.0/administrators/2").withJsonBody(json).withSession("username" -> "test")).get

      status(res) shouldEqual NO_CONTENT
      verify(validator).fromJson(json)
      verify(validator).validateUpdate(oldAdmin, newAdmin)
    }

    "delete an administrator" in new Helper() {
      stub(admins.isUrlAllowed("test", "/api/1.0/administrators/2", "DELETE")).toReturn(true)
      stub(admins.isLastSuperAdmin(2)).toReturn(false)

      val res = route(app, FakeRequest(DELETE, "/api/1.0/administrators/2").withSession("username" -> "test")).get

      status(res) shouldEqual NO_CONTENT
      verify(adminManager).delete(2)
    }

    "return 400 if it is the last superadmin" in new Helper() {
      stub(admins.isUrlAllowed("test", "/api/1.0/administrators/3", "DELETE")).toReturn(true)
      stub(admins.isLastSuperAdmin(3)).toReturn(true)

      val res = route(app, FakeRequest(DELETE, "/api/1.0/administrators/3").withSession("username" -> "test")).get

      status(res) shouldEqual BAD_REQUEST
      verify(adminManager, never()).delete(3)
    }

  }
}

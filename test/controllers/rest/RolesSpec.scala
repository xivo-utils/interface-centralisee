package controllers.rest

import akka.stream.Materializer
import be.objectify.deadbolt.scala.ActionBuilders
import model.{AdministratorInterface, UIAdministrator}
import org.mockito.Matchers.anyString
import org.mockito.Mockito._
import org.scalatest.TestData
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerTest
import play.api.libs.json.Json
import play.api.test.FakeRequest
import services.rights.EntityRightsInterface
import model.rights._
import services.UIAdministratorManager
import testutils.{PlayShouldSpec, TestConfig}

import scala.util.Success

class RolesSpec extends PlayShouldSpec with MockitoSugar with TestConfig with OneAppPerTest {

  override def newAppForTest(testData: TestData) = getApp

  def withMocks(test: (ValidationMocks, Roles) => Any): Unit = {
    val mocks = new ValidationMocks
    val controller =
      new Roles(mocks.entityRightsManager, mocks.adminManager, app.injector.instanceOf(classOf[ActionBuilders]))
    when(mocks.admins.isUrlAllowed(anyString, anyString, anyString)).thenReturn(true)
    test(mocks, controller)
  }

  class ValidationMocks {
    val entityRightsManager = mock[EntityRightsInterface]
    val admins = mock[AdministratorInterface]
    val adminManager = mock[UIAdministratorManager]
    def getRole(id: Option[Long] = Some(1), name: String = "testName") = UIRole(id, Some(name), List())
  }

  "The Roles REST controller" should {

    "create new Role" in withMocks {
      (mocks, controller) => {
        implicit val materializer: Materializer = app.materializer
        val role = mocks.getRole()
        when(mocks.entityRightsManager.create(role)).thenReturn(Success(role))
        val rq = FakeRequest(POST, "/api/1.0/roles").withJsonBody(Json.toJson(role)).withSession("username" -> "test")
        val res = call(controller.create, rq)
        status(res) shouldEqual CREATED
      }
    }

    "list Roles" in withMocks {
      (mocks, controller) => {
        implicit val materializer: Materializer = app.materializer
        val r1 = mocks.getRole(Some(123))
        val r2 = mocks.getRole(Some(124), "r2")
        when(mocks.entityRightsManager.all()).thenReturn(Success(List(r1, r2)))
        val rq = FakeRequest(GET, "/api/1.0/roles").withSession("username" -> "test")
        val res = call(controller.list(), rq)
        status(res) shouldEqual OK
        contentAsJson(res) shouldEqual Json.toJson(List(r1, r2))
      }
    }

    "get a Role" in withMocks {
      (mocks, controller) => {
        implicit val materializer: Materializer = app.materializer
        val r1 = mocks.getRole(Some(123))
        when(mocks.entityRightsManager.get(r1.id.get)).thenReturn(Success(r1))
        val rq = FakeRequest(GET, s"/api/1.0/emptyRole").withSession("username" -> "test")
        val res = call(controller.get(r1.id.get), rq)
        status(res) shouldEqual OK
        contentAsJson(res) shouldEqual Json.toJson(r1)
      }
    }

    val sampleRole = UIRole(Some(123), Some("r1"), List(UIXivoPermission(
      100, "XiVO foo", List(
        UIEntityPermission(1, "entity A", UIOperations(reading = true)),
        UIEntityPermission(2, "entity B", UIOperations(creating = true, deleting = true))
      )
    )))

    "get its own Role with erased id and name" in withMocks {
      (mocks, controller) => {
        implicit val materializer: Materializer = app.materializer
        val admin = UIAdministrator(Some(1), "n", "l", "p", superAdmin = false, ldap = false, List(Role(Some(123), "r1")))
        when(mocks.adminManager.getByLogin("test")).thenReturn(Success(admin))
        when(mocks.entityRightsManager.get(sampleRole.id.get)).thenReturn(Success(sampleRole))
        val rq = FakeRequest(GET, s"/api/1.0/myself").withSession("username" -> "test")
        val res = call(controller.myself(), rq)
        status(res) shouldEqual OK
        contentAsJson(res) shouldEqual Json.toJson(sampleRole.copy(id = None, name = None))
      }
    }

    "get its own Role with superadmin rights" in withMocks {
      (mocks, controller) => {
        implicit val materializer: Materializer = app.materializer
        val admin = UIAdministrator(Some(1), "n", "l", "p", true, false, List(Role(Some(123), "r1")))
        when(mocks.adminManager.getByLogin("test")).thenReturn(Success(admin))
        when(mocks.entityRightsManager.getSuperadminRole()).thenReturn(sampleRole)
        val rq = FakeRequest(GET, s"/api/1.0/myself").withSession("username" -> "test")
        val res = call(controller.myself, rq)
        status(res) shouldEqual OK
        contentAsJson(res) shouldEqual Json.toJson(sampleRole)
      }
    }

    "get an empty Role" in withMocks {
      (mocks, controller) => {
        implicit val materializer: Materializer = app.materializer
        val r1 = UIRole(None, Some("testRole"), List())
        when(mocks.entityRightsManager.getEmptyRole()).thenReturn(r1)
        val rq = FakeRequest(GET, s"/api/1.0/emptyRole").withSession("username" -> "test")
        val res = call(controller.empty(), rq)
        status(res) shouldEqual OK
        contentAsJson(res) shouldEqual Json.toJson(r1)
      }
    }


    "update a Role" in withMocks {
      (mocks, controller) => {
        implicit val materializer: Materializer = app.materializer
        val role = mocks.getRole(Some(2))
        when(mocks.entityRightsManager.update(role)).thenReturn(Success(role))
        val rq = FakeRequest(PUT, s"/api/1.0/roles/${role.id.get}").withJsonBody(Json.toJson(role))
          .withSession("username" -> "test")
        val res = call(controller.edit(role.id.get), rq)
        status(res) shouldEqual NO_CONTENT
      }
    }

    "delete a Role" in withMocks {
      (mocks, controller) => {
        implicit val materializer: Materializer = app.materializer
        val r1 = mocks.getRole(Some(123))
        when(mocks.entityRightsManager.delete(r1.id.get)).thenReturn(Success(r1))
        val rq = FakeRequest(DELETE, s"/api/1.0/roles/${r1.id.get}").withSession("username" -> "test")
        val res = call(controller.delete(r1.id.get), rq)
        status(res) shouldEqual NO_CONTENT
      }
    }

  }
}

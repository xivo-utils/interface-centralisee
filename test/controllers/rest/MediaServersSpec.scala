package controllers.rest

import akka.stream.Materializer
import model.MediaServer
import model.utils.{ConfigParser, GlobalXivoConnectorPool}
import org.scalatest.TestData
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerTest
import play.api.Application
import play.api.inject.guice.GuiceApplicationBuilder
import services.{XivoDb, XivoManager}
import testutils.{PlayShouldSpec, TestConfig}
import xivo.ldap.xivoconnection.XivoConnector
import xivo.restapi.model.Mds
import org.mockito.Mockito._
import play.api.libs.json.Json
import play.api.test.FakeRequest

import java.util
import java.util.UUID
import scala.util.Success

class MediaServersSpec extends PlayShouldSpec with MockitoSugar with TestConfig with OneAppPerTest {

  override def newAppForTest(testData: TestData): Application = new GuiceApplicationBuilder()
    .configure(testConfig)
    .build

  class Helper() {
    implicit val materializer: Materializer = app.materializer

    val xivoManager: XivoManager = mock[XivoManager]
    val configParser = mock[ConfigParser]
    val xivoConnector = mock[XivoConnector]
    val connectorPool = new GlobalXivoConnectorPool(configParser) {
      override def get(host: String, incallContext: String, token: String): XivoConnector = xivoConnector
    }

    val mds1 = MediaServer(1, "mds1", "Media Server 1", "1.1.1.2")
    val mds2 = MediaServer(2, "mds2", "Media Server 2", "1.1.1.3")
  }

  "MediaServers controller" should {
    "list media servers" in new Helper {
      val m1 = new Mds()
      m1.setId(1L)
      m1.setName("mds1")
      m1.setDisplayName("Media Server 1")
      m1.setVoipId("1.1.1.2")

      val m2 = new Mds()
      m2.setId(2L)
      m2.setName("mds2")
      m2.setDisplayName("Media Server 2")
      m2.setVoipId("1.1.1.3")

      val mdsList = new util.ArrayList[Mds]
      mdsList.add(m1)
      mdsList.add(m2)


      when(xivoConnector.listMds()).thenReturn(mdsList)
      when(xivoManager.get(1)).thenReturn(Success(XivoDb(Some(1), UUID.randomUUID(), "xivo1", "1.1.1.1", "ctx1", Some("token"))))
      val request = FakeRequest()
        .withSession("username" -> "test")

      val controller = new MediaServers(xivoManager, connectorPool)
      val result = controller.list(1).apply(request)

      contentAsJson(result) shouldEqual Json.toJson(List(mds1, mds2))
      status(result) shouldEqual OK
    }

    "get media server" in new Helper {
      val m1 = new Mds()
      m1.setId(1L)
      m1.setName("mds1")
      m1.setDisplayName("Media Server 1")
      m1.setVoipId("1.1.1.2")

      val mdsList = new util.ArrayList[Mds]
      mdsList.add(m1)

      when(xivoConnector.listMds()).thenReturn(mdsList)
      when(xivoManager.get(1)).thenReturn(Success(XivoDb(Some(1), UUID.randomUUID(), "xivo1", "1.1.1.1", "ctx1", Some("token"))))
      val request = FakeRequest()
        .withSession("username" -> "test")

      val controller = new MediaServers(xivoManager, connectorPool)
      val result = call(controller.get(1,"mds1"), request)

      contentAsJson(result) shouldEqual Json.toJson(mds1)
      status(result) shouldEqual OK
    }
  }
}

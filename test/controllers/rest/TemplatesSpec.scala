package controllers.rest

import java.util.UUID

import controllers.JsonAuthenticatedAction
import model.{AdministratorInterface, CombinedId}
import org.scalatest.mock.MockitoSugar
import org.mockito.Mockito._
import org.mockito.Matchers.anyString
import play.api.test.FakeRequest
import testutils.{PlayShouldSpec, TestConfig}
import akka.stream.Materializer
import be.objectify.deadbolt.scala.ActionBuilders
import org.scalatest.TestData
import org.scalatestplus.play.OneAppPerTest
import play.api.libs.json.Json
import services.TemplateManager
import services.helpers.GetTemplateFixture

import scala.util.Success

class TemplatesSpec extends PlayShouldSpec with MockitoSugar with TestConfig with OneAppPerTest with GetTemplateFixture {

  override def newAppForTest(testData: TestData) = getApp

  def withMocks(test: (ValidationMocks, Templates) => Any): Unit = {
    val mocks = new ValidationMocks
    val controller = new Templates(mocks.templateManager, app.injector.instanceOf(classOf[ActionBuilders]))
    when(mocks.admins.isUrlAllowed(anyString, anyString, anyString)).thenReturn(true)
    test(mocks, controller)
  }

  class ValidationMocks {
    val templateManager = mock[TemplateManager]
    val admins = mock[AdministratorInterface]
  }

  "The Templates REST controller" should {

    "create new Template" in withMocks {
      (mocks, controller) => {
        implicit val materializer: Materializer = app.materializer
        val template = getTemplate()
        when(mocks.templateManager.create(template)).thenReturn(Success(template))
        val rq = FakeRequest(POST, "/api/1.0/templates").withJsonBody(Json.toJson(template)).withSession("username" -> "test")
        val res = call(controller.create, rq)
        status(res) shouldEqual (CREATED)
      }
    }

    "list Templates" in withMocks {
      (mocks, controller) => {
        implicit val materializer: Materializer = app.materializer
        val t1 = getTemplate(Some(123))
        val t2 = getTemplate(Some(124), Some("t2"))
        when(mocks.templateManager.all()).thenReturn(Success(List(t1, t2)))
        val rq = FakeRequest(GET, "/api/1.0/templates").withSession("username" -> "test")
        val res = call(controller.list(), rq)
        status(res) shouldEqual (OK)
        contentAsJson(res) shouldEqual (Json.toJson(List(t1, t2)))
      }
    }

    "list Templates for entity" in withMocks {
      (mocks, controller) => {
        implicit val materializer: Materializer = app.materializer
        val t1 = getTemplate(Some(123))
        val t2 = getTemplate(Some(124), Some("t2"))
        val eId = CombinedId(UUID.randomUUID(), "test")
        when(mocks.templateManager.forEntity(eId)).thenReturn(Success(List(t1, t2)))
        val rq = FakeRequest(GET, s"/api/1.0/templates?entityId=${eId.toString()}").withSession("username" -> "test")
        val res = call(controller.list(Some(eId)), rq)
        status(res) shouldEqual (OK)
        contentAsJson(res) shouldEqual (Json.toJson(List(t1, t2)))
      }
    }

    "get a Template" in withMocks {
      (mocks, controller) => {
        implicit val materializer: Materializer = app.materializer
        val t1 = getTemplate(Some(123))
        when(mocks.templateManager.get(t1.properties.id.get)).thenReturn(Success(t1))
        val rq = FakeRequest(GET, s"/api/1.0/templates/${t1.properties.id.get}").withSession("username" -> "test")
        val res = call(controller.get(t1.properties.id.get), rq)
        status(res) shouldEqual (OK)
        contentAsJson(res) shouldEqual (Json.toJson(t1))
      }
    }

    "update a Template" in withMocks {
      (mocks, controller) => {
        implicit val materializer: Materializer = app.materializer
        val template = getTemplate(Some(2))
        when(mocks.templateManager.update(template)).thenReturn(Success(template))
        val rq = FakeRequest(PUT, s"/api/1.0/templates/${template.properties.id.get}").withJsonBody(Json.toJson(template)).withSession("username" -> "test")
        val res = call(controller.edit(template.properties.id.get), rq)
        status(res) shouldEqual (NO_CONTENT)
      }
    }

    "delete a Template" in withMocks {
      (mocks, controller) => {
        implicit val materializer: Materializer = app.materializer
        val t1 = getTemplate(Some(123))
        when(mocks.templateManager.delete(t1.properties.id.get)).thenReturn(Success(t1))
        val rq = FakeRequest(DELETE, s"/api/1.0/templates/${t1.properties.id.get}").withSession("username" -> "test")
        val res = call(controller.delete(t1.properties.id.get), rq)
        status(res) shouldEqual (NO_CONTENT)
      }
    }

  }
}

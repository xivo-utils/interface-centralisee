package controllers.rest

import controllers.JsonAuthenticatedAction
import model.AdministratorInterface
import org.scalatest.mock.MockitoSugar
import org.mockito.Mockito._
import org.mockito.Matchers.anyString
import play.api.test.FakeRequest
import services.UserRouteCoherencyInterface
import testutils.{PlayShouldSpec, TestConfig}
import akka.stream.Materializer
import be.objectify.deadbolt.scala.ActionBuilders
import org.scalatest.TestData
import org.scalatestplus.play.OneAppPerTest

class ValidationSpec extends PlayShouldSpec with MockitoSugar with TestConfig with OneAppPerTest {

  override def newAppForTest(testData: TestData) = getApp

  def withMocks(test: (ValidationMocks, Validation) => Any): Unit = {
    val mocks = new ValidationMocks
    val controller = new Validation(mocks.userRoute, app.injector.instanceOf(classOf[ActionBuilders]))
    when(mocks.admins.isUrlAllowed(anyString, anyString, anyString)).thenReturn(true)
    test(mocks, controller)
  }

  class ValidationMocks {
    val userRoute = mock[UserRouteCoherencyInterface]
    val admins = mock[AdministratorInterface]
  }

  "The Validation controller" should {

    "return csv with missing routes" in withMocks {
      (mocks, controller) => {
        implicit val materializer: Materializer = app.materializer
        when(mocks.userRoute.getIncoherentUsersAsCsv()).thenReturn("testCSVString")
        val rq = FakeRequest(GET, "/api/1.0/validation/missingRoutes").withSession("username" -> "test")
        val res = call(controller.missingRoutes, rq)
        status(res) shouldEqual (OK)
        contentAsString(res) shouldEqual ("testCSVString")
        header(CONTENT_DISPOSITION, res) shouldEqual (Some("""attachment;filename="usersWithMissingRoutes.csv""""))
      }
    }
  }
}

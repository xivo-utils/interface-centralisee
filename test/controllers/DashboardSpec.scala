package controllers

import org.scalatest.TestData
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerTest
import play.api.Application
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.test.FakeRequest
import testutils.{PlayShouldSpec, TestConfig}

class DashboardSpec extends PlayShouldSpec with OneAppPerTest with TestConfig with MockitoSugar {

  override def newAppForTest(testData: TestData): Application = new GuiceApplicationBuilder()
    .configure(testConfig)
    .build

  "The Dashboard controller" should {
    "render the dashboard page if logged in" in {
      val res = route(app, FakeRequest(GET, "/").withSession("username" -> "test")).get

      status(res) shouldEqual OK
    }

    "return the login page if not logged in" in {
      val res = route(app, FakeRequest(GET, "/")).get

      status(res) shouldEqual SEE_OTHER
      redirectLocation(res) shouldEqual(Some("/login"))

    }

  }
}

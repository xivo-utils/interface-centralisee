#!/bin/bash

if [[ -z "${TEST_LDAP_PORT}" ]]; then
  LDAP_PORT=389
else
  LDAP_PORT="${TEST_LDAP_PORT}"
fi
WAIT_FOR_START=15
WAIT_FOR_INIT=20

UNABLE_TO_CONNECT=1
UNABLE_TO_ADD=2

function connect_to_ldap() {
	nc -vz localhost "$LDAP_PORT"
	echo "$?"
}

function init_ldap() {
	LDIF_PATH=$(dirname $0)
	eval "ldapadd -x -H \"ldap://localhost:$LDAP_PORT\" -D \"cn=admin,dc=xivo,dc=org\" -w secret -f $LDIF_PATH/fullExport.ldif 1>&2"
	echo "$?"
}

echo "-----------------------------------------------------------------"
echo " Instanting a fusion directory web interface connected to"
echo " a docker volume with LDAP data."
echo " Once done, you can access it on @IP:12080 using fd-admin/secret"
echo "----------------------------------------------------------------"

docker volume create --name ldap-volume 

docker run -dit --name ldap-server-xivo \
	       -p "$LDAP_PORT":389 \
		   -e LDAP_DOMAIN=xivo.org \
		   -e LDAP_ROOTPW=secret \
		   -e LDAP_ADMIN_DN=admin \
		   -v ldap-volume:/var/openldap-data \
		   -h 192.168.56.1 \
		   mcreations/fusiondirectory-ldap

docker run -dit --name ldap-fusiondirectory-xivo \
	       -p 12080:80 \
		   -e LDAP_DOMAIN=xivo.org \
		   -e LDAP_ROOTPW=secret \
		   -e LDAP_ROOTDN=cn=admin,dc=xivo,dc=org \
		   --link ldap-server-xivo:ldap-server \
		   mcreations/fusiondirectory

count=0
connected=$(connect_to_ldap)
until [[ ("$connected" -eq 0 ) || ("$count" -gt "$WAIT_FOR_START") ]]; do
	connected=$(connect_to_ldap)
    count=$(($count+1))
    sleep 1
done

if [ "$count" -gt "$WAIT_FOR_START" ]; then 
	echo "Unable to connect to LDAP"
	exit $UNABLE_TO_CONNECT
fi

count=0
echo $count
added=$(init_ldap)
until [[ ("$added" -eq 0 ) || ("$count" -gt "$WAIT_FOR_INIT") ]]; do
	added=$(init_ldap)
	count=$(($count+1))
	sleep 1
done

if [ "$count" -gt "$WAIT_FOR_INIT" ]; then
	echo "Unable to add"
	exit $UNABLE_TO_ADD
fi

echo "LDAP init done"

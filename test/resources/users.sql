DROP TABLE IF EXISTS users CASCADE;
CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    entity_id INTEGER REFERENCES entities(id),
    id_on_xivo INTEGER NOT NULL,
    external_route_id INTEGER REFERENCES route(id),
    internal_route_id INTEGER REFERENCES route(id),
    template_id INTEGER,
    interval_id INTEGER
);

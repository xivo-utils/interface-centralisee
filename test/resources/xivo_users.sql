DROP TABLE IF EXISTS xivo_users CASCADE;

CREATE TABLE xivo_users (
  id SERIAL PRIMARY KEY,
  xivo_uuid UUID NOT NULL,
  entity_name VARCHAR(30),
  xivo_user_id INTEGER NOT NULL,
  first_name VARCHAR(128) NOT NULL,
  last_name VARCHAR(128) NOT NULL,
  internal_number VARCHAR(40),
  external_number VARCHAR(40),
  mail VARCHAR(128),
  cti_login VARCHAR(40),
  cti_password VARCHAR(128),
  provisioning_number VARCHAR(40),
  peer_sip_name peer_sip_name_modes,
  caller_id_mode caller_id_modes,
  custom_caller_id VARCHAR(20),
  ringing_time SMALLINT
);

ALTER TABLE xivo_users ADD CONSTRAINT xivo_users__unique UNIQUE(xivo_uuid, xivo_user_id);

DROP TABLE IF EXISTS template_xivo CASCADE;

CREATE TABLE template_xivo (
    template_id INTEGER NOT NULL REFERENCES template(id) ON DELETE CASCADE,
    xivo_id INTEGER NOT NULL,
    PRIMARY KEY(template_id, xivo_id)
);

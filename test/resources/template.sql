DROP TABLE IF EXISTS template CASCADE;
DROP TABLE IF EXISTS xivo_users CASCADE;
DROP TYPE IF EXISTS peer_sip_name_modes;
DROP TYPE IF EXISTS caller_id_modes;
DROP TYPE IF EXISTS voice_mail_number_modes;

CREATE TYPE peer_sip_name_modes AS ENUM ('auto', 'model', 'webrtc', 'ua');
CREATE TYPE caller_id_modes AS ENUM ('incomingNo', 'anonymous', 'custom');
CREATE TYPE voice_mail_number_modes AS ENUM ('short_number', 'custom');
CREATE TABLE template (
    id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    peer_sip_name peer_sip_name_modes NOT NULL,
    caller_id_mode caller_id_modes NOT NULL,
    custom_caller_id VARCHAR(20),
    ringing_time SMALLINT NOT NULL,
    voice_mail_enabled boolean NOT NULL,
    voice_mail_number_mode voice_mail_number_modes,
    voice_mail_custom_number VARCHAR(20),
    voice_mail_send_email boolean DEFAULT false NOT NULL,
    voice_mail_delete_after_notif boolean DEFAULT FALSE NOT NULL
);

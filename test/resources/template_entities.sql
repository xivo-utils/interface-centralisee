DROP TABLE IF EXISTS template_entities CASCADE;

CREATE TABLE template_entities (
    template_id INTEGER NOT NULL REFERENCES template(id) ON DELETE CASCADE,
    entity_combined_id CHARACTER VARYING(200) NOT NULL,
    PRIMARY KEY(template_id, entity_combined_id)
);

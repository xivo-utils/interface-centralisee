DROP TABLE IF EXISTS administrator_role CASCADE;

CREATE TABLE administrator_role (
    administrator_id INTEGER NOT NULL,
    role_id INTEGER NOT NULL,
    PRIMARY KEY (administrator_id, role_id)
);

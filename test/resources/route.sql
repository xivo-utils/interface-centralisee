DROP TABLE IF EXISTS route CASCADE;

CREATE TABLE route (
    id SERIAL PRIMARY KEY,
    digits character varying(50) NOT NULL,
    prefix boolean DEFAULT false NOT NULL,
    regexp character varying(50) DEFAULT '(.*)$'::character varying NOT NULL,
    target character varying(50) DEFAULT '\1'::character varying NOT NULL,
    subroutine character varying(50) DEFAULT NULL::character varying,
    context character varying(50) DEFAULT 'to_pit_stop'::character varying NOT NULL,
    comment character varying(1000),
    UNIQUE(digits, prefix)
);
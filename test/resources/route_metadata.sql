DROP TABLE IF EXISTS route_metadata CASCADE;

CREATE TABLE route_metadata(
    id SERIAL PRIMARY KEY,
    route_ref INTEGER,
    created_date TIMESTAMP WITHOUT TIME ZONE,
    created_by CHARACTER VARYING(100),
    modified_date TIMESTAMP WITHOUT TIME ZONE,
    modified_by CHARACTER VARYING(100),
    released_date TIMESTAMP WITHOUT TIME ZONE,
    released_by CHARACTER VARYING(100),
    manual BOOLEAN DEFAULT TRUE NOT NULL
);

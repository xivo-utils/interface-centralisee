DROP TABLE IF EXISTS template_intervals CASCADE;

CREATE TABLE template_intervals (
    template_id INTEGER NOT NULL REFERENCES template(id) ON DELETE CASCADE,
    interval_id INTEGER NOT NULL,
    PRIMARY KEY(template_id, interval_id)
);

DROP TYPE IF EXISTS interval_routing_modes CASCADE;
CREATE TYPE interval_routing_modes AS ENUM ('routed', 'not_routed', 'with_direct_number', 'with_customized_direct_number');

DROP TABLE IF EXISTS entity_intervals CASCADE;

CREATE TABLE entity_intervals (
    id SERIAL PRIMARY KEY,
    entity_id INTEGER REFERENCES entities(id),
    start VARCHAR(10) NOT NULL,
    "end" VARCHAR(10) NOT NULL,
    interval_routing_mode interval_routing_modes NOT NULL DEFAULT 'routed',
    direct_number VARCHAR(20),
    label VARCHAR(50)
);

DROP TABLE IF EXISTS permissions CASCADE;

CREATE TABLE permissions (
    id SERIAL UNIQUE,
    entity_id INTEGER NOT NULL,
    operation_id INTEGER NOT NULL,
    name CHARACTER VARYING(100) NOT NULL,
    PRIMARY KEY (id, entity_id, operation_id)
);

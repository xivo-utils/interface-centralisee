DROP TABLE IF EXISTS administrator_entity CASCADE;
DROP TABLE IF EXISTS administrators CASCADE;

CREATE TABLE administrators (
    id SERIAL PRIMARY KEY,
    login VARCHAR(40) UNIQUE NOT NULL,
    name VARCHAR(100) NOT NULL DEFAULT '' UNIQUE,
    hashed_password VARCHAR(128) NOT NULL,
    superadmin BOOLEAN DEFAULT FALSE,
    ldap BOOLEAN DEFAULT FALSE
);

CREATE TABLE administrator_entity (
    administrator_id INTEGER REFERENCES administrators(id),
    entity_id INTEGER REFERENCES entities(id),
    PRIMARY KEY (administrator_id, entity_id)
);

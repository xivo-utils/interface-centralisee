package testutils

import org.scalatest.{Matchers, WordSpec}
import org.scalatest.mock.MockitoSugar
import play.api.http.{HttpProtocol, Status, HeaderNames}
import play.api.test._

class IntegrationSpec extends WordSpec
  with IntegrationTestConfig
  with MockitoSugar
  with PlayRunners
  with HeaderNames
  with Status
  with HttpProtocol
  with DefaultAwaitTimeout
  with ResultExtractors
  with Writeables
  with RouteInvokers
  with FutureAwaits
  with Matchers {}

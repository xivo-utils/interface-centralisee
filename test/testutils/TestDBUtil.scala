package testutils

import java.sql.{Connection, DriverManager}
import java.util.Properties

import anorm.SQL
import org.dbunit.database.DatabaseConnection
import org.dbunit.dataset.xml.{FlatXmlDataSet, FlatXmlProducer}
import org.dbunit.operation.DatabaseOperation
import org.xml.sax.InputSource

trait TestDBUtil extends TestConfig {

  protected def openConnection(): Connection = {
    val URI = testConfig("db.default.url").toString
    val USER = testConfig("db.default.username").toString
    val PASSWORD = testConfig("db.default.password").toString
    val DB_DRIVER = testConfig("db.default.driver").toString

    Class.forName(DB_DRIVER)
    val props = new Properties()
    props.setProperty("user", USER)
    props.setProperty("password", PASSWORD)
    DriverManager.getConnection(URI, props)
  }

  protected def setupDB(filename: String)(implicit connection: Connection) = {
    val dataset = new FlatXmlDataSet(new FlatXmlProducer(new InputSource(getClass().getClassLoader().getResourceAsStream(filename))))
    for (table <- dataset.getTableNames()) {
      val createTable = scala.io.Source.fromInputStream(getClass().getClassLoader().getResourceAsStream(s"${table}.sql")).mkString
      SQL(createTable).execute
    }
    val dbunitConnection: DatabaseConnection = new DatabaseConnection(connection)
    DatabaseOperation.CLEAN_INSERT.execute(dbunitConnection, dataset)
  }

  protected def cleanTable(tableName: String)(implicit connection: Connection) {
    val st = connection.createStatement();
    st.executeUpdate(s"TRUNCATE TABLE $tableName CASCADE")
  }

}
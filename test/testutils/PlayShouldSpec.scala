package testutils

import org.scalatest._
import org.scalatestplus.play.WsScalaTestClient
import play.api.http.{HeaderNames, HttpProtocol, HttpVerbs, Status}
import play.api.test._

abstract class PlayShouldSpec extends WordSpec
  with Matchers
  with OptionValues
  with WsScalaTestClient
  with HeaderNames
  with Status
  with HttpProtocol
  with DefaultAwaitTimeout
  with ResultExtractors
  with Writeables
  with RouteInvokers
  with FutureAwaits
  with HttpVerbs {}

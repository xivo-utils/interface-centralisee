package testutils

case class RawAdmin(id: Option[Long], login: String, name: String, hashedPassword: String, superAdmin: Boolean, ldap: Boolean, entityIds: List[Long])

package testutils

import play.api.inject.guice.GuiceApplicationBuilder

trait IntegrationTestConfig extends TestConfig {

  val defaultLdapPortVariable = "TEST_LDAP_PORT"
  val defaultLdapPortValue = "389"
  lazy val testLdapPort = scala.util.Properties.envOrElse(defaultLdapPortVariable, defaultLdapPortValue)

  override lazy val testConfig = baseConfig ++ Map(
    "authentication.method" -> "ldap",
    "authentication.ldap.managerDN" -> "cn=admin,dc=xivo,dc=org",
    "authentication.ldap.managerPassword" -> "secret",
    "authentication.ldap.url" -> s"ldap://localhost:$testLdapPort",
    "authentication.ldap.searchBase" -> "ou=people,dc=xivo,dc=org",
    "authentication.ldap.userSearchFilter" -> "uid=%s")



  def app = new GuiceApplicationBuilder()
    .configure(testConfig)
    .build
}

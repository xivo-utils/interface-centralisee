package testutils

import play.api.inject.guice.GuiceApplicationBuilder

trait TestConfig {

  lazy val xivoCapacity = 1000
  lazy val restrictedUrls = List[String]()
  val defaultDbUrlVariable = "TEST_DB_DEFAULT_URL"
  val defaultDbUrlValue = "jdbc:postgresql://localhost/icx_test"
  lazy val testDefaultDbUrl = scala.util.Properties.envOrElse(defaultDbUrlVariable, defaultDbUrlValue)

  def baseConfig = Map(
    "db.default.url" -> testDefaultDbUrl,
    "db.default.username" -> "icx",
    "db.default.password" -> "icx",
    "db.default.driver" -> "org.postgresql.Driver",
    "evolutionplugin" -> "disabled",
    "xivoCapacity" -> xivoCapacity,
    "rights.restrictedUrls" -> restrictedUrls)

  lazy val testConfig = baseConfig ++ Map(
    "xivo.fileMapper" -> "conf/file_mapper",
    "authentication.method" -> "login",
    "authentication.xivo.restapi.users" -> "")


  def getApp = new GuiceApplicationBuilder()
    .configure(testConfig)
    .build
}

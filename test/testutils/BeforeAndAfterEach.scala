package testutils

import org.specs2.specification.BeforeAfterEach
import play.api.test.PlaySpecification

trait BeforeAndAfterEach extends PlaySpecification with BeforeAfterEach {
  def before = beforeEach()
  def after = afterEach()

  protected def beforeEach(): Unit = ()
  protected def afterEach(): Unit = ()
}

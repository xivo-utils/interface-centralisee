package testutils

import java.util.UUID

import anorm.SqlParser._
import anorm._
import model._

import scala.collection.mutable.ListBuffer

trait DbTest extends TestDBUtil {
  implicit val connection = openConnection()
  setupDB("database.xml")

  def insertXivo(xivo: Xivo): Xivo = {
    val genId: Option[Long] = SQL("INSERT INTO xivo (uuid, name, host, context_name, play_auth_token) VALUES ({uuid}::uuid, {name}, {host}, " +
      "{contextName}, {play_auth_token})")
      .on('id -> xivo.id, 'uuid -> xivo.uuid, 'name -> xivo.name, 'host -> xivo.host, 'contextName -> xivo.contextName, 'play_auth_token -> xivo.playAuthToken)
      .executeInsert()
    xivo.copy(id=genId)
  }

  val simpleEntity = get[Option[Long]]("id") ~
    get[CombinedId]("combined_id") ~
    get[String]("name") ~
    get[String]("display_name") ~
    get[Option[Long]]("xivo_id") ~
    get[UUID]("uuid") ~
    get[String]("xivo_name") ~
    get[String]("xivo_host") ~
    get[String]("xivo_context_name") ~
    get[List[Option[Int]]]("interval_id") ~
    get[List[Option[String]]]("starts") ~
    get[List[Option[String]]]("ends") ~
    get[List[Option[String]]]("interval_routing_mode") ~
    get[List[Option[String]]]("direct_number") ~
    get[List[Option[String]]]("labels") ~
    get[String]("presented_number") ~
    get[String]("mds_name") map {
    case id ~ combinedId ~ name ~ displayName ~ xId ~xUuid ~ xName ~ xHost ~ xContextName ~ intervalId ~ starts ~ ends ~
      routingMode ~ prefix ~ labels ~ presentedNum ~ mdsName=>
      Entity(id, combinedId, name, displayName, Xivo(xId, xUuid, xName, xHost, xContextName, None, Some("token")),
        ((0 until intervalId.length) map { i: Int =>
          Interval(intervalId(i), starts(i).get, ends(i).get,
            IntervalRoutingModes.fromString(routingMode(i).get).getOrElse(IntervalRoutingModes.Routed), prefix(i),
            labels(i).getOrElse("")) }).toList,
        presentedNum, mdsName)
  }

  def listEntities(): List[Entity] = SQL("SELECT e.id, e.combined_id, e.name, e.display_name, e.xivo_id, e.mds_name, x.uuid, " +
    "x.name AS xivo_name, x.host AS xivo_host, x.context_name AS xivo_context_name, " +
    "array_agg(ei.id) as interval_id, array_agg(ei.start) AS starts, array_agg(ei.end) AS ends, " +
    "array_agg(ei.interval_routing_mode) as interval_routing_mode, array_agg(ei.direct_number) as direct_number, " +
    "array_agg(label) as labels, e.presented_number " +
    "FROM entities e JOIN entity_intervals ei ON e.id = ei.entity_id " +
    "JOIN xivo x ON e.xivo_id = x.id GROUP BY e.id, e.combined_id, e.name, e.display_name, e.xivo_id, e.mds_name, x.name, x.host, " +
    "x.uuid, x.context_name, e.presented_number ORDER BY id ASC")
    .as(simpleEntity *)

  def insertEntity(e: Entity): Entity = {
    val genId: Option[Long] =
      SQL("INSERT INTO entities(combined_id, name, display_name, xivo_id, presented_number) VALUES ({combinedId}, {name}, {displayName}, {xivoId}, {presNum})").on(
      'combinedId -> e.combinedId.toString, 'name -> e.name, 'displayName -> e.displayName, 'xivoId -> e.xivo.id, 'presNum -> e.presentedNumber).executeInsert()
    var intervalsWithIds: ListBuffer[Interval] = new ListBuffer()
    e.intervals.foreach(i => {
      val intervalId = SQL("INSERT INTO entity_intervals(entity_id, start, \"end\", interval_routing_mode, direct_number, label) " +
        "VALUES ({eId}, {start}, {end}, {routing}::interval_routing_modes, {prefix}, {label})")
        .on('eId -> genId, 'start -> i.start, 'end -> i.end, 'routing -> IntervalRoutingModes.enumValues(i.routingMode),
          'prefix -> i.directNumber, 'label -> i.label).executeInsert()
      intervalsWithIds += i.copy(id = intervalId.map(_.toInt))
    })
    e.copy(id = genId).copy(intervals=intervalsWithIds.toList)
  }

  val simpleRoute = get[Option[Long]]("id") ~ get[String]("digits") ~ get[String]("regexp") ~ get[String]("context") ~ get[String]("target") map {
    case id ~ digits ~ regex ~ context ~ target => Route(id, digits, regex, context, target)
  }

  def listRoutes: List[Route] = SQL("SELECT id, digits, regexp, context, target FROM route").as(simpleRoute *)

  def insertRoute(route: Route): Option[Long] = SQL("INSERT INTO route (digits, prefix, regexp, target, context) VALUES ({digits}, {prefix}, {regexp}, {target}, {context})").on(
    'id -> route.id, 'digits -> route.digits, 'prefix -> route.prefix, 'regexp -> route.regexp,
    'target -> route.target, 'context -> route.context).executeInsert()

  val simpleUserLink = get[Option[Long]]("id") ~ get[Long]("entity_id") ~ get[Long]("id_on_xivo") ~
      get[Option[Long]]("external_route_id") ~ get[Option[Long]]("internal_route_id") ~
      get[Option[Int]]("interval_id") ~ get[Option[Int]]("template_id") map {
    case id ~ entityId ~ idOnXivo ~ extRouteId ~ intRouteId ~ intervalId ~ templateId =>
      UserLink(id, entityId, idOnXivo, extRouteId, intRouteId, intervalId, templateId)
  }

  def listUserLinks: List[UserLink] =
    SQL("SELECT id, entity_id, id_on_xivo, external_route_id, internal_route_id, interval_id, template_id " +
      "FROM users").as(simpleUserLink *)

  def insertUserLink(link: UserLink) =
    SQL("INSERT INTO users(id, entity_id, id_on_xivo, external_route_id, internal_route_id, interval_id, template_id) " +
      "VALUES ({id}, {entityId}, {idOnXivo}, {eRouteId}, {iRouteId}, {intervalId}, {templateId})").on(
    'id -> link.id, 'entityId -> link.entityId, 'idOnXivo -> link.idOnXivo, 'eRouteId -> link.externalRouteId,
      'iRouteId -> link.internalRouteId, 'intervalId -> link.intervalId, 'templateId -> link.templateId).executeUpdate()

  def listRouteEntityAssociations = SQL("SELECT entity_id, route_id FROM entities_routes").as((get[Long]("entity_id") ~ get[Long]("route_id") map {
    case eId ~ rId => (eId, rId)
  }) *)

  def simpleXivo = get[Option[Long]]("id") ~
    get[UUID]("uuid") ~
    get[String]("name") ~
    get[String]("host") ~
    get[String]("context_name") ~
    get[String]("play_auth_token") map {
    case id ~ uuid ~ name ~ host ~ contextName ~ token =>
      Xivo(id, uuid, name, host, contextName, None, Some(token))
  }

  def listXivo: List[Xivo] = SQL("SELECT id, uuid, name, host, context_name, play_auth_token FROM xivo").as(simpleXivo *)

  def insertAdmin(admin: Administrator, hash: String): Administrator = {
    val genId: Option[Long] = SQL("INSERT INTO administrators(login, name, hashed_password, superadmin, ldap) VALUES ({login}, {name}, {hash}, {superAdmin}, {ldap})").on(
    'login -> admin.login, 'name -> admin.name, 'superAdmin -> admin.superAdmin, 'hash -> hash, 'ldap -> admin.ldap).executeInsert()
    for(e <- admin.entities) {
      SQL("INSERT INTO administrator_entity(administrator_id, entity_id) VALUES ({adminId}, {entityId})").on('adminId -> genId, 'entityId -> e.id).executeUpdate()
    }
    admin.copy(id = genId, password=hash)
  }

  val simpleRawAdmin = get[Option[Long]]("id") ~
    get[String]("login") ~
    get[String]("name") ~
    get[String]("hashed_password") ~
    get[Boolean]("superadmin") ~
    get[Boolean]("ldap") ~
    get[List[Option[Long]]]("entity_ids") map {
    case id ~ login ~ name ~ hash ~ superAdmin ~ ldap ~ entityIds => RawAdmin(id, login, name, hash, superAdmin, ldap, entityIds.flatten)
  }

  def listRawAdmins: List[RawAdmin] = SQL("SELECT a.id, a.login, a.name, a.hashed_password, a.superadmin, a.ldap, array_agg(ae.entity_id) AS entity_ids" +
    " FROM administrators a LEFT JOIN administrator_entity ae ON a.id = ae.administrator_id GROUP BY a.id, a.login, a.hashed_password, a.ldap, a.superadmin")
    .as(simpleRawAdmin *)
}

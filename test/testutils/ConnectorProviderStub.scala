package testutils

import model.utils.XivoConnectorProvider
import xivo.ldap.xivoconnection.XivoConnector

trait ConnectorProviderStub {
  var xivoConnector: XivoConnector

  trait TestXivoConnectorProvider extends XivoConnectorProvider {
    override def get(host: String, incallContext: String, token: String): XivoConnector = xivoConnector
  }

}

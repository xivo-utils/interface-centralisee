package services

import java.util.UUID

import model._
import org.joda.time.DateTime
import org.scalatest.TestData
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerTest
import testutils.{DbTest, PlayShouldSpec}

import scala.util.Success

class UserLinkManagerSpec extends PlayShouldSpec with MockitoSugar with DbTest with OneAppPerTest {
  override def newAppForTest(testData: TestData) = getApp

  override def insertUserLink(userLink: UserLink) = {
    val res = super.insertUserLink(userLink)
    res shouldEqual 1
    res
  }

  class Helper {
    Seq("users", "route", "entity_intervals", "entities", "xivo").foreach(cleanTable)

    val xivoUuid = UUID.fromString("3acb24fa-0182-4c2a-89ad-4859312ffb7c")
    val xivo = insertXivo(Xivo(None, xivoUuid, "xivo", "192.168.1.123", "to_xivo1-name", None, Some("token")))
    val xivoDb = XivoDb(xivo.id, xivo.uuid, xivo.name, xivo.host, xivo.contextName, Some("token"))
    val intervalId = Some(112233)
    val myEntity = insertEntity(Entity(None, CombinedId(xivoUuid, "default"), "avranches", "Avranches", xivo, List(Interval(intervalId, "2000", "2099")), "52000", "default"))
    val otherEntity = insertEntity(Entity(None, CombinedId(xivoUuid, "other"), "avranches2", "Avranches2", xivo, List(Interval(Some(445566), "2100", "2199")), "52000", "default"))

    val routeManager = app.injector.instanceOf(classOf[RouteInterface])
    val routeMetaManager = app.injector.instanceOf(classOf[RouteMetaManager])

    val now = new DateTime()
    val eRoute = routeManager.create(Route(None, "52000", "(52000)", xivo.contextName, "2000"))
    routeMetaManager.create(RouteMeta(None, eRoute.id.get, now, "test", now, "test", now, "test", false))
    val iRoute = routeManager.create(Route(None, "2000", "(2000)", xivo.contextName, "2000"))
    routeMetaManager.create(RouteMeta(None, iRoute.id.get, now, "test", now, "test", now, "test", false))

    val userLinkManager = app.injector.instanceOf(classOf[UserLinkManager])
    val userLink10Id = 10L
    val userLink10 = UserLink(Some(userLink10Id), myEntity.id.get, 1, eRoute.id, iRoute.id, myEntity.intervals.head.id)
    val userLink20Id = 20L
    val userLink20 = UserLink(Some(userLink20Id), myEntity.id.get, 1, None, None)

  }

  "The UserLinkManager" should {

    "delete UserLink by id" in new Helper {
      insertUserLink(userLink10)
      userLinkManager.get(userLink10Id) shouldEqual Success(userLink10)
      userLinkManager.delete(userLink10Id) shouldEqual Success(userLink10)
      userLinkManager.get(userLink10Id).isFailure shouldEqual true
    }

    "update UserLink" in new Helper {
      insertUserLink(userLink20)
      userLinkManager.get(userLink20Id) shouldEqual Success(userLink20)
      val modified = userLink20.copy(
        entityId = otherEntity.id.get,
        idOnXivo = 998877L,
        externalRouteId = eRoute.id,
        internalRouteId = iRoute.id,
        intervalId = myEntity.intervals.head.id
      )
      userLinkManager.update(modified) shouldEqual Success(modified)
      userLinkManager.get(userLink20Id) shouldEqual Success(modified)
    }

    "get UserLink by id" in new Helper {
      insertUserLink(userLink10)
      userLinkManager.get(userLink10Id) shouldEqual Success(userLink10)
      insertUserLink(userLink20)
      userLinkManager.get(userLink20Id) shouldEqual Success(userLink20)
    }

    "get all UserLink-s" in new Helper {
      insertUserLink(userLink10)
      insertUserLink(userLink20)
      userLinkManager.all() shouldEqual Success(List(userLink10, userLink20))
    }

    "get all UserLink-s for Xivo" in new Helper {
      val xivoX2Uuid = UUID.fromString("96805a2b-aa1a-46f2-a7bb-4d4ac1549a2c")
      val xivoX2 = insertXivo(Xivo(None, xivoX2Uuid, "xivo2", "192.168.1.124", "to_xivo2-name", None, Some("token")))
      val intervalX2Id = Some(112233)
      val myX2Entity = insertEntity(Entity(None, CombinedId(xivoX2Uuid, "default"), "prague", "Prague", xivoX2, List(Interval(intervalX2Id, "9000", "9099")), "77000", "default"))
      val userLinkX2 = UserLink(Some(8765L), myX2Entity.id.get, 1, None, None)

      insertUserLink(userLink10)
      insertUserLink(userLink20)
      insertUserLink(userLinkX2)
      userLinkManager.findForXivo(xivo.id.get) shouldEqual Success(List(userLink10, userLink20))
    }

  }

}

package services

import model.RouteMeta
import org.joda.time.{DateTime, DateTimeUtils}
import org.scalatest.{BeforeAndAfterAllConfigMap, ConfigMap, TestData}
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerTest
import testutils.{DbTest, PlayShouldSpec}
import scala.util.{Failure, Success}

class RouteMetaManagerSpec extends PlayShouldSpec with MockitoSugar with DbTest with OneAppPerTest
  with BeforeAndAfterAllConfigMap {

  override def newAppForTest(testData: TestData) = getApp

  override def beforeAll(configMap: ConfigMap): Unit = {
    DateTimeUtils.setCurrentMillisFixed(10L)
  }

  override def afterAll(configMap: ConfigMap): Unit = {
    DateTimeUtils.setCurrentMillisSystem()
  }

  class Helper() {
    cleanTable("route_metadata")
    cleanTable("route_metadata")
    val routeMetaManager = app.injector.instanceOf(classOf[RouteMetaManager])
    val routeRef = 99
    val now = new DateTime()
    def getRouteMeta = RouteMeta(None, routeRef, now, "created", now, "updated", now, "deleted", false)
  }

  "The RouteMeta manager" should {

    "create RouteMeta in the database" in new Helper() {
      val created = routeMetaManager.create(getRouteMeta)
      created match {
        case Success(t) => t.id shouldNot be(None)
        case Failure(e) => fail(e.getMessage() + e.getStackTrace().mkString("\n"))
      }
    }

    "get RouteMeta from the database" in new Helper() {
      val routeMeta = routeMetaManager.create(getRouteMeta).get
      val expected = routeMeta.copy(id = routeMeta.id)
      routeMetaManager.get(routeRef) match {
        case Success(rm) => rm shouldBe routeMeta
        case Failure(e) => fail(e.getMessage() + e.getStackTrace().mkString("\n"))
      }
    }

    "update RouteMeta in the database" in new Helper() {
      val routeMeta = routeMetaManager.create(getRouteMeta).get
      val updated = routeMeta.copy(createdBy = "updated value")

      routeMetaManager.update(updated) match {
        case Success(rm) => rm shouldEqual(updated)
        case Failure(e) => fail(e.getMessage() + e.getStackTrace().mkString("\n"))
      }
    }

    "delete RouteMeta from the database" in new Helper() {
      val routeMeta = routeMetaManager.create(getRouteMeta).get

      val deleted = routeMetaManager.delete(routeRef)
      deleted match {
        case Success(rm) => rm shouldEqual routeMeta
        case Failure(e) => fail(e.getMessage() + e.getStackTrace().mkString("\n"))
      }
    }

    "setDeleted in the database" in new Helper() {
      val routeMeta = routeMetaManager.create(getRouteMeta).get
      val by = "testSetDeleted"
      DateTimeUtils.setCurrentMillisFixed(20L)
      val updated = new DateTime()

      routeMetaManager.setDeleted(routeRef, by) match {
        case Success(rm) => rm shouldEqual
          routeMeta.copy(modifiedDate = updated, modifiedBy = by, releasedDate = updated, releasedBy = by)
        case Failure(e) => fail(e.getMessage() + e.getStackTrace().mkString("\n"))
      }
    }

    "list administrators from the database" in new Helper() {
      val routeMeta99 = routeMetaManager.create(getRouteMeta).get
      val routeMeta100 = routeMetaManager.create(getRouteMeta.copy(routeRef=100, createdBy="100")).get

      val all = routeMetaManager.all()
      all match {
        case Success(rms) =>
           rms shouldBe List(routeMeta99, routeMeta100)
        case Failure(e) => fail(e.getMessage() + e.getStackTrace().mkString("\n"))
      }
    }


  }
}

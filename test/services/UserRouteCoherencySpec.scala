package services

import model._
import org.scalatest.mock.MockitoSugar
import org.mockito.Mockito._
import org.scalatestplus.play.OneAppPerSuite
import play.api.inject._
import play.api.inject.guice.GuiceApplicationBuilder
import testutils.{PlayShouldSpec, TestConfig}

class UserRouteCoherencySpec extends PlayShouldSpec with TestConfig with MockitoSugar with OneAppPerSuite {

  val entities = mock[EntityInterface]
  val users = mock[UserManager]
  val routes = mock[RouteInterface]
  override lazy val app = new GuiceApplicationBuilder()
    .configure(testConfig)
    .overrides(bind[EntityInterface].to(entities))
    .overrides(bind[UserManager].to(users))
    .overrides(bind[RouteInterface].to(routes))
    .build

  val userRoute = app.injector.instanceOf(classOf[UserRouteCoherencyImpl])

  "UserRouteCoherency" should {
    "find users without corresponding route" in {
      val e1 = mock[Entity]
      val e2 = mock[Entity]
      val u01 = mock[User]
      val u02 = mock[User]
      val u11 = mock[User]
      when(entities.listFromDb).thenReturn(List(e1, e2))
      when(users.getFromXivoAndCacheForEntity(e1)).thenReturn(UserList(List(u01, u02), List()))
      when(users.getFromXivoAndCacheForEntity(e2)).thenReturn(UserList(List(u11), List()))
      when(u01.internalNumber).thenReturn("200")
      when(u01.externalNumber).thenReturn(None)
      when(u02.internalNumber).thenReturn("200")
      when(u02.externalNumber).thenReturn(None)
      when(routes.routeExists("200")).thenReturn(true)
      when(u11.internalNumber).thenReturn("201")
      when(u11.externalNumber).thenReturn(Some("0123456"))
      when(routes.routeExists("201")).thenReturn(true)
      when(routes.routeExists("0123456")).thenReturn(false)
      val pbUsers = userRoute.getIncoherentUsers()
      pbUsers.length shouldBe(1)
      pbUsers.head shouldEqual(u11)
    }

    "not return users when routes are present" in {
      val e1 = mock[Entity]
      val e2 = mock[Entity]
      val u01 = mock[User]
      val u02 = mock[User]
      val u11 = mock[User]
      when(entities.listFromDb).thenReturn(List(e1, e2))
      when(users.getFromXivoAndCacheForEntity(e1)).thenReturn(UserList(List(u01, u02), List()))
      when(users.getFromXivoAndCacheForEntity(e2)).thenReturn(UserList(List(u11), List()))
      when(u01.internalNumber).thenReturn("200")
      when(u01.externalNumber).thenReturn(None)
      when(u02.internalNumber).thenReturn("200")
      when(u02.externalNumber).thenReturn(None)
      when(routes.routeExists("200")).thenReturn(true)
      when(u11.internalNumber).thenReturn("201")
      when(u11.externalNumber).thenReturn(Some("0123456"))
      when(routes.routeExists("201")).thenReturn(true)
      when(routes.routeExists("0123456")).thenReturn(true)
      val pbUsers = userRoute.getIncoherentUsers()
      pbUsers.length shouldBe(0)
    }

    "process correctly quoted extNo" in {
      val e1 = mock[Entity]
      val u01 = mock[User]
      when(entities.listFromDb).thenReturn(List(e1))
      when(users.getFromXivoAndCacheForEntity(e1)).thenReturn(UserList(List(u01), List()))
      when(u01.internalNumber).thenReturn("200")
      when(u01.externalNumber).thenReturn(Some("\"nom etc\""))
      when(routes.routeExists("200")).thenReturn(true)
      when(routes.routeExists("nom etc")).thenReturn(true)
      val pbUsers = userRoute.getIncoherentUsers()
      pbUsers.length shouldBe(0)
    }

  }

}

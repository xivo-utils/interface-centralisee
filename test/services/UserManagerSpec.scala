package services

import java.util.UUID

import model._
import model.utils.{ConfigParser, XivoConnectorPool}
import org.joda.time.{DateTime, DateTimeUtils}
import org.mockito.Matchers._
import org.mockito.Mockito._
import org.scalatest.TryValues._
import org.scalatest.mock.MockitoSugar
import org.scalatest.{BeforeAndAfterAllConfigMap, ConfigMap, TestData}
import org.scalatestplus.play.OneAppPerTest
import play.api.inject._
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import services.cache.CachedXivoUser.fromXivoUser
import services.cache.{CachedXivoUser, CachedXivoUserManager}
import services.helpers.GetTemplateFixture
import testutils._
import xivo.ldap.xivoconnection.{ExtensionLink, XivoConnector}
import xivo.restapi.connection.WebServicesException
import xivo.restapi.model.Links.UserVoicemail
import xivo.restapi.model.{User => XivoUser, _}

import scala.collection.JavaConversions._
import scala.util.{Failure, Success}

class UserManagerSpec extends PlayShouldSpec with MockitoSugar with TestConfig with DbTest
  with ConnectorProviderStub with OneAppPerTest with BeforeAndAfterAllConfigMap with GetTemplateFixture {

  override def beforeAll(configMap: ConfigMap): Unit = {
    DateTimeUtils.setCurrentMillisFixed(10L)
  }

  override def afterAll(configMap: ConfigMap): Unit = {
    DateTimeUtils.setCurrentMillisSystem()
  }

  var xivoConnector = mock[XivoConnector]

  override def newAppForTest(testData: TestData) = new GuiceApplicationBuilder()
    .configure(testConfig)
    .overrides(bind[CachedXivoUserManager].toInstance(mock[CachedXivoUserManager]))
    .overrides(bind[XivoUserTransformer].toInstance(mock[XivoUserTransformer]))
    .overrides(bind[TemplateManager].toInstance(mock[TemplateManager]))
    .build

  class Helper(vmMode: VoiceMailNumberMode.Type = VoiceMailNumberMode.ShortNumber) {
    val random = scala.util.Random
    val intervalId1 = Some(11)
    val intervalId2 = Some(12)

    List("users", "route", "route_metadata", "xivo").foreach(cleanTable)
    val xivoUuid = UUID.fromString("3acb24fa-0182-4c2a-89ad-4859312ffb7c")
    val xivo = insertXivo(Xivo(None, xivoUuid, "xivo1", "192.168.1.123", "to_xivo1-name", None, Some("token")))
    val xivoDb = XivoDb(xivo.id, xivo.uuid, xivo.name, xivo.host, xivo.contextName, Some("token"))
    val myEntity = insertEntity(Entity(None, mock[CombinedId], "avranches", "Avranches", xivo, List(Interval(intervalId1, "2000", "2099"), Interval(intervalId2, "4000", "4099")), "52000", "default"))
    reset(xivoConnector)

    val userManager = app.injector.instanceOf(classOf[UserManagerImpl])
    val configParser = mock[ConfigParser]
    userManager.connectorPool = new XivoConnectorPool(configParser) with TestXivoConnectorProvider
    val entityManager = mock[EntityInterface]
    val routeManager = app.injector.instanceOf(classOf[RouteInterface])
    val routeMetaManager = app.injector.instanceOf(classOf[RouteMetaManager])
    val cachedXivoUserManager = app.injector.instanceOf(classOf[CachedXivoUserManager])
    val userTransformer = app.injector.instanceOf(classOf[XivoUserTransformer])
    val templateManager = app.injector.instanceOf(classOf[TemplateManager])
    val now = new DateTime()

    val templateId = Some(1)
    val t = getTemplate(templateId.map(_.toLong))
      .copy(properties = getTemplate().properties.copy(voiceMailNumberMode = Some(vmMode)))
    stub(templateManager.get(1)).toReturn(Success(t))
    stub(xivoConnector.listVoicemails()).toReturn(List())

    val sampleUser: model.User = User(None, myEntity, "Pierre", "Durand", "2000", None, Some("test@example.com"), Some("p.durand"),
      Some("monmdp"), None, None, templateId)

    val defaultCtiProfile = new CtiProfile()
    defaultCtiProfile.setId(254)

    val voicemailSettings = VoicemailSettings(enabled = true, Some(VoiceMailNumberMode.ShortNumber), None, Some(true), Some(true))

    def getXivoUser(id: Int = 0, context: String = myEntity.name, firstName: String = "Pierre"): XivoUser = {
      val line = new Line("2000")
      line.setContext(context)
      line.setprovisioningCode("123456")
      val xivoUser = new XivoUser()
      if (id > 0) xivoUser.setId(id)
      xivoUser.setFirstname(firstName)
      xivoUser.setLastname("Durand")
      xivoUser.setLine(line)
      xivoUser.setRingSeconds(20)
      xivoUser.setOutcallerid(myEntity.presentedNumber)
      xivoUser.setCtiConfiguration(new CtiConfiguration(true, defaultCtiProfile))

      xivoUser
    }

    def createVoicemail(user: User): Voicemail = {
      val voicemail = new Voicemail()
      voicemail.setNumber(user.internalNumber)
      voicemail.setEmail(user.mail.get)
      voicemail.setContext(user.entity.name)
      voicemail.setName(user.firstName + " " + user.lastName)
      voicemail
    }
  }

  "The User interface implementation" should {
    def initXivoUser(xivouser: XivoUser): Unit = {

    }

    "insert a user in the database and in the xivo" in new Helper() {
      val user = User(None, myEntity, "Pierre", "Durand", "2000", None, None, None, None, None, intervalId1, templateId)
      val xivoUser = getXivoUser()
      val createdXivoUser = getXivoUser(id = 123)
      stub(xivoConnector.createUser(xivoUser)).toReturn(createdXivoUser)
      stub(userTransformer.createXivoUser(user)).toReturn(xivoUser)

      userManager.create(user, "test") match {
        case Success(userWithId) =>
          userWithId.id.isDefined shouldBe true
          val routes = listRoutes
          routes.map(r => r.copy(id = None)) shouldEqual List(Route(None, "2000", "(2000)", myEntity.xivo.contextName, "\\1"))
          listUserLinks shouldEqual List(
            UserLink(userWithId.id, user.entity.id.get, createdXivoUser.getId.toLong, None, routes.head.id, intervalId1, templateId))
          routeMetaManager.get(routes.head.id.get).get.copy(id = None) shouldEqual
            RouteMeta(None, routes.head.id.get, now, "test", now, "test", now, "test", false)
          verify(xivoConnector).createUser(xivoUser)
          verify(xivoConnector).createLineForUser(createdXivoUser)
        case Failure(e) => fail(e.getMessage() + e.getStackTrace().mkString("\n"))
      }

      verify(cachedXivoUserManager).createOrUpdateForXivoUser(any[CachedXivoUser])
    }

    "Create a proxy user without pushing to xivo database" in new Helper() {
      val user = User(None, myEntity, "Pierre", "Durand", "2000", None, None, None, None, None, intervalId1, templateId)
      val xivoUser = getXivoUser()
      stub(cachedXivoUserManager.getNextIdForXivo(user.entity.xivo.uuid)).toReturn(Success(42))

      userManager.create(user, "test", withConfiguration = false) match {
        case Success(userWithId) =>
          userWithId.id.isDefined shouldBe false
          val routes = listRoutes
          routes.map(r => r.copy(id = None)) shouldEqual List(Route(None, "2000", "(2000)", myEntity.xivo.contextName, "\\1"))
          listUserLinks shouldEqual List(
            UserLink(Some(2), user.entity.id.get, 42L, None, routes.head.id, intervalId1, templateId))
          routeMetaManager.get(routes.head.id.get).get.copy(id = None) shouldEqual
            RouteMeta(None, routes.head.id.get, now, "test", now, "test", now, "test", false)
          verify(xivoConnector, never()).createUser(any())
          verify(xivoConnector, never()).createLineForUser(any())
        case Failure(e) => fail(e.getMessage() + e.getStackTrace().mkString("\n"))
      }

      verify(cachedXivoUserManager).createOrUpdateForXivoUser(any[CachedXivoUser])
    }

    "insert a user in the xivo with a Cti configuration and a voicemail when associated attributes are defined" in new Helper() {
      val xivoUser = getXivoUser()
      val createdXivoUser = getXivoUser(id = 123)

      stub(xivoConnector.getDefaultCtiProfile()).toReturn(defaultCtiProfile)
      stub(xivoConnector.createUser(xivoUser)).toReturn(createdXivoUser)
      stub(userTransformer.createXivoUser(sampleUser)).toReturn(xivoUser)

      val userWithId = userManager.create(sampleUser).success.value

      userWithId.id.isDefined shouldBe true
      val routes = listRoutes
      routes.map(r => r.copy(id = None)) shouldEqual
        List(Route(None, "2000", "(2000)", myEntity.xivo.contextName, "\\1"))
      listUserLinks shouldEqual List(
        UserLink(userWithId.id, sampleUser.entity.id.get, createdXivoUser.getId.toLong, None,
          routes.find(r => r.digits == "2000").get.id, None, templateId))
      verify(xivoConnector).createUser(xivoUser)
      verify(xivoConnector).createLineForUser(createdXivoUser)
      verify(xivoConnector).createVoicemailForUser(createdXivoUser)

      verify(cachedXivoUserManager).createOrUpdateForXivoUser(any[CachedXivoUser])
    }

    "create a user with a special route for his external number" in new Helper() {
      val user =
        User(None, myEntity, "Pierre", "Durand", "2000", Some("12342000"), None, None, None, None, None, templateId)
      val line = new Line("2000")
      line.setContext(myEntity.name)
      line.setExtensionId(1)
      val incall = new IncomingCall()
      incall.setSda(user.externalNumber.get)

      val xivoUser = new XivoUser()
      xivoUser.setFirstname("Pierre")
      xivoUser.setLastname("Durand")
      xivoUser.setLine(line)
      xivoUser.setOutcallerid("12342000")
      xivoUser.setIncomingCall(incall)

      val route = Route(None, "52000", "(52000)", myEntity.name)
      val createdUser = new XivoUser()
      createdUser.setId(5)
      createdUser.setLine(line)
      createdUser.setIncomingCall(incall)

      stub(xivoConnector.createUser(xivoUser)).toReturn(createdUser)
      stub(userTransformer.createXivoUser(user)).toReturn(xivoUser)

      val userWithId = userManager.create(user).get

      userWithId.id.isDefined shouldBe true
      listUserLinks(0).externalRouteId.isDefined shouldBe true
      listRoutes.map(r => r.copy(id = None)) shouldEqual List(
        Route(None, "12342000", "(12342000)", xivo.contextName, "2000"),
        Route(None, "2000", "(2000)", xivo.contextName, "\\1"))

      verify(xivoConnector).createUser(xivoUser)
      verify(xivoConnector).createIncallForUser(createdUser)
    }

    "create a user with incoming call if in a routed interval" in new Helper() {
      val user =
        User(None, myEntity, "Pierre", "Durand", "2000", Some("12342000"), None, None, None, None, intervalId1, templateId)
      val line = new Line("2000")
      line.setContext(myEntity.name)
      line.setExtensionId(1)
      val incall = new IncomingCall()
      incall.setSda(user.externalNumber.get)
      val xivoUser = new XivoUser()
      xivoUser.setFirstname("Pierre")
      xivoUser.setLastname("Durand")
      xivoUser.setLine(line)
      xivoUser.setOutcallerid("12342000")
      xivoUser.setIncomingCall(incall)

      val route = Route(None, "52000", "(52000)", myEntity.name)
      val createdUser = new XivoUser()
      createdUser.setId(5)
      createdUser.setLine(line)
      createdUser.setIncomingCall(incall)
      stub(xivoConnector.createUser(xivoUser)).toReturn(createdUser)
      stub(userTransformer.createXivoUser(user)).toReturn(xivoUser)

      val userWithId = userManager.create(user).get

      userWithId.id.isDefined shouldBe true
      listUserLinks(0).externalRouteId.isDefined shouldBe true
      listRoutes.map(r => r.copy(id = None)) shouldEqual List(
        Route(None, "12342000", "(12342000)", xivo.contextName, "2000"),
        Route(None, "2000", "(2000)", xivo.contextName, "\\1"))

      verify(xivoConnector).createUser(xivoUser)
      verify(xivoConnector).createIncallForUser(createdUser)
    }

    "list users by Entity retrieving them from Xivo, cache, persist as User(Link) and return (with IDs of users deleted at Xivo)" in new Helper() {
      Seq("users", "xivo_users").foreach(cleanTable)
      val xuser1 = getXivoUser(100, myEntity.name, "Ann")
      val cached1 = fromXivoUser(xivoUuid, xuser1).copy(id = Some(501))
      val xuser2 = getXivoUser(200, myEntity.name, "Jacque")
      val cached2 = fromXivoUser(xivoUuid, xuser2).copy(id = Some(502))
      val xuser3 = getXivoUser(100, "other entity")
      val xusers = List(xuser1, xuser2, xuser3)

      val idOfUserDeletedAtXivo = 9999L
      val unusedXivoUserId = 123456L
      insertUserLink(UserLink(Some(idOfUserDeletedAtXivo), myEntity.id.get, unusedXivoUserId, None, None)) shouldEqual 1

      stub(xivoConnector.getAllUsers()).toReturn(xusers)
      stub(cachedXivoUserManager.createOrUpdateForXivoUser(fromXivoUser(xivoUuid, xuser1))).toReturn(Success(fromXivoUser(xivoUuid, xuser1)))
      stub(cachedXivoUserManager.createOrUpdateForXivoUser(fromXivoUser(xivoUuid, xuser2))).toReturn(Success(fromXivoUser(xivoUuid, xuser2)))
      stub(cachedXivoUserManager.createOrUpdateForXivoUser(fromXivoUser(xivoUuid, xuser3))).toReturn(Success(fromXivoUser(xivoUuid, xuser3)))
      stub(cachedXivoUserManager.allInEntity(xivoUuid, myEntity.name)).toReturn(Success(List(cached1, cached2)))

      val userList = userManager.getFromXivoAndCacheForEntity(myEntity)
      userList.users.map(_.firstName) shouldEqual List(xuser1.getFirstname, xuser2.getFirstname)
      userList.ghostIds shouldEqual List(idOfUserDeletedAtXivo)
    }

    "cache xivo users" in new Helper() {
      cleanTable("xivo_users")
      val xuser1 = getXivoUser(100, "avranches")
      val xuser2 = getXivoUser(200, "avranches", "Jacque")
      val xuser3 = getXivoUser(100, "other")
      val xusers = List(xuser1, xuser2, xuser3)
      stub(xivoConnector.getAllUsers()).toReturn(xusers)
      stub(cachedXivoUserManager.createOrUpdateForXivoUser(fromXivoUser(xivoUuid, xuser1))).toReturn(Success(fromXivoUser(xivoUuid, xuser1)))
      stub(cachedXivoUserManager.createOrUpdateForXivoUser(fromXivoUser(xivoUuid, xuser2))).toReturn(Success(fromXivoUser(xivoUuid, xuser2)))
      stub(cachedXivoUserManager.createOrUpdateForXivoUser(fromXivoUser(xivoUuid, xuser3))).toReturn(Success(fromXivoUser(xivoUuid, xuser3)))

      userManager.getAndCacheXivoUsers(xivo.uuid, xivo)

      verify(cachedXivoUserManager).createOrUpdateForXivoUser(fromXivoUser(xivoUuid, xuser1))
      verify(cachedXivoUserManager).createOrUpdateForXivoUser(fromXivoUser(xivoUuid, xuser2))
      verify(cachedXivoUserManager).createOrUpdateForXivoUser(fromXivoUser(xivoUuid, xuser3))
    }

    "list users by Entity retrieving them from Xivo user cache" in new Helper() {
      cleanTable("xivo_users")
      val xuser1 = getXivoUser(100, myEntity.name)
      val cached1 = fromXivoUser(xivoUuid, xuser1).copy(id = Some(501))
      val xuser2 = getXivoUser(200, myEntity.name, "Jacque")
      val cached2 = fromXivoUser(xivoUuid, xuser2).copy(id = Some(502))

      stub(cachedXivoUserManager.allInEntity(xivoUuid, myEntity.name)).toReturn(Success(List(cached1, cached2)))

      insertUserLink(UserLink(Some(1010), myEntity.id.get, xuser1.getId.toLong, None, None)) shouldEqual 1
      insertUserLink(UserLink(Some(1020), myEntity.id.get, xuser2.getId.toLong, None, None)) shouldEqual 1

      val users = userManager.listForEntityFromCache(myEntity)

      val usersSorted = users.users.sortBy(_.id)
      usersSorted.length shouldEqual 2
      usersSorted(0).id shouldEqual Some(1010)
      usersSorted(0).firstName shouldEqual xuser1.getFirstname
      usersSorted(1).id shouldEqual Some(1020)
      usersSorted(1).firstName shouldEqual xuser2.getFirstname
    }

    "Delete a proxy user without pushing to xivo database" in new Helper() {
      val user = sampleUser.copy(id = Some(9876))

      val eRouteId = routeManager.create(Route(None, "52000", "(52000)", xivo.contextName)).id.get
      routeMetaManager.create(RouteMeta(None, eRouteId, now, "test", now, "test", now, "test", false))
      val iRouteId = routeManager.create(Route(None, "2000", "(2000)", xivo.contextName)).id.get
      routeMetaManager.create(RouteMeta(None, iRouteId, now, "test", now, "test", now, "test", false))
      insertUserLink(UserLink(user.id, myEntity.id.get, 3, Some(eRouteId), Some(iRouteId)))

      stub(entityManager.getEntity(myEntity.id.get)).toReturn(Some(myEntity))
      stub(cachedXivoUserManager.delete(any(), any())).toReturn(Success(true))

      DateTimeUtils.setCurrentMillisFixed(20L)
      val deleted = new DateTime()

      userManager.delete(user.id.get, entityManager, "deleter", withConfiguration = false)
      listUserLinks shouldEqual List()

      routeManager.getRoute(eRouteId) match {
        case None => fail("external route not found")
        case Some(route) => route.context shouldEqual Route.targetDisabled
      }
      routeManager.getRoute(iRouteId) match {
        case None => fail("internal route not found")
        case Some(route) => route.context shouldEqual Route.targetDisabled
      }

      routeMetaManager.get(eRouteId).get.copy(id = None) shouldBe
        RouteMeta(None, eRouteId, now, "test", deleted, "deleter", deleted, "deleter", false)

      routeMetaManager.get(iRouteId).get.copy(id = None) shouldBe
        RouteMeta(None, iRouteId, now, "test", deleted, "deleter", deleted, "deleter", false)
    }

    "Delete a XiVO user" in new Helper() {
      val user = sampleUser.copy(id = Some(9876))

      val xivoUser = getXivoUser(444)
      val voicemail = createVoicemail(user)
      voicemail.setId(555)
      xivoUser.setVoicemail(voicemail)

      val eRouteId = routeManager.create(Route(None, "52000", "(52000)", xivo.contextName)).id.get
      routeMetaManager.create(RouteMeta(None, eRouteId, now, "test", now, "test", now, "test", false))
      val iRouteId = routeManager.create(Route(None, "2000", "(2000)", xivo.contextName)).id.get
      routeMetaManager.create(RouteMeta(None, iRouteId, now, "test", now, "test", now, "test", false))
      insertUserLink(UserLink(user.id, myEntity.id.get, 3, Some(eRouteId), Some(iRouteId)))

      stub(xivoConnector.getUser(3)).toReturn(xivoUser)
      stub(xivoConnector.getUsersIdsAssociatedToVoicemail(voicemail.getId)).toReturn(Nil)
      stub(entityManager.getEntity(myEntity.id.get)).toReturn(Some(myEntity))
      stub(cachedXivoUserManager.delete(xivoUuid, xivoUser.getId)).toReturn(Success(true))

      DateTimeUtils.setCurrentMillisFixed(20L)
      val deleted = new DateTime()

      userManager.delete(user.id.get, entityManager, "deleter")
      listUserLinks shouldEqual List()

      routeManager.getRoute(eRouteId) match {
        case None => fail("external route not found")
        case Some(route) => route.context shouldEqual Route.targetDisabled
      }
      routeManager.getRoute(iRouteId) match {
        case None => fail("internal route not found")
        case Some(route) => route.context shouldEqual Route.targetDisabled
      }

      routeMetaManager.get(eRouteId).get.copy(id = None) shouldBe
        RouteMeta(None, eRouteId, now, "test", deleted, "deleter", deleted, "deleter", false)

      routeMetaManager.get(iRouteId).get.copy(id = None) shouldBe
        RouteMeta(None, iRouteId, now, "test", deleted, "deleter", deleted, "deleter", false)

      val order = org.mockito.Mockito.inOrder(xivoConnector, xivoConnector)
      order.verify(xivoConnector).getUser(3)
      order.verify(xivoConnector).deleteVoicemailForUser(xivoUser)
      order.verify(xivoConnector).deleteUser(xivoUser)
      order.verify(xivoConnector).deleteLineForUser(xivoUser)
      order.verify(xivoConnector).deleteIncallForUser(xivoUser)
    }

    "reset user's line to autoprov" in new Helper() {
      val user = sampleUser.copy(id = Some(9876))

      val xivoUser = getXivoUser(444)
      xivoUser.getLine.setDeviceId("some-device-id")

      val eRouteId = routeManager.create(Route(None, "52000", "(52000)", xivo.contextName)).id.get
      routeMetaManager.create(RouteMeta(None, eRouteId, now, "test", now, "test", now, "test", false))
      val iRouteId = routeManager.create(Route(None, "2000", "(2000)", xivo.contextName)).id.get
      routeMetaManager.create(RouteMeta(None, iRouteId, now, "test", now, "test", now, "test", false))
      insertUserLink(UserLink(user.id, myEntity.id.get, 3, Some(eRouteId), Some(iRouteId)))

      stub(xivoConnector.getUser(3)).toReturn(xivoUser)
      stub(entityManager.getEntity(myEntity.id.get)).toReturn(Some(myEntity))

      DateTimeUtils.setCurrentMillisFixed(20L)
      val deleted = new DateTime()

      userManager.disconnectDevice(user.id.get, entityManager, "deleter") shouldEqual true

      val order = org.mockito.Mockito.inOrder(xivoConnector, xivoConnector)
      order.verify(xivoConnector).resetAutoprov(any[Device])
      order.verify(xivoConnector).synchronizeDevice(any[Device])
    }

    "returns false if asked to reset user's line to autoprov but user's line has no device" in new Helper() {
      val user = sampleUser.copy(id = Some(9876))

      val xivoUser = getXivoUser(444)

      val eRouteId = routeManager.create(Route(None, "52000", "(52000)", xivo.contextName)).id.get
      routeMetaManager.create(RouteMeta(None, eRouteId, now, "test", now, "test", now, "test", false))
      val iRouteId = routeManager.create(Route(None, "2000", "(2000)", xivo.contextName)).id.get
      routeMetaManager.create(RouteMeta(None, iRouteId, now, "test", now, "test", now, "test", false))
      insertUserLink(UserLink(user.id, myEntity.id.get, 3, Some(eRouteId), Some(iRouteId)))

      stub(xivoConnector.getUser(3)).toReturn(xivoUser)
      stub(entityManager.getEntity(myEntity.id.get)).toReturn(Some(myEntity))

      DateTimeUtils.setCurrentMillisFixed(20L)
      val deleted = new DateTime()

      userManager.disconnectDevice(user.id.get, entityManager, "deleter") shouldEqual false
      verify(xivoConnector, never()).resetAutoprov(any[Device])
      verify(xivoConnector, never()).synchronizeDevice(any[Device])
    }

    "returns false if asked to reset user's line to autoprov but user has no line" in new Helper() {
      val user = sampleUser.copy(id = Some(9876))

      val xivoUser = getXivoUser(444)
      xivoUser.setLine(null)

      val eRouteId = routeManager.create(Route(None, "52000", "(52000)", xivo.contextName)).id.get
      routeMetaManager.create(RouteMeta(None, eRouteId, now, "test", now, "test", now, "test", false))
      val iRouteId = routeManager.create(Route(None, "2000", "(2000)", xivo.contextName)).id.get
      routeMetaManager.create(RouteMeta(None, iRouteId, now, "test", now, "test", now, "test", false))
      insertUserLink(UserLink(user.id, myEntity.id.get, 3, Some(eRouteId), Some(iRouteId)))

      stub(xivoConnector.getUser(3)).toReturn(xivoUser)
      stub(entityManager.getEntity(myEntity.id.get)).toReturn(Some(myEntity))

      DateTimeUtils.setCurrentMillisFixed(20L)
      val deleted = new DateTime()

      userManager.disconnectDevice(user.id.get, entityManager, "deleter") shouldEqual false
      verify(xivoConnector, never()).resetAutoprov(any[Device])
      verify(xivoConnector, never()).synchronizeDevice(any[Device])
    }

    "delete a user which is not on the xivo" in new Helper() {
      val userId = 5
      val xivoUserId = 6
      val eRouteId = routeManager.create(Route(None, "52000", "(52000)", xivo.contextName)).id.get
      routeMetaManager.create(RouteMeta(None, eRouteId, now, "test", now, "test", now, "test", false))
      val iRouteId = routeManager.create(Route(None, "2000", "(2000)", xivo.contextName)).id.get
      routeMetaManager.create(RouteMeta(None, iRouteId, now, "test", now, "test", now, "test", false))
      insertUserLink(UserLink(Some(userId), myEntity.id.get, xivoUserId, Some(eRouteId), Some(iRouteId)))
      stub(xivoConnector.getUser(xivoUserId)).toThrow(new WebServicesException("test", 404))
      stub(entityManager.getEntity(myEntity.id.get)).toReturn(Some(myEntity))
      DateTimeUtils.setCurrentMillisFixed(30L)
      val deleted = new DateTime()

      userManager.delete(userId, entityManager, "deleter")

      listUserLinks shouldEqual List()
      listRoutes.map(r => r.copy(id = None)) shouldEqual
        List(Route(None, "52000", "(52000)", "to_pit_stop"), Route(None, "2000", "(2000)", "to_pit_stop"))

      routeMetaManager.get(eRouteId).get.copy(id = None) shouldBe
        RouteMeta(None, eRouteId, now, "test", deleted, "deleter", deleted, "deleter", false)

      routeMetaManager.get(iRouteId).get.copy(id = None) shouldBe
        RouteMeta(None, iRouteId, now, "test", deleted, "deleter", deleted, "deleter", false)

      verify(xivoConnector).getUser(xivoUserId)
      verify(xivoConnector, never()).deleteUser(anyObject())
      verify(xivoConnector, never()).deleteVoicemailForUser(anyObject())
      verify(xivoConnector, never()).deleteLineForUser(anyObject())

      verify(cachedXivoUserManager, never()).delete(anyLong())

    }

    "throw a NoSuchElementException if the user does not exist" in new Helper() {
      intercept[NoSuchElementException] {
        userManager.delete(3, entityManager) shouldEqual ("L'utilisateur 3 n'existe pas")
      }
    }

    "retrieve a User by id" in new Helper() {
      val user = User(Some(2L), myEntity, "Pierre", "Durand", "2000", Some("52000"), Some("pdurand@test.com"), Some("pdurand"), Some("pwd"), Some("123456"), peerSipName = Some(PeerSipNameMode.Auto))

      def sampleLine = {
        val line = new Line("2000")
        line.setContext(myEntity.name)
        line.setprovisioningCode("123456")
        line.setExtensionId(777)
        line.setNumber("1111")
        line
      }

      val line = sampleLine

      val voicemail = new Voicemail()
      voicemail.setNumber("2000")
      voicemail.setEmail("pdurand@test.com")
      voicemail.setAttach(true)
      voicemail.setDeleteMessages(true)
      val ctiConfig = new CtiConfiguration(true, new CtiProfile())
      val xivoUser = new XivoUser()
      xivoUser.setFirstname("Pierre")
      xivoUser.setLastname("Durand")
      xivoUser.setUsername("pdurand")
      xivoUser.setPassword("pwd")
      xivoUser.setLine(line)
      xivoUser.setVoicemail(voicemail)
      xivoUser.setCtiConfiguration(ctiConfig)
      val eRouteId = routeManager.create(Route(None, "52000", "(52000)", xivo.contextName)).id.get
      val iRouteId = routeManager.create(Route(None, "2000", "(2000)", xivo.contextName)).id.get
      insertUserLink(UserLink(Some(2), myEntity.id.get, 3, Some(eRouteId), Some(iRouteId)))

      stub(xivoConnector.getUser(3)).toReturn(xivoUser)
      stub(xivoConnector.getInternalNumberForUser(myEntity.name, 3)).toReturn(new ExtensionLink(789, "2222"))
      stub(entityManager.getEntity(myEntity.id.get)).toReturn(Some(myEntity))

      userManager.getUserWithVoicemailFromXivo(user.id.get, entityManager) shouldEqual Some(UserWithVM(
        user.copy(internalNumber = "2222", peerSipName = Some(PeerSipNameMode.Auto)),
        VoicemailSettings(enabled = true, Some(VoiceMailNumberMode.Custom), Some("2000"), Some(true), Some(true))
      ))
    }

    "retrieve a User with webrtc line" in new Helper() {
      val user = User(Some(2L), myEntity, "Pierre", "Durand", "2000", Some("52000"), Some("pdurand@test.com"), Some("pdurand"), Some("pwd"), Some("123456"), peerSipName = Some(PeerSipNameMode.Auto))

      def sampleLine = {
        val line = new Line("2000")
        line.setContext(myEntity.name)
        line.setprovisioningCode("123456")
        line.setExtensionId(777)
        line.setNumber("1111")
        line.setOptions("webrtc", "yes")
        line.setOptions("type", "friend")
        line
      }

      val line = sampleLine

      val voicemail = new Voicemail()
      voicemail.setNumber("2000")
      voicemail.setEmail("pdurand@test.com")
      voicemail.setAttach(true)
      voicemail.setDeleteMessages(true)
      val ctiConfig = new CtiConfiguration(true, new CtiProfile())
      val xivoUser = new XivoUser()
      xivoUser.setFirstname("Pierre")
      xivoUser.setLastname("Durand")
      xivoUser.setUsername("pdurand")
      xivoUser.setPassword("pwd")
      xivoUser.setLine(line)
      xivoUser.setVoicemail(voicemail)
      xivoUser.setCtiConfiguration(ctiConfig)
      val eRouteId = routeManager.create(Route(None, "52000", "(52000)", xivo.contextName)).id.get
      val iRouteId = routeManager.create(Route(None, "2000", "(2000)", xivo.contextName)).id.get
      insertUserLink(UserLink(Some(2), myEntity.id.get, 3, Some(eRouteId), Some(iRouteId)))

      stub(xivoConnector.getUser(3)).toReturn(xivoUser)
      stub(xivoConnector.getInternalNumberForUser(myEntity.name, 3)).toReturn(new ExtensionLink(789, "2222"))
      stub(entityManager.getEntity(myEntity.id.get)).toReturn(Some(myEntity))

      userManager.getUserWithVoicemailFromXivo(user.id.get, entityManager) shouldEqual Some(UserWithVM(
        user.copy(internalNumber = "2222", peerSipName = Some(PeerSipNameMode.WebRTC)),
        VoicemailSettings(enabled = true, Some(VoiceMailNumberMode.Custom), Some("2000"), Some(true), Some(true))
      ))
    }

    "retrieve a User with ua line" in new Helper() {
      val user = User(Some(2L), myEntity, "Pierre", "Durand", "2000", Some("52000"), Some("pdurand@test.com"), Some("pdurand"), Some("pwd"), Some("123456"), peerSipName = Some(PeerSipNameMode.Auto))

      def sampleLine = {
        val line = new Line("2000")
        line.setContext(myEntity.name)
        line.setprovisioningCode("123456")
        line.setExtensionId(777)
        line.setNumber("1111")
        line.setOptions("webrtc", "ua")
        line.setOptions("type", "friend")
        line
      }

      val line = sampleLine

      val voicemail = new Voicemail()
      voicemail.setNumber("2000")
      voicemail.setEmail("pdurand@test.com")
      voicemail.setAttach(true)
      voicemail.setDeleteMessages(true)
      val ctiConfig = new CtiConfiguration(true, new CtiProfile())
      val xivoUser = new XivoUser()
      xivoUser.setFirstname("Pierre")
      xivoUser.setLastname("Durand")
      xivoUser.setUsername("pdurand")
      xivoUser.setPassword("pwd")
      xivoUser.setLine(line)
      xivoUser.setVoicemail(voicemail)
      xivoUser.setCtiConfiguration(ctiConfig)
      val eRouteId = routeManager.create(Route(None, "52000", "(52000)", xivo.contextName)).id.get
      val iRouteId = routeManager.create(Route(None, "2000", "(2000)", xivo.contextName)).id.get
      insertUserLink(UserLink(Some(2), myEntity.id.get, 3, Some(eRouteId), Some(iRouteId)))

      stub(xivoConnector.getUser(3)).toReturn(xivoUser)
      stub(xivoConnector.getInternalNumberForUser(myEntity.name, 3)).toReturn(new ExtensionLink(789, "2222"))
      stub(entityManager.getEntity(myEntity.id.get)).toReturn(Some(myEntity))

      userManager.getUserWithVoicemailFromXivo(user.id.get, entityManager) shouldEqual Some(UserWithVM(
        user.copy(internalNumber = "2222", peerSipName = Some(PeerSipNameMode.UniqueAccount)),
        VoicemailSettings(enabled = true, Some(VoiceMailNumberMode.Custom), Some("2000"), Some(true), Some(true))
      ))
    }

    "retrieve a User with auto line" in new Helper() {
      val user = User(Some(2L), myEntity, "Pierre", "Durand", "2000", Some("52000"), Some("pdurand@test.com"), Some("pdurand"), Some("pwd"), Some("123456"), peerSipName = Some(PeerSipNameMode.Auto))

      def sampleLine = {
        val line = new Line("2000")
        line.setContext(myEntity.name)
        line.setprovisioningCode("123456")
        line.setExtensionId(777)
        line.setNumber("1111")
        line.setOptions("type", "friend")
        line
      }

      val line = sampleLine

      val voicemail = new Voicemail()
      voicemail.setNumber("2000")
      voicemail.setEmail("pdurand@test.com")
      voicemail.setAttach(true)
      voicemail.setDeleteMessages(true)
      val ctiConfig = new CtiConfiguration(true, new CtiProfile())
      val xivoUser = new XivoUser()
      xivoUser.setFirstname("Pierre")
      xivoUser.setLastname("Durand")
      xivoUser.setUsername("pdurand")
      xivoUser.setPassword("pwd")
      xivoUser.setLine(line)
      xivoUser.setVoicemail(voicemail)
      xivoUser.setCtiConfiguration(ctiConfig)
      val eRouteId = routeManager.create(Route(None, "52000", "(52000)", xivo.contextName)).id.get
      val iRouteId = routeManager.create(Route(None, "2000", "(2000)", xivo.contextName)).id.get
      insertUserLink(UserLink(Some(2), myEntity.id.get, 3, Some(eRouteId), Some(iRouteId)))

      stub(xivoConnector.getUser(3)).toReturn(xivoUser)
      stub(xivoConnector.getInternalNumberForUser(myEntity.name, 3)).toReturn(new ExtensionLink(789, "2222"))
      stub(entityManager.getEntity(myEntity.id.get)).toReturn(Some(myEntity))

      userManager.getUserWithVoicemailFromXivo(user.id.get, entityManager) shouldEqual Some(UserWithVM(
        user.copy(internalNumber = "2222", peerSipName = Some(PeerSipNameMode.Auto)),
        VoicemailSettings(enabled = true, Some(VoiceMailNumberMode.Custom), Some("2000"), Some(true), Some(true))
      ))
    }

    trait UpdateHelper extends Helper {
      val oldUser = User(Some(2L), myEntity, "Jean", "Dupond", "2000", Some("52000"), Some("jdupond@example.com"), Some("jdupond"), Some("0000"))
    }

    "update a XiVO user (and his voicemail) with new values" in new UpdateHelper {
      val idOnXivo = 3
      val eRouteId = routeManager.create(Route(None, "52000", "(52000)", xivo.contextName, "2000")).id.get
      routeMetaManager.create(RouteMeta(None, eRouteId, now, "test", now, "test", now, "test", false))
      val iRouteId = routeManager.create(Route(None, "2000", "(2000)", xivo.contextName, "2000")).id.get
      routeMetaManager.create(RouteMeta(None, iRouteId, now, "test", now, "test", now, "test", false))
      insertUserLink(UserLink(Some(2), myEntity.id.get, idOnXivo, Some(eRouteId), Some(iRouteId)))

      val oldXivoUser = new XivoUser()
      oldXivoUser.setId(idOnXivo)
      oldXivoUser.setFirstname("Jean")
      oldXivoUser.setLastname("Dupond")
      oldXivoUser.setCallerid(""""Jean Dupond"""")
      oldXivoUser.setDescription("Test")
      val ctiProfile = new CtiProfile()
      stub(xivoConnector.getDefaultCtiProfile).toReturn(ctiProfile)
      val ctiConfig = new CtiConfiguration(true, ctiProfile)
      oldXivoUser.setCtiConfiguration(ctiConfig)
      val oldVm = new Voicemail()
      oldVm.setId(9876)
      oldVm.setName("Jean Dupond")
      oldVm.setNumber("2000")
      oldVm.setPassword("0000")
      oldVm.setContext(myEntity.name)
      oldVm.setEmail("jdupond@example.com")
      oldXivoUser.setVoicemail(oldVm)
      val oldLine = new Line("2000")
      oldLine.setContext(myEntity.name)
      oldLine.setDeviceId("some-device")
      oldXivoUser.setLine(oldLine)
      stub(xivoConnector.getUser(idOnXivo)).toReturn(oldXivoUser)
      var userVoicemail = new UserVoicemail()
      userVoicemail.voicemail_id = oldVm.getId
      userVoicemail.user_id = idOnXivo
      stub(xivoConnector.getUsersIdsAssociatedToVoicemail(oldVm.getId)).toReturn(List(userVoicemail))

      val newUser = User(Some(2L), myEntity, "Marc", "Durand", "2001", Some("52001"), Some("mdurand@example.com"), Some("mdurand"), Some("1234"))
      val expectedXivoUser = new XivoUser()
      expectedXivoUser.setId(idOnXivo)
      expectedXivoUser.setCtiConfiguration(ctiConfig)
      val expectedLine = new Line("2001")
      expectedLine.setContext(myEntity.name)
      expectedLine.setDeviceId("some-device")
      expectedXivoUser.setFirstname("Marc")
      expectedXivoUser.setLastname("Durand")
      expectedXivoUser.setUsername("mdurand")
      expectedXivoUser.setCallerid("\"Marc Durand\"")
      expectedXivoUser.setPassword("1234")
      expectedXivoUser.setDescription("Test")
      expectedXivoUser.setLine(expectedLine)
      expectedXivoUser.setVoicemail(oldVm)
      expectedXivoUser.setOutcallerid("52001")
      expectedXivoUser.setEmail("mdurand@example.com")
      stub(userTransformer.updateXivoUser(oldXivoUser, newUser)).toReturn(expectedXivoUser)
      val updatedUserWithVM = UserWithVM(newUser, voicemailSettings)
      when(userTransformer.updatePrivateVoicemail(oldVm, updatedUserWithVM)).thenCallRealMethod
      DateTimeUtils.setCurrentMillisFixed(99L)
      val updatedDate = new DateTime()
      val releaseDate = new DateTime()

      userManager.update(UserWithVM(oldUser, voicemailSettings), updatedUserWithVM, "testUpdate", withConfiguration = true)

      verify(userTransformer).updatePrivateVoicemail(oldVm, updatedUserWithVM)
      verify(xivoConnector).updateUser(expectedXivoUser, false)
      verify(xivoConnector).updateVoicemailForUser(expectedXivoUser)
      verify(xivoConnector).updateLineForUser(expectedXivoUser)
      verify(xivoConnector).synchronizeDevice(any())
      listRoutes.map(_.copy(id = None)) shouldEqual List(
        Route(None, "52000", "(52000)", "to_pit_stop", "\\1"),
        Route(None, "52001", "(52001)", xivo.contextName, "2001"),
        Route(None, "2001", "(2001)", xivo.contextName, "\\1"))

      routeMetaManager.get(eRouteId).get.copy(id = None) shouldEqual
        RouteMeta(None, eRouteId, now, "test", updatedDate, "testUpdate", releaseDate, "testUpdate", false)
      routeMetaManager.get(iRouteId).get.copy(id = None) shouldEqual
        RouteMeta(None, iRouteId, now, "test", updatedDate, "testUpdate", now, "test", false)

      verify(cachedXivoUserManager).createOrUpdateForXivoUser(fromXivoUser(xivoUuid, expectedXivoUser))

      Option(expectedXivoUser.getVoicemail).exists { voicemail =>
        voicemail.getName == "Marc Durand" &&
          voicemail.getNumber == "2001" &&
          voicemail.getPassword == "0000" &&
          voicemail.getEmail == "mdurand@example.com" &&
          voicemail.getAttach
      } shouldEqual true
    }

    "update a XiVO user (without voicemail) with new internal number" in new UpdateHelper {
      val idOnXivo = 3
      val eRouteId = routeManager.create(Route(None, "52000", "(52000)", xivo.contextName, "2000")).id.get
      routeMetaManager.create(RouteMeta(None, eRouteId, now, "test", now, "test", now, "test", false))
      val iRouteId = routeManager.create(Route(None, "2000", "(2000)", xivo.contextName, "2000")).id.get
      routeMetaManager.create(RouteMeta(None, iRouteId, now, "test", now, "test", now, "test", false))
      insertUserLink(UserLink(Some(2), myEntity.id.get, idOnXivo, Some(eRouteId), Some(iRouteId)))

      val oldXivoUser = new XivoUser()
      oldXivoUser.setId(idOnXivo)
      oldXivoUser.setFirstname("Jean")
      oldXivoUser.setLastname("Dupond")
      oldXivoUser.setCallerid(""""Jean Dupond"""")
      oldXivoUser.setDescription("Test")
      val ctiProfile = new CtiProfile()
      stub(xivoConnector.getDefaultCtiProfile).toReturn(ctiProfile)
      val ctiConfig = new CtiConfiguration(true, ctiProfile)
      oldXivoUser.setCtiConfiguration(ctiConfig)
      val oldLine = new Line("2000")
      oldLine.setContext(myEntity.name)
      oldLine.setDeviceId("some-device")
      oldXivoUser.setLine(oldLine)
      stub(xivoConnector.getUser(idOnXivo)).toReturn(oldXivoUser)

      val newUser = User(Some(2L), myEntity, "Marc", "Durand", "3001", Some("53001"), Some("mdurand@example.com"), Some("mdurand"), Some("1234"))

      val expectedXivoUser = new XivoUser()
      expectedXivoUser.setId(idOnXivo)
      expectedXivoUser.setCtiConfiguration(ctiConfig)
      val expectedLine = new Line("3001")
      expectedLine.setContext(myEntity.name)
      expectedLine.setDeviceId("some-device")
      expectedXivoUser.setFirstname("Marc")
      expectedXivoUser.setLastname("Durand")
      expectedXivoUser.setUsername("mdurand")
      expectedXivoUser.setCallerid("\"Marc Durand\"")
      expectedXivoUser.setPassword("1234")
      expectedXivoUser.setDescription("Test")
      expectedXivoUser.setLine(expectedLine)
      expectedXivoUser.setOutcallerid("52001")
      expectedXivoUser.setEmail("mdurand@example.com")
      stub(userTransformer.updateXivoUser(oldXivoUser, newUser)).toReturn(expectedXivoUser)
      val updatedUserWithVM = UserWithVM(newUser, voicemailSettings)
      DateTimeUtils.setCurrentMillisFixed(99L)
      val updatedDate = new DateTime()
      val releaseDate = new DateTime()

      userManager.update(UserWithVM(oldUser, voicemailSettings), updatedUserWithVM, "testUpdate", withConfiguration = true)

      verify(xivoConnector).updateUser(expectedXivoUser, false)
      verify(xivoConnector).updateVoicemailForUser(expectedXivoUser)
      verify(xivoConnector).updateLineForUser(expectedXivoUser)
      verify(xivoConnector).synchronizeDevice(any())
      listRoutes.map(_.copy(id = None)) shouldEqual List(
        Route(None, "52000", "(52000)", "to_pit_stop", "\\1"),
        Route(None, "53001", "(53001)", xivo.contextName, "3001"),
        Route(None, "3001", "(3001)", xivo.contextName, "\\1"))
      routeMetaManager.get(eRouteId).get.copy(id = None) shouldEqual
        RouteMeta(None, eRouteId, now, "test", updatedDate, "testUpdate", releaseDate, "testUpdate", false)
      routeMetaManager.get(iRouteId).get.copy(id = None) shouldEqual
        RouteMeta(None, iRouteId, now, "test", updatedDate, "testUpdate", now, "test", false)

      verify(cachedXivoUserManager).createOrUpdateForXivoUser(fromXivoUser(xivoUuid, expectedXivoUser))
    }

    "update a XiVO user with new internal number in different interval" in new UpdateHelper {
      val idOnXivo = 3
      val eRouteId = routeManager.create(Route(None, "52000", "(52000)", xivo.contextName, "2000")).id.get
      routeMetaManager.create(RouteMeta(None, eRouteId, now, "test", now, "test", now, "test", false))
      val iRouteId = routeManager.create(Route(None, "2000", "(2000)", xivo.contextName, "2000")).id.get
      routeMetaManager.create(RouteMeta(None, iRouteId, now, "test", now, "test", now, "test", false))


      insertUserLink(UserLink(Some(2), myEntity.id.get, idOnXivo, Some(eRouteId), Some(iRouteId), intervalId1))
      listUserLinks(0).intervalId.get shouldEqual 11

      val oldXivoUser = new XivoUser()
      oldXivoUser.setId(idOnXivo)
      oldXivoUser.setFirstname("Jean")
      oldXivoUser.setLastname("Dupond")
      oldXivoUser.setCallerid(""""Jean Dupond"""")
      oldXivoUser.setDescription("Test")
      val ctiProfile = new CtiProfile()
      stub(xivoConnector.getDefaultCtiProfile).toReturn(ctiProfile)
      val ctiConfig = new CtiConfiguration(true, ctiProfile)
      oldXivoUser.setCtiConfiguration(ctiConfig)
      val oldLine = new Line("2000")
      oldLine.setContext(myEntity.name)
      oldLine.setDeviceId("some-device")
      oldXivoUser.setLine(oldLine)
      stub(xivoConnector.getUser(idOnXivo)).toReturn(oldXivoUser)

      val newUser = User(Some(2L), myEntity, "Marc", "Durand", "3001", Some("53001"), Some("mdurand@example.com"), Some("mdurand"), Some("1234"), intervalId = intervalId2)

      val expectedXivoUser = new XivoUser()
      expectedXivoUser.setId(idOnXivo)
      expectedXivoUser.setCtiConfiguration(ctiConfig)
      val expectedLine = new Line("3001")
      expectedLine.setContext(myEntity.name)
      expectedLine.setDeviceId("some-device")
      expectedXivoUser.setFirstname("Marc")
      expectedXivoUser.setLastname("Durand")
      expectedXivoUser.setUsername("mdurand")
      expectedXivoUser.setCallerid("\"Marc Durand\"")
      expectedXivoUser.setPassword("1234")
      expectedXivoUser.setDescription("Test")
      expectedXivoUser.setLine(expectedLine)
      expectedXivoUser.setOutcallerid("52001")
      expectedXivoUser.setEmail("mdurand@example.com")
      stub(userTransformer.updateXivoUser(oldXivoUser, newUser)).toReturn(expectedXivoUser)
      val updatedUserWithVM = UserWithVM(newUser, voicemailSettings)
      DateTimeUtils.setCurrentMillisFixed(99L)
      val updatedDate = new DateTime()
      val releaseDate = new DateTime()

      userManager.update(UserWithVM(oldUser, voicemailSettings), updatedUserWithVM, "testUpdate", withConfiguration = true)

      verify(xivoConnector).updateUser(expectedXivoUser, false)
      verify(xivoConnector).updateVoicemailForUser(expectedXivoUser)
      verify(xivoConnector).updateLineForUser(expectedXivoUser)
      verify(xivoConnector).synchronizeDevice(any())
      listRoutes.map(_.copy(id = None)) shouldEqual List(
        Route(None, "52000", "(52000)", "to_pit_stop", "\\1"),
        Route(None, "53001", "(53001)", xivo.contextName, "3001"),
        Route(None, "3001", "(3001)", xivo.contextName, "\\1"))
      routeMetaManager.get(eRouteId).get.copy(id = None) shouldEqual
        RouteMeta(None, eRouteId, now, "test", updatedDate, "testUpdate", releaseDate, "testUpdate", false)
      routeMetaManager.get(iRouteId).get.copy(id = None) shouldEqual
        RouteMeta(None, iRouteId, now, "test", updatedDate, "testUpdate", now, "test", false)

      listUserLinks(0).intervalId.get shouldEqual 12

      verify(cachedXivoUserManager).createOrUpdateForXivoUser(fromXivoUser(xivoUuid, expectedXivoUser))
    }

    "update a XiVO user with new incall" in new UpdateHelper {
      val idOnXivo = 3
      val eRouteId = routeManager.create(Route(None, "52000", "(52000)", xivo.contextName, "2000")).id.get
      routeMetaManager.create(RouteMeta(None, eRouteId, now, "test", now, "test", now, "test", false))
      val iRouteId = routeManager.create(Route(None, "2000", "(2000)", xivo.contextName, "2000")).id.get
      routeMetaManager.create(RouteMeta(None, iRouteId, now, "test", now, "test", now, "test", false))
      insertUserLink(UserLink(Some(2), myEntity.id.get, idOnXivo, Some(eRouteId), Some(iRouteId)))

      val oldXivoUser = new XivoUser()
      oldXivoUser.setId(idOnXivo)
      oldXivoUser.setFirstname("Jean")
      oldXivoUser.setLastname("Dupond")
      oldXivoUser.setCallerid(""""Jean Dupond"""")
      oldXivoUser.setDescription("Test")
      val ctiProfile = new CtiProfile()
      stub(xivoConnector.getDefaultCtiProfile).toReturn(ctiProfile)
      val ctiConfig = new CtiConfiguration(true, ctiProfile)
      oldXivoUser.setCtiConfiguration(ctiConfig)
      val oldLine = new Line("2000")
      oldLine.setContext(myEntity.name)
      oldLine.setDeviceId("some-device")
      oldXivoUser.setLine(oldLine)

      val incall = new IncomingCall()
      incall.setSda("2000")
      oldXivoUser.setIncomingCall(incall)

      stub(xivoConnector.getUser(idOnXivo)).toReturn(oldXivoUser)

      val newUser = User(Some(2L), myEntity, "Marc", "Durand", "3001", Some("53001"), Some("mdurand@example.com"), Some("mdurand"), Some("1234"))

      val expectedXivoUser = new XivoUser()
      expectedXivoUser.setId(idOnXivo)
      expectedXivoUser.setCtiConfiguration(ctiConfig)

      val expectedLine = new Line("3001")
      expectedLine.setContext(myEntity.name)
      expectedLine.setDeviceId("some-device")

      val expectedIncall = new IncomingCall()
      expectedIncall.setSda("3001")
      expectedXivoUser.setIncomingCall(expectedIncall)

      expectedXivoUser.setFirstname("Marc")
      expectedXivoUser.setLastname("Durand")
      expectedXivoUser.setUsername("mdurand")
      expectedXivoUser.setCallerid("\"Marc Durand\"")
      expectedXivoUser.setPassword("1234")
      expectedXivoUser.setDescription("Test")
      expectedXivoUser.setLine(expectedLine)
      expectedXivoUser.setOutcallerid("52001")
      expectedXivoUser.setEmail("mdurand@example.com")
      stub(userTransformer.updateXivoUser(oldXivoUser, newUser)).toReturn(expectedXivoUser)
      val updatedUserWithVM = UserWithVM(newUser, voicemailSettings)
      DateTimeUtils.setCurrentMillisFixed(99L)
      val updatedDate = new DateTime()
      val releaseDate = new DateTime()

      userManager.update(UserWithVM(oldUser, voicemailSettings), updatedUserWithVM, "testUpdate", withConfiguration = true)

      verify(xivoConnector).updateUser(expectedXivoUser, false)
      verify(xivoConnector).updateVoicemailForUser(expectedXivoUser)
      verify(xivoConnector).updateLineForUser(expectedXivoUser)
      verify(xivoConnector).updateIncallForUser(expectedXivoUser)
      verify(xivoConnector).synchronizeDevice(any())
      listRoutes.map(_.copy(id = None)) shouldEqual List(
        Route(None, "52000", "(52000)", "to_pit_stop", "\\1"),
        Route(None, "53001", "(53001)", xivo.contextName, "3001"),
        Route(None, "3001", "(3001)", xivo.contextName, "\\1"))
      routeMetaManager.get(eRouteId).get.copy(id = None) shouldEqual
        RouteMeta(None, eRouteId, now, "test", updatedDate, "testUpdate", releaseDate, "testUpdate", false)
      routeMetaManager.get(iRouteId).get.copy(id = None) shouldEqual
        RouteMeta(None, iRouteId, now, "test", updatedDate, "testUpdate", now, "test", false)

      verify(cachedXivoUserManager).createOrUpdateForXivoUser(fromXivoUser(xivoUuid, expectedXivoUser))
    }

    "update a Proxy user with whole bunch of new data" in new UpdateHelper {
      val id = 3
      val eRouteId = routeManager.create(Route(None, "52000", "(52000)", xivo.contextName, "2000")).id.get
      routeMetaManager.create(RouteMeta(None, eRouteId, now, "test", now, "test", now, "test", false))
      val iRouteId = routeManager.create(Route(None, "2000", "(2000)", xivo.contextName, "2000")).id.get
      routeMetaManager.create(RouteMeta(None, iRouteId, now, "test", now, "test", now, "test", false))
      insertUserLink(UserLink(Some(2), myEntity.id.get, id, Some(eRouteId), Some(iRouteId)))

      val newUser = User(Some(2L), myEntity, "Marc", "Durand", "3001", Some("53001"), Some("mdurand@example.com"), Some("mdurand"), Some("4321"))

      val noVM = VoicemailSettings(enabled = false, None,None,None,None)
      val updatedUserWithoutVM = UserWithVM(newUser, noVM)
      DateTimeUtils.setCurrentMillisFixed(99L)
      val updatedDate = new DateTime()
      val releaseDate = new DateTime()

      userManager.update(UserWithVM(oldUser, noVM), updatedUserWithoutVM, "testUpdate", withConfiguration = false)

      listRoutes.map(_.copy(id = None)) shouldEqual List(
        Route(None, "52000", "(52000)", "to_pit_stop", "\\1"),
        Route(None, "53001", "(53001)", xivo.contextName, "3001"),
        Route(None, "3001", "(3001)", xivo.contextName, "\\1"))
      routeMetaManager.get(eRouteId).get.copy(id = None) shouldEqual
        RouteMeta(None, eRouteId, now, "test", updatedDate, "testUpdate", releaseDate, "testUpdate", false)
      routeMetaManager.get(iRouteId).get.copy(id = None) shouldEqual
        RouteMeta(None, iRouteId, now, "test", updatedDate, "testUpdate", now, "test", false)

      verify(cachedXivoUserManager).createOrUpdateForXivoUser(CachedXivoUser.fromUser(xivoUuid, newUser, oldUser.id.get.toInt))
    }

    "remove the voicemail, the cti configuration and the external route if they no longer exist" in new UpdateHelper {
      val idOnXivo = 3
      val eRouteId = routeManager.create(Route(None, "52000", "(52000)", xivo.contextName, "2000")).id.get
      routeMetaManager.create(RouteMeta(None, eRouteId, now, "test", now, "test", now, "test", false))
      val iRouteId = routeManager.create(Route(None, "2000", "(2000)", xivo.contextName, "2000")).id.get
      routeMetaManager.create(RouteMeta(None, iRouteId, now, "test", now, "test", now, "test", false))
      insertUserLink(UserLink(Some(2), myEntity.id.get, idOnXivo, Some(eRouteId), Some(iRouteId)))

      val oldXivoUser = new XivoUser()
      oldXivoUser.setId(idOnXivo)
      oldXivoUser.setFirstname("Jean")
      oldXivoUser.setLastname("Dupond")
      oldXivoUser.setCallerid(""""Jean Dupond"""")
      val ctiProfile = new CtiProfile()
      val ctiConfig = new CtiConfiguration(true, ctiProfile)
      oldXivoUser.setCtiConfiguration(ctiConfig)
      val oldVm = new Voicemail()
      oldVm.setId(112233)
      oldVm.setName("Jean Dupond")
      oldVm.setNumber("2000")
      oldVm.setContext(myEntity.name)
      oldVm.setEmail("jdupond@example.com")
      oldXivoUser.setVoicemail(oldVm)
      val oldLine = new Line("2000")
      oldLine.setContext(myEntity.name)
      oldXivoUser.setLine(oldLine)
      stub(xivoConnector.getUser(idOnXivo)).toReturn(oldXivoUser)

      val newUser = User(Some(2L), myEntity, "Marc", "Durand", "2001", None, None, None, None)
      val expectedXivoUser = new XivoUser()
      expectedXivoUser.setId(idOnXivo)
      val expectedLine = new Line("2001")
      expectedLine.setContext(myEntity.name)
      expectedXivoUser.setFirstname("Marc")
      expectedXivoUser.setLastname("Durand")
      expectedXivoUser.setCallerid("\"Marc Durand\"")
      expectedXivoUser.setUsername("")
      expectedXivoUser.setLine(expectedLine)
      expectedXivoUser.setOutcallerid(myEntity.presentedNumber)
      expectedXivoUser.setVoicemail(oldVm)
      stub(userTransformer.updateXivoUser(oldXivoUser, newUser)).toReturn(expectedXivoUser)
      DateTimeUtils.setCurrentMillisFixed(99L)
      val updatedDate = new DateTime()
      val releaseDate = new DateTime()

      userManager.update(UserWithVM(oldUser, voicemailSettings), UserWithVM(newUser, VoicemailSettings.disabled), "testUpdate", withConfiguration = true)

      verify(xivoConnector).updateUser(expectedXivoUser, false)
      verify(xivoConnector).updateLineForUser(expectedXivoUser)
      verify(xivoConnector).deleteVoicemailForUser(expectedXivoUser)
      listRoutes shouldEqual
        List(Route(Some(eRouteId), "52000", "(52000)", "to_pit_stop", "\\1"),
          Route(Some(iRouteId), "2001", "(2001)", xivo.contextName, "\\1"))
      routeMetaManager.all().get.map(rm => rm.copy(id = None)) shouldEqual
        List(RouteMeta(None, eRouteId, now, "test", updatedDate, "testUpdate", releaseDate, "testUpdate", false),
          RouteMeta(None, iRouteId, now, "test", updatedDate, "testUpdate", now, "test", false))

      expectedXivoUser.getVoicemail shouldEqual null
    }

    "create the voicemail, the cti configuration and routes if they did not exist" in new UpdateHelper {
      val idOnXivo = 3
      insertUserLink(UserLink(Some(2), myEntity.id.get, idOnXivo, None, None))

      val oldXivoUser = new XivoUser()
      oldXivoUser.setId(idOnXivo)
      oldXivoUser.setFirstname("Jean")
      oldXivoUser.setLastname("Dupond")
      oldXivoUser.setCallerid(""""Jean Dupond"""")
      val oldLine = new Line("2000")
      oldLine.setContext(myEntity.name)
      oldXivoUser.setLine(oldLine)
      oldXivoUser.setVoicemail(null)
      stub(xivoConnector.getUser(idOnXivo)).toReturn(oldXivoUser)

      val newUser = User(Some(2L), myEntity, "Marc", "Durand", "2001", Some("52001"), Some("mdurand@example.com"), Some("mdurand"), Some("1234"))
      val expectedXivoUser = new XivoUser()
      expectedXivoUser.setId(idOnXivo)
      val ctiProfile = new CtiProfile()
      stub(xivoConnector.getDefaultCtiProfile).toReturn(ctiProfile)
      val ctiConfig = new CtiConfiguration(true, ctiProfile)
      oldXivoUser.setCtiConfiguration(ctiConfig)
      expectedXivoUser.setCtiConfiguration(ctiConfig)
      val expectedLine = new Line("2001")
      expectedLine.setContext(myEntity.name)
      expectedXivoUser.setFirstname("Marc")
      expectedXivoUser.setLastname("Durand")
      expectedXivoUser.setCallerid("\"Marc Durand\"")
      expectedXivoUser.setUsername("mdurand")
      expectedXivoUser.setPassword("1234")
      expectedXivoUser.setLine(expectedLine)
      expectedXivoUser.setOutcallerid("52001")
      stub(userTransformer.updateXivoUser(oldXivoUser, newUser)).toReturn(expectedXivoUser)
      when(userTransformer.updatePrivateVoicemail(any(), any())).thenCallRealMethod
      when(userTransformer.addNewVoicemailToUser(any(), any(), any())).thenCallRealMethod

      userManager.update(UserWithVM(oldUser, VoicemailSettings.disabled), UserWithVM(newUser, voicemailSettings), "testUpdate", withConfiguration = true)

      verify(xivoConnector).updateUser(expectedXivoUser, false)
      verify(xivoConnector).createVoicemailForUser(expectedXivoUser)
      verify(xivoConnector).updateLineForUser(expectedXivoUser)

      val routes = listRoutes
      routes.map(_.copy(id = None)) shouldEqual
        List(
          Route(None, "52001", "(52001)", xivo.contextName, "2001"),
          Route(None, "2001", "(2001)", xivo.contextName, "\\1")
        )
      routeMetaManager.get(routes.head.id.get).map(e => e.copy(id = None, routeRef = 0)) shouldEqual
        Success(RouteMeta(None, 0, now, "testUpdate", now, "testUpdate", now, "testUpdate", manual = false))

      Option(expectedXivoUser.getVoicemail).exists { voicemail =>
        voicemail.getName == "Marc Durand" &&
          voicemail.getNumber == "2001" &&
          voicemail.getPassword == "2001" &&
          voicemail.getEmail == "mdurand@example.com" &&
          voicemail.getAttach
      } shouldEqual true
    }

    "provide a proper JSON writer" in new Helper() {
      val user = User(Some(5), myEntity, "Eric", "Lerouge", "2000", Some("52000"), Some("eric.lerouge@test.com"),
        Some("elerouge"), Some("abcd"), Some("123456"))
      Json.toJson(user) shouldEqual Json.obj(
        "id" -> 5,
        "entity" -> Json.toJson(myEntity),
        "firstName" -> "Eric",
        "lastName" -> "Lerouge",
        "internalNumber" -> "2000",
        "externalNumber" -> "52000",
        "mail" -> "eric.lerouge@test.com",
        "ctiLogin" -> "elerouge",
        "ctiPassword" -> "abcd",
        "provisioningNumber" -> "123456"
      )

      Json.toJson(user.copy(intervalId = Some(1), templateId = Some(1))) shouldEqual Json.obj(
        "id" -> 5,
        "entity" -> Json.toJson(myEntity),
        "firstName" -> "Eric",
        "lastName" -> "Lerouge",
        "internalNumber" -> "2000",
        "externalNumber" -> "52000",
        "mail" -> "eric.lerouge@test.com",
        "ctiLogin" -> "elerouge",
        "ctiPassword" -> "abcd",
        "provisioningNumber" -> "123456",
        "intervalId" -> 1,
        "templateId" -> 1
      )
    }

    "search users across XiVOS using xivo user cache" in new Helper() {
      cleanTable("xivo_users")
      val xuser1 = getXivoUser(100, myEntity.name)
      val cached1 = fromXivoUser(xivoUuid, xuser1)
      val xuser2 = getXivoUser(200, myEntity.name, "Jacque")
      val cached2 = fromXivoUser(xivoUuid, xuser2)
      val query = "someQuery"

      stub(cachedXivoUserManager.findAcrossXivos(query, 100)).toReturn(Success(List(cached1, cached2)))
      stub(entityManager.getEntity(myEntity.id.get)).toReturn(Some(myEntity))

      insertUserLink(UserLink(Some(1010), myEntity.id.get, xuser1.getId.toLong, None, None)) shouldEqual 1
      insertUserLink(UserLink(Some(1020), myEntity.id.get, xuser2.getId.toLong, None, None)) shouldEqual 1

      val users = userManager.findAcrossXivos(entityManager, query, 100)
      users.map(u => (u.id, u.firstName)) shouldEqual List(
        (Some(1010), xuser1.getFirstname),
        (Some(1020), xuser2.getFirstname)
      )
    }

    "update internal and external routes even if old UserLink has no routes" in new UpdateHelper() {
      val idOnXivo = 3
      insertUserLink(UserLink(Some(2), myEntity.id.get, idOnXivo, None, None))

      val oldXivoUser = new XivoUser()
      oldXivoUser.setId(idOnXivo)
      oldXivoUser.setFirstname("Jean")
      oldXivoUser.setLastname("Dupond")
      oldXivoUser.setCallerid(""""Jean Dupond"""")
      val oldLine = new Line("2000")
      oldLine.setContext(myEntity.name)
      oldXivoUser.setLine(oldLine)
      oldXivoUser.setVoicemail(null)
      stub(xivoConnector.getUser(idOnXivo)).toReturn(oldXivoUser)

      val newUser = User(Some(2L), myEntity, "Marc", "Durand", "2001", Some("52001"), Some("mdurand@example.com"), Some("mdurand"), Some("1234"))
      val expectedXivoUser = new XivoUser()
      expectedXivoUser.setId(idOnXivo)
      val ctiProfile = new CtiProfile()
      stub(xivoConnector.getDefaultCtiProfile).toReturn(ctiProfile)
      val ctiConfig = new CtiConfiguration(true, ctiProfile)
      oldXivoUser.setCtiConfiguration(ctiConfig)
      expectedXivoUser.setCtiConfiguration(ctiConfig)
      val expectedLine = new Line("2001")
      expectedLine.setContext(myEntity.name)
      expectedXivoUser.setFirstname("Marc")
      expectedXivoUser.setLastname("Durand")
      expectedXivoUser.setCallerid("\"Marc Durand\"")
      expectedXivoUser.setUsername("mdurand")
      expectedXivoUser.setPassword("1234")
      expectedXivoUser.setLine(expectedLine)
      expectedXivoUser.setOutcallerid("52001")
      stub(userTransformer.updateXivoUser(oldXivoUser, newUser)).toReturn(expectedXivoUser)
      when(userTransformer.updatePrivateVoicemail(any(), any())).thenCallRealMethod
      when(userTransformer.addNewVoicemailToUser(any(), any(), any())).thenCallRealMethod

      insertRoute(Route(None, "2000", "(2000)", xivo.contextName, "\\1"))

      userManager.update(UserWithVM(oldUser, VoicemailSettings.disabled), UserWithVM(newUser, VoicemailSettings.disabled), "testUpdate", withConfiguration = true)

      verify(xivoConnector).updateUser(expectedXivoUser, false)
      verify(xivoConnector).updateLineForUser(expectedXivoUser)

      val routes = listRoutes
      routes.map(_.copy(id = None)) shouldEqual
        List(
          Route(None, "52001", "(52001)", xivo.contextName, "2001"),
          Route(None, "2001", "(2001)", xivo.contextName, "\\1")
        )
      routeMetaManager.get(routes.head.id.get).map(e => e.copy(id = None, routeRef = 0)) shouldEqual
        Success(RouteMeta(None, 0, now, "testUpdate", now, "testUpdate", now, "testUpdate", manual = false))
    }

  }
}

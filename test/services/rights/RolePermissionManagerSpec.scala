package services.rights

import model.rights.RolePermission
import org.scalatest.TestData
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerTest
import testutils.{TestDBUtil, DbTest, PlayShouldSpec}

import scala.util.{Failure, Success}

class RolePermissionManagerSpec extends PlayShouldSpec with MockitoSugar with DbTest with OneAppPerTest {
  override def newAppForTest(testData: TestData) = getApp

  class Helper() {
    cleanTable("role_permission")
    val rolePermissionManager  = app.injector.instanceOf(classOf[RolePermissionManager])
    def getRolePermission(permissionId: Int = 1) = RolePermission(1, permissionId)
  }

  "The RolePermission manager" should {

    "create an rolePermission in the database" in new Helper() {
      val created = rolePermissionManager.create(getRolePermission())
      created match {
        case Success(t) => t shouldEqual getRolePermission()
        case Failure(e) => fail(e.getMessage())
      }
    }

    "get a rolePermission from the database" in new Helper() {
      val rolePermission = rolePermissionManager.create(getRolePermission()).get
      rolePermissionManager.get(rolePermission.permissionId) match {
        case Success(x) => x shouldBe rolePermission
        case Failure(e) => fail(e.getMessage())
      }
    }

    "get a rolePermission list for role from the database" in new Helper() {
      val rolePermission = rolePermissionManager.create(getRolePermission()).get
      rolePermissionManager.getForRole(rolePermission.roleId) match {
        case Success(x) => x shouldBe List(rolePermission)
        case Failure(e) => fail(e.getMessage())
      }
    }

    "update a rolePermission in the database" in new Helper() {
      val rolePermission = rolePermissionManager.create(getRolePermission()).get
      val updated = rolePermission.copy(permissionId = 2)

      val fromDb = rolePermissionManager.update(updated)
      fromDb match {
        case Success(t) => t.shouldEqual(updated)
        case Failure(e) => fail(e.getMessage() + e.getStackTrace().mkString("\n"))
      }
    }

    "delete a rolePermission from the database" in new Helper() {
      val rolePermission = rolePermissionManager.create(getRolePermission()).get

      val deleted = rolePermissionManager.delete(rolePermission.permissionId)
      deleted match {
        case Success(t) => t.shouldEqual(rolePermission)
        case Failure(e) => fail(e.getMessage())
      }
    }

    "list rolePermission from the database" in new Helper() {
      val rolePermission1 = rolePermissionManager.create(getRolePermission(1)).get
      val rolePermission2 = rolePermissionManager.create(getRolePermission(2)).get

      val all = rolePermissionManager .all()
      all match {
        case Success(t) => t shouldBe List(rolePermission1, rolePermission2)
        case Failure(e) => fail(e.getMessage())
      }
    }


  }
}

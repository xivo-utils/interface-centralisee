package services.rights

import model.rights.AdministratorRole
import org.scalatest.TestData
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerTest
import testutils.{TestDBUtil, DbTest, PlayShouldSpec}

import scala.util.{Failure, Success}

class AdministratorRoleManagerSpec extends PlayShouldSpec with MockitoSugar with DbTest with OneAppPerTest {
  override def newAppForTest(testData: TestData) = getApp

  class Helper() {
    cleanTable("administrator_role")
    val adminRoleManager  = app.injector.instanceOf(classOf[AdministratorRoleManager])
    def getAdminRole(adminId: Long = 1, roleId: Long = 10) = AdministratorRole(adminId, roleId)
  }

  "The AdministratorRole manager" should {

    "create an adminRole in the database" in new Helper() {
      val created = adminRoleManager.create(getAdminRole())
      created match {
        case Success(t) => t shouldEqual getAdminRole()
        case Failure(e) => fail(e.getMessage())
      }
    }

    "get a adminRole from the database" in new Helper() {
      val adminRole = List(
        adminRoleManager.create(getAdminRole(1, 10)).get,
        adminRoleManager.create(getAdminRole(1, 11)).get
      )
      adminRoleManager.get(1) match {
        case Success(x) => x shouldBe adminRole
        case Failure(e) => fail(e.getMessage())
      }
    }

    "update a adminRole in the database" in new Helper() {
      val adminRole = adminRoleManager.create(getAdminRole()).get
      val updated = adminRole.copy(roleId = 2)

      val fromDb = adminRoleManager.update(updated)
      fromDb match {
        case Success(t) => t.shouldEqual(updated)
        case Failure(e) => fail(e.getMessage() + e.getStackTrace().mkString("\n"))
      }
    }

    "delete a adminRole from the database" in new Helper() {
      val adminRoles = List(
        adminRoleManager.create(getAdminRole(1, 10)).get,
        adminRoleManager.create(getAdminRole(1, 11)).get
      )

      val deleted = adminRoleManager.delete(1)
      deleted match {
        case Success(t) => t.shouldEqual(adminRoles)
        case Failure(e) => fail(e.getMessage())
      }
    }

    "list adminRole from the database" in new Helper() {
      val adminRole1 = adminRoleManager.create(getAdminRole(1)).get
      val adminRole2 = adminRoleManager.create(getAdminRole(2)).get

      val all = adminRoleManager .all()
      all match {
        case Success(t) => t shouldBe List(adminRole1, adminRole2)
        case Failure(e) => fail(e.getMessage())
      }
    }


  }
}

package services.rights

import java.util.UUID
import model.rights._
import model._
import org.scalatest.TestData
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerTest
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.inject._
import org.mockito.Mockito.stub
import testutils.{PlayShouldSpec, TestConfig}
import scala.util.{Failure, Success}

class EntityRightsManagerSpec extends PlayShouldSpec with TestConfig with MockitoSugar with OneAppPerTest {
  override def newAppForTest(testData: TestData) = new GuiceApplicationBuilder()
    .configure(testConfig)
    .overrides(bind[EntityInterface].toInstance(mock[EntityInterface]))
    .overrides(bind[XivoInterface].toInstance(mock[XivoInterface]))
    .overrides(bind[OperationManager].toInstance(mock[OperationManager]))
    .overrides(bind[PermissionManager].toInstance(mock[PermissionManager]))
    .overrides(bind[RoleManager].toInstance(mock[RoleManager]))
    .overrides(bind[RolePermissionManager].toInstance(mock[RolePermissionManager]))
    .build

  class Helper() {
    val entityRightsManager= app.injector.instanceOf(classOf[EntityRightsInterface])
    val entityManager = app.injector.instanceOf(classOf[EntityInterface])
    val xivoManager = app.injector.instanceOf(classOf[XivoInterface])
    val operationManager = app.injector.instanceOf(classOf[OperationManager])
    val permissionManager = app.injector.instanceOf(classOf[PermissionManager])
    val roleManager = app.injector.instanceOf(classOf[RoleManager])
    val rolePermissionManager = app.injector.instanceOf(classOf[RolePermissionManager])
    val x1 = Xivo(Some(1), UUID.randomUUID(), "x1", "192.168.0.1", "to_x1", None, Some("token"))
    val e1 = Entity(Some(1), CombinedId(x1.uuid, "1"), "e1", "e1" , x1, List(Interval(None, "1000", "1099")), "pres1", "default")
    val x2 = Xivo(Some(2), UUID.randomUUID(), "x2", "192.168.0.2", "to_x2", None, Some("token"))
    val e2 = Entity(Some(2), CombinedId(x2.uuid, "2"), "e2", "e2" , x2, List(Interval(None, "1100", "1199")), "pres2", "default"  )

    def stubGet(): UIRole = {
      stub(roleManager.get(1)).toReturn(Success(Role(Some(1), "testRole")))
      stub(rolePermissionManager.getForRole(1)).toReturn(Success(List(RolePermission(1, 10), RolePermission(2, 11))))
      stub(xivoManager.list).toReturn(List(x1, x2))
      stub(entityManager.listFromDb).toReturn(List(e1, e2))
      stub(permissionManager.getForEntity(e1.id.get))
        .toReturn(Success(List(Permissions(Some(10), e1.id.get, 101, "testRole"))))
      stub(permissionManager.getForEntity(e2.id.get))
        .toReturn(Success(List(Permissions(Some(11), e2.id.get, 102, "testRole"))))
      stub(operationManager.get(101)).toReturn(Success(Operation(Some(101), "testRole", true, true, true, true)))
      stub(operationManager.get(102)).toReturn(Success(Operation(Some(102), "testRole", true, true, false, false)))

      UIRole(Some(1), Some("testRole"), List(
        UIXivoPermission(1,x1.name,List(
          UIEntityPermission(1,e1.name,UIOperations(true,true,true,true)))),
        UIXivoPermission(2,x2.name,List(
          UIEntityPermission(2,e2.name,UIOperations(true,true,false,false))))
      ))
    }

    def stubCreate(name: String = "testRole"): UIRole = {
      val r = Role(None, name)
      stub(roleManager.create(r)).toReturn(Success(r.copy(id=Some(1))))
      val o = Operation(None, name, true, true, true, false)
      stub(operationManager.create(o)).toReturn(Success(o.copy(id=Some(10))))
      val p = Permissions(None, e1.id.get, 10, name)
      stub(permissionManager.create(p)).toReturn(Success(p.copy(id=Some(100))))
      stub(rolePermissionManager.create(RolePermission(1, 100))).toReturn(Success(RolePermission(1,100)))

      UIRole(None, Some(name), List(
        UIXivoPermission(1, "xivo", List(
          UIEntityPermission(e1.id.get, e1.name, UIOperations(true, true, true, false))
        ))))
    }

    def stubDelete(): UIRole = {
      val res = stubGet()
      stub(rolePermissionManager.delete(10)).toReturn(Success(RolePermission(1, 10)))
      stub(rolePermissionManager.delete(11)).toReturn(Success(RolePermission(2, 11)))
      stub(permissionManager.delete(10)).toReturn(Success(Permissions(Some(10), e1.id.get, 101, "testRole")))
      stub(permissionManager.delete(11)).toReturn(Success(Permissions(Some(11), e2.id.get, 102, "testRole")))
      stub(operationManager.delete(101)).toReturn(Success(Operation(Some(101), "testRole", true, false, false, false)))
      stub(operationManager.delete(102)).toReturn(Success(Operation(Some(102), "testRole", true, false, false, false)))
      stub(roleManager.delete(1)).toReturn(Success(Role(Some(1), "testRole")))
      res
    }

  }

  "The EntityRights manager" should {
    "return an empty UIRole with xivos and entities" in new Helper() {
      stub(entityManager.listFromDb).toReturn(List(e1, e2))
      stub(xivoManager.list).toReturn(List(x1, x2))
      entityRightsManager.getEmptyRole() shouldEqual
        UIRole(None, None, List(
          UIXivoPermission(x1.id.get, x1.name, List(
            UIEntityPermission(e1.id.get, e1.name, UIOperations())
          )),
          UIXivoPermission(x2.id.get, x2.name, List(
            UIEntityPermission(e2.id.get, e2.name, UIOperations())
          ))
        ))
    }

    "return a superadmin UIRole with xivos and entities" in new Helper() {
      stub(entityManager.listFromDb).toReturn(List(e1, e2))
      stub(xivoManager.list).toReturn(List(x1, x2))
      entityRightsManager.getSuperadminRole() shouldEqual
        UIRole(None, None, List(
          UIXivoPermission(x1.id.get, x1.name, List(
            UIEntityPermission(e1.id.get, e1.name, UIOperations(true, true, true, true))
          )),
          UIXivoPermission(x2.id.get, x2.name, List(
            UIEntityPermission(e2.id.get, e2.name, UIOperations(true, true, true, true))
          ))
        ))
    }

    "get an UIRole" in new Helper() {
      val expected = stubGet()
      entityRightsManager.get(expected.id.get) match {
        case Success(role) =>
          role shouldEqual expected
        case Failure(e) =>
          fail("Could not get role: " + e + "\n" + e.getStackTrace.toList.mkString("\n"))
      }
    }

    "create an UIRole" in new Helper() {
      val role = stubCreate()
      entityRightsManager.create(role) match {
        case Success(uir) =>
          uir.id.get shouldEqual 1
        case Failure(e) =>
          fail("Could not create role: " + e + "\n" + e.getStackTrace.toList.mkString("\n"))
      }
    }

    "get all UIRoles" in new Helper() {
      stub(roleManager.all()).toReturn(Success(List(Role(Some(1), "1"), Role(Some(2), "2"))))
      entityRightsManager.all() match {
        case Success(roles) =>
          roles shouldEqual List(UIRole(Some(1), Some("1"), List()), UIRole(Some(2), Some("2"), List()))
        case Failure(e) =>
          fail("Could not list roles: " + e + "\n" + e.getStackTrace.toList.mkString("\n"))
      }
    }

    "delete UIRole" in new Helper() {
      val expected = stubDelete()
      entityRightsManager.delete(expected.id.get) match {
        case Success(role) =>
          role shouldEqual expected
        case Failure(e) =>
          fail("Could not delete role: " + e + "\n" + e.getStackTrace.toList.mkString("\n"))
      }
    }

    "update UIRole" in new Helper() {
      val uiRole = UIRole(Some(1), Some("testUpdate"), List(
        UIXivoPermission(10,"testUpdate",List(
          UIEntityPermission(101,"testUpdate",UIOperations(true,true,false,false))))
      ))
      val role = Role(Some(1), "testUpdate")

      stub(roleManager.get(1)).toReturn(Success(role))
      stub(rolePermissionManager.delete(10)).toReturn(Success(RolePermission(1, 10)))
      stub(permissionManager.delete(10)).toReturn(Success(Permissions(Some(10), 101, 1001, "testUpdate")))
      stub(operationManager.delete(101)).toReturn(Success(Operation(Some(1001), "testUpdate", true, false, false, false)))
      val o = Operation(None, "testUpdate", true, true, false, false)
      stub(operationManager.create(o)).toReturn(Success(o.copy(id=Some(1001))))
      val p = Permissions(None, 101, 1001, "testUpdate")
      stub(permissionManager.create(p)).toReturn(Success(p.copy(id=Some(101))))
      stub(rolePermissionManager.create(RolePermission(1, 101))).toReturn(Success(RolePermission(1,100)))
      stub(roleManager.update(Role(Some(1), "newName"))).toReturn(Success(Role(Some(1), "newName")))

      val expected = uiRole.copy(name = Some("newName"))
      entityRightsManager.update(expected) match {
        case Success(uir) =>
          uir shouldEqual(expected)
        case Failure(e) =>
          fail("Could not update role: " + e + "\n" + e.getStackTrace.toList.mkString("\n"))
      }
    }

  }

}

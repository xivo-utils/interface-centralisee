package services.rights

import model.rights.Permissions
import org.scalatest.TestData
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerTest
import testutils.{TestDBUtil, DbTest, PlayShouldSpec}

import scala.util.{Failure, Success}

class PermissionManagerSpec extends PlayShouldSpec with MockitoSugar with DbTest with OneAppPerTest {
  override def newAppForTest(testData: TestData) = getApp

  class Helper() {
    cleanTable("permissions")
    val permissionsManager = app.injector.instanceOf(classOf[PermissionManager])
    def getPermission(name: String  = "testPermission") = Permissions(None, 1, 1, name)
  }

  "The Permission manager" should {

    "create an permission in the database" in new Helper() {
      val created = permissionsManager.create(getPermission())
      created match {
        case Success(t) => t.id shouldNot be(None)
        case Failure(e) => fail(e.getMessage())
      }
    }

    "get a permission from the database" in new Helper() {
      val permission = permissionsManager.create(getPermission()).get
      val expected = permission.copy(id = permission.id)
      permissionsManager.get(permission.id.get) match {
        case Success(x) => x shouldBe expected
        case Failure(e) => fail(e.getMessage())
      }
    }

    "get a permission for entity from the database" in new Helper() {
      val permission = permissionsManager.create(getPermission()).get
      val expected = permission.copy(id = permission.id)
      permissionsManager.getForEntity(permission.entityId) match {
        case Success(x) => x shouldBe List(expected)
        case Failure(e) => fail(e.getMessage())
      }
    }

    "update a permission in the database" in new Helper() {
      val permission = permissionsManager.create(getPermission()).get
      val updated = permission.copy(name = "updated test name")

      val fromDb = permissionsManager.update(updated)
      fromDb match {
        case Success(t) => t.shouldEqual(updated)
        case Failure(e) => fail(e.getMessage() + e.getStackTrace().mkString("\n"))
      }
    }

    "delete a permission from the database" in new Helper() {
      val permission = permissionsManager.create(getPermission()).get

      val deleted = permissionsManager.delete(permission.id.get)
      deleted match {
        case Success(t) => t.shouldEqual(permission)
        case Failure(e) => fail(e.getMessage())
      }
    }

    "list permission from the database" in new Helper() {
      val permission1 = permissionsManager.create(getPermission("test list 1")).get
      val permission2 = permissionsManager.create(getPermission("test list 2")).get

      val all = permissionsManager.all()
      all match {
        case Success(t) => t shouldBe List(permission1, permission2)
        case Failure(e) => fail(e.getMessage())
      }
    }


  }
}

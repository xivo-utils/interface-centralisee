package services.rights

import model.rights.Operation
import org.scalatest.TestData
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerTest
import testutils.{TestDBUtil, DbTest, PlayShouldSpec}

import scala.util.{Failure, Success}

class OperationManagerSpec extends PlayShouldSpec with MockitoSugar with DbTest with OneAppPerTest {
  override def newAppForTest(testData: TestData) = getApp

  class Helper() {
    cleanTable("operations")
    val operationsManager = app.injector.instanceOf(classOf[OperationManager])
    def getOperation(name: String  = "testOperation") = Operation(None, "testOperation", true)
  }

  "The Operation manager" should {

    "create an operation in the database" in new Helper() {
      val created = operationsManager.create(getOperation())
      created match {
        case Success(t) => t.id shouldNot be(None)
        case Failure(e) => fail(e.getMessage())
      }
    }

    "get a operation from the database" in new Helper() {
      val operation = operationsManager.create(getOperation()).get
      val expected = operation.copy(id = operation.id)
      operationsManager.get(operation.id.get) match {
        case Success(x) => x shouldBe operation
        case Failure(e) => fail(e.getMessage())
      }
    }

    "update a operation in the database" in new Helper() {
      val operation = operationsManager.create(getOperation()).get
      val updated = operation.copy(name = "updated test name")

      val fromDb = operationsManager.update(updated)
      fromDb match {
        case Success(t) => t.shouldEqual(updated)
        case Failure(e) => fail(e.getMessage() + e.getStackTrace().mkString("\n"))
      }
    }

    "delete a operation from the database" in new Helper() {
      val operation = operationsManager.create(getOperation()).get

      val deleted = operationsManager.delete(operation.id.get)
      deleted match {
        case Success(t) => t.shouldEqual(operation)
        case Failure(e) => fail(e.getMessage())
      }
    }

    "list operation from the database" in new Helper() {
      val operation1 = operationsManager.create(getOperation("test list 1")).get
      val operation2 = operationsManager.create(getOperation("test list 2")).get

      val all = operationsManager.all()
      all match {
        case Success(t) => t shouldBe List(operation1, operation2)
        case Failure(e) => fail(e.getMessage())
      }
    }


  }
}

package services.rights

import model.rights.Role
import org.scalatest.TestData
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerTest
import testutils.{TestDBUtil, DbTest, PlayShouldSpec}
import scala.util.{Failure, Success}

class RoleManagerSpec extends PlayShouldSpec with MockitoSugar with DbTest with OneAppPerTest {
  override def newAppForTest(testData: TestData) = getApp

  class Helper() {
    cleanTable("roles")
    val roleManager = app.injector.instanceOf(classOf[RoleManager])
    def getRole(name: String  = "testRole") = Role(None, name)
  }

  "The Role manager" should {

    "create a role in the database" in new Helper() {
      val created = roleManager.create(getRole())
      created match {
        case Success(t) => t.id shouldNot be(None)
        case Failure(e) => fail(e.getMessage())
      }
    }

    "get a role from the database" in new Helper() {
      val role = roleManager.create(getRole()).get
      val expected = role.copy(id = role.id)
      roleManager.get(role.id.get) match {
        case Success(x) => x shouldBe role
        case Failure(e) => fail(e.getMessage())
      }
    }

    "update a role in the database" in new Helper() {
      val role = roleManager.create(getRole()).get
      val updated = role.copy(name = "updated test name")

      val fromDb = roleManager.update(updated)
      fromDb match {
        case Success(t) => t.shouldEqual(updated)
        case Failure(e) => fail(e.getMessage() + e.getStackTrace().mkString("\n"))
      }
    }

    "delete a role from the database" in new Helper() {
      val role = roleManager.create(getRole()).get

      val deleted = roleManager.delete(role.id.get)
      deleted match {
        case Success(t) => t.shouldEqual(role)
        case Failure(e) => fail(e.getMessage())
      }
    }

    "list roles from the database" in new Helper() {
      val role1 = roleManager.create(getRole("test list 1")).get
      val role2 = roleManager.create(getRole("test list 2")).get

      val all = roleManager.all()
      all match {
        case Success(t) => t shouldBe List(role1, role2)
        case Failure(e) => fail(e.getMessage())
      }
    }


  }
}

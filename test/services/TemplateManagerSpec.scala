package services

import java.util.UUID
import model.{CombinedId, PeerSipNameMode, TemplateEntity, TemplateInterval, TemplateXivo}
import org.scalatest.TestData
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerTest
import services.helpers.GetTemplateFixture
import testutils.{DbTest, PlayShouldSpec, TestDBUtil}

import scala.util.Success
import scala.util.Failure

class TemplateManagerSpec extends PlayShouldSpec with MockitoSugar with DbTest with OneAppPerTest {

  override def newAppForTest(testData: TestData) = getApp

  class Helper() extends GetTemplateFixture {
    cleanTable("template")
    val templateManager = app.injector.instanceOf(classOf[TemplateManager])
  }

  "The Template manager implementation" should {

    "create a template in the database" in new Helper() {
      val template = getTemplate()

      val created = templateManager.create(template)
      created match {
        case Success(t) => created.get.properties.id shouldNot be (None)
        case Failure(e) => fail(e.getMessage())
      }
    }

    "create a webrtc template in the database" in new Helper() {
      val template = getTemplate(peerSipNameMode = PeerSipNameMode.WebRTC)

      val created = templateManager.create(template)
      created match {
        case Success(t) =>
          created.get.properties.id shouldNot be (None)
          created.get.properties.peerSipName shouldEqual PeerSipNameMode.WebRTC
        case Failure(e) => fail(e.getMessage())
      }
    }

    "create a ua template in the database" in new Helper() {
      val template = getTemplate(peerSipNameMode = PeerSipNameMode.UniqueAccount)

      val created = templateManager.create(template)
      created match {
        case Success(t) =>
          created.get.properties.id shouldNot be (None)
          created.get.properties.peerSipName shouldEqual PeerSipNameMode.UniqueAccount
        case Failure(e) => fail(e.getMessage())
      }
    }

    "get a template from the database" in new Helper() {
      val template = templateManager.create(getTemplate()).get
      val templateId = template.properties.id
      val expected = template.copy(properties = template.properties.copy(id = templateId))
      val fromDb = templateManager.get(templateId.get)
      fromDb match {
        case Success(t) => t.properties shouldBe template.properties
        case Failure(e) => fail(e.getMessage())
      }
    }

    "update a template in the database" in new Helper() {
      val template = templateManager.create(getTemplate()).get
      val updated =
        template
          .copy(properties = template.properties.copy(name = "updated name"))
          .copy(xivos = List(template.xivos.head, TemplateXivo(2, template.properties.id)))
          .copy(entities = List(TemplateEntity(CombinedId(UUID.randomUUID(), "second"), template.properties.id)))
          .copy(intervals = List(TemplateInterval(2, template.properties.id)))
          .copy(properties = template.properties.copy(peerSipName = PeerSipNameMode.WebRTC))

      val fromDb = templateManager.update(updated)
      fromDb match {
        case Success(t) => t.shouldEqual(updated)
        case Failure(e) => fail(e.getMessage() + e.getStackTrace().mkString("\n"))
      }
    }

    "delete a template from the database" in new Helper() {
      val template = templateManager.create(getTemplate()).get

      val deleted = templateManager.delete(template.properties.id.get)
      deleted match {
        case Success(t) => t.shouldEqual(template)
        case Failure(e) => fail(e.getMessage())
      }
    }

    "list templates from the database" in new Helper() {
      val template1 = templateManager.create(getTemplate()).get
      val template2 = templateManager.create(getTemplate(None, Some("test2"))).get

      val all = templateManager.all()
      all match {
        case Success(t) => t shouldBe List(template2, template1)
        case Failure(e) => fail(e.getMessage())
      }
    }


    "list templates for Entity" in new Helper() {
      val template1 = templateManager.create(getTemplate()).get
      val template2 = templateManager.create(getTemplate(None, Some("test2"))).get

      val res = templateManager.forEntity(template1.entities.head.entityCombinedId)
      res match {
        case Success(t) => t shouldBe List(template1)
        case Failure(e) => fail(e.getMessage())
      }
    }

  }
}

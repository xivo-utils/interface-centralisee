package services

import org.scalatest.TestData
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerTest
import testutils.{DbTest, PlayShouldSpec}

import scala.util.{Failure, Success}
import java.util.UUID

import model.Xivo

class XivoManagerSpec extends PlayShouldSpec with MockitoSugar with DbTest with OneAppPerTest {
  override def newAppForTest(testData: TestData) = getApp

  class Helper() {
    cleanTable("xivo")
    val xivoManager = app.injector.instanceOf(classOf[XivoManager])
    def getXivo(name: String, id: Option[Long] = Some(1)) =
      XivoDb(id, UUID.randomUUID(), name, s"192.168.1.$name", "to_" + name.replace(' ', '_'), Some("token"))
  }

  "The xivoDb" should {
    "create from xivo" in {
      val name = "fromXdb"
      val id = Some(1L)
      val uuid = UUID.randomUUID()
      val ctxName = "to_context"
      val host = "fromHost"
        XivoDb.fromXivo(Xivo(id, uuid, name, host, ctxName, None, Some("token"))) shouldEqual(XivoDb(id, uuid, name, host, ctxName, Some("token")))
    }
  }

  "The Xivo manager" should {

    "create a Xivo in the database" in new Helper() {
      val created = xivoManager.create(getXivo("testCreate", None))
      created match {
        case Success(t) => t.id shouldNot be(None)
        case Failure(e) => fail(e.getMessage())
      }
    }

    "get a xivo from the database" in new Helper() {
      val xivo = xivoManager.create(getXivo("test get")).get
      val expected = xivo.copy(id = xivo.id)
      xivoManager.get(xivo.id.get) match {
        case Success(x) => x shouldBe xivo
        case Failure(e) => fail(e.getMessage())
      }
    }

    "update a template in the database" in new Helper() {
      val xivo = xivoManager.create(getXivo("test update")).get
      val updated = xivo.copy(name = "updated test name")

      val fromDb = xivoManager.update(updated)
      fromDb match {
        case Success(t) => t.shouldEqual(updated)
        case Failure(e) => fail(e.getMessage() + e.getStackTrace().mkString("\n"))
      }
    }

    "delete a xivo from the database" in new Helper() {
      val xivo = xivoManager.create(getXivo("test delete")).get

      val deleted = xivoManager.delete(xivo.id.get)
      deleted match {
        case Success(t) => t.shouldEqual(xivo)
        case Failure(e) => fail(e.getMessage())
      }
    }

    "list xivos from the database" in new Helper() {
      val xivo1 = xivoManager.create(getXivo("test list 1")).get
      val xivo2 = xivoManager.create(getXivo("test list 2")).get

      val all = xivoManager.all()
      all match {
        case Success(t) => t shouldBe List(xivo1, xivo2)
        case Failure(e) => fail(e.getMessage())
      }
    }


  }
}

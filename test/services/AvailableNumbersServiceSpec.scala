package services

import java.util.UUID

import model.{Interval, Route, RouteInterface, RouteMeta}
import org.joda.time.DateTime
import org.scalatest.TestData
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerTest
import play.api.Application
import services.helpers.GetTemplateFixture
import testutils.{DbTest, PlayShouldSpec, TestConfig}
import org.scalatest.OptionValues._
import org.scalatest.TryValues._
import services.cache.{CachedXivoUser, CachedXivoUserManager}
import xivo.restapi.model.{Line, User}

class AvailableNumbersServiceSpec extends PlayShouldSpec with MockitoSugar with TestConfig with DbTest
  with OneAppPerTest{

  override def newAppForTest(testData: TestData): Application = getApp

  class Helper {
    cleanTable("route")
    cleanTable("route_metadata")
    cleanTable("xivo_users")
    val availableNumbersService = app.injector.instanceOf(classOf[AvailableNumbersService])
    val route = app.injector.instanceOf(classOf[RouteInterface])
    val routeMetaManager = app.injector.instanceOf(classOf[RouteMetaManager])
    val cachedXivoUserManager = app.injector.instanceOf(classOf[CachedXivoUserManager])

    def createRouteMeta(routeRef: Long, releasedDate: DateTime) = RouteMeta(None, routeRef,
      new DateTime(2005, 3, 26, 12, 0, 0, 0), "xxx", new DateTime(2005, 3, 26, 12, 3, 0, 0), "yyy",
      releasedDate, "zzz")

    val xivoUuid = UUID.fromString("3acb24fa-0182-4c2a-89ad-4859312ffb7c")

    def getCachedXivoUser(firstName: String, xivoUserId: Int, internalNumber: String) =
      CachedXivoUser(None, xivoUuid, Some("main entity"), xivoUserId, firstName, "Smith", Some(internalNumber), Some("445566"), Some("nobody@nowhere.com"),
        Some("nobody"), Some("***"), Some("123456"), None, None, None, None)
  }

  "findAvailableNumbers" should {

    "return no numbers for empty list of intervals" in new Helper {
      availableNumbersService.findAvailableNumbers(List()) shouldEqual List()
    }

    "return interval with unused numbers" in new Helper {
      val intervals = List(Interval(None, "100", "105"))

      availableNumbersService.findAvailableNumbers(intervals) shouldEqual List("100", "101", "102", "103", "104", "105")
    }

    "return interval with unused numbers padded according to interval padding" in new Helper {
      val intervals = List(Interval(None, "00100", "00105"))

      availableNumbersService.findAvailableNumbers(intervals) shouldEqual List("00100", "00101", "00102", "00103", "00104", "00105")
    }

    "return first three numbers for interval with three unused numbers" in new Helper {
      val intervals = List(Interval(None, "100", "102"))
      availableNumbersService.findAvailableNumbers(intervals) shouldEqual List("100", "101", "102")
    }

    "combine unused numbers from two intervals" in new Helper {
      val intervals = List(Interval(None, "100", "103"), Interval(None, "200", "203"))
      availableNumbersService.findAvailableNumbers(intervals) shouldEqual List("100", "101", "102", "103", "200", "201", "202", "203")
    }

    "skip used number without route_metedata" in new Helper {
      val intervals = List(Interval(None, "100", "103"), Interval(None, "200", "299"))
      route.create(Route(None, "101", "", ""))
      route.create(Route(None, "200", "", ""))
      availableNumbersService.findAvailableNumbers(intervals) should not contain List("101", "200")
    }

    "don't skip used number with route.target=to_pit_stop without route_metedata, put them after unused" in new Helper {
      val intervals = List(Interval(None, "100", "102"), Interval(None, "200", "201"))
      route.create(Route(None, "100", "", Route.targetDisabled))
      route.create(Route(None, "200", "", Route.targetDisabled))
      availableNumbersService.findAvailableNumbers(intervals) shouldEqual List("100", "101", "102", "200", "201")
    }

    "don't skip used number with route.tagget=to_pit_stop with route_metedata, put them after unused ordered by route_metedata.releasedAt" in new Helper {
      val intervals = List(Interval(None, "100", "103"), Interval(None, "200", "200"))
      val route100 = route.create(Route(None, "100", "", Route.targetDisabled))
      routeMetaManager.create(createRouteMeta(route100.id.value, new DateTime(2005, 1, 1, 1, 0, 0, 0))).success
      val route101 = route.create(Route(None, "101", "", Route.targetDisabled))
      routeMetaManager.create(createRouteMeta(route101.id.value, new DateTime(2004, 1, 1, 1, 0, 0, 0))).success
      val route102 = route.create(Route(None, "102", "", Route.targetDisabled))
      routeMetaManager.create(createRouteMeta(route102.id.value, null)).success // old data, inconsistent with Route.target == Route.targetDisabled

      availableNumbersService.findAvailableNumbers(intervals) shouldEqual List("103", "200", "102", "101", "100")
    }

    "do not duplicate number if in multiple route metadata" in new Helper {
      val intervals = List(Interval(None, "100", "103"))
      val route100 = route.create(Route(None, "100", "", Route.targetDisabled))
      routeMetaManager.create(createRouteMeta(route100.id.value, new DateTime(2005, 1, 1, 1, 0, 0, 0))).success
      routeMetaManager.create(createRouteMeta(route100.id.value, new DateTime(2004, 1, 1, 1, 0, 0, 0))).success

      availableNumbersService.findAvailableNumbers(intervals) shouldEqual List("101", "102", "103", "100")
    }

    "skip number found in xivo_users cache" in new Helper {
      val intervals = List(Interval(None, "100", "103"), Interval(None, "200", "203"))
      cachedXivoUserManager.create(getCachedXivoUser("Jean", 100, "101")).success
      cachedXivoUserManager.create(getCachedXivoUser("James", 200, "200")).success
      availableNumbersService.findAvailableNumbers(intervals) shouldEqual List("100", "102", "103", "201", "202", "203")
    }

  }

}

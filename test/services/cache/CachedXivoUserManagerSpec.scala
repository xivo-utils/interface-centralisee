package services.cache

import java.util.UUID
import model._
import org.scalatest.TestData
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerTest
import services.XivoDb
import org.mockito.Mockito._
import services.cache.CachedXivoUser.{fromUser, fromXivoUser}
import testutils.{DbTest, PlayShouldSpec}
import xivo.restapi.model.{Line, User => XivoUser}

import scala.util.{Failure, Success}

class CachedXivoUserManagerSpec extends PlayShouldSpec with MockitoSugar with DbTest with OneAppPerTest {
  override def newAppForTest(testData: TestData) = getApp

  class Helper() {
    cleanTable("xivo_users")

    val cachedXivoUserManager = app.injector.instanceOf(classOf[CachedXivoUserManager])

    val xivoUuid = UUID.fromString("3acb24fa-0182-4c2a-89ad-4859312ffb7c")

    def getCachedXivoUser(
      id: Option[Long] = None,
      xivoUserId: Int = 500,
      firstName: String = "John",
      lastName: String = "Smith",
      internalNumber: Option[String] = Some("112233"),
      externalNumber: Option[String] = Some("445566"),
      xivoUuid: UUID = xivoUuid
    ) = {
      CachedXivoUser(id, xivoUuid, Some("main entity"), xivoUserId,
        firstName, lastName, internalNumber, externalNumber, Some("nobody@nowhere.com"), Some("nobody"), Some("***"),
        Some("123456"), Some(20), Some(PeerSipNameMode.WebRTC), Some(CallerIdMode.Custom), Some("555777"))
    }

    val minimalCachedXivoUser = getCachedXivoUser().copy(entityName = None, mail = None, ctiLogin = None,
      ctiPassword = None, provisioningNumber = None)

    def getXivoUser(context: String, id: Int = 1, firstName: String = "Pierre", callerId: String = "3000"): XivoUser = {
      val line = new Line("2000")
      line.setContext(context)
      line.setprovisioningCode("123456")
      line.setOptions("webrtc", "yes")
      val xivoUser = new XivoUser()
      xivoUser.setId(id)
      xivoUser.setFirstname(firstName)
      xivoUser.setLastname("Durand")
      xivoUser.setLine(line)
      xivoUser.setOutcallerid(callerId)
      xivoUser.setUsername("aaa")
      xivoUser.setPassword("bbb")
      xivoUser.setRingSeconds(20)

      xivoUser
    }
  }

  "The CachedXivoUser" should {

    "create custom caller id from xivo user" in new Helper() {
      val xivoUser = getXivoUser("avranches", callerId = "123")
      val user: CachedXivoUser = fromXivoUser(xivoUuid, xivoUser)

      user.callerIdMode.get shouldEqual CallerIdMode.Custom
      user.customCallerId.get shouldEqual "123"
    }

    "create anonymous caller id from xivo user" in new Helper() {
      val xivoUser = getXivoUser("avranches", callerId = "anonymous")
      val user: CachedXivoUser = fromXivoUser(xivoUuid, xivoUser)

      user.callerIdMode.get shouldEqual CallerIdMode.Anonymous
      user.customCallerId.get shouldEqual "anonymous"
    }

    "be created from xivo user" in new Helper() {
      val xivoUser = getXivoUser("avranches")
      fromXivoUser(xivoUuid, xivoUser) shouldEqual CachedXivoUser(
        id = None,
        xivoUuid = xivoUuid,
        entityName = Some("avranches"),
        xivoUserId = 1,
        firstName = "Pierre",
        lastName = "Durand",
        internalNumber = Some("2000"),
        externalNumber = Some("3000"),
        mail = None,
        ctiLogin = Some("aaa"),
        ctiPassword = Some("bbb"),
        provisioningNumber = Some("123456"),
        ringingTime = Some(20),
        peerSipName = Some(PeerSipNameMode.WebRTC),
        callerIdMode = Some(CallerIdMode.Custom),
        customCallerId = Some("3000")
      )
    }

    "be created from GCU user" in new Helper() {
      val xivo = insertXivo(Xivo(None, UUID.randomUUID(), "xivo42", "192.168.1.142", "xivo1-name", None, Some("token")))
      val entity = insertEntity(Entity(None, mock[CombinedId], "aavranches", "AAvranches", xivo, List(Interval(None, "2000", "2099")), "52000", "default"))
      val user = User(Some(23), entity, "James", "Bond", "1000", Some("123451000"), Some("jbond@007.com"), Some("jbond"), Some("password"), Some("123456"), Some(3), Some(4), Some(42), Some(PeerSipNameMode.WebRTC), Some(CallerIdMode.Anonymous), Some("Custom"))

      fromUser(xivo.uuid, user, 42) shouldEqual CachedXivoUser(
        None,
        xivo.uuid,
        Some(entity.name),
        42,
        "James",
        "Bond",
        Some("1000"),
        Some("123451000"),
        Some("jbond@007.com"),
        Some("jbond"),
        Some("password"),
        Some("123456"),
        Some(42),
        Some(PeerSipNameMode.WebRTC),
        Some(CallerIdMode.Anonymous),
        Some("Custom")
      )
    }

    "be converted to User" in new Helper() {
      val xivo = insertXivo(Xivo(None, UUID.randomUUID(), "xivo1", "192.168.1.123", "xivo1-name", None, Some("token")))
      val entity = insertEntity(Entity(None, mock[CombinedId], "avranches", "Avranches", xivo, List(Interval(None, "2000", "2099")), "52000", "default"))
      val _intervalId = Some(123)
      val _templateId = Some(456)

      getCachedXivoUser().toUser(Some(123L), entity, _intervalId, _templateId) shouldEqual User(
        id = Some(123),
        entity = entity,
        firstName = "John",
        lastName = "Smith",
        internalNumber = "112233",
        externalNumber = Some("445566"),
        mail = Some("nobody@nowhere.com"),
        ctiLogin = Some("nobody"),
        ctiPassword = Some("***"),
        provisioningNumber = Some("123456"),
        intervalId = _intervalId,
        templateId = _templateId,
        ringingTime = Some(20),
        peerSipName = Some(PeerSipNameMode.WebRTC),
        callerIdMode = Some(CallerIdMode.Custom),
        customCallerId = Some("555777")
      )
    }
  }

  "The CachedXivoUserManager" should {

    "create a CachedXivoUser in the database" in new Helper() {
      val created = cachedXivoUserManager.create(getCachedXivoUser())
      created match {
        case Success(t) => t.id shouldNot be(None)
        case Failure(e) => fail(e.getMessage)
      }

      cachedXivoUserManager.get(created.get.id.get) match {
        case Success(x) => x shouldBe created.get
        case Failure(e) => fail(e.getMessage)
      }
    }

    "create a minimal CachedXivoUser in the database" in new Helper() {
      val created = cachedXivoUserManager.create(minimalCachedXivoUser)
      created match {
        case Success(t) => t.id shouldNot be(None)
        case Failure(e) => fail(e.getMessage)
      }
    }

    "get a CachedXivoUser from the database" in new Helper() {
      val xivoUser = cachedXivoUserManager.create(getCachedXivoUser()).get
      cachedXivoUserManager.get(xivoUser.id.get) match {
        case Success(x) => x shouldBe xivoUser
        case Failure(e) => fail(e.getMessage)
      }
    }

    "get a CachedXivoUser from the database using xivoUuid and xivoUserId" in new Helper() {
      val xivoUser = cachedXivoUserManager.create(getCachedXivoUser()).get
      cachedXivoUserManager.get(xivoUser.xivoUuid, xivoUser.xivoUserId) match {
        case Success(x) => x shouldBe xivoUser
        case Failure(e) => fail(e.getMessage)
      }
    }

    "update a CachedXivoUser in the database" in new Helper() {
      val old = cachedXivoUserManager.create(getCachedXivoUser()).get
      val updated = old.copy(firstName = "updated test name")

      cachedXivoUserManager.update(updated)

      cachedXivoUserManager.get(updated.id.get) match {
        case Success(t) => t.shouldEqual(updated)
        case Failure(e) => fail(e.getMessage + e.getStackTrace.mkString("\n"))
      }
    }

    "delete a CachedXivoUser from the database" in new Helper() {
      val xivoUser = cachedXivoUserManager.create(getCachedXivoUser()).get
      val deleted = cachedXivoUserManager.delete(xivoUser.id.get)
      deleted match {
        case Success(t) => t.shouldEqual(xivoUser)
        case Failure(e) => fail(e.getMessage)
      }
    }

    "list CachedXivoUser's from the database" in new Helper() {
      val user1 = cachedXivoUserManager.create(getCachedXivoUser(xivoUserId = 111, firstName = "Joe")).get
      val user2 = cachedXivoUserManager.create(getCachedXivoUser(xivoUserId = 222, firstName = "Ann")).get

      val all = cachedXivoUserManager.all()
      all match {
        case Success(t) => t shouldBe List(user1, user2)
        case Failure(e) => fail(e.getMessage)
      }
    }

    "list CachedXivoUser's from one entity from the database" in new Helper() {
      val user1 = cachedXivoUserManager.create(getCachedXivoUser(xivoUserId = 111, firstName = "Joe").copy(entityName = Some("OTHER"))).get
      val user2 = cachedXivoUserManager.create(getCachedXivoUser(xivoUserId = 222, firstName = "Ann")).get
      val otherXivoUuid = UUID.fromString("3042a755-e8f8-420e-905e-cba1cfcf4f53")
      val user3 = cachedXivoUserManager.create(getCachedXivoUser(xivoUserId = 111, firstName = "Pepe").copy(xivoUuid = otherXivoUuid)).get

      val all = cachedXivoUserManager.allInEntity(xivoUuid, "main entity")
      all match {
        case Success(t) => t shouldBe List(user2)
        case Failure(e) => fail(e.getMessage)
      }
    }

    "create a CachedXivoUser in the database using createOrUpdate" in new Helper() {
      val created = cachedXivoUserManager.createOrUpdate(getCachedXivoUser(id = Some(1234)))
      created match {
        case Success(t) => t.id shouldNot be(None)
        case Failure(e) => fail(e.getMessage + e. getStackTrace.mkString("\n"))
      }
    }

    "update a CachedXivoUser in the database using createOrUpdate" in new Helper() {
      val old = cachedXivoUserManager.create(getCachedXivoUser()).get
      val updated = old.copy(firstName = "updated test name")

      cachedXivoUserManager.createOrUpdate(updated)

      cachedXivoUserManager.get(updated.id.get) match {
        case Success(t) => t.shouldEqual(updated)
        case Failure(e) => fail(e.getMessage + e.getStackTrace.mkString("\n"))
      }
    }

    "create a CachedXivoUser in the database using createOrUpdateForXivoUser" in new Helper() {
      val created = cachedXivoUserManager.createOrUpdateForXivoUser(getCachedXivoUser())
      created match {
        case Success(t) => t.id shouldNot be(None)
        case Failure(e) => fail(e.getMessage + e. getStackTrace.mkString("\n"))
      }
    }

    "update a CachedXivoUser in the database using createOrUpdateForXivoUser" in new Helper() {
      val old = cachedXivoUserManager.create(getCachedXivoUser()).get
      val updated = old.copy(firstName = "updated test name")

      cachedXivoUserManager.createOrUpdateForXivoUser(updated)

      cachedXivoUserManager.get(updated.id.get) match {
        case Success(t) => t.shouldEqual(updated)
        case Failure(e) => fail(e.getMessage + e.getStackTrace.mkString("\n"))
      }
    }

    "find users accross XiVOs searching first_name, last_name, internal_number, external_number" in new Helper() {
      def create(cachedXivoUser: CachedXivoUser) = cachedXivoUserManager.create(cachedXivoUser).get
      val user1 = create(getCachedXivoUser(xivoUserId = 100, firstName = "John 101", internalNumber = Some("800"), externalNumber = Some("900")))
      val user2 = create(getCachedXivoUser(xivoUserId = 200, firstName = "John", internalNumber = Some("800"), externalNumber = Some("900"), lastName = "Smith 210"))
      val user3 = create(getCachedXivoUser(xivoUserId = 300, firstName = "Peter", internalNumber = Some("2104"), externalNumber = Some("900")))
      val user4 = create(getCachedXivoUser(xivoUserId = 400, firstName = "Isaac", internalNumber = Some("935"), externalNumber = Some("109")))
      create(getCachedXivoUser(xivoUserId = 500, firstName = "Christopher", internalNumber = Some("111"), externalNumber = Some("222")))

      val all = cachedXivoUserManager.findAcrossXivos("10")
      all match {
        case Success(t) => t shouldBe List(user4, user2, user1, user3)
        case Failure(e) => fail(e.getMessage + e.getStackTrace.mkString("\n"))
      }
    }

    "count CachedXivoUser with given external number" in new Helper() {
      List("entities", "entity_intervals", "route", "xivo", "users", "xivo_users").foreach(cleanTable)

      val xivoUsers: Vector[CachedXivoUser] = Vector(
        cachedXivoUserManager.create(getCachedXivoUser(xivoUserId = 111, firstName = "Aaa", externalNumber = Some("0123"))),
        cachedXivoUserManager.create(getCachedXivoUser(xivoUserId = 222, firstName = "Bbb", externalNumber = Some("123"))),
        cachedXivoUserManager.create(getCachedXivoUser(xivoUserId = 333, firstName = "Ccc", externalNumber = Some("0123"))),
        cachedXivoUserManager.create(getCachedXivoUser(xivoUserId = 444, firstName = "Ddd", externalNumber = Some("00123"))),
        cachedXivoUserManager.create(getCachedXivoUser(xivoUserId = 555, firstName = "Eee", externalNumber = Some("0123"))),
        cachedXivoUserManager.create(getCachedXivoUser(xivoUserId = 666, firstName = "Fff", externalNumber = None))
      ).map(_.get)

      val xivo = insertXivo(Xivo(None, xivoUuid, "xivo1", "192.168.1.123", "to_xivo1-name", None, Some("token")))
      val myEntity = insertEntity(Entity(None, mock[CombinedId], "avranches", "Avranches", xivo, List(Interval(None, "2000", "2099")), "52000", "default"))
      xivoUsers.foreach { xu => insertUserLink(UserLink(Some(10000L + xu.xivoUserId), myEntity.id.get, xu.xivoUserId, None, None)) }

      cachedXivoUserManager.countForExternalNumber("0123") shouldEqual Success(3)
      cachedXivoUserManager.countForExternalNumber("0123", Some(10222L)) shouldEqual Success(3)
      cachedXivoUserManager.countForExternalNumber("0123", Some(10333L)) shouldEqual Success(2)
    }

    "count CachedXivoUser for given Xivo" in new Helper() {
      List("xivo_users").foreach(cleanTable)

      val otherXivoUuid = UUID.fromString("9578e178-2a3a-44e0-ac7b-08fea55f7f55")
      val emptyXivoUuid = UUID.fromString("41733d56-8f50-4fa4-bb0c-4017dbaba907")

      val xivoUsers: Vector[CachedXivoUser] = Vector(
        cachedXivoUserManager.create(getCachedXivoUser(xivoUserId = 111, firstName = "Aaa")),
        cachedXivoUserManager.create(getCachedXivoUser(xivoUserId = 222, firstName = "Bbb")),
        cachedXivoUserManager.create(getCachedXivoUser(xivoUuid = otherXivoUuid, xivoUserId = 333, firstName = "Ccc")),
        cachedXivoUserManager.create(getCachedXivoUser(xivoUserId = 444, firstName = "Ddd")),
        cachedXivoUserManager.create(getCachedXivoUser(xivoUuid = otherXivoUuid, xivoUserId = 555, firstName = "Eee")),
        cachedXivoUserManager.create(getCachedXivoUser(xivoUserId = 666, firstName = "Fff"))
      ).map(_.get)

      cachedXivoUserManager.countForXivo(emptyXivoUuid) shouldEqual Success(0)
      cachedXivoUserManager.countForXivo(xivoUuid) shouldEqual Success(4)
      cachedXivoUserManager.countForXivo(otherXivoUuid) shouldEqual Success(2)
    }

    "get next Id for a Proxy user createion" in new Helper() {
      List("xivo_users").foreach(cleanTable)

      val otherXivoUuid = UUID.fromString("9578e178-2a3a-44e0-ac7b-08fea55f7f55")
      val emptyXivoUuid = UUID.fromString("41733d56-8f50-4fa4-bb0c-4017dbaba907")

      val xivoUsers: Vector[CachedXivoUser] = Vector(
        cachedXivoUserManager.create(getCachedXivoUser(xivoUserId = 111, firstName = "Aaa")),
        cachedXivoUserManager.create(getCachedXivoUser(xivoUserId = 115, firstName = "Bbb")),
        cachedXivoUserManager.create(getCachedXivoUser(xivoUuid = otherXivoUuid, xivoUserId = 111, firstName = "Ccc")),
        cachedXivoUserManager.create(getCachedXivoUser(xivoUserId = 444, firstName = "Ddd")),
        cachedXivoUserManager.create(getCachedXivoUser(xivoUuid = otherXivoUuid, xivoUserId = 112, firstName = "Eee")),
        cachedXivoUserManager.create(getCachedXivoUser(xivoUserId = 445, firstName = "Fff"))
      ).map(_.get)

      cachedXivoUserManager.getNextIdForXivo(emptyXivoUuid) shouldEqual Success(1)
      cachedXivoUserManager.getNextIdForXivo(otherXivoUuid) shouldEqual Success(113)
      cachedXivoUserManager.getNextIdForXivo(xivoUuid) shouldEqual Success(446)
    }

  }
}


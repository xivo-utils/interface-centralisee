package services.cache

import java.util.UUID

import model._
import org.mockito.Mockito._
import org.scalatest.{BeforeAndAfterAllConfigMap, TestData}
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerTest
import play.api.inject._
import play.api.inject.guice.GuiceApplicationBuilder
import services.{UserLinkManager, UserManager, UserManagerImpl, XivoDb}
import services.helpers.GetTemplateFixture
import testutils.{ConnectorProviderStub, DbTest, PlayShouldSpec, TestConfig}
import xivo.ldap.xivoconnection.XivoConnector
import xivo.restapi.model.{CtiConfiguration, Line, User}
import xivo.restapi.model.{User => XivoUser, _}
import services.cache.CachedXivoUser.fromXivoUser

import scala.collection.JavaConverters._
import scala.util.Success

class UserCachingServiceSpec extends PlayShouldSpec with MockitoSugar with TestConfig with DbTest
  with ConnectorProviderStub with OneAppPerTest with BeforeAndAfterAllConfigMap with GetTemplateFixture {

  var xivoConnector = mock[XivoConnector]

  override def newAppForTest(testData: TestData) = new GuiceApplicationBuilder()
    .configure(testConfig)
    .overrides(bind[UserManager].toInstance(mock[UserManager]))
    .overrides(bind[UserLinkManager].toInstance(mock[UserLinkManager]))
    .build

  class Helper() {
    val userManager = app.injector.instanceOf(classOf[UserManager])
    val userLinkManager = app.injector.instanceOf(classOf[UserLinkManager])
    val userCachingService = app.injector.instanceOf(classOf[UserCachingService])

    List("users", "route", "route_metadata", "xivo", "xivo_users").foreach(cleanTable)
    val xivoUuid = UUID.fromString("3acb24fa-0182-4c2a-89ad-4859312ffb7c")
    val xivo = insertXivo(Xivo(None, xivoUuid, "xivo1", "192.168.1.123", "to_xivo1-name", None, Some("token")))
    val xivoDb = XivoDb(xivo.id, xivo.uuid, xivo.name, xivo.host, xivo.contextName, Some("token"))
    val myEntity = insertEntity(Entity(None, mock[CombinedId], "avranches", "Avranches", xivo, List(Interval(Some(1101), "2000", "2099")), "52000", "default"))
    reset(xivoConnector)

    val defaultCtiProfile = new CtiProfile()
    defaultCtiProfile.setId(254)

    def getXivoUser(id: Int = 0, context: String = myEntity.name, firstName: String = "Pierre"): XivoUser = {
      val line = new Line("2000")
      line.setContext(context)
      line.setprovisioningCode("123456")
      val xivoUser = new XivoUser()
      if (id > 0) xivoUser.setId(id)
      xivoUser.setFirstname(firstName)
      xivoUser.setLastname("Durand")
      xivoUser.setLine(line)
      xivoUser.setOutcallerid(myEntity.presentedNumber)
      xivoUser.setCtiConfiguration(new CtiConfiguration(true, defaultCtiProfile))

      xivoUser
    }
  }

  "UserCachingService" should {

    "cache xivo users" in new Helper() {
      val xuser1 = getXivoUser(100, "avranches")
      val xuser2 = getXivoUser(200, "avranches", "Jacque")
      val xuser3 = getXivoUser(300, "avranches", "Ann")
      val xusers = List(xuser1, xuser2, xuser3)

      stub(userManager.getAndCacheXivoUsers(xivo.uuid, xivoDb)).toReturn(xusers)
      stub(userLinkManager.findForXivo(xivoDb.id.get)).toReturn(Success(List(
        UserLink(None, myEntity.id.get, 200, None, None, None, None)
      )))

      val userLink1 = UserLink(None, myEntity.id.get, 100, None, None, None, None)
      stub(userLinkManager.create(userLink1)).toReturn(Success(userLink1))
      val userLink2 = UserLink(None, myEntity.id.get, 300, None, None, None, None)
      stub(userLinkManager.create(userLink2)).toReturn(Success(userLink2))

      userCachingService.cacheForXivo(xivoDb)

      verify(userLinkManager, times(0)).create(UserLink(None, myEntity.id.get, 200, None, None, None, None))
    }

  }

}

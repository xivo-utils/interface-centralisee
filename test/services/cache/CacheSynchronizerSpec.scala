package services.cache

import java.util.UUID

import akka.actor.ActorSystem
import akka.testkit.{TestActorRef, TestKit}
import org.scalatest.mock.MockitoSugar
import org.scalatest.{ShouldMatchers, WordSpecLike}
import services.cache.CacheScheduler.{StartCachingAllXivos, StartCachingXivo}
import services.{XivoDb, XivoManager}
import org.mockito.Mockito.{stub, verify}

import scala.util.Success

class CacheSynchronizerSpec extends TestKit(ActorSystem("CacheSynchronizerSpec")) with WordSpecLike with ShouldMatchers
  with MockitoSugar {
  "The Cache synchronizer" should {

    "cache xivo users for all Xivos on StartCachingAllXivos message" in {
      val userCachingService = mock[UserCachingService]
      val xivoManager = mock[XivoManager]
      val xivoUuid1 = UUID.fromString("ff8d56cc-8808-40bc-9b40-74754efd8cdf")
      val xivoUuid2 = UUID.fromString("f699ba7d-2f7b-442e-8e00-e956f7d98fed")
      val xivoDb1 = XivoDb(Some(100), xivoUuid1, "x1", "@x1", "to_x1", Some("token"))
      val xivoDb2 = XivoDb(Some(200), xivoUuid2, "x2", "@x2", "to_x2", Some("token"))
      stub(xivoManager.all()).toReturn(Success(List(xivoDb1, xivoDb2)))
      val cacheSync = TestActorRef(new CacheSynchronizer(userCachingService, xivoManager))
      cacheSync ! StartCachingAllXivos
      verify(userCachingService).cacheForXivo(xivoDb1)
      verify(userCachingService).cacheForXivo(xivoDb2)
    }

    "cache xivo users for single xivo on StartCachingXivo message" in {

      val userCachingService = mock[UserCachingService]
      val xivoManager = mock[XivoManager]
      val xivoUuid1 = UUID.fromString("ff8d56cc-8808-40bc-9b40-74754efd8cdf")
      val xivoId = 100L
      val xivoDb1 = XivoDb(Some(xivoId), xivoUuid1, "x1", "@x1", "to_x1", Some("token"))
      stub(xivoManager.get(xivoId)).toReturn(Success(xivoDb1))
      val cacheSync = TestActorRef(new CacheSynchronizer(userCachingService, xivoManager))
      cacheSync ! StartCachingXivo(xivoId)
      verify(userCachingService).cacheForXivo(xivoDb1)
    }

  }

}

package services

import java.util.UUID
import model.{PeerSipNameMode, _}
import model.utils.{ConfigParser, GlobalXivoConnectorPool, XivoConnectorPool}
import org.mockito.Mockito._
import org.scalatest.mock.MockitoSugar
import org.scalatest.{ShouldMatchers, WordSpec}
import testutils.ConnectorProviderStub
import xivo.ldap.xivoconnection.XivoConnector
import xivo.restapi.model.{User => XivoUser, _}

import scala.util.Success

class XivoUserTransformerSpec extends WordSpec with ShouldMatchers with ConnectorProviderStub with MockitoSugar {

  var xivoConnector = mock[XivoConnector]

  class Helper() {
    reset(xivoConnector)
    val templateManager = mock[TemplateManager]
    val configParser = mock[ConfigParser]
    val connectorPool = new GlobalXivoConnectorPool(configParser) {
      override def get(host: String, incallContext: String, token: String): XivoConnector = xivoConnector
    }
    val xivoUserTransf = new XivoUserTransformer(templateManager: TemplateManager, configParser: ConfigParser, connectorPool)
    xivoUserTransf.connectorPool = new XivoConnectorPool(configParser) with TestXivoConnectorProvider

    def getUserAndExpected(routingMode: IntervalRoutingModes.Type, outcallerIdProcessing: Boolean = true,
                           sipNameMode: PeerSipNameMode.Type = PeerSipNameMode.Model, registrar: String = "default",
                           userSipNameMode: Option[PeerSipNameMode.Type] = None, ringingTime: Option[Int] = None): (User, XivoUser) = {
      val intervalId = 10
      val templateId = 5
      stub(templateManager.get(templateId)).toReturn(Success(Template(
        TemplateProperties(Some(templateId), "tName", sipNameMode, CallerIdMode.Custom, Some("123999"), 15,
          true, Some(VoiceMailNumberMode.Custom), Some("4444"), true, true), List(), List(), List())))
      stub(configParser.incallContext).toReturn("from-extern")
      val xUuid = UUID.randomUUID()
      val user = User(
        None,
        Entity(None, CombinedId(xUuid, "default"), "default", "Default ctx",
          Xivo(None, xUuid, "xivo", "xHost", "to_xivo", None, Some("token")),
          List(Interval(Some(intervalId), "1000", "1999", routingMode, Some("22456"))),
          "presented", registrar),
        "first", "last", "1001", Some("1231001"), Some("test@example.org"), Some("login"), Some("password"), None,
        Some(intervalId), Some(templateId), peerSipName = userSipNameMode, ringingTime = ringingTime)
      val expected = new XivoUser()
      expected.setFirstname("first")
      expected.setLastname("last")
      val expectedRingingTime = user.ringingTime.getOrElse(15)
      expected.setRingSeconds(expectedRingingTime)
      expected.setEmail("test@example.org")
      val line = new Line("1001")
      line.setContext("default")
      line.setName("1001")
      expected.setLine(line)
      val incall = new IncomingCall()
      incall.setContext("from-extern")
      incall.setSda("1001")
      expected.setIncomingCall(incall)

      expected.setUsername("login")
      expected.setPassword("password")
      val profile = new CtiProfile()
      profile.setId(1)
      profile.setName("1")
      expected.setCtiConfiguration(new CtiConfiguration(true, profile))
      expected.setOutcallerid("123999")
      stub(xivoConnector.getDefaultCtiProfile).toReturn(profile)
      (user, expected)
    }
  }

  "XivoUserTransformer" should {
    "convert user to xivo user for routed" in new Helper() {
      val (user, expected) = getUserAndExpected(IntervalRoutingModes.Routed)

      xivoUserTransf.createXivoUser(user) shouldEqual expected
    }

    "convert user to xivo user for routed with sda" in new Helper() {
      val (user, expected) = getUserAndExpected(IntervalRoutingModes.RoutedWithDirectNumber)

      xivoUserTransf.createXivoUser(user) shouldEqual expected
    }

    "convert user to xivo user for routed with custom sda" in new Helper() {
      val (user, expected) = getUserAndExpected(IntervalRoutingModes.RoutedWithCustomDirectNumber)

      xivoUserTransf.createXivoUser(user) shouldEqual expected
    }

    "not set outcallerId when disabled" in new Helper() {
      stub(configParser.disableOutcallerIdProcessing).toReturn(true)
      val (user, _) = getUserAndExpected(IntervalRoutingModes.Routed)
      xivoUserTransf.createXivoUser(user).getOutcallerid shouldEqual null
    }

    "update xivoUser using values from user" in new Helper() {
      val idOnXivo = 3
      val xivo = Xivo(None, UUID.randomUUID(), "xivo1", "192.168.1.123", "xivo1-name", None, Some("token"))
      val myEntity = Entity(None, mock[CombinedId], "avranches", "Avranches", xivo,
        List(Interval(None, "2000", "2099")), "52000", "default")

      val oldXivoUser = new XivoUser()
      oldXivoUser.setId(idOnXivo)
      oldXivoUser.setFirstname("Jean")
      oldXivoUser.setLastname("Dupond")
      oldXivoUser.setCallerid(""""Jean Dupond"""")
      oldXivoUser.setDescription("Test")
      oldXivoUser.setRingSeconds(30)

      val ctiProfile = new CtiProfile()
      val ctiConfig = new CtiConfiguration(true, ctiProfile)
      oldXivoUser.setCtiConfiguration(ctiConfig)

      val oldVm = new Voicemail()
      oldVm.setName("Jean Dupond")
      oldVm.setNumber("2000")
      oldVm.setContext(myEntity.name)
      oldVm.setEmail("jdupond@example.com")
      oldXivoUser.setVoicemail(oldVm)

      val oldLine = new Line("2000")
      oldLine.setContext(myEntity.name)
      oldXivoUser.setLine(oldLine)

      val newUser = User(Some(2L), myEntity, "Marc", "Durand", "2001", Some("52001"), None, Some("mdurand"), Some("1234"),
        ringingTime = Some(60), callerIdMode = Some(CallerIdMode.IncomingNo))

      val expectedLine = new Line("2001")
      expectedLine.setContext(myEntity.name)

      val expectedVm = oldVm


      val expectedXivoUser = new XivoUser()
      expectedXivoUser.setId(idOnXivo)
      expectedXivoUser.setCtiConfiguration(ctiConfig)
      expectedXivoUser.setFirstname("Marc")
      expectedXivoUser.setLastname("Durand")
      expectedXivoUser.setUsername("mdurand")
      expectedXivoUser.setCallerid(""""Marc Durand"""")
      expectedXivoUser.setPassword("1234")
      expectedXivoUser.setDescription("Test")
      expectedXivoUser.setLine(expectedLine)
      expectedXivoUser.setVoicemail(expectedVm)
      expectedXivoUser.setOutcallerid("52001")
      expectedXivoUser.setRingSeconds(60)

      val res = xivoUserTransf.updateXivoUser(oldXivoUser, newUser)
      res.setCtiConfiguration(ctiConfig)
      res shouldEqual expectedXivoUser
    }

    "update xivoUser using values from user with empty cti profile" in new Helper() {
      val idOnXivo = 3
      val xivo = Xivo(None, UUID.randomUUID(), "xivo1", "192.168.1.123", "xivo1-name", None, Some("token"))
      val myEntity = Entity(None, mock[CombinedId], "avranches", "Avranches", xivo,
        List(Interval(None, "2000", "2099")), "52000", "default")

      val oldXivoUser = new XivoUser()
      oldXivoUser.setId(idOnXivo)
      oldXivoUser.setFirstname("Jean")
      oldXivoUser.setLastname("Dupond")
      oldXivoUser.setCallerid(""""Jean Dupond"""")
      oldXivoUser.setDescription("Test")

      val ctiProfile = new CtiProfile()
      val ctiConfig = new CtiConfiguration(false, ctiProfile)
      oldXivoUser.setCtiConfiguration(ctiConfig)

      val oldVm = new Voicemail()
      oldVm.setName("Jean Dupond")
      oldVm.setNumber("2000")
      oldVm.setContext(myEntity.name)
      oldVm.setEmail("jdupond@example.com")
      oldXivoUser.setVoicemail(oldVm)

      val oldLine = new Line("2000")
      oldLine.setContext(myEntity.name)
      oldXivoUser.setLine(oldLine)

      val newUser = User(Some(2L), myEntity, "Marc", "Durand", "2001", Some("52001"), None, None, None, callerIdMode = Some(CallerIdMode.IncomingNo) )

      val expectedLine = new Line("2001")
      expectedLine.setContext(myEntity.name)

      val expectedVm = oldVm


      val expectedXivoUser = new XivoUser()
      expectedXivoUser.setId(idOnXivo)
      expectedXivoUser.setCtiConfiguration(ctiConfig)
      expectedXivoUser.setUsername("")
      expectedXivoUser.setPassword("")
      expectedXivoUser.setFirstname("Marc")
      expectedXivoUser.setLastname("Durand")
      expectedXivoUser.setCallerid(""""Marc Durand"""")
      expectedXivoUser.setDescription("Test")
      expectedXivoUser.setLine(expectedLine)
      expectedXivoUser.setVoicemail(expectedVm)
      expectedXivoUser.setOutcallerid("52001")

      val res = xivoUserTransf.updateXivoUser(oldXivoUser, newUser)
      res.setCtiConfiguration(ctiConfig)
      res shouldEqual expectedXivoUser
    }

    "do not update xivo user outcallerId when disabled" in new Helper() {
      stub(configParser.disableOutcallerIdProcessing).toReturn(true)
      val (userWOExtNo, xivoUser) = getUserAndExpected(IntervalRoutingModes.Routed)
      val user = userWOExtNo.copy(externalNumber = Some("12345"))
      xivoUserTransf.updateXivoUser(xivoUser, user).getOutcallerid() shouldEqual null
    }

    "update private voicemail" in new Helper() {
      val voicemail = new Voicemail()
      voicemail.setName("aaa bbb")
      voicemail.setContext("default")
      voicemail.setNumber("4444")
      voicemail.setPassword("4444")
      voicemail.setEmail("foo@bar")
      voicemail.setAttach(false)
      voicemail.setDeleteMessages(false)

      val (user, _) = getUserAndExpected(IntervalRoutingModes.Routed)
      val updatedUser = user.copy(mail = Some("aaa@bbb"), firstName = "Jane", lastName = "Doe")

      val voicemailSettings = VoicemailSettings(enabled = true, numberMode = Some(VoiceMailNumberMode.ShortNumber),
        customNumber = None, sendEmail = Some(true), deleteAfterNotif = Some(true))

      xivoUserTransf.updatePrivateVoicemail(voicemail, UserWithVM(updatedUser, voicemailSettings))
      voicemail.getName shouldEqual "Jane Doe"
      voicemail.getEmail shouldEqual "aaa@bbb"
      voicemail.getAttach shouldEqual true
      voicemail.getDeleteMessages shouldEqual true
    }

    "set no options for user line by template settings" in new Helper() {
      val (user, _) = getUserAndExpected(IntervalRoutingModes.Routed, sipNameMode = PeerSipNameMode.Auto)

      xivoUserTransf.createXivoUser(user).getLine.getOptions shouldEqual null
    }

    "set options for webrtc user line by applying template settings" in new Helper() {
      val (user, _) = getUserAndExpected(IntervalRoutingModes.Routed, sipNameMode = PeerSipNameMode.WebRTC)

      xivoUserTransf.createXivoUser(user).getLine.getOptions.toString shouldEqual "[[webrtc, yes]]"
    }

    "set options for ua user line by applying template settings" in new Helper() {
      val (user, _) = getUserAndExpected(IntervalRoutingModes.Routed, sipNameMode = PeerSipNameMode.UniqueAccount)

      xivoUserTransf.createXivoUser(user).getLine.getOptions.toString shouldEqual "[[webrtc, ua]]"
    }

    "set options for webrtc user line by applying user settings overriding template settings" in new Helper() {
      val (user, _) = getUserAndExpected(IntervalRoutingModes.Routed, sipNameMode = PeerSipNameMode.Auto, userSipNameMode = Some(PeerSipNameMode.WebRTC))

      xivoUserTransf.createXivoUser(user).getLine.getOptions.toString shouldEqual "[[webrtc, yes]]"
    }

    "set options for ua user line by applying user settings overriding template settings" in new Helper() {
      val (user, _) = getUserAndExpected(IntervalRoutingModes.Routed, sipNameMode = PeerSipNameMode.Auto, userSipNameMode = Some(PeerSipNameMode.UniqueAccount))

      xivoUserTransf.createXivoUser(user).getLine.getOptions.toString shouldEqual "[[webrtc, ua]]"
    }

    "set default registrar" in new Helper() {
      val (user, _) = getUserAndExpected(IntervalRoutingModes.Routed, sipNameMode = PeerSipNameMode.UniqueAccount, registrar = "default")

      xivoUserTransf.createXivoUser(user).getLine.getRegistrar shouldEqual "default"
    }

    "set mds0 registrar" in new Helper() {
      val (user, _) = getUserAndExpected(IntervalRoutingModes.Routed, sipNameMode = PeerSipNameMode.UniqueAccount, registrar = "mds0")

      xivoUserTransf.createXivoUser(user).getLine.getRegistrar shouldEqual "mds0"
    }

    "set different ringingTime as template" in new Helper() {
      val (user, _) = getUserAndExpected(IntervalRoutingModes.Routed, sipNameMode = PeerSipNameMode.UniqueAccount, ringingTime = Some(60))
      xivoUserTransf.createXivoUser(user).getRingSeconds shouldEqual 60
    }

    "update caller id to external number" in new Helper() {
      val idOnXivo = 3
      val xivo = Xivo(None, UUID.randomUUID(), "xivo1", "192.168.1.123", "xivo1-name", None, Some("token"))
      val myEntity = Entity(None, mock[CombinedId], "avranches", "Avranches", xivo,
        List(Interval(None, "2000", "2099")), "52000", "default")

      val oldXivoUser = new XivoUser()
      oldXivoUser.setId(idOnXivo)
      oldXivoUser.setFirstname("Jean")
      oldXivoUser.setLastname("Dupond")
      oldXivoUser.setCallerid(""""Jean Dupond"""")
      oldXivoUser.setDescription("Test")

      val ctiProfile = new CtiProfile()
      val ctiConfig = new CtiConfiguration(false, ctiProfile)
      oldXivoUser.setCtiConfiguration(ctiConfig)

      val oldLine = new Line("2000")
      oldLine.setContext(myEntity.name)
      oldXivoUser.setLine(oldLine)

      val newUser = User(Some(2L), myEntity, "Marc", "Durand", "2001", Some("52001"), None, None, None, callerIdMode = Some(CallerIdMode.IncomingNo) )

      val res = xivoUserTransf.updateXivoUser(oldXivoUser, newUser)

      res.getOutcallerid shouldEqual "52001"
      res.getCallerid shouldEqual """"Marc Durand""""
    }

    "update caller id to custom number" in new Helper() {
      val idOnXivo = 3
      val xivo = Xivo(None, UUID.randomUUID(), "xivo1", "192.168.1.123", "xivo1-name", None, Some("token"))
      val myEntity = Entity(None, mock[CombinedId], "avranches", "Avranches", xivo,
        List(Interval(None, "2000", "2099")), "52000", "default")

      val oldXivoUser = new XivoUser()
      oldXivoUser.setId(idOnXivo)
      oldXivoUser.setFirstname("Jean")
      oldXivoUser.setLastname("Dupond")
      oldXivoUser.setCallerid(""""Jean Dupond"""")
      oldXivoUser.setDescription("Test")

      val ctiProfile = new CtiProfile()
      val ctiConfig = new CtiConfiguration(false, ctiProfile)
      oldXivoUser.setCtiConfiguration(ctiConfig)

      val oldLine = new Line("2000")
      oldLine.setContext(myEntity.name)
      oldXivoUser.setLine(oldLine)

      val newUser = User(Some(2L), myEntity, "Marc", "Durand", "2001", Some("52001"), None, None, None,
        callerIdMode = Some(CallerIdMode.Custom), customCallerId = Some("1234") )

      val res = xivoUserTransf.updateXivoUser(oldXivoUser, newUser)

      res.getOutcallerid shouldEqual "1234"
      res.getCallerid shouldEqual """"Marc Durand""""
    }

    "update caller id to anonymous number" in new Helper() {
      val idOnXivo = 3
      val xivo = Xivo(None, UUID.randomUUID(), "xivo1", "192.168.1.123", "xivo1-name", None, Some("token"))
      val myEntity = Entity(None, mock[CombinedId], "avranches", "Avranches", xivo,
        List(Interval(None, "2000", "2099")), "52000", "default")

      val oldXivoUser = new XivoUser()
      oldXivoUser.setId(idOnXivo)
      oldXivoUser.setFirstname("Jean")
      oldXivoUser.setLastname("Dupond")
      oldXivoUser.setCallerid(""""Jean Dupond"""")
      oldXivoUser.setDescription("Test")

      val ctiProfile = new CtiProfile()
      val ctiConfig = new CtiConfiguration(false, ctiProfile)
      oldXivoUser.setCtiConfiguration(ctiConfig)

      val oldLine = new Line("2000")
      oldLine.setContext(myEntity.name)
      oldXivoUser.setLine(oldLine)

      val newUser = User(Some(2L), myEntity, "Marc", "Durand", "2001", Some("52001"), None, None, None,
        callerIdMode = Some(CallerIdMode.Anonymous), customCallerId = Some("1234") )

      val res = xivoUserTransf.updateXivoUser(oldXivoUser, newUser)

      res.getOutcallerid shouldEqual null
      res.getCallerid shouldEqual """"Marc Durand""""
    }
  }

  "XivoUserTransformer.createVoicemail" should {
    "create base voicemail" in {
      val voicemail = XivoUserTransformer.createVoicemail("some context", "54321")
      voicemail.getName shouldEqual "54321"
      voicemail.getContext shouldEqual "some context"
      voicemail.getNumber shouldEqual "54321"
      voicemail.getPassword shouldEqual "54321"
    }
  }
}
package services

import java.util.UUID

import model._
import model.utils.Step
import org.mockito.Mockito._
import org.scalatest.TestData
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerTest
import play.api.inject.guice.GuiceApplicationBuilder
import testutils.{PlayShouldSpec, TestConfig}
import xivo.ldap.xivoconnection.XivoConnector
import model.User
import xivo.restapi.model.{User => XivoUser, _}
import scala.collection.JavaConverters._

class UserVoicemailOperationsSpec extends PlayShouldSpec with MockitoSugar with OneAppPerTest with TestConfig {

  override def newAppForTest(testData: TestData) = new GuiceApplicationBuilder()
    .configure(testConfig)
    .build

  trait Helper {
    implicit val connector = mock[XivoConnector]

    val xivoUuid = UUID.fromString("3acb24fa-0182-4c2a-89ad-4859312ffb7c")
    val xivo = Xivo(None, xivoUuid, "xivo1", "192.168.1.123", "to_xivo1-name", None, Some("token"))
    val xivoDb = XivoDb(xivo.id, xivo.uuid, xivo.name, xivo.host, xivo.contextName, Some("token"))
    val myEntity = Entity(None, mock[CombinedId], "avranches", "Avranches", xivo, List(Interval(Some(11), "2000", "2099")), "52000", "default")

    def sampleXivoUser(): XivoUser = {
      val xivoUser = new XivoUser()
      xivoUser.setId(123)
      xivoUser.setFirstname("Jean")
      xivoUser.setLastname("Dupond")
      xivoUser.setCallerid(""""Jean Dupond"""")
      xivoUser.setDescription("Test")
      xivoUser
    }
    val xivoUser = sampleXivoUser()
    val oldXivoUser = sampleXivoUser()

    val oldVm = new Voicemail()
    oldVm.setId(456)
    oldVm.setName("Jean Dupond")
    oldVm.setNumber("2000")
    oldVm.setPassword("0000")
    oldVm.setContext(myEntity.name)
    oldVm.setEmail("jdupond@example.com")

    val sharedVm = new Voicemail()
    sharedVm.setId(8888)
    sharedVm.setName("Sales department")
    sharedVm.setNumber("123456")
    sharedVm.setPassword("9999")
    sharedVm.setContext(myEntity.name)
    sharedVm.setEmail("foo@bar")
    sharedVm.setAttach(false)
    sharedVm.setDeleteMessages(false)

    val sharedVmOld = new Voicemail()
    sharedVmOld.setId(9999)
    sharedVmOld.setName("IT department")
    sharedVmOld.setNumber("998877")
    sharedVmOld.setPassword("1234")
    sharedVmOld.setContext(myEntity.name)
    sharedVmOld.setEmail("xxx@yyy")
    sharedVmOld.setAttach(true)
    sharedVmOld.setDeleteMessages(true)

    stub(connector.listVoicemails()).toReturn(List(sharedVmOld, sharedVm).asJava)

    val vmOperations = app.injector.instanceOf(classOf[UserVoicemailOperations])

    val oldUser = User(Some(2L), myEntity, "Jean","Dupond", "1010", Some("52001"), Some("jdupond@example.com"), Some("jdupon"), Some("9876"))
    val newUser = User(Some(2L), myEntity, "Marc", "Durand", "2001", Some("52001"), Some("mdurand@example.com"), Some("mdurand"), Some("1234"))

    val vmSettingsShort = VoicemailSettings(enabled = true, Some(VoiceMailNumberMode.ShortNumber), None, Some(true), Some(true))
    val vmSettingsCustom = VoicemailSettings(enabled = true, Some(VoiceMailNumberMode.Custom), Some(sharedVm.getNumber), Some(true), Some(true))


    def testStepCreateNewVoicemail(step: Step, updated: UserWithVM, xivoUser: XivoUser): Unit = {
      reset(connector)
      step.execute

      verify(connector).createVoicemailForUser(xivoUser)
      Option(xivoUser.getVoicemail).exists { voicemail =>
        voicemail.getName == "Marc Durand" &&
          voicemail.getNumber == "2001" &&
          voicemail.getPassword == "2001" &&
          voicemail.getEmail == "mdurand@example.com" &&
          voicemail.getAttach
      } shouldEqual true
      verifyNoMoreInteractions(connector)
    }

    def testStepDeleteVoicemail(step: Step, xivoUser: XivoUser): Unit = {
      reset(connector)
      step.execute

      verify(connector).deleteVoicemailForUser(xivoUser)
      xivoUser.getVoicemail shouldEqual null
      verifyNoMoreInteractions(connector)
    }

    def testStepAssociateCustomVoicemail(step: Step, xivoUser: XivoUser): Unit = {
      reset(connector)
      step.execute
      verify(connector).associateVoicemailToUser(xivoUser)
      Option(xivoUser.getVoicemail).exists { voicemail =>
        voicemail.getName == "Sales department" &&
        voicemail.getNumber == "123456" &&
        voicemail.getPassword == "9999" &&
        voicemail.getEmail == "foo@bar" &&
        voicemail.getAttach == false
      } shouldEqual true
      verifyNoMoreInteractions(connector)
    }

    def testStepDisassociateCustomVoicemailFromUser(step: Step, xivoUser: XivoUser): Unit = {
      reset(connector)
      step.execute

      verify(connector).dissociateVoicemailFromUser(xivoUser)
      xivoUser.getVoicemail shouldEqual null
      verifyNoMoreInteractions(connector)
    }
  }

  "UserVoicemailOperations atomic steps" should {

    "create step CreateNewVoicemail" in new Helper {
      val updated = UserWithVM(newUser, vmSettingsShort)

      val step = vmOperations.stepCreateNewVoicemail(xivoUser, updated)
      testStepCreateNewVoicemail(step, updated, xivoUser)
    }

    "create step DeleteVoicemail" in new Helper {
      val updated = UserWithVM(newUser, VoicemailSettings.disabled)
      xivoUser.setVoicemail(oldVm)

      val step = vmOperations.stepDeleteVoicemail(xivoUser)
      testStepDeleteVoicemail(step, xivoUser)
    }

    "create step AssociateCustomVoicemail" in new Helper {
      val updated = UserWithVM(newUser, vmSettingsCustom)

      val step = vmOperations.stepAssociateCustomVoicemail(xivoUser, updated)
      testStepAssociateCustomVoicemail(step, xivoUser)
    }

    "create step DisassociateCustomVoicemailFromUser" in new Helper {
      val old = UserWithVM(newUser, vmSettingsCustom)
      xivoUser.setVoicemail(sharedVm)

      val step = vmOperations.stepDisassociateCustomVoicemailFromUser(xivoUser, old)
      testStepDisassociateCustomVoicemailFromUser(step, xivoUser)
    }
  }


  "UserVoicemailOperations steps on create" should {

    "create user without voicemail" in new Helper {
      val created = UserWithVM(newUser, VoicemailSettings.disabled)

      val step = vmOperations.stepOnCreateUser(xivoUser, created)
      step.isEmpty shouldEqual true
    }

    "create user with voicemail with ShortNumber mode" in new Helper {
      val created = UserWithVM(newUser, vmSettingsShort)

      val step = vmOperations.stepOnCreateUser(xivoUser, created)
      step.nonEmpty shouldEqual true
      testStepCreateNewVoicemail(step.get, created, xivoUser)
    }

    "create user with voicemail with Custom mode" in new Helper {
      val created = UserWithVM(newUser, vmSettingsCustom)

      val step = vmOperations.stepOnCreateUser(xivoUser, created)
      step.nonEmpty shouldEqual true
      testStepAssociateCustomVoicemail(step.get, xivoUser)
    }

  }


  "UserVoicemailOperations steps on update" should {

    "update voicemail from None to None" in new Helper {
      val old = UserWithVM(oldUser, VoicemailSettings.disabled)
      val updated = UserWithVM(newUser, VoicemailSettings.disabled)

      val steps = vmOperations.stepsOnUpdateUser(oldXivoUser, xivoUser, old, updated)
      steps.isEmpty shouldEqual true
    }

    "update voicemail from None to ShortNumber" in new Helper {
      val old = UserWithVM(oldUser, VoicemailSettings.disabled)
      val updated = UserWithVM(newUser, vmSettingsShort)

      val steps = vmOperations.stepsOnUpdateUser(oldXivoUser, xivoUser, old, updated)
      steps.length shouldEqual 1
      testStepCreateNewVoicemail(steps.head, updated, xivoUser)
    }

    "update voicemail from None to Custom" in new Helper {
      val old = UserWithVM(oldUser, VoicemailSettings.disabled)
      val updated = UserWithVM(newUser, vmSettingsCustom)

      val steps = vmOperations.stepsOnUpdateUser(oldXivoUser, xivoUser, old, updated)
      steps.length shouldEqual 1
      testStepAssociateCustomVoicemail(steps.head, xivoUser)
    }

    "update voicemail from ShortNumber to None" in new Helper {
      val old = UserWithVM(oldUser, vmSettingsShort)
      val updated = UserWithVM(newUser, VoicemailSettings.disabled)
      xivoUser.setVoicemail(oldVm)

      val steps = vmOperations.stepsOnUpdateUser(oldXivoUser, xivoUser, old, updated)
      steps.length shouldEqual 1
      testStepDeleteVoicemail(steps.head, xivoUser)
    }

    "update voicemail from ShortNumber to ShortNumber" in new Helper {
      val vmSettingsShortOld = vmSettingsShort.copy(sendEmail = Some(false))
      oldVm.setAskPassword(false)
      val old = UserWithVM(oldUser, vmSettingsShortOld)
      xivoUser.setVoicemail(oldVm)
      val updated = UserWithVM(newUser, vmSettingsShort)
      val steps = vmOperations.stepsOnUpdateUser(oldXivoUser, xivoUser, old, updated)
      steps.length shouldEqual 1
      reset(connector)

      steps.head.execute
      verify(connector).updateVoicemailForUser(xivoUser)
      Option(xivoUser.getVoicemail).exists { voicemail =>
        voicemail.getName == "Marc Durand" &&
          voicemail.getNumber == "2001" &&
          voicemail.getPassword == "0000" &&
          voicemail.getEmail == "mdurand@example.com" &&
          voicemail.getAttach
      } shouldEqual true
      verifyNoMoreInteractions(connector)
    }

    "update voicemail from ShortNumber to Custom" in new Helper {
      val old = UserWithVM(oldUser, vmSettingsShort)
      xivoUser.setVoicemail(oldVm)
      val updated = UserWithVM(newUser, vmSettingsCustom)

      val steps = vmOperations.stepsOnUpdateUser(oldXivoUser, xivoUser, old, updated)
      steps.length shouldEqual 2
      testStepDeleteVoicemail(steps.head, xivoUser)
      testStepAssociateCustomVoicemail(steps(1), xivoUser)
    }

    "update voicemail from Custom to None" in new Helper {
      val old = UserWithVM(oldUser, vmSettingsCustom)
      val updated = UserWithVM(newUser, VoicemailSettings.disabled)
      xivoUser.setVoicemail(sharedVm)

      val steps = vmOperations.stepsOnUpdateUser(oldXivoUser, xivoUser, old, updated)
      steps.length shouldEqual 1
      testStepDisassociateCustomVoicemailFromUser(steps.head, xivoUser)
    }

    "update voicemail from Custom to ShortNumber" in new Helper {
      val old = UserWithVM(oldUser, vmSettingsCustom)
      xivoUser.setVoicemail(sharedVm)
      val updated = UserWithVM(newUser, vmSettingsShort)

      val steps = vmOperations.stepsOnUpdateUser(oldXivoUser, xivoUser, old, updated)
      steps.length shouldEqual 2
      testStepDisassociateCustomVoicemailFromUser(steps.head, xivoUser)
      testStepCreateNewVoicemail(steps(1), updated, xivoUser)
    }

    "update voicemail from Custom to Custom" in new Helper {
      val vmSettingsCustomOld = vmSettingsCustom.copy(customNumber = Some(sharedVmOld.getNumber))
      val old = UserWithVM(oldUser, vmSettingsCustomOld)
      xivoUser.setVoicemail(sharedVmOld)
      val updated = UserWithVM(newUser, vmSettingsCustom)

      val steps = vmOperations.stepsOnUpdateUser(oldXivoUser, xivoUser, old, updated)
      steps.length shouldEqual 2
      testStepDisassociateCustomVoicemailFromUser(steps.head, xivoUser)
      testStepAssociateCustomVoicemail(steps(1), xivoUser)
      sharedVm.getNumber
    }

  }

  "UserVoicemailOperations steps on delete" should {

    "delete user without voicemail" in new Helper {
      val deleted = UserWithVM(oldUser, VoicemailSettings.disabled)

      val step = vmOperations.stepOnDeleteUser(oldXivoUser, deleted)
      step.isEmpty shouldEqual true
    }

    "delete user with voicemail with ShortNumber mode" in new Helper {
      val deleted = UserWithVM(oldUser, vmSettingsShort)
      xivoUser.setVoicemail(oldVm)

      val step = vmOperations.stepOnDeleteUser(xivoUser, deleted)
      step.nonEmpty shouldEqual true
      testStepDeleteVoicemail(step.get, xivoUser)
    }

    "delete user with voicemail with Custom mode" in new Helper {
      val deleted = UserWithVM(oldUser, vmSettingsCustom)
      xivoUser.setVoicemail(sharedVm)

      val step = vmOperations.stepOnDeleteUser(xivoUser, deleted)
      step.nonEmpty shouldEqual true
      testStepDisassociateCustomVoicemailFromUser(step.get, xivoUser)
    }

  }

}

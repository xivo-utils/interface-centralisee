package services

import model.UIAdministrator
import model.rights.{AdministratorRole, Role}
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerSuite
import play.api.inject.guice.GuiceApplicationBuilder
import services.rights.{AdministratorRoleManager, RoleManager}
import testutils.{PlayShouldSpec, TestConfig}
import play.api.inject.bind
import org.mockito.Mockito.stub
import org.mockito.Matchers.any
import scala.util.{Failure, Success}

class UIAdministratorManagerSpec extends PlayShouldSpec with TestConfig with MockitoSugar with OneAppPerSuite {
  override lazy val app =  new GuiceApplicationBuilder()
    .configure(testConfig)
    .overrides(bind[AdministratorManager].toInstance(mock[AdministratorManager]))
    .overrides(bind[RoleManager].toInstance(mock[RoleManager]))
    .overrides(bind[AdministratorRoleManager].toInstance(mock[AdministratorRoleManager]))
    .build

  class Helper() {
    val adminManager = app.injector.instanceOf(classOf[AdministratorManager])
    val roleManager = app.injector.instanceOf(classOf[RoleManager])
    val adminRoleManager = app.injector.instanceOf(classOf[AdministratorRoleManager])
    val administratorManager = app.injector.instanceOf(classOf[UIAdministratorManager])

    val uiAdmin = UIAdministrator(Some(1), "aName", "aLogin", "aPassword", false, false,
      List(Role(Some(10), "r10"), Role(Some(11), "r11")))

    def stubGet(id: Option[Long] = Some(1)): UIAdministrator = {
      stub(adminManager.get(id.get)).toReturn(Success(AdministratorDb(Some(1), "aName", "aLogin", "aPassword", false, false)))
      stub(adminRoleManager.get(id.get)).toReturn(Success(List(AdministratorRole(id.get, 10), AdministratorRole(id.get, 11))))
      stub(roleManager.get(10)).toReturn(Success(Role(Some(10), "r10")))
      stub(roleManager.get(11)).toReturn(Success(Role(Some(11), "r11")))
      uiAdmin.copy(id = id)
    }

  }

  "The AdministratorDb" should {
    "create from administrator" in {
      val adb = AdministratorDb.fromAdministrator(UIAdministrator(Some(1), "aName", "aLogin", "", false, false, List()))
      adb.copy(hashedPassword="") shouldEqual AdministratorDb(Some(1), "aName", "aLogin", "", false, false)
    }
  }

  "The UIAdministrator manager" should {

    "get an administrator from the database" in new Helper() {
      val expected = stubGet(Some(1))
      administratorManager.get(1) match {
        case Success(x) => x shouldBe expected
        case Failure(e) => fail(e.getMessage() + "\n" +  e.getStackTrace().mkString("\n"))
      }
    }

    "get an administrator from the database by login" in new Helper() {
      val expected = stubGet(Some(1))
      stub(adminManager.getByLogin("aLogin")).toReturn(Success(
        AdministratorDb(Some(1), "aName", "aLogin", "aPassword", false, false)))
      administratorManager.getByLogin("aLogin") match {
        case Success(x) => x shouldBe expected
        case Failure(e) => fail(e.getMessage() + "\n" +  e.getStackTrace().mkString("\n"))
      }
    }


    "create an Administrator in the database" in new Helper() {
      val uia = UIAdministrator(None, "aName", "aLogin", "secret", false, false,
        List(Role(Some(10), "rName")))
      stub(adminManager.create(any())).toReturn(
        Success(AdministratorDb(Some(2), "aName", "aLogin", "password", false, false))
      )
      stub(adminRoleManager.create(AdministratorRole(2, 10))).toReturn(Success(AdministratorRole(2, 10)))

      administratorManager.create(uia) match {
        case Success(admin) => admin shouldEqual uia.copy(id=Some(2))
        case Failure(e) => fail(e.getMessage() + "\n" + e.getStackTrace().mkString("\n"))
      }
    }

    "update an template in the database" in new Helper() {
      val admin = UIAdministrator(Some(1), "name", "login", "passwd", false, false, List(Role(Some(10), "rName")))
      val adminRole = AdministratorRole(1, 10)
      val adminDb = AdministratorDb(Some(1), "name", "login", "passwd", false, false)
      stub(adminManager.update(adminDb)).toReturn(
        Success(adminDb)
      )
      stub(adminRoleManager.delete(1)).toReturn(Success(List(adminRole)))
      stub(adminRoleManager.create(adminRole)).toReturn(Success(adminRole))

      administratorManager.update(admin) match {
        case Success(result) => result shouldEqual admin
        case Failure(e) => fail(e.getMessage() + "\n" + e.getStackTrace().mkString("\n"))
      }
    }

    "delete an administrator from the database" in new Helper() {
      val expected =stubGet(Some(1))
      stub(adminRoleManager.delete(1)).toReturn(Success(List()))
      stub(adminManager.delete(1)).toReturn(Success(
        AdministratorDb(expected.id, expected.name, expected.login, expected.password, expected.superAdmin, expected.ldap)))

      administratorManager.delete(1) match {
        case Success(x) => x shouldBe expected
        case Failure(e) => fail(e.getMessage() + "\n" +  e.getStackTrace().mkString("\n"))
      }
    }

    "list administrators from the database" in new Helper() {
      val result = List(
        UIAdministrator(Some(1), "an1", "al1", "aPassword", false, false, List(Role(Some(10), "r10"), Role(Some(11), "r11"))),
        UIAdministrator(Some(2), "an2", "al2", "bPassword", false, false, List(Role(Some(10), "r10")))
      )

      stub(adminManager.all()).toReturn(Success(List(
        AdministratorDb(Some(1), "an1", "al1", "aPassword", false, false),
        AdministratorDb(Some(2), "an2", "al2", "bPassword", false, false))))

      stub(adminRoleManager.all()).toReturn(Success(List(
        AdministratorRole(1, 10),
        AdministratorRole(1, 11),
        AdministratorRole(2, 10))))

      stub(roleManager.all()).toReturn(Success(List(Role(Some(10), "r10"), Role(Some(11), "r11"), Role(Some(12), "r12"))))

      administratorManager.all() match {
        case Success(x) => x shouldBe result
        case Failure(e) => fail(e.getMessage() + "\n" +  e.getStackTrace().mkString("\n"))
      }
    }
  }
}

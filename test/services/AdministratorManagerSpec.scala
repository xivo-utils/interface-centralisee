package services

import model.UIAdministrator
import org.scalatest.TestData
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerTest
import testutils.{TestDBUtil, DbTest, PlayShouldSpec}
import scala.util.{Failure, Success}

class AdministratorManagerSpec extends PlayShouldSpec with MockitoSugar with DbTest with OneAppPerTest {
  override def newAppForTest(testData: TestData) = getApp

  class Helper() {
    cleanTable("administrators")
    val administratorManager = app.injector.instanceOf(classOf[AdministratorManager])
    def getAdministrator(name: String, id: Option[Long] = Some(1), passwd: String = "passwd") = AdministratorDb(id, name, name, passwd, false, false)
  }

  "The AdministratorDb" should {
    "create from administrator" in {
      val adb = AdministratorDb.fromAdministrator(UIAdministrator(Some(1), "aName", "aLogin", "", false, false, List()))
      adb.copy(hashedPassword="") shouldEqual AdministratorDb(Some(1), "aName", "aLogin", "", false, false)
    }
  }

  "The Administrator manager" should {

    "create an Administrator in the database" in new Helper() {
      val created = administratorManager.create(getAdministrator("testCreate", None))
      created match {
        case Success(t) => t.id shouldNot be(None)
        case Failure(e) => fail(e.getMessage() + e.getStackTrace().mkString("\n"))
      }
    }

    "get an administrator from the database" in new Helper() {
      val administrator = administratorManager.create(getAdministrator("test get")).get
      val expected = administrator.copy(id = administrator.id)
      administratorManager.get(administrator.id.get) match {
        case Success(x) => x.copy(hashedPassword=administrator.hashedPassword) shouldBe administrator
        case Failure(e) => fail(e.getMessage() + e.getStackTrace().mkString("\n"))
      }
    }

    "get an administrator from the database by login" in new Helper() {
      val administrator = administratorManager.create(getAdministrator("test_get_by_login")).get
      val expected = administrator.copy(id = administrator.id)
      administratorManager.getByLogin(administrator.login) match {
        case Success(x) => x.copy(hashedPassword=administrator.hashedPassword) shouldBe administrator
        case Failure(e) => fail(e.getMessage() + e.getStackTrace().mkString("\n"))
      }
    }

    "update an administrator in the database" in new Helper() {
      val administrator = administratorManager.create(getAdministrator("test update")).get
      val updated = administrator.copy(name = "updated test name")

      administratorManager.update(updated) match {
        case Success(t) => t.copy(hashedPassword=administrator.hashedPassword) shouldEqual(updated)
        case Failure(e) => fail(e.getMessage() + e.getStackTrace().mkString("\n"))
      }
    }

    "update the password if different while updating an administrator " in new Helper() {
      val administrator = administratorManager.create(getAdministrator("test update")).get
      val updated = administrator.copy(name = "updated test name", hashedPassword = "newPwd")

      administratorManager.update(updated) match {
        case Success(t) =>
          t.copy(hashedPassword="newPwd") shouldEqual(updated)
          (administrator.hashedPassword == t.hashedPassword) shouldBe false
        case Failure(e) => fail(e.getMessage() + e.getStackTrace().mkString("\n"))
      }
    }


    "delete an administrator from the database" in new Helper() {
      val administrator = administratorManager.create(getAdministrator("test delete")).get

      val deleted = administratorManager.delete(administrator.id.get)
      deleted match {
        case Success(t) => t.copy(hashedPassword=administrator.hashedPassword) shouldEqual(administrator)
        case Failure(e) => fail(e.getMessage() + e.getStackTrace().mkString("\n"))
      }
    }

    "list administrators from the database" in new Helper() {
      val administrator1 = administratorManager.create(getAdministrator("test list 1", None, "pwd")).get
      val administrator2 = administratorManager.create(getAdministrator("test list 2", None, "pwd")).get

      val all = administratorManager.all()
      all match {
        case Success(t) =>
          t.map(e => e.copy(hashedPassword="pwd")) shouldBe
            List(administrator1.copy(hashedPassword="pwd"), administrator2.copy(hashedPassword="pwd"))
        case Failure(e) => fail(e.getMessage() + e.getStackTrace().mkString("\n"))
      }
    }


  }
}

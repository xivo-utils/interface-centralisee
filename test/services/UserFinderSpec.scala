package services

import java.util.UUID

import model._
import org.joda.time.DateTime
import org.mockito.Mockito.stub
import org.scalatest.TestData
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerTest
import services.cache.CachedXivoUser.fromXivoUser
import testutils.{DbTest, PlayShouldSpec}
import xivo.restapi.model.{CtiConfiguration, CtiProfile, Line, User => XivoUser}

class UserFinderSpec extends PlayShouldSpec with MockitoSugar with DbTest with OneAppPerTest {
  override def newAppForTest(testData: TestData) = getApp

  override def insertUserLink(userLink: UserLink) = {
    val res = super.insertUserLink(userLink)
    res shouldEqual 1
    res
  }

  class Helper {
    Seq("xivo_users", "users", "route", "entity_intervals", "entities", "xivo").foreach(cleanTable)

    val xivoUuid = UUID.fromString("3acb24fa-0182-4c2a-89ad-4859312ffb7c")
    val xivo = insertXivo(Xivo(None, xivoUuid, "xivo", "192.168.1.123", "to_xivo1-name", None, Some("token")))
    val xivoDb = XivoDb(xivo.id, xivo.uuid, xivo.name, xivo.host, xivo.contextName, Some("token"))
    val intervalId = Some(112233)

    val myEntity = insertEntity(Entity(None, CombinedId(xivoUuid, "default"), "avranches", "Avranches", xivo, List(Interval(intervalId, "2000", "2099")), "52000", "default"))
    val otherEntity = insertEntity(Entity(None, CombinedId(xivoUuid, "other"), "avranches2", "Avranches2", xivo, List(Interval(Some(445566), "2100", "2199")), "52000", "default"))

    val routeManager = app.injector.instanceOf(classOf[RouteInterface])
    val routeMetaManager = app.injector.instanceOf(classOf[RouteMetaManager])

    val now = new DateTime()
    val eRoute = routeManager.create(Route(None, "52000", "(52000)", xivo.contextName, "2000"))
    routeMetaManager.create(RouteMeta(None, eRoute.id.get, now, "test", now, "test", now, "test", false))
    val iRoute = routeManager.create(Route(None, "2000", "(2000)", xivo.contextName, "2000"))
    routeMetaManager.create(RouteMeta(None, iRoute.id.get, now, "test", now, "test", now, "test", false))

    val userFinder = app.injector.instanceOf(classOf[UserFinder])

    val userLink10Id = 10L
    val userLink10 = UserLink(Some(userLink10Id), myEntity.id.get, 1, eRoute.id, iRoute.id, myEntity.intervals.head.id)
    val userLink20Id = 20L
    val userLink20 = UserLink(Some(userLink20Id), myEntity.id.get, 1, None, None)
    val userLink110Id = 110L
    val userLink110 = UserLink(Some(userLink110Id), otherEntity.id.get, 1, None, None)

    val defaultCtiProfile = new CtiProfile()
    defaultCtiProfile.setId(254)

    def getXivoUser(id: Int = 0, context: String = myEntity.name, firstName: String = "Pierre"): XivoUser = {
      val line = new Line("2000")
      line.setContext(context)
      line.setprovisioningCode("123456")
      val xivoUser = new XivoUser()
      if (id > 0) xivoUser.setId(id)
      xivoUser.setFirstname(firstName)
      xivoUser.setLastname("Durand")
      xivoUser.setLine(line)
      xivoUser.setRingSeconds(20)
      xivoUser.setOutcallerid(myEntity.presentedNumber)
      xivoUser.setCtiConfiguration(new CtiConfiguration(true, defaultCtiProfile))

      xivoUser
    }

    val xuser1 = getXivoUser(100, myEntity.name)
    val cached1 = fromXivoUser(xivoUuid, xuser1)
    val xuser2 = getXivoUser(200, myEntity.name, "Jacque")
    val cached2 = fromXivoUser(xivoUuid, xuser2)
  }

  "The UserFinder" should {

    "list both user IDs for entity" in new Helper {
      insertUserLink(userLink20)
      insertUserLink(userLink110)
      insertUserLink(userLink10)

      def ids(userLink: UserLink) = (userLink.id.get, userLink.idOnXivo)
      userFinder.listBothUserIdsForEntity(myEntity.id.get) shouldEqual List(ids(userLink10), ids(userLink20))
      userFinder.listBothUserIdsForEntity(otherEntity.id.get) shouldEqual List(ids(userLink110))
      userFinder.listBothUserIdsForEntity(-1) shouldEqual List()
    }

    "list user IDs for entity" in new Helper {
      insertUserLink(userLink20)
      insertUserLink(userLink110)
      insertUserLink(userLink10)

      userFinder.listUserIdsForEntity(myEntity.id.get) shouldEqual List(userLink10.id.get, userLink20.id.get)
      userFinder.listUserIdsForEntity(otherEntity.id.get) shouldEqual List(userLink110.id.get)
      userFinder.listUserIdsForEntity(-1) shouldEqual List()
    }

    "list users across XiVOS using xivo user cache" in new Helper() {
      val entityManager = mock[EntityInterface]
      stub(entityManager.getEntity(myEntity.id.get)).toReturn(Some(myEntity))

      insertUserLink(UserLink(Some(1010), myEntity.id.get, xuser1.getId.toLong, None, None))
      insertUserLink(UserLink(Some(1020), myEntity.id.get, xuser2.getId.toLong, None, None))
      insertUserLink(userLink110)

      val users = userFinder.listAcrossXivosFromCache(entityManager, List(cached1, cached2))
      users.map(u => (u.id, u.firstName)) shouldEqual List(
        (Some(1010), xuser1.getFirstname),
        (Some(1020), xuser2.getFirstname)
      )
    }

    "list users across XiVOS using xivo user cache when user is missing from cache" in new Helper() {
      val entityManager = mock[EntityInterface]
      stub(entityManager.getEntity(myEntity.id.get)).toReturn(Some(myEntity))

      insertUserLink(UserLink(Some(1010), myEntity.id.get, xuser1.getId.toLong, None, None))
      insertUserLink(UserLink(Some(1020), myEntity.id.get, xuser2.getId.toLong, None, None))
      insertUserLink(userLink110)

      val users = userFinder.listAcrossXivosFromCache(entityManager, List(cached2))
      users.map(u => (u.id, u.firstName)) shouldEqual List((Some(1020), xuser2.getFirstname))
    }

    "list users for Entity using xivo user cache" in new Helper() {
      insertUserLink(UserLink(Some(1010), myEntity.id.get, xuser1.getId.toLong, None, None))
      insertUserLink(UserLink(Some(1020), myEntity.id.get, xuser2.getId.toLong, None, None))
      insertUserLink(userLink110)

      val cachedUsers = Map(cached1.xivoUserId -> cached1, cached2.xivoUserId -> cached2)
      val userList = userFinder.listForEntityFromCache(myEntity, cachedUsers)
      userList.users.map(u => (u.id, u.firstName)) shouldEqual List(
        (Some(1010), xuser1.getFirstname),
        (Some(1020), xuser2.getFirstname)
      )
      userList.ghostIds.isEmpty shouldEqual true
    }

    "list users for Entity using xivo user cache when user is missing from cache" in new Helper() {
      insertUserLink(UserLink(Some(1010), myEntity.id.get, xuser1.getId.toLong, None, None))
      insertUserLink(UserLink(Some(1020), myEntity.id.get, xuser2.getId.toLong, None, None))
      insertUserLink(userLink110)

      val cachedUsers = Map(cached2.xivoUserId -> cached2)
      val userList = userFinder.listForEntityFromCache(myEntity, cachedUsers)
      userList.users.map(u => (u.id, u.firstName)) shouldEqual List((Some(1020), xuser2.getFirstname))
      userList.ghostIds shouldEqual List(1010)
    }

  }

}

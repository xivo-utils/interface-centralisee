package services.helpers

import java.util.UUID

import model.{CombinedId, TemplateEntity, TemplateXivo, _}

trait GetTemplateFixture {
  def getTemplate(id: Option[Long] = None, name: Option[String] = None, peerSipNameMode: PeerSipNameMode.Type = PeerSipNameMode.Auto) : Template = Template(
    TemplateProperties(
      id,
      name.getOrElse("Test template"),
      peerSipNameMode,
      CallerIdMode.IncomingNo,
      None,
      45,
      true,
      Some(VoiceMailNumberMode.ShortNumber),
      None,
      false,
      false
    ),
    List(
      TemplateXivo(1, id)
    ),
    List(
      TemplateEntity(CombinedId(UUID.randomUUID(), "default"), id)
    ),
    List(
      TemplateInterval(1, id)
    )

  )
}

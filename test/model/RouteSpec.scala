package model

import java.util.UUID
import org.scalatest.TestData
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerTest
import testutils.{DbTest, PlayShouldSpec}

class RouteSpec extends PlayShouldSpec with MockitoSugar with DbTest with OneAppPerTest {

  override def newAppForTest(testData: TestData) = getApp

  class Helper() {
    cleanTable("route")
    val routeManager = app.injector.instanceOf(classOf[RouteInterface])
  }

  "The Route interface implementation" should {

    "insert a route in the database" in new Helper() {
      val route = Route(None, "30", "(30[0-9]{2})", "to-monxivo", "45\\1")

      val createdRoute = routeManager.create(route)

      createdRoute.id.isDefined shouldBe true
      listRoutes shouldEqual List(createdRoute)
    }

    "update a route if it exists on creation" in new Helper() {
      val route = routeManager.create(Route(None, "40", "(40[0-9]{2})", "to-myxivo"))

      val createdRoute = routeManager.create(route.copy(target = "to-myxivo-new"))

      createdRoute.id.isDefined shouldBe true
      listRoutes shouldEqual List(createdRoute)
    }

    "setDeleted a route by id" in new Helper() {

      val createdRoute = routeManager.create(Route(None, "30", "(30[0-9]{2})", "to-monxivo"))

      routeManager.setDeleted(createdRoute.id.get)

      listRoutes shouldEqual List(createdRoute.copy(context=Route.targetDisabled))
    }

    "setDeletedByDigits an existing route" in new Helper() {
      val createdRoute = routeManager.create(Route(None, "30", "(30[0-9]{2})", "to-monxivo"))
      val updatedRoute = routeManager.setDeletedByDigits("30")
      updatedRoute shouldEqual createdRoute.copy(context=Route.targetDisabled, regexp = "(30)")
    }

    "setDeletedByDigits a non-existing route" in new Helper() {
      val updatedRoute = routeManager.setDeletedByDigits("30")
      updatedRoute shouldEqual Route(updatedRoute.id, "30", "(30)", Route.targetDisabled, "\\1")
    }

    "update an existing route" in new Helper() {
      val createdRoute = routeManager.create(Route(None, "30", "(30[0-9]{2})", "to-monxivo"))
      val newRoute = createdRoute.copy(digits="40")

      routeManager.update(newRoute)

      listRoutes shouldEqual List(newRoute)
    }

    "tell if a route already exists" in new Helper() {
      private val digits1 = "021487896"
      routeManager.create(Route(None, digits1, "(.*)", "to-test"))
      val digits2 = "8827346664"
      routeManager.create(Route(None, digits2, "(.*)", Route.targetDisabled))

      routeManager.routeExists(digits1) shouldBe true
      routeManager.routeExists(digits2) shouldBe true
      routeManager.routeExists("78945612") shouldBe false
    }

    "tell if a number is used" in new Helper() {
      val digits1 = "8346772"
      routeManager.create(Route(None, digits1, "(.*)", "to-test"))
      val digits2 = "93667344"
      routeManager.create(Route(None, digits2, "(.*)", Route.targetDisabled))

      routeManager.numberUsed(digits1) shouldBe true
      routeManager.numberUsed(digits2) shouldBe false
      routeManager.numberUsed("833677744") shouldBe false
    }

    "insert an external route in the database" in new Helper() {
      val xivo = Xivo(Some(1), UUID.randomUUID(), "testXivo", "testHost", "to_xivoTestTrunk", None, Some("token"))
      val entity = Entity(Some(1), mock[CombinedId], "name", "displayName", xivo, List(Interval(None, "1000", "1099")), "presented", "default")
      val user = User(Some(1), entity, "first", "last", "1001", Some("0222221001"), None, None, None)
      val createdRoute = routeManager.createExternal(user)

      createdRoute.copy(id=None) shouldBe Route(None, "0222221001", "(0222221001)", "to_xivoTestTrunk", "1001")
      listRoutes shouldEqual List(createdRoute)
    }

    "insert an internal route in the database" in new Helper() {
      val xivo = Xivo(Some(1), UUID.randomUUID(), "testXivo", "testHost", "to_xivoTestTrunk", None, Some("token"))
      val entity = Entity(Some(1), mock[CombinedId], "name", "displayName", xivo, List(Interval(None, "1000", "1099")), "presented", "default")
      val user = User(Some(1), entity, "first", "last", "1001", None, None, None, None)
      val createdRoute = routeManager.createInternal(user)

      createdRoute.copy(id=None) shouldBe Route(None, "1001", "(1001)", "to_xivoTestTrunk", "\\1")
      listRoutes shouldEqual List(createdRoute)
    }

  }
}

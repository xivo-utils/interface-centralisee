package model

import java.util.UUID

import org.scalatest.{ShouldMatchers, WordSpec}
import play.api.libs.json._

class TemplateSpec extends WordSpec with ShouldMatchers {

  "The Template model" should {
    "serialize a template from JSON" in {
      val randomCombinedId: CombinedId = CombinedId(UUID.randomUUID(), "default")
      val templateJSON: String =
        s"""{"id": 1234, "name": "Template 1", "peerSipName": "auto", "routedInboundPrefix": "01234567",
           |"callerIdMode": "incomingNo", "ringingTime": 45, "voiceMailEnabled": true,
           |"voiceMailNumberMode": "short_number", "voiceMailCustomNumber": "1000", "voiceMailSendEmail": false, "voiceMailDeleteAfterNotif": false,
           |"xivos": [1, 2], "entities": ["$randomCombinedId"], "intervals": [15,27,28]}""".stripMargin
      val json: JsValue = Json.parse(templateJSON)
      val expected: Template = Template(
        TemplateProperties(Some(1234), "Template 1", PeerSipNameMode.Auto, CallerIdMode.IncomingNo, None, 45, true, Some(VoiceMailNumberMode.ShortNumber), Some("1000"), false, false),
        List(TemplateXivo(1, Some(1234)), TemplateXivo(2, Some(1234))),
        List(TemplateEntity(randomCombinedId, Some(1234))),
        List(TemplateInterval(15, Some(1234)), TemplateInterval(27, Some(1234)), TemplateInterval(28, Some(1234)))
      )
      json.validate[Template] match {
        case JsSuccess(o, _) => o shouldEqual expected
        case JsError(error) => fail(JsError.toJson(error).toString)
      }
    }

    "serialize a template from JSON with disabled voicemail" in {
      val randomCombinedId: CombinedId = CombinedId(UUID.randomUUID(), "default")
      val templateJSON: String =
        s"""{"id": 1234, "name": "Template 1", "peerSipName": "auto", "routedInboundPrefix": "01234567",
           |"callerIdMode": "incomingNo", "ringingTime": 45, "voiceMailEnabled": false,
           |"voiceMailNumberMode": "short_number", "voiceMailCustomNumber": "1000", "voiceMailSendEmail": true,
           |"xivos": [1, 2], "entities": ["$randomCombinedId"], "intervals": [15,27,28]}""".stripMargin
      val json: JsValue = Json.parse(templateJSON)
      val expected: Template = Template(
        TemplateProperties(Some(1234), "Template 1", PeerSipNameMode.Auto, CallerIdMode.IncomingNo, None, 45, false, Some(VoiceMailNumberMode.ShortNumber), Some("1000"), false, false),
        List(TemplateXivo(1, Some(1234)), TemplateXivo(2, Some(1234))),
        List(TemplateEntity(randomCombinedId, Some(1234))),
        List(TemplateInterval(15, Some(1234)), TemplateInterval(27, Some(1234)), TemplateInterval(28, Some(1234)))
      )
      json.validate[Template] match {
        case JsSuccess(o, _) => o shouldEqual expected
        case JsError(error) => fail(JsError.toJson(error).toString)
      }
    }

    "serialize a template from JSON with enabled voicemail" in {
      val randomCombinedId: CombinedId = CombinedId(UUID.randomUUID(), "default")
      val templateJSON: String =
        s"""{"id": 1234, "name": "Template 1", "peerSipName": "auto", "routedInboundPrefix": "01234567",
           |"callerIdMode": "incomingNo", "ringingTime": 45, "voiceMailEnabled": true,
           |"voiceMailNumberMode": "short_number", "voiceMailCustomNumber": "1000", "voiceMailSendEmail": true,
           |"xivos": [1, 2], "entities": ["$randomCombinedId"], "intervals": [15,27,28]}""".stripMargin
      val json: JsValue = Json.parse(templateJSON)
      val expected: Template = Template(
        TemplateProperties(Some(1234), "Template 1", PeerSipNameMode.Auto, CallerIdMode.IncomingNo, None, 45, true, Some(VoiceMailNumberMode.ShortNumber), Some("1000"), true, false),
        List(TemplateXivo(1, Some(1234)), TemplateXivo(2, Some(1234))),
        List(TemplateEntity(randomCombinedId, Some(1234))),
        List(TemplateInterval(15, Some(1234)), TemplateInterval(27, Some(1234)), TemplateInterval(28, Some(1234)))
      )
      json.validate[Template] match {
        case JsSuccess(o, _) => o shouldEqual expected
        case JsError(error) => fail(JsError.toJson(error).toString)
      }
    }

    "serialize a template to JSON" in {
      val randomCombinedId: CombinedId = CombinedId(UUID.randomUUID(), "default")
      val template = Template(
        TemplateProperties(Some(1234), "Template 1", PeerSipNameMode.Auto, CallerIdMode.IncomingNo, None, 45, true, Some(VoiceMailNumberMode.ShortNumber), Some("1000"), false, false),
        List(TemplateXivo(1, Some(1234)), TemplateXivo(2, Some(1234))),
        List(TemplateEntity(randomCombinedId, Some(1234))),
        List(TemplateInterval(1, Some(1234)), TemplateInterval(7, Some(1234)))
      )
      val expectedJSON = Json.obj(
        "id"-> 1234, "name"-> "Template 1", "peerSipName"-> "auto", "callerIdMode"-> "incomingNo", "ringingTime"-> 45,
        "voiceMailEnabled"-> true, "voiceMailNumberMode"-> "short_number", "voiceMailCustomNumber"-> "1000",
        "voiceMailSendEmail"-> false, "voiceMailDeleteAfterNotif" -> false,  "xivos"-> JsArray(Array(JsNumber(1), JsNumber(2))),
        "entities"-> JsArray(Array(JsString(s"$randomCombinedId"))), "intervals" -> JsArray(Array(JsNumber(1), JsNumber(7))))

      val json = Json.toJson(template)
      json shouldEqual expectedJSON

    }

    "serialize a WebRTC template from JSON" in {
      val randomCombinedId: CombinedId = CombinedId(UUID.randomUUID(), "default")
      val templateJSON: String =
        s"""{"id": 1234, "name": "Template 1", "peerSipName": "webrtc", "routedInboundPrefix": "01234567",
           |"callerIdMode": "incomingNo", "ringingTime": 45, "voiceMailEnabled": true,
           |"voiceMailNumberMode": "short_number", "voiceMailCustomNumber": "1000", "voiceMailSendEmail": false,
           |"xivos": [1, 2], "entities": ["$randomCombinedId"], "intervals": [15,27,28]}""".stripMargin
      val json: JsValue = Json.parse(templateJSON)
      val expected: Template = Template(
        TemplateProperties(Some(1234), "Template 1", PeerSipNameMode.WebRTC, CallerIdMode.IncomingNo, None, 45, true, Some(VoiceMailNumberMode.ShortNumber), Some("1000"), false, false),
        List(TemplateXivo(1, Some(1234)), TemplateXivo(2, Some(1234))),
        List(TemplateEntity(randomCombinedId, Some(1234))),
        List(TemplateInterval(15, Some(1234)), TemplateInterval(27, Some(1234)), TemplateInterval(28, Some(1234)))
      )
      json.validate[Template] match {
        case JsSuccess(o, _) => o shouldEqual expected
        case JsError(error) => fail(JsError.toJson(error).toString)
      }
    }

    "serialize a WebRTC template to JSON" in {
      val randomCombinedId: CombinedId = CombinedId(UUID.randomUUID(), "default")
      val template = Template(
        TemplateProperties(Some(1234), "Template 1", PeerSipNameMode.WebRTC, CallerIdMode.IncomingNo, None, 45, true, Some(VoiceMailNumberMode.ShortNumber), Some("1000"), false, false),
        List(TemplateXivo(1, Some(1234)), TemplateXivo(2, Some(1234))),
        List(TemplateEntity(randomCombinedId, Some(1234))),
        List(TemplateInterval(1, Some(1234)), TemplateInterval(7, Some(1234)))
      )
      val expectedJSON = Json.obj(
        "id"-> 1234, "name"-> "Template 1", "peerSipName"-> "webrtc", "callerIdMode"-> "incomingNo", "ringingTime"-> 45,
        "voiceMailEnabled"-> true, "voiceMailNumberMode"-> "short_number", "voiceMailCustomNumber"-> "1000",
        "voiceMailSendEmail"-> false, "voiceMailDeleteAfterNotif" -> false, "xivos"-> JsArray(Array(JsNumber(1), JsNumber(2))),
        "entities"-> JsArray(Array(JsString(s"$randomCombinedId"))), "intervals" -> JsArray(Array(JsNumber(1), JsNumber(7))))

      val json = Json.toJson(template)
      json shouldEqual expectedJSON

    }

    "serialize a UA template from JSON" in {
      val randomCombinedId: CombinedId = CombinedId(UUID.randomUUID(), "default")
      val templateJSON: String =
        s"""{"id": 1234, "name": "Template 1", "peerSipName": "ua", "routedInboundPrefix": "01234567",
           |"callerIdMode": "incomingNo", "ringingTime": 45, "voiceMailEnabled": true,
           |"voiceMailNumberMode": "short_number", "voiceMailCustomNumber": "1000", "voiceMailSendEmail": false,
           |"voiceMailDeleteAfterNotif": false, "xivos": [1, 2], "entities": ["$randomCombinedId"], "intervals": [15,27,28]}""".stripMargin
      val json: JsValue = Json.parse(templateJSON)
      val expected: Template = Template(
        TemplateProperties(Some(1234), "Template 1", PeerSipNameMode.UniqueAccount, CallerIdMode.IncomingNo, None, 45, true, Some(VoiceMailNumberMode.ShortNumber), Some("1000"), false, false),
        List(TemplateXivo(1, Some(1234)), TemplateXivo(2, Some(1234))),
        List(TemplateEntity(randomCombinedId, Some(1234))),
        List(TemplateInterval(15, Some(1234)), TemplateInterval(27, Some(1234)), TemplateInterval(28, Some(1234)))
      )
      json.validate[Template] match {
        case JsSuccess(o, _) => o shouldEqual expected
        case JsError(error) => fail(JsError.toJson(error).toString)
      }
    }

    "serialize a UA template to JSON" in {
      val randomCombinedId: CombinedId = CombinedId(UUID.randomUUID(), "default")
      val template = Template(
        TemplateProperties(Some(1234), "Template 1", PeerSipNameMode.UniqueAccount, CallerIdMode.IncomingNo, None, 45, true, Some(VoiceMailNumberMode.ShortNumber), Some("1000"), false, false),
        List(TemplateXivo(1, Some(1234)), TemplateXivo(2, Some(1234))),
        List(TemplateEntity(randomCombinedId, Some(1234))),
        List(TemplateInterval(1, Some(1234)), TemplateInterval(7, Some(1234)))
      )
      val expectedJSON = Json.obj(
        "id"-> 1234, "name"-> "Template 1", "peerSipName"-> "ua", "callerIdMode"-> "incomingNo", "ringingTime"-> 45,
        "voiceMailEnabled"-> true, "voiceMailNumberMode"-> "short_number", "voiceMailCustomNumber"-> "1000",
        "voiceMailSendEmail"-> false, "voiceMailDeleteAfterNotif" -> false, "xivos"-> JsArray(Array(JsNumber(1), JsNumber(2))),
        "entities"-> JsArray(Array(JsString(s"$randomCombinedId"))), "intervals" -> JsArray(Array(JsNumber(1), JsNumber(7))))

      val json = Json.toJson(template)
      json shouldEqual expectedJSON

    }

    "get VoicemailSettings from TemplateProperties" in {

      val properties = TemplateProperties(
        id = Some(1234),
        name = "Template 1",
        peerSipName = PeerSipNameMode.Auto,
        callerIdMode = CallerIdMode.IncomingNo,
        customCallerId = None,
        ringingTime = 45,
        voiceMailEnabled = true,
        voiceMailNumberMode = Some(VoiceMailNumberMode.Custom),
        voiceMailCustomNumber = Some("1000"),
        voiceMailSendEmail = false,
        voiceMailDeleteAfterNotif = false
      )

      properties.voicemailSettings shouldEqual VoicemailSettings(
        enabled = true,
        numberMode = Some(VoiceMailNumberMode.Custom),
        customNumber = Some("1000"),
        sendEmail = Some(false),
        deleteAfterNotif = Some(false))

      properties.copy(voiceMailEnabled = false).voicemailSettings shouldEqual VoicemailSettings.disabled
    }
  }

}

package model

import org.scalatest.{ShouldMatchers, WordSpec}
import play.api.libs.json.Json

class MediaServerSpec extends WordSpec with ShouldMatchers {

  "MediaServer" should {
    "serialize a template to JSON" in {
      val mediaServer = MediaServer(1, "mds1", "Media Server 1", "1.1.1.1")

      val expected = Json.obj("id" -> 1, "name"-> "mds1", "displayName"-> "Media Server 1", "voipIp"-> "1.1.1.1")

      val json = Json.toJson(mediaServer)
      json shouldEqual expected
    }

  }
}

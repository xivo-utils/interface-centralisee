package model.rights

import org.scalatest.{ShouldMatchers, WordSpec}
import play.api.libs.json._

class RoleSpec extends WordSpec with ShouldMatchers {

  "The Role" should {
    "encode to json" in {
      val json = JsObject(Seq("id" -> JsNumber(1), "name" -> JsString("name")))

      json.validate[Role] match {
        case JsSuccess(o, _) => o shouldEqual Role(Some(1), "name")
        case JsError(error) => fail(JsError.toJson(error).toString)
      }
    }

    "decode from json" in {
      val role = Role(Some(2), "testName")
      Json.toJson(role) shouldEqual JsObject(Seq("id" -> JsNumber(2), "name" -> JsString("testName")))
    }
  }

}

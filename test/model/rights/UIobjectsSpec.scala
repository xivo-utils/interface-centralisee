package model.rights

import org.scalatest._
import play.api.libs.json.{Json, _}

class UIobjectsSpec extends WordSpec with ShouldMatchers {

  val json = """{"id":1,"name":"rName","xivo":[{"xivoId":1,"xivoName":"xName","entities":""" +
            s"""[{"entityId":2,"entityName":"eName","operations":{"reading":false,""" +
             """"creating":false,"editing":false,"deleting":false}}]}]}"""

  "The UIRole" should {
    "decode from json" in {
      Json.parse(json).validate[UIRole] match {
        case JsSuccess(o, _) => o shouldEqual UIRole(Some(1), Some("rName"), List(UIXivoPermission(1L, "xName",
          List(UIEntityPermission(2, "eName", UIOperations())))))
        case JsError(error) => fail(JsError.toJson(error).toString)
      }
    }

    "encode to json" in {
      val role = UIRole(Some(1), Some("rName"), List(UIXivoPermission(1L, "xName",
        List(UIEntityPermission(2, "eName", UIOperations())))))
      Json.toJson(role).toString() shouldEqual json
    }

    "encode to json even if not complete" in {
      val role = UIRole(Some(1), Some("rName"), List())
      Json.toJson(role).toString() shouldEqual """{"id":1,"name":"rName","xivo":[]}"""
    }

  }

}

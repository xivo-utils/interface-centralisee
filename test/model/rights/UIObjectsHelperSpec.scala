package model.rights

import org.scalatest.{Matchers, WordSpec}

class UIObjectsHelperSpec extends WordSpec with Matchers {

  "combineOperations" should {
    "combines properly all rights" in {
      import model.rights.UIObjectsHelper.combineOperations
      val booleans = List(true, false)
      val allOperations = for {
        reading <- booleans
        creating <- booleans
        editing <- booleans
        deleting <- booleans
      } yield UIOperations(reading, creating, editing, deleting)

      for {
        op1 <- allOperations
        op2 <- allOperations
      } {
        val combined = combineOperations(op1, op2)
        combined.reading shouldEqual op1.reading || op2.reading
        combined.creating shouldEqual op1.creating || op2.creating
        combined.editing shouldEqual op1.editing || op2.editing
        combined.deleting shouldEqual op1.deleting || op2.deleting
      }
    }
  }

  "groupEntityPermissions" should {
    import model.rights.UIObjectsHelper.aggregateEntityPermissions

    "return disjuncted UIEntityPermission intact" in {
      val permissions = List(
        UIEntityPermission(1, "entity A", UIOperations(reading = true)),
        UIEntityPermission(2, "entity B", UIOperations(creating = true))
      )
      aggregateEntityPermissions(permissions) shouldEqual permissions
    }
    "return related UIEntityPermission combined" in {
      val permissions = List(
        UIEntityPermission(1, "entity A", UIOperations(reading = true)),
        UIEntityPermission(1, "entity A", UIOperations(creating = true))
      )
      aggregateEntityPermissions(permissions) shouldEqual List(
        UIEntityPermission(1, "entity A", UIOperations(reading = true, creating = true))
      )
    }
    "join identical UIEntityPermission's" in {
      val permissions = List(
        UIEntityPermission(1, "entity A", UIOperations(reading = true)),
        UIEntityPermission(1, "entity A", UIOperations(reading = true))
      )
      aggregateEntityPermissions(permissions) shouldEqual List(
        UIEntityPermission(1, "entity A", UIOperations(reading = true))
      )
    }
    "group properly complex sample" in {
      val permissions = List(
        UIEntityPermission(1, "entity A", UIOperations(reading = true)),
        UIEntityPermission(1, "entity A", UIOperations(reading = true)),
        UIEntityPermission(1, "entity A", UIOperations(creating = true)),
        UIEntityPermission(2, "entity B", UIOperations(creating = true))
      )
      aggregateEntityPermissions(permissions) shouldEqual List(
        UIEntityPermission(1, "entity A", UIOperations(reading = true, creating = true)),
        UIEntityPermission(2, "entity B", UIOperations(creating = true))
      )
    }
  }

  "groupUIRoles" should {

    import model.rights.UIObjectsHelper.aggregateUIRoles

    "return disjuncted UIEntityPermission intact" in {
      val permXivoFoo = UIXivoPermission(
        100, "XiVO foo", List(
          UIEntityPermission(1, "entity A", UIOperations(reading = true)),
          UIEntityPermission(2, "entity B", UIOperations(creating = true))
        )
      )
      val permXivoBar = UIXivoPermission(
        200, "XiVO bar", List(
          UIEntityPermission(1, "entity A", UIOperations(editing = true)),
          UIEntityPermission(2, "entity B", UIOperations())
        )
      )
      val xivoPermissions = List(
        UIRole(Some(1000), Some("Role A"), List(permXivoFoo)),
        UIRole(Some(2000), Some("Role B"), List(permXivoBar))
      )

      val grouped = aggregateUIRoles(xivoPermissions)
      grouped.size shouldEqual 2
      grouped.contains(permXivoFoo) shouldEqual true
      grouped.contains(permXivoBar) shouldEqual true
    }

    "return related UIEntityPermission combined" in {
      val permXivoFoo1 = UIXivoPermission(
        100, "XiVO foo", List(
          UIEntityPermission(1, "entity A", UIOperations(reading = true)),
          UIEntityPermission(2, "entity B", UIOperations(creating = true))
        )
      )
      val permXivoFoo2 = UIXivoPermission(
        100, "XiVO foo", List(
          UIEntityPermission(1, "entity A", UIOperations(editing = true)),
          UIEntityPermission(2, "entity B", UIOperations())
        )
      )
      val xivoPermissions = List(
        UIRole(Some(1000), Some("Role A"), List(permXivoFoo1)),
        UIRole(Some(2000), Some("Role B"), List(permXivoFoo2))
      )

      aggregateUIRoles(xivoPermissions) shouldEqual List(
        UIXivoPermission(
          100, "XiVO foo", List(
            UIEntityPermission(1, "entity A", UIOperations(reading = true, editing = true)),
            UIEntityPermission(2, "entity B", UIOperations(creating = true))
          )
        )
      )
    }

    "aggregates multiple UIXivoPermission inside single UIXivoPermission [edge case]" in {
      val permXivoFoo1 = UIXivoPermission(
        100, "XiVO foo", List(
          UIEntityPermission(1, "entity A", UIOperations(reading = true)),
          UIEntityPermission(2, "entity B", UIOperations(creating = true))
        )
      )
      val permXivoFoo2 = UIXivoPermission(
        100, "XiVO foo", List(
          UIEntityPermission(1, "entity A", UIOperations(editing = true)),
          UIEntityPermission(2, "entity B", UIOperations())
        )
      )
      val xivoPermissions = List(
        UIRole(Some(1000), Some("Role A"), List(permXivoFoo1, permXivoFoo2))
      )
      aggregateUIRoles(xivoPermissions) shouldEqual List(
        UIXivoPermission(
          100, "XiVO foo", List(
            UIEntityPermission(1, "entity A", UIOperations(reading = true, editing = true)),
            UIEntityPermission(2, "entity B", UIOperations(creating = true))
          )
        )
      )
    }

    "aggregates multiple UIEntityPermission inside single UIXivoPermission [edge case]" in {
      val permXivoFoo1 = UIXivoPermission(
        100, "XiVO foo", List(
          UIEntityPermission(1, "entity A", UIOperations(reading = true)),
          UIEntityPermission(1, "entity A", UIOperations(creating = true))
        )
      )
      val xivoPermissions = List(
        UIRole(Some(1000), Some("Role A"), List(permXivoFoo1))
      )
      aggregateUIRoles(xivoPermissions) shouldEqual List(
        UIXivoPermission(
          100, "XiVO foo", List(
            UIEntityPermission(1, "entity A", UIOperations(reading = true, creating = true))
          )
        )
      )
    }

    "group properly complex sample" in {
      val permXivoFoo1 = UIXivoPermission(
        100, "XiVO foo", List(
          UIEntityPermission(1, "entity A", UIOperations(reading = true)),
          UIEntityPermission(1, "entity A", UIOperations(creating = true)),
          UIEntityPermission(2, "entity B", UIOperations(creating = true))
        )
      )
      val permXivoFoo2 = UIXivoPermission(
        100, "XiVO foo", List(
          UIEntityPermission(1, "entity A", UIOperations(editing = true)),
          UIEntityPermission(2, "entity B", UIOperations())
        )
      )
      val permXivoFoo3 = UIXivoPermission(
        100, "XiVO foo", List(
          UIEntityPermission(2, "entity B", UIOperations(deleting = true))
        )
      )
      val permXivoBar1 = UIXivoPermission(
        200, "XiVO bar", List(
          UIEntityPermission(1, "entity A", UIOperations(editing = true)),
          UIEntityPermission(2, "entity B", UIOperations(reading = true))
        )
      )
      val permXivoBar2 = UIXivoPermission(
        200, "XiVO bar", List(
          UIEntityPermission(1, "entity A", UIOperations(editing = true, creating = true)),
          UIEntityPermission(2, "entity B", UIOperations(deleting = true))
        )
      )
      val xivoPermissions = List(
        UIRole(Some(1000), Some("Role A"), List(permXivoFoo1, permXivoFoo2, permXivoBar1)),
        UIRole(Some(2000), Some("Role B"), List(permXivoFoo3, permXivoBar2))
      )

      val grouped = aggregateUIRoles(xivoPermissions)
      grouped.size shouldEqual 2
      grouped.contains(UIXivoPermission(
        100, "XiVO foo", List(
          UIEntityPermission(1, "entity A", UIOperations(creating = true, reading = true, editing = true)),
          UIEntityPermission(2, "entity B", UIOperations(creating = true, deleting = true))
        )
      )) shouldEqual true
      grouped.contains(UIXivoPermission(
        200, "XiVO bar", List(
          UIEntityPermission(1, "entity A", UIOperations(editing = true, creating = true)),
          UIEntityPermission(2, "entity B", UIOperations(reading = true, deleting = true))
        )
      )) shouldEqual true
    }

  }

}

package model

import org.scalatest.mock.MockitoSugar
import testutils.PlayShouldSpec
import org.mockito.Mockito._

class IncoherentUserSpec extends PlayShouldSpec with MockitoSugar {

  val entity = mock[Entity]
  when(entity.displayName).thenReturn("entityName")
  val xivo = mock[Xivo]
  when(entity.xivo).thenReturn(xivo)
  when(xivo.name).thenReturn("xivoName")
  val u = User(Some(2), entity, "firstname", "lastname", "intNo", Some("\"extNo\""), None, None, None, None)

  "The IncoherentUser" should {
    "be created from a user instance" in {
      val iu = IncoherentUser(u)
      iu shouldEqual (IncoherentUser("firstname", "lastname", "intNo", Some("extNo"), "xivoName", "entityName"))
    }

    "convert to json" in {
      val u2 = u.copy(internalNumber = "newIntNo", firstName="other")
      val csv = IncoherentUser.convertToCsv(List(IncoherentUser(u), IncoherentUser(u2)))
      csv shouldEqual(
        """Firstname|Lastname|internalNumber|externalNumber|XivoName|EntityName
          |"firstname"|"lastname"|"intNo"|"extNo"|"xivoName"|"entityName"
          |"other"|"lastname"|"newIntNo"|"extNo"|"xivoName"|"entityName"
          |""".stripMargin
        )
    }
  }
}

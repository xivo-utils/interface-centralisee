package model

import org.scalatest._
import play.api.data.validation.ValidationError
import play.api.libs.json.{JsError, JsResult, JsSuccess, Json}
import model.Interval.{allDigitsErr, greaterThanStart}
import org.specs2.specification.Scope

import scala.util.Try

class IntervalSpec extends WordSpec with ShouldMatchers {

  "The Interval constructor" should {
    class Helper extends Scope {
      val interval = Interval(Some(200), "2000", "2999", IntervalRoutingModes.Routed, None, "testLabel")
    }

    "verify start and end matches regex " + Interval.IntervalRegex in new Helper {
      val valid = Seq("1", "123456", "0", "000000", "000001")
      valid.foreach { i => interval.copy(start = i, end = i) }

      val invalid = Seq("", " ", "a", "12345_", "-0", "1" * (Interval.IntervalMaxLength + 1))
      invalid.foreach { i =>
        intercept[IllegalArgumentException] {
          interval.copy(start = i)
        }.getMessage should include (Interval.errDoesNotMatch("start", Interval.IntervalRegex))

        intercept[IllegalArgumentException] {
          interval.copy(end = i)
        }.getMessage should include (Interval.errDoesNotMatch("end", Interval.IntervalRegex))
      }
    }

    "prevent start different length then end" in new Helper {
      val invalid = Seq(("1234", "123"), ("123", "1234"), ("123", "0123"), ("0123", "123"))

      invalid.foreach { case (_start, _end) =>
        intercept[IllegalArgumentException] {
          interval.copy(start = _start, end = _end)
        }.getMessage should include (Interval.ErrorStartEndSameLength)
      }
    }

    "allow start to equal end" in new Helper {
      interval.copy(start = "123", end = "123")
    }

    "prevent start greater then end" in new Helper {
      intercept[IllegalArgumentException] {
        interval.copy(start = "123", end = "122")
      }.getMessage should include (Interval.ErrorStartEndOrder)
    }

    "prevent too long mail" in new Helper {
      intercept[IllegalArgumentException] {
        interval.copy(label = "x" * (Interval.LabelMaxLength + 1))
      }.getMessage should include (Interval.errorFieldTooLong("label"))
    }

  }

  "The Interval.writes" should {
    "encode to json" in {
      val i = Interval(Some(200), "2000", "2999", IntervalRoutingModes.Routed, None, "testLabel")
      Json.toJson(i) shouldEqual Json.obj(
        "id" -> 200,
        "start" -> "2000",
        "end" -> "2999",
        "start_ext_num" -> "2000",
        "end_ext_num" -> "2999",
        "routing_mode" -> "routed",
        "label" -> "testLabel")
    }
  }

  "The Interval.reads" should {
    "decode from json with RoutedWithDirectNumber" in {
      val j = Json.obj("id" -> 200, "start" -> "3000", "end" -> "3999", "routing_mode" -> "with_direct_number",
        "direct_number" -> "12345", "label" -> "ggr")
      val i = j.validate[Interval]
      i.get shouldEqual Interval(Some(200), "3000", "3999", IntervalRoutingModes.RoutedWithDirectNumber, Some("12345"), "ggr")
    }

    "decode from json with RoutedWithCustomDirectNumber" in {
      val j = Json.obj("id" -> 200, "start" -> "3000", "end" -> "3999", "routing_mode" -> "with_customized_direct_number",
        "direct_number" -> "12345", "label" -> "ggr")
      val i = j.validate[Interval]
      i.get shouldEqual Interval(Some(200), "3000", "3999", IntervalRoutingModes.RoutedWithCustomDirectNumber, Some("12345"), "ggr")
    }

    "fail to decode from json when start or end are not sequence of digits" in {
      val j = Json.obj("id" -> 200, "start" -> "aaa", "end" -> "bbb", "routing_mode" -> "with_customized_direct_number",
        "direct_number" -> "12345", "label" -> "ggr")
      j.validate[Interval] match {
        case err: JsError => JsError.toFlatForm(err) shouldEqual
          Seq("obj.end" -> Seq(allDigitsErr), "obj.start" -> Seq(allDigitsErr))
        case _ => fail
      }
    }

    "fail to decode from json when end is lower than start" in {
      val j = Json.obj("id" -> 200, "start" -> "200", "end" -> "199", "routing_mode" -> "routed",
        "direct_number" -> "", "label" -> "ggr")
      j.validate[Interval] match {
        case err: JsError => JsError.toFlatForm(err) shouldEqual Seq("obj.end" -> Seq(greaterThanStart))
        case _ => fail
      }
    }

    "fail to decode from json when RoutedWithDirectNumber and direct_number is not not non-empty sequence of digits" in {
      val j = Json.obj("id" -> 200, "start" -> "1000", "end" -> "1999", "routing_mode" -> "with_direct_number",
        "direct_number" -> "aaaa", "label" -> "ggr")
      j.validate[Interval] match {
        case err: JsError => JsError.toFlatForm(err) shouldEqual Seq("obj.direct_number" -> Seq(allDigitsErr))
        case _ => fail
      }
    }

    "fail to decode from json when start or end are too short" in {
      val j = Json.obj("id" -> 200, "start" -> "", "end" -> "", "routing_mode" -> "with_customized_direct_number",
        "direct_number" -> "12345", "label" -> "ggr")
      val errMinLength = ValidationError(List("error.minLength"), 1)
      j.validate[Interval] match {
        case err: JsError => JsError.toFlatForm(err) shouldEqual Seq(
          "obj.end" -> Seq(errMinLength),
          "obj.start" -> Seq(errMinLength)
        )
        case _ => fail
      }
    }

    "fail to decode from json when start, end or direct_number are too long" in {
      def errMaxLength(max: Int) = ValidationError(List("error.maxLength"), max)

      val j = Json.obj("id" -> 200, "start" -> "12345678901", "end" -> "12345678901",
        "routing_mode" -> "with_direct_number", "direct_number" -> "123456789012345678901", "label" -> "ggr")
      j.validate[Interval] match {
        case err: JsError => JsError.toFlatForm(err).toSet shouldEqual Set(
          "obj.end" -> Seq(errMaxLength(Interval.IntervalMaxLength)),
          "obj.start" -> Seq(errMaxLength(Interval.IntervalMaxLength)),
          "obj.direct_number" -> Seq(errMaxLength(Interval.DirectNumberMaxLength))
        )
        case _ => fail
      }
    }

    "decode from json when Routed and direct_number is empty string" in {
      val j = Json.obj("id" -> 150, "start" -> "100", "end" -> "199", "routing_mode" -> "routed",
        "direct_number" -> "", "label" -> "ggr")
      j.validate[Interval] match {
        case JsSuccess(Interval(Some(150), "100", "199", IntervalRoutingModes.Routed, None, "ggr"), _) =>
        case other => fail
      }
    }

    "decode from json when Routed and direct_number is non-empty string" in {
      val j = Json.obj("id" -> 150, "start" -> "100", "end" -> "199", "routing_mode" -> "routed",
        "direct_number" -> "12345", "label" -> "ggr")
      j.validate[Interval] match {
        case JsSuccess(Interval(Some(150), "100", "199", IntervalRoutingModes.Routed, None, "ggr"), _) =>
        case other => fail
      }
    }
  }

  "The Interval.includes" should {
    "return true if the given interval is inside the current interval" in {
      val i = Interval(Some(100), "2000", "2900")
      val j = Interval(Some(101), "2100", "2199")
      i.includes(j) shouldEqual true
    }

    "return false if the given interval overruns the current interval" in {
      val i = Interval(Some(200), "2000", "2900")
      val j = Interval(Some(201), "2800", "2999")
      val k = Interval(Some(202), "1000", "2100")
      val l = Interval(Some(203), "00100", "10000")
      i.includes(j) shouldEqual false
      i.includes(k) shouldEqual false
      i.includes(l) shouldEqual false
    }
  }

  "Interval.extNumInRangeOfIntervalRoutedWithCustDirNum" should {
    import model.Interval.{extNumInRangeOfIntervalRoutedWithCustDirNum => inRange}
    def interval(start: String, end: String, directNumber: String) =
      Interval(Some(123), start, end, IntervalRoutingModes.RoutedWithCustomDirectNumber, Some(directNumber))

    "1035 be in 1000 - 1099 range" in {
      inRange("1035", interval("1000", "1099", "1000")) shouldEqual true
    }

    "999 or 0999 not be in 1000 - 1099 range" in {
      inRange("999", interval("1000", "1099", "1000")) shouldEqual false
      inRange("0999", interval("1000", "1099", "1000")) shouldEqual false
    }

    "1100 not be in 1000 - 1099 range" in {
      inRange("1100", interval("1000", "1099", "1000")) shouldEqual false
    }

    "001035 be in 001000 - 001099 range" in {
      inRange("001035", interval("1000", "1099", "001000")) shouldEqual true
    }

    "01035 not be in 001000 - 001099 range" in {
      inRange("01035", interval("1000", "1099", "001000")) shouldEqual false
    }
  }

}

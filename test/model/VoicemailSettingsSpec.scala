package model

import org.scalatest.{ShouldMatchers, WordSpec}
import model.VoicemailSettings.CustomNumberMaxLength
import xivo.restapi.model.{Line, Voicemail, User => XivoUser}
import play.api.libs.json._

class VoicemailSettingsSpec extends WordSpec with ShouldMatchers {

  "The VoicemailSettings constructor" should {

    "guard that numberMode is non-empty when voicemail is enabled" in {
      intercept[IllegalArgumentException] {
        VoicemailSettings(enabled = true, numberMode = None, customNumber = None, sendEmail = Some(true), None)
      }.getMessage should include ("numberMode")
    }

    "guard that numberMode is empty when voicemail is disabled" in {
      intercept[IllegalArgumentException] {
        VoicemailSettings(enabled = false, numberMode = Some(VoiceMailNumberMode.Custom), customNumber = None, sendEmail = None, None)
      }.getMessage should include ("numberMode")
    }

    "guard that customNumber is non-empty for Custom voice number mode" in {
      intercept[IllegalArgumentException] {
        VoicemailSettings(enabled = true, numberMode = Some(VoiceMailNumberMode.Custom), customNumber = None,
          sendEmail = Some(true), None)
      }.getMessage should include ("customNumber")
    }

    "guard that customNumber is not too long" in {
      intercept[IllegalArgumentException] {
        VoicemailSettings(enabled = true, numberMode = Some(VoiceMailNumberMode.Custom),
          customNumber = Some("1" * (CustomNumberMaxLength + 1)), sendEmail = Some(true), None)
      }.getMessage should include ("customNumber")
    }

    "guard that sendEmail is non-empty when voicemail is enabled" in {
      intercept[IllegalArgumentException] {
        VoicemailSettings(enabled = true, numberMode = Some(VoiceMailNumberMode.ShortNumber), customNumber = None,
          sendEmail = None, None)
      }.getMessage should include ("sendEmail")
    }

    "guard that sendEmail is empty when voicemail is disabled" in {
      intercept[IllegalArgumentException] {
        VoicemailSettings(enabled = false, numberMode = None, customNumber = None, sendEmail = Some(false), None)
      }.getMessage should include ("sendEmail")
    }

    "guard that deleteAfterNotif is non-empty when sendEmail is enabled" in {
      intercept[IllegalArgumentException] {
        VoicemailSettings(enabled = true, numberMode = Some(VoiceMailNumberMode.ShortNumber), customNumber = None,
          sendEmail = Some(true), None)
      }.getMessage should include ("deleteAfterNotif")
    }
  }

  "VoicemailSettings.fromXivoUser" should {

    "return VoicemailSettings with disabled voicemail for XivoUser without voicemail" in {
      val xivoUser = new XivoUser()
      VoicemailSettings.fromXivoUser(xivoUser) shouldEqual VoicemailSettings(
        enabled = false,
        numberMode = None,
        customNumber = None,
        sendEmail = None,
        deleteAfterNotif = None
      )
    }

    "return VoicemailSettings with number mode ShortNumber when user and voicemail has the same number" in {
      val xivoUser = new XivoUser()
      val shortNumber = "1234"
      xivoUser.setLine(new Line(shortNumber))
      val voicemail = new Voicemail
      voicemail.setNumber(shortNumber)
      voicemail.setAttach(true)
      voicemail.setDeleteMessages(true)
      xivoUser.setVoicemail(voicemail)

      VoicemailSettings.fromXivoUser(xivoUser) shouldEqual VoicemailSettings(
        enabled = true,
        numberMode = Some(VoiceMailNumberMode.ShortNumber),
        customNumber = None,
        sendEmail = Some(true),
        deleteAfterNotif = Some(true)
      )
    }

    "return VoicemailSettings with number mode Custom when user and voicemail has different number" in {
      val xivoUser = new XivoUser()
      xivoUser.setLine(new Line("1234"))
      val voicemail = new Voicemail
      voicemail.setNumber("5678")
      voicemail.setAttach(false)
      voicemail.setDeleteMessages(true)
      xivoUser.setVoicemail(voicemail)

      VoicemailSettings.fromXivoUser(xivoUser) shouldEqual VoicemailSettings(
        enabled = true,
        numberMode = Some(VoiceMailNumberMode.Custom),
        customNumber = Some("5678"),
        sendEmail = Some(false),
        deleteAfterNotif = Some(false)
      )
    }

  }

  "VoicemailSettings.jsonReads" should {

    "parse enabled voicemail" in {
      val json = Json.parse("""
        {
          "enabled": true,
          "numberMode": "custom",
          "customNumber": "12345",
          "sendEmail": true,
          "deleteAfterNotif": true
        }
      """)
      VoicemailSettings.jsonReads.reads(json) shouldEqual JsSuccess(VoicemailSettings(
        enabled = true,
        numberMode = Some(VoiceMailNumberMode.Custom),
        customNumber = Some("12345"),
        sendEmail = Some(true),
        deleteAfterNotif = Some(true)
      ))
    }

    "parse enabled voicemail, missing sendEmail and deleteAfterNotif defaulted to false" in {
      val json = Json.parse("""
        {
          "enabled": true,
          "numberMode": "custom",
          "customNumber": "12345"
        }
      """)
      VoicemailSettings.jsonReads.reads(json) shouldEqual JsSuccess(VoicemailSettings(
        enabled = true,
        numberMode = Some(VoiceMailNumberMode.Custom),
        customNumber = Some("12345"),
        sendEmail = Some(false),
        deleteAfterNotif = Some(false)
      ))
    }

    "parse disabled voicemail, all optional fields changed to None" in {
      val json = Json.parse("""
        {
          "enabled": false,
          "numberMode": "custom",
          "customNumber": "12345",
          "sendEmail": true,
          "deleteAfterNotif": true
        }
      """)
      VoicemailSettings.jsonReads.reads(json) shouldEqual JsSuccess(VoicemailSettings.disabled)
    }

  }

}

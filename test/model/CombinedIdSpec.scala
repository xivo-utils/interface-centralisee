package model

import java.util.UUID

import org.scalatest.{ShouldMatchers, WordSpec}
import play.api.libs.json._

class CombinedIdSpec extends WordSpec with ShouldMatchers {

  "The CombinedId" should {
    "decode from json" in {
      val expected: CombinedId = CombinedId(UUID.randomUUID(), "default")
      val json = s""""${expected.toString}""""

      val parsedJson: JsValue = Json.parse(json)

      parsedJson.validate[CombinedId] match {
        case JsSuccess(o, _) => o shouldEqual expected
        case JsError(error) => fail(JsError.toJson(error).toString)
      }
    }

    "encode to json" in {
      val c = CombinedId(UUID.randomUUID(), "contextName")
      Json.toJson(c) shouldEqual JsString(c.toString)
    }

    "extract CombinedId from string" in {
      val uuid = UUID.randomUUID()
      val s = s"name@$uuid"
      val res = CombinedId.extractCombinedId(s)
      res.contextName shouldEqual "name"
      res.xivoUuid shouldEqual uuid
    }
  }
}

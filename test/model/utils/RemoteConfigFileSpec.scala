package model.utils

import testutils.{TestConfig, BeforeAndAfterEach}
import java.io.{FileWriter, File}

class RemoteConfigFileSpec extends BeforeAndAfterEach with TestConfig {

  "The RemoteConfigFile singleton" should {
    "list the configured config files" in {
      val fileContent =
        """# comment
          |my_file /usr/share/my_file asterisk 123
          |your_file /etc/your_file   asterisk:www-data 456
        """.stripMargin
      val xivoConfDir = "/testconf/xivo_conf/"
      val fileMapper = new File("/tmp/file_mapper")
      val fw = new FileWriter(fileMapper)
      fw.write(fileContent)
      fw.flush()
      fw.close()

      RemoteConfigFile.all(fileMapper, xivoConfDir) shouldEqual List(
        RemoteConfigFile(s"$xivoConfDir/my_file", "/usr/share/my_file", "asterisk", "123"),
        RemoteConfigFile(s"$xivoConfDir/your_file", "/etc/your_file", "asterisk:www-data", "456")
      )
    }
  }

}

package model.utils

import play.api.test.PlaySpecification

class CachedFinderSpec extends PlaySpecification {
  "CachedFinder" should {
    "cache repeated searches" in {
      var counter = 0
      def find(key: Long): Option[String] =
        if (key > 0) {
          counter += 1
          Some(key.toString)
        } else {
          None
        }
      
      val cachedFinder = CachedFinder(find)

      cachedFinder(-1) shouldEqual None
      cachedFinder(111) shouldEqual Some("111")
      cachedFinder(111) shouldEqual Some("111")
      counter shouldEqual 1
      
      cachedFinder(5555) shouldEqual Some("5555")
      cachedFinder(5555) shouldEqual Some("5555")
      cachedFinder(5555) shouldEqual Some("5555")
      counter shouldEqual 2
    }
  }
}

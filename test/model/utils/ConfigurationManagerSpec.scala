package model.utils

import java.util.UUID

import model.{Xivo, XivoCreation}
import org.mockito.Mockito.{inOrder, stub, verify, when}
import org.scalatest.mock.MockitoSugar
import play.api.test.{FakeApplication, WithApplication}
import testutils._
import xivo.ldap.xivoconnection.XivoConnector
import xivo.restapi.connection.WebServicesException

class ConfigurationManagerSpec extends BeforeAndAfterEach with TestConfig with MockitoSugar with ConnectorProviderStub {

  override var xivoConnector: XivoConnector = null;
  val file1 = RemoteConfigFile("local_path_1", "remote_path_1", "asterisk", "644")
  val file2 = RemoteConfigFile("local_path_2", "remote_path_2", "asterisk:www-data", "744")
  var configMgr : ConfigurationManager = null
  val config: ConfigParser = mock[ConfigParser]

  "The ConfigurationManager" should {

    val myConfig = testConfig + (("config.directory", "/testconf"))

    "configure the xivo (version 16.08.2 and therefore postgres 9.4)" in new WithApplication(FakeApplication(additionalConfiguration = myConfig)) {
      val localIp = "192.168.10.20"
      val xc = XivoCreation("xivo1", "192.168.10.23", List(), "to_xivo1", true, Some("token"))
      stub(configMgr.ipResolver.getExternalIp(xc.host)).toReturn(localIp)
      stub(configMgr.ssh.getRemoteCommandOutput(xc.host, "cat /usr/share/xivo/XIVO-VERSION")).toReturn(Some("16.08.2\n"))
      when(xivoConnector.getUsersNumber).thenThrow(new WebServicesException("", 403)).thenReturn(0)

      configMgr.configure(xc)

      verify(configMgr.ssh).copyFile(xc.host, file1)
      verify(configMgr.ssh).executeRemoteCommand(xc.host, "mkdir -p /usr/share/asterisk/agi-bin")
      verify(configMgr.ssh).copyFile(xc.host, file1)
      verify(configMgr.ssh).copyFile(xc.host, file2)
      verify(configMgr.ssh).executeRemoteCommand(xc.host, "echo listen_addresses=\\'*\\' >> /etc/postgresql/9.4/main/postgresql.conf")
      verify(configMgr.ssh).executeRemoteCommand(xc.host, s"echo host asterisk asterisk $localIp/32 md5 >> /etc/postgresql/9.4/main/pg_hba.conf")
      verify(configMgr.ssh).executeRemoteCommand(xc.host, "/usr/bin/xivo-service restart all")

      val order = inOrder(xivoConnector)
      order.verify(xivoConnector).getUsersNumber
      order.verify(xivoConnector).createWebServicesUserWithAcl(xc.name, "", localIp, "{}")
      order.verify(xivoConnector).getUsersNumber
    }

    "configure the xivo (version Freya.05 and therefore postgres 11 in docker)" in new WithApplication(FakeApplication(additionalConfiguration = myConfig)) {
      val localIp = "192.168.10.20"
      val xc = XivoCreation("xivo1", "192.168.10.23", List(), "to_xivo1", true, Some("token"))
      stub(configMgr.ipResolver.getExternalIp(xc.host)).toReturn(localIp)
      stub(configMgr.ssh.getRemoteCommandOutput(xc.host, "cat /usr/share/xivo/XIVO-VERSION")).toReturn(Some("2020.18.05\n"))
      when(xivoConnector.getUsersNumber).thenThrow(new WebServicesException("", 403)).thenReturn(0)

      configMgr.configure(xc)

      verify(configMgr.ssh).copyFile(xc.host, file1)
      verify(configMgr.ssh).executeRemoteCommand(xc.host, "mkdir -p /usr/share/asterisk/agi-bin")
      verify(configMgr.ssh).copyFile(xc.host, file1)
      verify(configMgr.ssh).copyFile(xc.host, file2)
      verify(configMgr.ssh).executeRemoteCommand(xc.host, s"echo host asterisk asterisk $localIp/32 md5 >> /var/lib/postgresql/11/main/pg_hba.conf")
      verify(configMgr.ssh).executeRemoteCommand(xc.host, "bash --login -c '/usr/bin/xivo-service restart all'")

      val order = inOrder(xivoConnector)
      order.verify(xivoConnector).getUsersNumber
      order.verify(xivoConnector).createWebServicesUserWithAcl(xc.name, "", localIp, "{}")
      order.verify(xivoConnector).getUsersNumber
    }

    "copy the configuration files to the xivo" in new WithApplication(FakeApplication(additionalConfiguration = myConfig)) {
      val xivo = Xivo(Some(1), UUID.randomUUID(), "my-xivo", "192.168.3.2", "to_my_xivo", None, Some("token"))

      configMgr.synchronizeFiles(xivo)

      verify(configMgr.ssh).copyFile(xivo.host, file1)
      verify(configMgr.ssh).copyFile(xivo.host, file2)
    }

    "createWebServicesAccess without acl for xivo before 16.01" in new WithApplication(FakeApplication(additionalConfiguration = myConfig)) {
      val xc = XivoCreation("xivo15", "192.168.10.16", List(), "to_xivo15", true, Some("token"))
      stub(configMgr.ssh.getRemoteCommandOutput(xc.host, "cat /usr/share/xivo/XIVO-VERSION")).toReturn(Some("15.19\n"))
      stub(configMgr.ipResolver.getExternalIp(xc.host)).toReturn("localIp")
      configMgr.createWebServicesAccess(xivoConnector, xc)
      verify(xivoConnector).createWebServicesUser(xc.name, "", "localIp")
    }

    "createWebServicesAccess with empty acl for xivo 16.01" in new WithApplication(FakeApplication(additionalConfiguration = myConfig)) {
      val acl = "{}"
      val xc = XivoCreation("xivo16", "192.168.10.16", List(), "to_xivo16", true, Some("token"))
      stub(configMgr.ssh.getRemoteCommandOutput(xc.host, "cat /usr/share/xivo/XIVO-VERSION")).toReturn(Some("16.01\n"))
      stub(configMgr.ipResolver.getExternalIp(xc.host)).toReturn("localIp")
      configMgr.createWebServicesAccess(xivoConnector, xc)
      verify(xivoConnector).createWebServicesUserWithAcl(xc.name, "", "localIp", acl)
    }

    "retrieve play auth token" in new WithApplication(FakeApplication(additionalConfiguration = myConfig)) {
      val acl = "{}"
      val xc = XivoCreation("xivo16", "192.168.10.16", List(), "to_xivo16", true, Some("token"))

      stub(config.defaultPlayAuthToken).toReturn("default-token")
      stub(configMgr.ssh.getRemoteCommandOutput(xc.host, s"cat /etc/docker/xivo/custom.env | grep -oP -m 1 \'PLAY_AUTH_TOKEN=\\K.*\'")).toReturn(Some("token"))
      configMgr.getPlayAuthToken(xc.host) shouldEqual "token"

      stub(configMgr.ssh.getRemoteCommandOutput(xc.host, s"cat /etc/docker/xivo/custom.env | grep -oP -m 1 \'PLAY_AUTH_TOKEN=\\K.*\'")).toReturn(None)
      configMgr.getPlayAuthToken(xc.host) shouldEqual "default-token"
    }

    "get postgresql version depending on the xivo version (version 15.20) (version >=15.20 => postgresql 9.4)" in new WithApplication(FakeApplication(additionalConfiguration = myConfig)) {
      val xc = XivoCreation("xivo16", "192.168.10.16", List(), "to_xivo16", true, Some("token"))
      stub(configMgr.ssh.getRemoteCommandOutput(xc.host, "cat /usr/share/xivo/XIVO-VERSION")).toReturn(Some("15.20\n"))
      configMgr.getPostgresqlVersion(xc.host) shouldEqual Some("9.4")
    }

    "get postgresql version depending on the xivo version (version 16.08.2) (version >=15.20 => postgresql 9.4)" in new WithApplication(FakeApplication(additionalConfiguration = myConfig)) {
      val xc = XivoCreation("xivo16", "192.168.10.16", List(), "to_xivo16", true, Some("token"))
      stub(configMgr.ssh.getRemoteCommandOutput(xc.host, "cat /usr/share/xivo/XIVO-VERSION")).toReturn(Some("16.08.2\n"))
      configMgr.getPostgresqlVersion(xc.host) shouldEqual Some("9.4")
    }

    "get postgresql version depending on the xivo version (version 2019.05.00) (version >=2019.05.00 => postgresql 11)" in new WithApplication(FakeApplication(additionalConfiguration = myConfig)) {
      val xc = XivoCreation("xivo16", "192.168.10.16", List(), "to_xivo16", true, Some("token"))
      stub(configMgr.ssh.getRemoteCommandOutput(xc.host, "cat /usr/share/xivo/XIVO-VERSION")).toReturn(Some("2019.05.00\n"))
      configMgr.getPostgresqlVersion(xc.host) shouldEqual Some("11")
    }

    "get postgresql version depending on the xivo version (version 2020.18.05) (version >=2019.05.00 => postgresql 11)" in new WithApplication(FakeApplication(additionalConfiguration = myConfig)) {
      val xc = XivoCreation("xivo16", "192.168.10.16", List(), "to_xivo16", true, Some("token"))
      stub(configMgr.ssh.getRemoteCommandOutput(xc.host, "cat /usr/share/xivo/XIVO-VERSION")).toReturn(Some("2020.18.05\n"))
      configMgr.getPostgresqlVersion(xc.host) shouldEqual Some("11")
    }


  }

  override protected def beforeEach(): Unit = {
    val configParser = mock[ConfigParser]
    val connectorPool = new XivoConnectorPool(configParser) with TestXivoConnectorProvider
    configMgr = new ConfigurationManager(List(file1, file2), connectorPool, config)
    configMgr.ipResolver = mock[NetworkUtils]
    configMgr.ssh = mock[SshManager]
    configMgr.connectorPool = connectorPool
    xivoConnector = mock[XivoConnector]
  }
}

package model.utils

import org.mockito.Mockito.{inOrder, never, stub, verify}
import org.scalatest.mock.MockitoSugar
import play.api.test.PlaySpecification

class AutoRollbackTransactionSpec extends PlaySpecification with MockitoSugar {

  "A Transaction" should {
    "execute each step in the given order" in {
      val step1 = mock[Step]
      val step2 = mock[Step]
      val step3 = mock[Step]

      new AutoRollbackTransaction(
        List(step1, step2, step3)
      ).execute()

      val order = inOrder(step1, step2, step3)
      order.verify(step1).execute
      order.verify(step2).execute
      order.verify(step3).execute
      success
    }

    "rollback the steps executed before the exception is thrown and re-throw the exception" in {
      val step1 = mock[Step]
      val step2 = mock[Step]
      val step3 = mock[Step]
      val step4 = mock[Step]
      stub(step3.execute).toThrow(new RuntimeException("test exception"))

      new AutoRollbackTransaction(
        List(step1, step2, step3, step4)
      ).execute() should throwA[RuntimeException]("test exception")

      val order = inOrder(step1, step2, step3, step4)
      order.verify(step1).execute
      order.verify(step2).execute
      order.verify(step3).execute
      order.verify(step2).rollback
      order.verify(step1).rollback
      verify(step4, never()).execute
      verify(step3, never()).rollback
      success
    }

    "add single step" in {
      val step = mock[Step]
      val transaction = new AutoRollbackTransaction()
      transaction.addStep(step)
      transaction.execute()
      verify(step).execute
      success
    }

    "add multiple step" in {
      val step1 = mock[Step]
      val step2 = mock[Step]
      val transaction = new AutoRollbackTransaction()
      transaction.addSteps(Seq(step1, step2))
      transaction.execute()
      val order = inOrder(step1, step2)
      order.verify(step1).execute
      order.verify(step2).execute
      success
    }
  }
}

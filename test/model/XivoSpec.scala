package model

import java.security.InvalidParameterException
import java.util.UUID

import org.mockito.Mockito._
import org.mockito.Matchers
import org.scalatest.mock.MockitoSugar
import play.api.libs.json.Json
import model.utils.{XivoConnectorPool, _}
import org.scalatest.TestData
import org.scalatestplus.play.OneAppPerTest
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.inject._
import services.cache.CacheScheduler
import services.{XivoDb, XivoManager}
import xivo.ldap.xivoconnection.XivoConnector
import xivo.restapi.connection.WebServicesException
import xivo.restapi.model.{Context, Info}

import scala.collection.JavaConversions._
import testutils._

import scala.util.Success

class XivoSpec extends PlayShouldSpec with DbTest with MockitoSugar  with OneAppPerTest {

  override def newAppForTest(testData: TestData) = new GuiceApplicationBuilder()
    .configure(testConfig)
    .overrides(bind[XivoManager].toInstance(mock[XivoManager]))
    .overrides(bind[CacheScheduler].toInstance(mock[CacheScheduler]))
    .build()

  val uuid = UUID.fromString("e99297fe-fa84-4cd0-a893-d68b8d907cc9")
  val uuid2 = UUID.fromString("2bdfcdd3-57d2-4d7a-ba85-9cafede25008")

  class Helper {
    cleanTable("xivo")
    val xivoConnector = mock[XivoConnector]
    val xivoConnector2 = mock[XivoConnector]
    val configManager = mock[ConfigurationManager]

    val xivoManager = app.injector.instanceOf(classOf[XivoManager])
    val xivoInterface = app.injector.instanceOf(classOf[XivoInterface])
    val cacheScheduler = app.injector.instanceOf(classOf[CacheScheduler])
    val configParser = mock[ConfigParser]

    trait TestXivoConnectorProvider extends XivoConnectorProvider {
      override def get(host: String, incallContext: String, token: String): XivoConnector = xivoConnector
    }
    def newConnectorPool(): XivoConnectorPool = new XivoConnectorPool(configParser) with TestXivoConnectorProvider

    val connectorPool = newConnectorPool()

    xivoInterface.connectorPool = connectorPool
    xivoInterface.configManagerFactory = mock[ConfigurationManagerFactory]
    stub(xivoInterface.configManagerFactory.get(Matchers.any[XivoConnectorPool], Matchers.any[ConfigParser])).toReturn(configManager)
    stub(xivoConnector.getUsersNumber).toReturn(100)
  }

  class LocalXivoConnectorProvider(get: (String, String) => XivoConnector) extends XivoConnectorProvider


  class HelperWithTwoXivos(configureXivo: Boolean) extends Helper {
    val x1 = Xivo(Some(100), uuid, "xivo1", "192.168.3.3", "to_xivo1", None, Some("token"))
    val x2 = Xivo(Some(101), uuid2, "xivo2", "192.168.3.4", "to_xivo2", None, Some("token"))
    val uuidxc = UUID.fromString("09116f3b-70f7-4639-a96b-23675cec06c3")
    val xc = XivoCreation("xivo3", "192.168.5.3", List(x2.id.get), "to_xivo3", configure = configureXivo, Some("token"))
    val xivoDb = XivoDb(None, uuidxc, xc.name, xc.host, "to_xivo3", Some("token"))

    override def newConnectorPool() = new XivoConnectorPool(configParser) with XivoConnectorProvider {
      override def get(host: String, incallContext: String, token: String): XivoConnector = host match {
        case xc.host => xivoConnector
        case x2.host => xivoConnector2
        case _ => fail(s"XivoConnectorProvider.get unexpected host=$host")
      }
    }
  }

  "The Xivo singleton" should {
    "get a xivo by id" in new Helper {
      val xivo = insertXivo(Xivo(None, UUID.randomUUID(), "xivo1", "192.168.2.3", "xivo1-trunk", Some(xivoCapacity), Some("token")))
      xivoInterface.connectorPool = newConnectorPool()
      stub(xivoManager.get(xivo.id.get)).toReturn(Success(XivoDb.fromXivo(xivo)))

      xivoInterface.getXivo(xivo.id.get) shouldEqual Some(xivo)
    }

    "get a xivo by uuid" in new Helper {
      val xivo = insertXivo(Xivo(None, uuid, "xivo1", "192.168.2.3", "xivo1-trunk", Some(xivoCapacity), Some("token")))
      xivoInterface.connectorPool = newConnectorPool()
      stub(xivoManager.get(uuid)).toReturn(Success(XivoDb.fromXivo(xivo)))

      xivoInterface.getXivo(uuid) shouldEqual Some(xivo)
    }

    "encode a Xivo in JSON" in new Helper {
      val xivo = Xivo(Some(2), uuid, "xivo1", "192.168.2.3", "to_xivo1", Some(512), Some("token"))

      Json.toJson(xivo) shouldEqual Json.obj(
      "id" -> 2,
      "uuid" -> uuid.toString,
      "name" -> "xivo1",
      "host" -> "192.168.2.3",
      "contextName" -> "to_xivo1",
      "remainingSlots" -> 512,
      "isProxy" -> false)
    }

    "encode a Proxy in JSON" in new Helper {
      val xivo = Xivo(Some(2), uuid, "proxy1", "192.168.2.3", "to_proxy1", Some(512), Some("token"), isProxy = true)

      Json.toJson(xivo) shouldEqual Json.obj(
        "id" -> 2,
        "uuid" -> uuid.toString,
        "name" -> "proxy1",
        "host" -> "192.168.2.3",
        "contextName" -> "to_proxy1",
        "remainingSlots" -> 512,
        "isProxy" -> true)
    }

    "list the Xivos" in new Helper() {
      val xivo1 = insertXivo(Xivo(None, UUID.fromString("37af25bd-bb98-4cbd-bc2a-7530b89c2907"), "xivo1", "192.168.2.3", "xivo1-trunk", Some(xivoCapacity), Some("token")))
      val xivo2 = insertXivo(Xivo(None, UUID.fromString("59298e06-4e6f-4353-9f88-834cc9bd999a"), "xivo2", "192.168.2.4", "xivo1-trunk", Some(xivoCapacity), Some("token")))
      stub(xivoManager.all()).toReturn(Success(List(XivoDb.fromXivo(xivo1), XivoDb.fromXivo(xivo2))))

      xivoInterface.list shouldEqual List(xivo1, xivo2)
    }

    "list the Xivos with extended information" in new Helper() {
      val xivo1 = insertXivo(Xivo(None, UUID.fromString("37af25bd-bb98-4cbd-bc2a-7530b89c2907"), "xivo1", "192.168.2.3", "xivo1-trunk", Some(xivoCapacity), Some("token")))
      val xivo2 = insertXivo(Xivo(None, UUID.fromString("59298e06-4e6f-4353-9f88-834cc9bd999a"), "xivo2", "192.168.2.4", "xivo1-trunk", Some(xivoCapacity), Some("token")))
      stub(xivoConnector.getXivoInfo()).toReturn(new Info(xivo1.uuid.toString)).toThrow(new Error("Some exception message"))
      stub(xivoManager.all()).toReturn(Success(List(XivoDb.fromXivo(xivo1), XivoDb.fromXivo(xivo2))))

      xivoInterface.listExtended shouldEqual List(XivoExtended(xivo1, isReachable = true), XivoExtended(xivo2, isReachable = false))
    }

    "list the xivo and proxies with extended information" in new Helper() {
      val xivo1 = insertXivo(Xivo(None, UUID.fromString("37af25bd-bb98-4cbd-bc2a-7530b89c2907"), "xivo1", "192.168.2.3", "xivo1-trunk", Some(xivoCapacity), Some("token")))
      val proxy1 = insertXivo(Xivo(None, UUID.fromString("59298e06-4e6f-4353-9f88-834cc9bd999a"), "proxy1", "192.168.2.4", "proxy1-trunk", Some(xivoCapacity), Some("token"), isProxy = true))
      stub(xivoConnector.getXivoInfo()).toReturn(new Info(xivo1.uuid.toString)).toThrow(new Error("Some exception message"))
      stub(xivoManager.all()).toReturn(Success(List(XivoDb.fromXivo(xivo1), XivoDb.fromXivo(proxy1))))

      xivoInterface.listExtended shouldEqual List(XivoExtended(xivo1, isReachable = true), XivoExtended(proxy1, isReachable = true))
    }

    "create a xivo" in new Helper() {
      val uuid2 = UUID.randomUUID()
      val x1 = Xivo(Some(100), uuid, "xivo1", "192.168.3.3", "to_xivo1", None, Some("token"))
      val x2 = Xivo(Some(101), uuid2, "xivo2", "192.168.3.4", "to_xivo2", None, Some("token"))
      val uuidxc = UUID.randomUUID()
      val xc = XivoCreation("xivo3", "192.168.5.3", List(x2.id.get), "to_xivo3", true, Some("token"))
      val xivoDb = XivoDb(None, uuidxc, xc.name, xc.host, "to_xivo3", Some("token"))

      trait LocalXivoConnectorProvider extends XivoConnectorProvider {
        override def get(host: String, incallContext: String, token: String): XivoConnector = host match {
          case xc.host => xivoConnector
          case x2.host => xivoConnector2
          case _ => throw new InvalidParameterException("Hôte inattendu")
        }
      }
      xivoInterface.connectorPool = new XivoConnectorPool(configParser) with LocalXivoConnectorProvider
      val routingContext = new Context("routage", List())

      stub(xivoConnector.getXivoInfo).toReturn(new Info(uuidxc.toString))
      when(xivoConnector.getUsersNumber).thenThrow(new WebServicesException("", 403)).thenReturn(3)
      stub(xivoManager.get(x1.id.get)).toReturn(Success(XivoDb.fromXivo(x1)))
      stub(xivoManager.get(x2.id.get)).toReturn(Success(XivoDb.fromXivo(x2)))
      val newXivoId = 55L
      stub(xivoManager.create(xivoDb)).toReturn(Success(xivoDb.copy(id=Some(newXivoId))))
      stub(configManager.configure(Matchers.any[model.XivoCreation])).toReturn(List())
      stub(configManager.getPlayAuthToken(Matchers.any[String])).toReturn("token")

      val result = xivoInterface.create(xc)

      result.result.id.isDefined shouldBe true
      result.result.copy(id=None) shouldEqual Xivo(None, uuidxc, "xivo3", "192.168.5.3", "to_xivo3", None, Some("token"))
      result.notices shouldEqual List()
      verify(xivoManager).create(xivoDb)

      val expectedC2 = new Context(s"to_xivo2", List())
      val expectedC3 = new Context(s"to_xivo3", List())
      val order = org.mockito.Mockito.inOrder(xivoConnector, configManager)
      order.verify(configManager).configure(xc)
      order.verify(xivoConnector).createContext(expectedC2)
      order.verify(xivoConnector).createContext(routingContext)
      order.verify(xivoConnector).createContextInclusion("default", "routage", 255)
      order.verify(xivoConnector).deleteContextInclusion("default", "to-extern")
      verify(xivoConnector2).createContext(expectedC3)
      verify(cacheScheduler).startCachingXivo(newXivoId)
    }

    "create a xivo without configuration" in new HelperWithTwoXivos(configureXivo = false) {
      xivoInterface.connectorPool = newConnectorPool()
      stub(xivoConnector.getXivoInfo).toReturn(new Info(uuidxc.toString))
      stub(xivoManager.get(x1.id.get)).toReturn(Success(XivoDb.fromXivo(x1)))
      stub(xivoManager.get(x2.id.get)).toReturn(Success(XivoDb.fromXivo(x2)))
      private val newXivoId = 55L
      stub(xivoManager.create(xivoDb)).toReturn(Success(xivoDb.copy(id=Some(newXivoId))))
      stub(configManager.configure(Matchers.any[model.XivoCreation])).toReturn(List())
      stub(configManager.getPlayAuthToken(Matchers.any[String])).toReturn("token")

      val result = xivoInterface.create(xc)

      result.result.id.isDefined shouldBe true
      result.result.copy(id=None) shouldEqual Xivo(None, uuidxc, "xivo3", "192.168.5.3", "to_xivo3", None, Some("token"))
      result.notices shouldEqual List()
      verify(xivoManager).create(xivoDb)

      verify(configManager, never()).configure(xc)
      verify(cacheScheduler).startCachingXivo(newXivoId)
    }

    "return a notification if a context already exist" in new Helper {
      val x1 = Xivo(Some(100), UUID.randomUUID(), "xivo1", "192.168.3.3", "to_xivo1", None, Some("token"))
      val xc = XivoCreation("xivo3", "192.168.5.3", List(x1.id.get), "to_xivo3", true, Some("token"))
      val xivoDb = XivoDb(None, uuid, xc.name, xc.host, "to_xivo3", Some("token"))

      override def newConnectorPool() = new XivoConnectorPool(configParser) with XivoConnectorProvider {
        override def get(host: String, incallContext: String, token: String): XivoConnector = host match {
          case xc.host => xivoConnector
          case x1.host => xivoConnector2
          case _ => fail(s"XivoConnectorProvider.get unexpected host=$host")
        }
      }

      xivoInterface.connectorPool = newConnectorPool()
      val routingContext = new Context("routage", List())
      stub(xivoConnector.contextExists("routage")).toReturn(true)
      stub(xivoConnector.contextExists("to_xivo1")).toReturn(true)
      stub(xivoConnector.getXivoInfo).toReturn(new Info(uuid.toString))
      stub(configManager.configure(xc)).toReturn(Nil)
      stub(xivoManager.get(x1.id.get)).toReturn(Success(XivoDb.fromXivo(x1)))
      private val newXivoId = 55L
      stub(xivoManager.create(xivoDb)).toReturn(Success(xivoDb.copy(id=Some(newXivoId))))
      stub(configManager.configure(Matchers.any[model.XivoCreation])).toReturn(List())
      stub(configManager.getPlayAuthToken(Matchers.any[String])).toReturn("token")

      when(xivoConnector.getUsersNumber).thenThrow(new WebServicesException("", 403)).thenReturn(3)

      val result = xivoInterface.create(xc)

      result.result.id.isDefined shouldBe true
      result.result.copy(id=None) shouldEqual Xivo(None, uuid, "xivo3", "192.168.5.3", "to_xivo3", None, Some("token"))
      result.notices shouldEqual List(
        xivoInterface.NoticeContextExists.format("to_xivo1"),
        xivoInterface.NoticeContextExists.format("routage"))
      verify(xivoManager).create(xivoDb)

      val expectedC3 = new Context(s"to_xivo3", List())
      val order = org.mockito.Mockito.inOrder(xivoConnector, configManager)
      order.verify(configManager).configure(xc)
      order.verify(xivoConnector).contextExists("to_xivo1")
      order.verify(xivoConnector).deleteContextInclusion("default", "to-extern")
      verify(xivoConnector2).createContext(expectedC3)
      verify(xivoConnector, never()).createTrunk(Matchers.anyObject())
      verify(cacheScheduler).startCachingXivo(newXivoId)
    }

    "return a notification if xivo configuration return some error" in new HelperWithTwoXivos(configureXivo = true) {
      xivoInterface.connectorPool = newConnectorPool()
      val routingContext = new Context("routage", List())

      stub(xivoConnector.getXivoInfo).toReturn(new Info(uuidxc.toString))
      when(xivoConnector.getUsersNumber).thenThrow(new WebServicesException("", 403)).thenReturn(3)
      stub(xivoManager.get(x1.id.get)).toReturn(Success(XivoDb.fromXivo(x1)))
      stub(xivoManager.get(x2.id.get)).toReturn(Success(XivoDb.fromXivo(x2)))
      val newXivoId = 55L
      stub(xivoManager.create(xivoDb)).toReturn(Success(xivoDb.copy(id=Some(newXivoId))))
      stub(configManager.configure(Matchers.any[model.XivoCreation])).toReturn(List("some error"))
      stub(configManager.getPlayAuthToken(Matchers.any[String])).toReturn("token")

      val result = xivoInterface.create(xc)

      result.result.id.isDefined shouldBe true
      result.result.copy(id=None) shouldEqual Xivo(None, uuidxc, "xivo3", "192.168.5.3", "to_xivo3", None, Some("token"))
      result.notices shouldEqual List("some error")
      verify(xivoManager).create(xivoDb)

      val expectedC2 = new Context(s"to_xivo2", List())
      val expectedC3 = new Context(s"to_xivo3", List())
      val order = org.mockito.Mockito.inOrder(xivoConnector, configManager)
      order.verify(configManager).configure(xc)
      order.verify(xivoConnector).createContext(expectedC2)
      order.verify(xivoConnector).createContext(routingContext)
      order.verify(xivoConnector).createContextInclusion("default", "routage", 255)
      order.verify(xivoConnector).deleteContextInclusion("default", "to-extern")
      verify(xivoConnector2).createContext(expectedC3)
      verify(cacheScheduler).startCachingXivo(newXivoId)
    }

    "synchronize config files for all xivo" in new Helper {
      val x1 = Xivo(Some(100), UUID.randomUUID(), "test", "192.168.1.1", "xivo1-trunk", Some(xivoCapacity), Some("token"))
      val x2 = Xivo(Some(101), UUID.randomUUID(), "test2", "192.168.1.2", "xivo1-trunk", Some(xivoCapacity), Some("token"))
      stub(xivoManager.all()).toReturn(Success(List(XivoDb.fromXivo(x1), XivoDb.fromXivo(x2))))

      xivoInterface.synchronizeConfigFiles()

      verify(configManager).synchronizeFiles(x1)
      verify(configManager).synchronizeFiles(x2)
    }
  }

  "The Xivo class" should {
    "tell 2 xivo are equal even with different remainingSlots" in {
      Xivo(Some(1), uuid, "test", "192.168.1.1", "to_test", Some(2), Some("token")) shouldEqual
        Xivo(Some(1), uuid, "test", "192.168.1.1", "to_test", None, Some("token"))
    }
  }

}

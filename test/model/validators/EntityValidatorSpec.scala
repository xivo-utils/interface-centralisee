package model.validators

import java.security.InvalidParameterException
import java.util.UUID

import model._
import model.utils.{ConfigParser, GlobalXivoConnectorPool, XivoConnectorPool, XivoConnectorProvider}
import org.mockito.Mockito.stub
import org.scalatest.mock.MockitoSugar
import org.specs2.specification.Scope
import play.api.libs.json.Json
import testutils._
import xivo.ldap.xivoconnection.XivoConnector

class EntityValidatorSpec extends BeforeAndAfterEach with MockitoSugar with ConnectorProviderStub {

  val xivo = Xivo(Some(1), UUID.randomUUID(), "xivo1", "192.168.1.123", "to_xivo1", None, Some("token"))
  val xivo2 = Xivo(Some(2), UUID.randomUUID(), "xivo2", "192.168.1.124", "to_xivo2", None, Some("token"))
  override var xivoConnector: XivoConnector = _

  class Helper extends Scope {
    val configParser = mock[ConfigParser]
    xivoConnector = mock[XivoConnector]
    val connectorPool = new GlobalXivoConnectorPool(configParser) {
      override def get(host: String, incallContext: String, token: String): XivoConnector = xivoConnector
    }
    val xivoManager = mock[XivoInterface]
    val entities = mock[EntityInterface]
    val entityValidator = new EntityValidator(entities, xivoManager, configParser, connectorPool)
  }

  "The EntityValidator.fromJson" should {

    "decode an entity from JSON" in new Helper() {
      val json = Json.obj(
        "id" -> 3,
        "name" -> "avranches",
        "displayName" -> "Avranches",
        "xivoId" -> xivo.id,
        "intervals" -> List(
          Json.obj("start" -> "2000", "end" -> "2099", "routing_mode" -> "routed", "label" -> "test"),
          Json.obj("start" -> "2210", "end" -> "2219", "routing_mode" -> "routed", "label" -> "test")),
        "presentedNumber" -> "52000",
        "mdsName" -> "default")
      stub(xivoManager.getXivo(xivo.id.get)).toReturn(Some(xivo))
      entityValidator.fromJson(json) shouldEqual Entity(Some(3L), CombinedId(xivo.uuid, "avranches"), "avranches", "Avranches", xivo,
        List(
          Interval(None, "2000", "2099", IntervalRoutingModes.Routed, None, "test" ),
          Interval(None, "2210", "2219", IntervalRoutingModes.Routed, None, "test")),
        "52000", "default")
    }

    "throw an exception if fields are missing" in new Helper() {
      val json = Json.obj(
        "id" -> 3,
        "name" -> "avranches",
        "displayName" -> "Avranches",
        "xivoId" -> xivo.id)
      entityValidator.fromJson(json) should throwA[InvalidParameterException](EntityValidator.ErrorRequiredFields("name", "displayName", "xivoId", "intervals", "presentedNumber", "mdsName"))
    }
  }

  "The EntityValidator.validateCreation" should {

    "check there is at least one interval" in new Helper() {
      stub(entities.listFromDb).toReturn(List())

      entityValidator.validateCreation(Entity(None, mock[CombinedId], "avranches", "Avranches", xivo, List(Interval(None, "1000", "1009")), "568741", "default"))
      entityValidator.validateCreation(Entity(None, mock[CombinedId], "avranches", "Avranches", xivo, List(), "568741", "default")) should throwA[IllegalArgumentException](EntityValidator.ErrorNoInterval)
    }

    "check the provided intervals do not overlap with each other" in new Helper() {
      stub(configParser.allowIntervalOverlap).toReturn(false)
      stub(entities.listFromDb).toReturn(List())

      entityValidator.validateCreation(Entity(None, mock[CombinedId], "avranches", "Avranches", xivo, List(Interval(None, "1000", "1009"), Interval(None, "1020", "1029")), "568741", "default"))
      entityValidator.validateCreation(Entity(None, mock[CombinedId], "avranches", "Avranches", xivo, List(Interval(None, "1000", "1009"), Interval(None, "1005", "1029")), "568741", "default"))should
        throwA[InvalidParameterException](EntityValidator.ErrorIntervalOverlap)
    }

    "allow intervals overlap if allowed in the config" in new Helper() {
      stub(configParser.allowIntervalOverlap).toReturn(true)
      stub(entities.listFromDb).toReturn(List())

      entityValidator.validateCreation(Entity(None, mock[CombinedId], "avranches", "Avranches", xivo, List(Interval(None, "1000", "1009"), Interval(None, "1020", "1029")), "568741", "default"))
      entityValidator.validateCreation(Entity(None, mock[CombinedId], "avranches", "Avranches", xivo, List(Interval(None, "1000", "1009"), Interval(None, "1005", "1029")), "568741", "default"))

    }

    "check the number intervals of a new entity do not overlap with the existing ones" in new Helper() {
      stub(configParser.allowIntervalOverlap).toReturn(false)
      stub(entities.listFromDb).toReturn(List(Entity(None, mock[CombinedId], "avranches", "Avranches", xivo, List(Interval(None, "2000", "2099")), "52000", "default"),
        Entity(None, mock[CombinedId], "pontorson", "Pontorson", xivo, List(Interval(None, "03000", "03099")), "52001", "default")))

      entityValidator.validateCreation(Entity(None, mock[CombinedId], "cherbourg", "Cherbourg", xivo, List(Interval(None, "1000", "1099"), Interval(None, "2050", "3049")), "52000", "default")) should
        throwA[InvalidParameterException](EntityValidator.ErrorIntervalOverlap)
      entityValidator.validateCreation(Entity(None, mock[CombinedId], "cherbourg", "Cherbourg", xivo, List(Interval(None, "1000", "1099"), Interval(None, "1050", "2049")), "52000", "default")) should
        throwA[InvalidParameterException](EntityValidator.ErrorIntervalOverlap)
      entityValidator.validateCreation(Entity(None, mock[CombinedId], "cherbourg", "Cherbourg", xivo, List(Interval(None, "1000", "1099"), Interval(None, "1050", "3049")), "52000", "default")) should
        throwA[InvalidParameterException](EntityValidator.ErrorIntervalOverlap)
      entityValidator.validateCreation(Entity(None, mock[CombinedId], "cherbourg", "Cherbourg", xivo, List(Interval(None, "1000", "1099"), Interval(None, "3000", "3099")), "52000", "default"))
      success
    }

    "check that the entity name consists only of alphanumeric small characters and is not greater than 30 characters" in new Helper() {
      stub(entities.listFromDb).toReturn(List())

      entityValidator.validateCreation(Entity(None, mock[CombinedId], "pont orson", "Pontorson", xivo, List(Interval(None, "2000", "2099")), "52000", "default")) should throwA[IllegalArgumentException](EntityValidator.ErrorNameRegexp)
      entityValidator.validateCreation(Entity(None, mock[CombinedId], "Pontorson", "Pontorson", xivo, List(Interval(None, "2000", "2099")), "52000", "default")) should throwA[IllegalArgumentException](EntityValidator.ErrorNameRegexp)
      entityValidator.validateCreation(Entity(None, mock[CombinedId], "pontôrson", "Pontorson", xivo, List(Interval(None, "2000", "2099")), "52000", "default")) should throwA[IllegalArgumentException](EntityValidator.ErrorNameRegexp)
      entityValidator.validateCreation(Entity(None, mock[CombinedId], "pontorsonpontorsonpontorsonpontorson", "Pontorson", xivo, List(Interval(None, "2000", "2099")), "52000", "default")) should throwA[IllegalArgumentException](EntityValidator.ErrorNameRegexp)
      entityValidator.validateCreation(Entity(None, mock[CombinedId], "pontorson09", "Pontorson", xivo, List(Interval(None, "2000", "2099")), "52000", "default"))
      success
    }

    "check that the name and the display name do not already exist" in new Helper() {
      stub(entities.listFromDb).toReturn(List(Entity(None, mock[CombinedId], "avranches", "Avranches", xivo, List(Interval(None, "2000", "2099")), "52000", "default")))

      entityValidator.validateCreation(Entity(None, mock[CombinedId], "avranches2", "Avranches", xivo, List(Interval(None, "2100", "2199")), "52000", "default")) should throwA[InvalidParameterException](EntityValidator.ErrorUniqueNames)
      entityValidator.validateCreation(Entity(None, mock[CombinedId], "avranches", "Avranches2", xivo, List(Interval(None, "2100", "2199")), "52000", "default")) should throwA[InvalidParameterException](EntityValidator.ErrorUniqueNames)
      entityValidator.validateCreation(Entity(None, mock[CombinedId], "avranches2", "Avranches2", xivo, List(Interval(None, "2100", "2199")), "52000", "default"))
      success
    }

    "check that the context does not exist on the xivo" in new Helper() {
      stub(xivoConnector.contextExists("avranches")).toReturn(true)
      stub(xivoConnector.contextExists("pontorson")).toReturn(false)
      stub(entities.listFromDb).toReturn(List())

      entityValidator.validateCreation(Entity(None, mock[CombinedId], "avranches", "Avranches", xivo, List(Interval(None, "2100", "2199")), "52000", "default")) should throwA[InvalidParameterException](EntityValidator.ErrorContextExists.format("avranches"))
      entityValidator.validateCreation(Entity(None, mock[CombinedId], "pontorson", "Pontorson", xivo, List(Interval(None, "2100", "2199")), "52000", "default"))
      success
    }
  }

  "The EntityValidator.validateUpdate" should {
    "check the xivo has not been changed" in new Helper() {
      val oldE = Entity(Some(1), mock[CombinedId], "test", "Test", xivo, List(Interval(None, "2000", "2099")), "1234", "default")
      val newE = Entity(Some(1), mock[CombinedId], "test", "Test", xivo2, List(Interval(None, "2000", "2099")), "1234", "default")

      entityValidator.validateUpdate(oldE, newE) should throwA[InvalidParameterException](EntityValidator.ErrorXivoChanged)
    }

    "check the name has not been changed" in new Helper() {
      val oldE = Entity(Some(1), mock[CombinedId], "test", "Test", xivo, List(Interval(None, "2000", "2099")), "1234", "default")
      val newE = Entity(Some(1), mock[CombinedId], "test2", "Test", xivo, List(Interval(None, "2000", "2099")), "1234", "default")

      entityValidator.validateUpdate(oldE, newE) should throwA[InvalidParameterException](EntityValidator.ErrorNameChanged)
    }

    "check the old intervals are included in the new ones" in new Helper() {
      val combinedId = CombinedId(xivo.uuid, "test")
      val oldE = Entity(Some(1), combinedId, "test", "Test", xivo, List(Interval(None, "1000", "1099"), Interval(None, "2000", "2099")), "1234", "default")
      stub(entities.allBut(oldE.combinedId)).toReturn(List())
      entityValidator.validateUpdate(oldE, Entity(None, combinedId, "test", "Test", xivo, List(Interval(None, "1999", "2100"), Interval(None, "0999", "1100")), "1234", "default"))
      entityValidator.validateUpdate(oldE, Entity(None, combinedId, "test", "Test", xivo, List(Interval(None, "0999", "2100")), "1234", "default"))
      entityValidator.validateUpdate(oldE, Entity(None, combinedId, "test", "Test", xivo, List(Interval(None, "1000", "1099"), Interval(None, "2001", "2099")), "1234", "default")) should throwA[InvalidParameterException](EntityValidator.ErrorIntervalInclusion)
      entityValidator.validateUpdate(oldE, Entity(None, combinedId, "test", "Test", xivo, List(Interval(None, "2000", "2098")), "1234", "default")) should throwA[InvalidParameterException](EntityValidator.ErrorIntervalInclusion)
    }

    "check the display name does not already exist" in new Helper() {
      val oldE = Entity(Some(1), mock[CombinedId], "test", "Test", xivo, List(Interval(None, "2000", "2099")), "1234", "default")
      val newDisplayName = "NewTest"
      val otherE = Entity(Some(2),mock[CombinedId],  "newtest", newDisplayName, xivo, List(Interval(None, "2100", "2199")), "1234", "default")
      stub(entities.getByDisplayName(newDisplayName)).toReturn(Some(otherE))
      stub(entities.allBut(oldE.combinedId)).toReturn(List())

      entityValidator.validateUpdate(oldE, Entity(None, mock[CombinedId], "test", newDisplayName, xivo, List(Interval(None, "2000", "2099")), "1234", "default")) should
        throwA[InvalidParameterException](EntityValidator.ErrorUniqueNames)
    }

    "check the new intervals do not overlap the existing ones" in new Helper() {
      stub(configParser.allowIntervalOverlap).toReturn(false)
      val combinedId = CombinedId(xivo.uuid, "test")
      val oldE = Entity(Some(1), combinedId, "test", "Test", xivo, List(Interval(None, "2200", "2299")), "7894", "default")
      stub(entities.allBut(oldE.combinedId)).toReturn(List(Entity(None, combinedId, "avranches", "Avranches", xivo, List(Interval(None, "2000", "2099")), "52000", "default"),
        Entity(None, combinedId, "pontorson", "Pontorson", xivo, List(Interval(None, "3000", "3099")), "52001", "default")))

      entityValidator.validateUpdate(oldE, Entity(None, combinedId, "test", "Test", xivo, List(Interval(None, "2100", "2399")), "7894", "default"))
      entityValidator.validateUpdate(oldE, Entity(None, combinedId, "test", "Test", xivo, List(Interval(None, "2050", "2299")), "7894", "default")) should throwA[InvalidParameterException](EntityValidator.ErrorIntervalOverlap)
      entityValidator.validateUpdate(oldE, Entity(None, combinedId, "test", "Test", xivo, List(Interval(None, "2200", "3049")), "7894", "default")) should throwA[InvalidParameterException](EntityValidator.ErrorIntervalOverlap)
    }

    "check the provided intervals do not overlap with each other" in new Helper() {
      stub(configParser.allowIntervalOverlap).toReturn(false)
      val combinedId = CombinedId(xivo.uuid, "test")
      val oldE = Entity(Some(1), combinedId, "test", "Test", xivo, List(Interval(None, "1000", "1009")), "1234", "default")
      stub(entities.allBut(oldE.combinedId)).toReturn(List())

      entityValidator.validateUpdate(oldE, Entity(None, combinedId, "test", "Test", xivo, List(Interval(None, "1000", "1009"), Interval(None, "1020", "1029")), "568741", "default"))
      entityValidator.validateUpdate(oldE, Entity(None, combinedId, "test", "Test", xivo, List(Interval(None, "1000", "1009"), Interval(None, "1005", "1029")), "568741", "default"))should
        throwA[InvalidParameterException](EntityValidator.ErrorIntervalOverlap)
    }

  }
}

package model.validators

import java.security.InvalidParameterException

import model._
import org.mockito.Mockito.{stub, verify}
import org.scalatest.mock.MockitoSugar
import play.api.libs.json.Json
import model.utils.{NetworkUtils, SshManager}
import testutils._
import java.util.UUID

class XivoValidatorSpec extends PlayShouldSpec with MockitoSugar with TestConfig {

  class Helper() {
    val xivoManager = mock[XivoInterface]
    val xivoValidator = new XivoValidator(xivoManager)
    val netUtils = mock[NetworkUtils]
    xivoValidator.netUtils = netUtils
    val sshManager = mock[SshManager]
    xivoValidator.sshManager = sshManager
  }

  "The XivoValidator" should {
    "parse a XivoCreation from JSON" in new Helper() {
      xivoValidator.fromJson(Json.obj(
      "name" -> "xivo2",
      "host" -> "192.168.2.1",
      "linkedXivo" -> List(1,3),
      "contextName" -> "testName",
      "sshKey" -> ""
      )) shouldEqual XivoCreation("xivo2", "192.168.2.1", List(1, 3), "testName", true, None)
    }

    "parse a XivoCreation from JSON with configure key" in new Helper() {
      xivoValidator.fromJson(Json.obj(
        "name" -> "xivo2",
        "host" -> "192.168.2.1",
        "linkedXivo" -> List(1,3),
        "contextName" -> "testName",
        "sshKey" -> "",
        "configure" -> true
      )) shouldEqual XivoCreation("xivo2", "192.168.2.1", List(1, 3), "testName", true, None)
    }

    "throw an InvalidParametersException if contextName is missing" in new Helper() {
      intercept[InvalidParameterException] {
        xivoValidator.fromJson(Json.obj(
          "name" -> "xivo2",
          "host" -> "192.168.2.1",
          "linkedXivo" -> List(1, 3),
          "sshKey" -> ""
        ))
      }
    }

    "throw an InvalidParametersException if parameters are missing" in new Helper() {
      intercept[InvalidParameterException] {
        xivoValidator.fromJson(Json.obj(
          "host" -> "192.168.2.1",
          "linkedXivo" -> List(1, 3)
        ))
      }
    }

    "check the name and the host are unique" in new Helper() {
      stub(xivoManager.list).toReturn(List(Xivo(Some(1), UUID.randomUUID(), "xivo1", "192.168.1.2", "to_xivo1", None, Some("token"))))

      val e = intercept[InvalidParameterException] {
        xivoValidator.validate(XivoCreation("xivo1", "192.168.5.2", List(), "to_xivo1", true, Some("token")))
      }.getMessage shouldBe XivoValidator.ErrorUniqueValues
      intercept[InvalidParameterException] {
        xivoValidator.validate(XivoCreation("xivo2", "192.168.1.2", List(), "to_xivo1", true, Some("token")))
      }.getMessage shouldBe XivoValidator.ErrorUniqueValues
    }

    "check the provided ids exist" in new Helper() {
      val x1 = Xivo(Some(1), UUID.randomUUID(), "xivo1", "192.168.1.2", "to_xivo1", None, Some("token"))
      stub(xivoManager.list).toReturn(List(x1))
      stub(xivoManager.getXivo(x1.id.get)).toReturn(Some(x1))
      stub(xivoManager.getXivo(10L)).toReturn(None)

      intercept[InvalidParameterException] {
        xivoValidator.validate(XivoCreation("xivo3", "192.168.1.4", List(x1.id.get, 10), "to_xivo3", true, Some("token")))
      }.getMessage shouldBe XivoValidator.ErrorNoSuchXivo.format(10)
    }

    "check the xivo is reachable on the network" in new Helper() {
      val xc1 = XivoCreation("xivo3", "192.168.1.4", List(), "to_xivo3", true, Some("token"))
      stub(xivoManager.list).toReturn(List())
      stub(netUtils.isHostReachable(xc1.host)).toReturn(false)

      intercept[InvalidParameterException] {
        xivoValidator.validate(xc1)
      }.getMessage shouldBe(XivoValidator.ErrorUnreachable)
      verify(netUtils).isHostReachable(xc1.host)
    }

    "check if it is possible to connect with the ssh key" in new Helper() {
      val xc1 = XivoCreation("xivo3", "192.168.1.4", List(), "to_xivo3", true, Some("token"))
      val xc2 = XivoCreation("xivo4", "192.168.1.5", List(), "to_xivo4", true, Some("token"))
      stub(xivoManager.list).toReturn(List())
      stub(netUtils.isHostReachable(xc1.host)).toReturn(true)
      stub(netUtils.isHostReachable(xc2.host)).toReturn(true)
      stub(sshManager.executeRemoteCommand(xc1.host, "exit")).toReturn(255)
      stub(sshManager.executeRemoteCommand(xc2.host, "exit")).toReturn(0)

      intercept[InvalidParameterException] {
        xivoValidator.validate(xc1)
      }.getMessage shouldBe XivoValidator.ErrorSshKey

      verify(sshManager).executeRemoteCommand(xc1.host, "exit")
      xivoValidator.validate(xc2)
      verify(sshManager).executeRemoteCommand(xc2.host, "exit")
    }

    "check if the contextName contains only allowed characters" in new Helper() {
      val xc1 = XivoCreation("xivo3", "192.168.1.4", List(), "éàçtrt", true, Some("token"))
      stub(xivoManager.list).toReturn(List())
      stub(netUtils.isHostReachable(xc1.host)).toReturn(true)
      stub(sshManager.executeRemoteCommand(xc1.host, "exit")).toReturn(0)

      intercept[InvalidParameterException] {
        xivoValidator.validate(xc1)
      }.getMessage shouldBe XivoValidator.ErrorWrongContextName

      intercept[InvalidParameterException] {
        xivoValidator.validate(xc1.copy(contextName = "(e)"))
      }.getMessage shouldBe XivoValidator.ErrorWrongContextName

      xivoValidator.validate(xc1.copy(contextName = "e15_5-A"))
    }

  }
}

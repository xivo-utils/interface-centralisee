package model.validators

import java.security.InvalidParameterException
import java.util.UUID

import model._
import org.mockito.Mockito._
import org.scalatest.mock.MockitoSugar
import org.specs2.specification.Scope
import play.api.libs.json.Json
import model.utils.{ConfigParser, GlobalXivoConnectorPool, XivoConnectorPool}
import services.cache.CachedXivoUserManager
import services.{TemplateManager, UIAdministratorManager}
import testutils._
import xivo.ldap.xivoconnection.XivoConnector

import scala.util.{Success, Try}

class UserValidatorSpec extends BeforeAndAfterEach with MockitoSugar with ConnectorProviderStub {
  val xivo = Xivo(Some(1), UUID.randomUUID(), "xivo1", "192.168.1.123", "to_xivo1", None, Some("token"))
  val entity1 = Entity(Some(1), CombinedId(xivo.uuid, "avranches"), "avranches", "Avranches", xivo, List(
    Interval(Some(12), "2000", "2099", IntervalRoutingModes.RoutedWithDirectNumber, Some("55")),
    Interval(None, "2200", "2299")), "52000", "default")

  val entity2 = Entity(Some(2), CombinedId(xivo.uuid, "pontorson"), "pontorson", "Pontorson", xivo, List(
    Interval(None, "3000", "3099")), "52001", "default")

  val entity3 = entity1.copy(intervals = List(
    Interval(Some(12), "2001", "2099", IntervalRoutingModes.RoutedWithCustomDirectNumber, Some("10000"))
  ))

  override var xivoConnector: XivoConnector = null

  class Helper extends Scope {
    val configParser = mock[ConfigParser]
    xivoConnector = mock[XivoConnector]
    val connectorPool = new GlobalXivoConnectorPool(configParser) {
      override def get(host: String, incallContext: String, token: String): XivoConnector = xivoConnector
    }
    val entities = mock[EntityInterface]
    val admins = mock[AdministratorInterface]
    val adminManager = mock[UIAdministratorManager]
    val routes = mock[RouteInterface]
    val templates = mock[TemplateManager]
    val cachedXivoUserManager = mock[CachedXivoUserManager]

    val validator = new UserValidator(entities, List(entity1), routes, templates, adminManager, cachedXivoUserManager, connectorPool)
    validator.connectorPool = connectorPool
  }

  "The validator object" should {

    "not validate a user whose internal number is outside of the entity interval" in new Helper() {
      stub(xivoConnector.extensionExists("2000", entity1.name)).toReturn(false)
      val admin = mock[UIAdministrator]
      stub(adminManager.getByLogin("tartampion")).toReturn(Success(admin))
      stub(entities.listForAdmin(admin)).toReturn(List(entity1))
      validator.validateCreation(User(None, entity1, "Lucien", "Chardon", "1000", None, None, None, None), true) should
        throwA[InvalidParameterException](UserValidator.ErrorNumberOutOfIntervals(entity1.intervals))

      validator.validateCreation(User(None, entity1, "Lucien", "Chardon", "2000", None, None, None, None), true)
      validator.validateCreation(User(None, entity1, "Lucien", "Chardon", "2202", None, None, None, None), true)
      success
    }

    "not validate a user whose internal number is already used" in new Helper() {
      stub(xivoConnector.extensionExists("2000", entity1.name)).toReturn(true)
      stub(xivoConnector.extensionExists("2001", entity1.name)).toReturn(false)
      val admin = mock[UIAdministrator]
      stub(adminManager.getByLogin("tartampion")).toReturn(Success(admin))
      stub(entities.listForAdmin(admin)).toReturn(List(entity1))

      validator.validateCreation(User(None, entity1, "Lucien", "Chardon", "2000", None, None, None, None), true) should
        throwA[InvalidParameterException](UserValidator.ErrorExtensionExists.format("2000", entity1.name))

      validator.validateCreation(User(None, entity1, "Lucien", "Chardon", "2001", None, None, None, None), true)
      success
    }

    "not validate creation if no space is left on the xivo" in new Helper() {
      stub(xivoConnector.extensionExists("2000", entity1.name)).toReturn(false)
      val xivo2 = xivo.copy(remainingSlots = Some(0))
      val entity2 = entity1.copy(xivo=xivo2)
      validator.validateCreation(User(None, entity2, "Lucien", "Chardon", "2000", None, None, Some("l.chardon"), Some("pwd")), true) should
        throwA[InvalidParameterException](UserValidator.ErrorNoSlot)
    }

    "not validate a user whose external number is already used" in new Helper() {
      val extNum = "012345789"
      stub(routes.findRouteByDigits(extNum)).toReturn(Some(Route(None, extNum, s"($extNum)", "to-monxivo")))

      validator.validateCreation(User(None, entity1, "Lucien", "Chardon", "2000", Some(extNum), None, None, None), true) should
        throwA[InvalidParameterException](UserValidator.ExternalNumberIsAlreadyUsed(extNum))
    }

    "forbid to create a User in an entity on which the administrator has no right" in new Helper() {
      stub(xivoConnector.extensionExists("3000", entity2.name)).toReturn(false)

      validator.validateCreation(User(None, entity2, "Daniel", "Séchard", "3000", None, None, None, None), true) should
        throwA[InvalidParameterException](UserValidator.ErrorInsufficientRights)
    }

    "compare entities by combinedId while validating rights" in new Helper() {
      val entity2 = entity1.copy(id=None)
      validator.validateCreation(User(None, entity2, "Lucien", "Chardon", "2000", None, None, Some("l.chardon"), Some("pwd")), true)
      success
    }

    "get a User from a Map" in new Helper() {
      stub(entities.getByDisplayName(entity1.displayName)).toReturn(Some(entity1))
      val map = Map(
        "entity.displayName" -> entity1.displayName,
        "firstName" -> "David",
        "lastName" -> "Séchard",
        "internalNumber" -> "2000",
        "externalNumber" -> "52000",
        "mail" -> "dsechard@example.com",
        "ctiLogin" -> "dsechard",
        "ctiPassword" -> "0000",
        "intervalId" -> "1",
        "templateId" -> "1",
        "ringingTime" -> "20",
        "peerSipName" -> "webrtc",
        "callerIdMode" -> "custom",
        "customCallerId" -> "D-Séché"
      )

      validator.fromMap(map) shouldEqual
        User(None, entity1, "David", "Séchard", "2000", Some("52000"), Some("dsechard@example.com"),
          Some("dsechard"), Some("0000"), None, Some(1), Some(1), Some(20),
          Some(model.PeerSipNameMode.WebRTC), Some(model.CallerIdMode.Custom), Some("D-Séché"))
    }

    "throw an exception if parameters are missing in the map" in new Helper() {
      val map = Map(
        "lastName" -> "Séchard",
        "entity.displayName" -> entity1.displayName,
        "internalNumber" -> "2000",
        "mail" -> "dsechard@example.com",
        "externalNumber" -> "52000",
        "ctiLogin" -> "dsechard",
        "ctiPassword" -> "0000"
      )

      validator.fromMap(map) should throwA[InvalidParameterException](UserValidator.ErrorMissingParameters(
        "entity.displayName", "firstName", "lastName", "internalNumber", "templateId"))
    }

    "get a user from JSON without cti profile" in new Helper() {
      stub(entities.getEntity(entity1.combinedId)).toReturn(Some(entity1))
      stub(templates.get(9)).toReturn(mock[Try[Template]])
      val json = Json.obj(
        "entityCId" -> entity1.combinedId,
        "firstName" -> "Lucien",
        "lastName" -> "Chardon",
        "internalNumber" -> "2000",
        "mail" -> "l.chardon@test.com",
        "ctiLogin" -> "",
        "ctiPassword" -> "",
        "intervalId" -> 12,
        "templateId" -> 9,
        "ringingTime" -> 20,
        "peerSipName" -> "webrtc",
        "callerIdMode" -> "custom",
        "customCallerId" -> "Chon-dar"
      )

      validator.fromJson(json) shouldEqual
        User(None, entity1, "Lucien", "Chardon", "2000", Some("552000"), Some("l.chardon@test.com"), None,
          None, None, Some(12), Some(9), Some(20), Some(PeerSipNameMode.WebRTC), Some(CallerIdMode.Custom), Some("Chon-dar"))
    }

    "get a user from JSON with external number in interval RoutedWithDirectNumber" in new Helper() {
      stub(entities.getEntity(entity1.combinedId)).toReturn(Some(entity1))
      stub(templates.get(9)).toReturn(mock[Try[Template]])
      val json = Json.obj(
       "entityCId" -> entity1.combinedId,
        "firstName" -> "Lucien",
        "lastName" -> "Chardon",
        "internalNumber" -> "2000",
        "mail" -> "l.chardon@test.com",
        "ctiLogin" -> "lchardon",
        "ctiPassword" -> "1234",
        "intervalId" -> 12,
        "templateId" -> 9,
        "ringingTime" -> 20,
        "peerSipName" -> "ua",
        "callerIdMode" -> "custom",
        "customCallerId" -> "Chon-dar"
      )

      validator.fromJson(json) shouldEqual
        User(None, entity1, "Lucien", "Chardon", "2000", Some("552000"), Some("l.chardon@test.com"), Some("lchardon"),
          Some("1234"), None, Some(12), Some(9), Some(20), Some(PeerSipNameMode.UniqueAccount), Some(CallerIdMode.Custom), Some("Chon-dar"))
    }

    "get a user from JSON with external number in interval RoutedWithCustomDirectNumber" in new Helper() {
      stub(entities.getEntity(entity3.combinedId)).toReturn(Some(entity3))
      stub(templates.get(9)).toReturn(mock[Try[Template]])
      val json = Json.obj(
       "entityCId" -> entity3.combinedId,
        "firstName" -> "Lucien",
        "lastName" -> "Chardon",
        "internalNumber" -> "2010",
        "externalNumber" -> "01230599",
        "mail" -> "l.chardon@test.com",
        "ctiLogin" -> "lchardon",
        "ctiPassword" -> "1234",
        "intervalId" -> 12,
        "templateId" -> 9,
        "ringingTime" -> 20,
        "peerSipName" -> "ua",
        "callerIdMode" -> "custom",
        "customCallerId" -> "Chon-dar"
      )

      validator.fromJson(json) shouldEqual
        User(None, entity3, "Lucien", "Chardon", "2010", Some("01230599"), Some("l.chardon@test.com"), Some("lchardon"),
          Some("1234"), None, Some(12), Some(9), Some(20), Some(PeerSipNameMode.UniqueAccount), Some(CallerIdMode.Custom), Some("Chon-dar"))
    }

    "get a user from JSON without external number" in new Helper() {
      stub(entities.getEntity(entity2.combinedId)).toReturn(Some(entity2))
      stub(templates.get(9)).toReturn(mock[Try[Template]])
      val json = Json.obj(
        "entityCId" -> entity2.combinedId,
        "firstName" -> "Lucien",
        "lastName" -> "Chardon",
        "internalNumber" -> "2000",
        "mail" -> "l.chardon@test.com",
        "ctiLogin" -> "lchardon",
        "ctiPassword" -> "1234",
        "intervalId" -> 12,
        "templateId" -> 9,
        "ringingTime" -> 20,
        "peerSipName" -> "ua",
        "callerIdMode" -> "custom",
        "customCallerId" -> "Chon-dar"
      )

      validator.fromJson(json) shouldEqual
        User(None, entity2, "Lucien", "Chardon", "2000", None, Some("l.chardon@test.com"), Some("lchardon"),
          Some("1234"), None, Some(12), Some(9), Some(20), Some(PeerSipNameMode.UniqueAccount), Some(CallerIdMode.Custom), Some("Chon-dar"))
    }

    "throw an exception if parameters are missing in the JSON" in new Helper() {
      stub(entities.getEntity(entity1.combinedId)).toReturn(Some(entity1))
      val json = Json.obj(
        "entityId" -> entity1.combinedId,
        "lastName" -> "Chardon",
        "internalNumber" -> "2000",
        "externalNumber" -> "52000",
        "mail" -> "l.chardon@test.com",
        "ctiLogin" -> "lchardon",
        "ctiPassword" -> "1234"
      )

      validator.fromJson(json) should throwA[InvalidParameterException](
        UserValidator.ErrorMissingParameters("entityCId", "firstName", "lastName", "internalNumber", "intervalId",
            "templateId", "ringingTime", "peerSipName", "callerIdMode"))
    }

    "throw an exception if the entity does not exist" in new Helper() {
      stub(entities.getByDisplayName("tartampion")).toReturn(None)
      val map = Map(
        "entity.displayName" -> "tartampion",
        "firstName" -> "David",
        "lastName" -> "Séchard",
        "internalNumber" -> "2000",
        "externalNumber" -> "52000",
        "mail" -> "dsechard@example.com",
        "ctiLogin" -> "dsechard",
        "ctiPassword" -> "0000",
        "intervalId" -> "1",
        "templateId" -> "1",
        "ringingTime" -> "20",
        "peerSipName" -> "webrtc",
        "callerIdMode" -> "custom",
        "customCallerId" -> "D-Séché"
      )

      validator.fromMap(map) should throwA[InvalidParameterException](UserValidator.ErrorNoSuchEntity.format("tartampion"))
    }

    "not validate an update modifying the id" in new Helper() {
      val oldUser = User(Some(2L), entity1, "Lucien", "Chardon", "2000", None, None, None, None)
      val newUser = User(Some(3L), entity1, "Lucien", "Chardon", "2000", None, None, None, None)

      validator.validateUpdate(oldUser, newUser) should throwA[InvalidParameterException](UserValidator.ErrorIdModification)
    }

    "not validate an update modifying the entity" in new Helper() {
      val oldUser = User(Some(2L), entity1, "Lucien", "Chardon", "2000", None, None, None, None)
      val newUser = User(Some(2L), entity2, "Lucien", "Chardon", "2000", None, None, None, None)

      validator.validateUpdate(oldUser, newUser) should throwA[InvalidParameterException](UserValidator.ErrorEntityModification)
    }

    "not validate a user whose internal number is outside of the entity interval" in new Helper() {
      val oldUser = User(Some(2L), entity1, "Lucien", "Chardon", "2000", None, None, None, None)
      validator.validateUpdate(oldUser, oldUser.copy(internalNumber = "2203"))
      validator.validateUpdate(oldUser, oldUser.copy(internalNumber = "4000")) should throwA[InvalidParameterException](
        UserValidator.ErrorNumberOutOfIntervals(entity1.intervals))
    }

    "not validate a user whose internal number is already used" in new Helper() {
      val oldUser = User(Some(2L), entity1, "Lucien", "Chardon", "2000", None, None, None, None)
      stub(xivoConnector.extensionExists("2001", entity1.name)).toReturn(true)

      validator.validateUpdate(oldUser, oldUser.copy(internalNumber = "2001")) should throwA[InvalidParameterException](
        UserValidator.ErrorExtensionExists.format("2001", entity1.name))
    }

    "validate a user creation with external number and interval RoutedWithCustomDirectNumber" in new Helper() {
      val interval = entity3.intervals.head
      val externalNumber = "10098"
      val user = User(None, entity3, "Lucien", "Chardon", "2001", Some(externalNumber), None, None, None, intervalId = interval.id)
      stub(cachedXivoUserManager.countForExternalNumber(externalNumber, None)).toReturn(Success(0))
      stub(routes.findRouteByDigits(externalNumber)).toReturn(None)
      validator.validateCreation(user, true)
    }

    "validate a user update with external number and interval RoutedWithCustomDirectNumber" in new Helper() {
      val interval = entity3.intervals.head
      val oldExternalNumber = "10098"
      val newExternalNumber = "10097"
      val user = User(Some(1), entity3, "Lucien", "Chardon", "2001", Some(oldExternalNumber), None, None, None, intervalId = interval.id)
      stub(cachedXivoUserManager.countForExternalNumber(newExternalNumber, user.id)).toReturn(Success(0))
      stub(routes.findRouteByDigits(newExternalNumber)).toReturn(None)
      validator.validateUpdate(user, user.copy(firstName = "Jean", externalNumber = Some(newExternalNumber)))
    }

    "not validate a user creation when external number in interval RoutedWithCustomDirectNumber is not unique" in new Helper() {
      val interval = entity3.intervals.head
      val externalNumber = "10098"
      val user = User(None, entity3, "Lucien", "Chardon", "2001", Some(externalNumber), None, None, None, intervalId = interval.id)
      stub(cachedXivoUserManager.countForExternalNumber(externalNumber, None)).toReturn(Success(1))
      stub(routes.findRouteByDigits(externalNumber)).toReturn(None)
      validator.validateCreation(user, true) should
        throwA[InvalidParameterException](UserValidator.ExternalNumberIsAlreadyUsed(externalNumber))
    }

    "not validate a user update when external number in interval RoutedWithCustomDirectNumber is not unique" in new Helper() {
      val interval = entity3.intervals.head
      val oldExternalNumber = "10098"
      val newExternalNumber = "10097"
      val user = User(None, entity3, "Lucien", "Chardon", "2001", Some(oldExternalNumber), None, None, None, intervalId = interval.id)
      stub(cachedXivoUserManager.countForExternalNumber(newExternalNumber, user.id)).toReturn(Success(1))
      stub(routes.findRouteByDigits(newExternalNumber)).toReturn(None)
      validator.validateUpdate(user, user.copy(firstName = "Jean", externalNumber = Some(newExternalNumber))) should
        throwA[InvalidParameterException](UserValidator.ExternalNumberIsAlreadyUsed(newExternalNumber))
    }

    "not validate a user creation when external number in interval RoutedWithCustomDirectNumber is unique in xivo_users but not in routes" in new Helper() {
      val interval = entity3.intervals.head
      val externalNumber = "10098"
      val user = User(None, entity3, "Lucien", "Chardon", "2001", Some(externalNumber), None, None, None, intervalId = interval.id)
      stub(cachedXivoUserManager.countForExternalNumber(externalNumber, None)).toReturn(Success(0))
      stub(routes.findRouteByDigits(externalNumber)).toReturn(Some(Route(None, externalNumber, s"($externalNumber)", "to-monxivo")))
      validator.validateCreation(user, true) should
        throwA[InvalidParameterException](UserValidator.ExternalNumberIsAlreadyUsed(externalNumber))
    }

    "not validate a user update when external number in interval RoutedWithCustomDirectNumber is unique in xivo_users but not in routes" in new Helper() {
      val interval = entity3.intervals.head
      val oldExternalNumber = "10098"
      val newExternalNumber = "10097"
      val user = User(None, entity3, "Lucien", "Chardon", "2001", Some(oldExternalNumber), None, None, None, intervalId = interval.id)
      stub(cachedXivoUserManager.countForExternalNumber(newExternalNumber, user.id)).toReturn(Success(0))
      stub(routes.findRouteByDigits(newExternalNumber)).toReturn(Some(Route(None, newExternalNumber, s"($newExternalNumber)", "to-monxivo")))
      validator.validateUpdate(user, user.copy(firstName = "Jean", externalNumber = Some(newExternalNumber))) should
        throwA[InvalidParameterException](UserValidator.ExternalNumberIsAlreadyUsed(newExternalNumber))
    }

    "not validate a user creation when external number in interval RoutedWithCustomDirectNumber is not in range" in new Helper() {
      val interval = entity3.intervals.head
      val externalNumber = "010098"
      val user = User(None, entity3, "Lucien", "Chardon", "2001", Some(externalNumber), None, None, None, intervalId = interval.id)
      stub(cachedXivoUserManager.countForExternalNumber(externalNumber, None)).toReturn(Success(0))
      stub(routes.findRouteByDigits(externalNumber)).toReturn(None)
      validator.validateCreation(user, true) should
        throwA[InvalidParameterException](UserValidator.ExternalNumberOutOfRange(externalNumber, interval))
    }

    "not validate a user update when external number in interval RoutedWithCustomDirectNumber is not in range" in new Helper() {
      val interval = entity3.intervals.head
      val oldExternalNumber = "10098"
      val newExternalNumber = "1009"
      val user = User(None, entity3, "Lucien", "Chardon", "2001", Some(oldExternalNumber), None, None, None, intervalId = interval.id)
      stub(cachedXivoUserManager.countForExternalNumber(newExternalNumber, user.id)).toReturn(Success(0))
      stub(routes.findRouteByDigits(newExternalNumber)).toReturn(None)
      validator.validateUpdate(user, user.copy(firstName = "Jean", externalNumber = Some(newExternalNumber))) should
        throwA[InvalidParameterException](UserValidator.ExternalNumberOutOfRange(newExternalNumber, interval))
    }

    "compute external number in Routed interval" in {
      val interval = Interval(None, "2200", "2299", IntervalRoutingModes.Routed)
      UserValidator.computeExtNumber(interval, "2215") shouldEqual Some("2215")
    }

    "compute external number in interval RoutedWithDirectNumber" in {
      val interval = Interval(Some(12), "2000", "2099", IntervalRoutingModes.RoutedWithDirectNumber, Some("550"))
      UserValidator.computeExtNumber(interval, "2056") shouldEqual Some("5502056")
      UserValidator.computeExtNumber(interval, "0012") shouldEqual Some("5500012")
    }

    "compute external number in interval RoutedWithCustomDirectNumber" in {
      val interval = Interval(Some(12), "2001", "2099", IntervalRoutingModes.RoutedWithCustomDirectNumber, Some("10000"))
      UserValidator.computeExtNumber(interval, "2010") shouldEqual Some("10009")
    }

    "get a user from JSON for proxy" in new Helper() {
      stub(entities.getEntity(entity1.combinedId)).toReturn(Some(entity1))
      stub(templates.get(9)).toReturn(mock[Try[Template]])
      val json = Json.obj(
        "entityCId" -> entity1.combinedId,
        "firstName" -> "Lucien",
        "lastName" -> "Chardon",
        "internalNumber" -> "2000",
        "intervalId" -> 12
      )

      validator.fromProxyUserJson(json) shouldEqual
        User(None, entity1, "Lucien", "Chardon", "2000", Some("552000"), None, None,
          None, None, Some(12), None, None, None, None, None)
    }

  }

}

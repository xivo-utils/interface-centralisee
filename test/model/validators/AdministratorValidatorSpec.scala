package model.validators

import java.security.InvalidParameterException
import model._
import model.rights.Role
import org.mockito.Mockito._
import org.scalatest.mock.MockitoSugar
import org.specs2.specification.Scope
import play.api.libs.json.Json
import services.UIAdministratorManager
import services.rights.RoleManager
import testutils.BeforeAndAfterEach
import scala.util.{Failure, Success}

class AdministratorValidatorSpec extends BeforeAndAfterEach with MockitoSugar {
  
  val role1 = Role(Some(1), "1")
  val role2 = Role(Some(2), "2")

  class Helper() extends Scope {
    val roles = mock[RoleManager]
    val admins = mock[AdministratorInterface]
    val adminManager = mock[UIAdministratorManager]
    val administratorValidator = new AdministratorValidator(roles, admins, adminManager)
  }

  "The AdministratorValidator" should {
    "get an administrator from its JSON representation" in new Helper() {
      stub(roles.get(role1.id.get)).toReturn(Success(role1))
      stub(roles.get(role2.id.get)).toReturn(Success(role2))
      val json = Json.obj(
      "login" -> "durand",
        "name" -> "Jean Durand",
        "password" -> "toto",
        "superAdmin" -> false,
        "ldap" -> false,
        "roleIds" -> List(role1.id, role2.id)
      )

      administratorValidator.fromJson(json) shouldEqual
        UIAdministrator(None, "Jean Durand", "durand", "toto", false, false, List(role1, role2))
    }

    "get an administrator from its JSON representation even if it's a superadmin" in new Helper() {
      stub(roles.get(role1.id.get)).toReturn(Success(role1))
      stub(roles.get(role2.id.get)).toReturn(Success(role2))
      val jsonNoEntity = Json.obj(
        "login" -> "durand",
        "name" -> "Jean Durand",
        "password" -> "toto",
        "superAdmin" -> true,
        "ldap" -> false
      )

      administratorValidator.fromJson(jsonNoEntity) shouldEqual
        UIAdministrator(None, "Jean Durand", "durand", "toto", true, false, List())
    }


    "throw an exception if a parameter is missing" in new Helper() {
      val json = Json.obj(
        "login" -> "durand",
        "name" -> "Jean Durand",
        "superAdmin" -> false,
        "roleIds" -> role1.id
      )

      administratorValidator.fromJson(json) should
        throwA[InvalidParameterException](AdministratorValidator.ErrorRequiredFields("login", "name", "password",
          "superAdmin", "ldap"))
    }

    "throw an exception if the role does not exist" in new Helper() {
      stub(roles.get(role1.id.get + 1)).toReturn(Failure(new Exception("Not found")))
      val json = Json.obj(
        "login" -> "durand",
        "name" -> "Jean Durand",
        "password" -> "toto",
        "superAdmin" -> false,
        "ldap" -> false,
        "roleIds" -> List(role1.id.get + 1)
      )

      administratorValidator.fromJson(json) should
        throwA[InvalidParameterException](AdministratorValidator.ErrorNoSuchRole.format(role1.id.get + 1))
    }

    "throw an exception if the login already exists" in new Helper() {
      stub(adminManager.all()).toReturn(Success(
        List(UIAdministrator(Some(10), "test name", "test", "test", true, false, List()))))

      administratorValidator.validate(UIAdministrator(None, "test name", "test", "haha", false, false, List())) should
        throwA[InvalidParameterException](AdministratorValidator.ErrorLoginExists.format("test"))
      administratorValidator.validate(UIAdministrator(None, "test 2", "test2", "haha", false, false, List()))
      success
    }

    "throw an exception when updating with an existing login" in new Helper() {
      val oldAdmin = UIAdministrator(Some(10), "test", "test name", "test", true, false, List())
      val otherAdmin = UIAdministrator(Some(11), "autre test name", "autretest", "test", false, false, List(role1))
      stub(adminManager.all()).toReturn(Success(List(oldAdmin, otherAdmin)))
      val newAdmin = UIAdministrator(None, "autre test name", "autretest", "superpass", false, false, List(role2))

      administratorValidator.validateUpdate(oldAdmin, newAdmin) should
        throwA[InvalidParameterException](AdministratorValidator.ErrorLoginExists.format("autretest"))
      administratorValidator.validateUpdate(oldAdmin, newAdmin.copy(login="test2"))
      administratorValidator.validateUpdate(oldAdmin, newAdmin.copy(login="test"))
      success
    }

  }
}

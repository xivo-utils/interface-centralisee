package model.validators

import model._
import org.mockito.Mockito.stub
import org.scalatest.mock.MockitoSugar
import play.api.libs.json.Json
import testutils._

import java.security.InvalidParameterException
import java.util.UUID

class ProxyValidatorSpec extends PlayShouldSpec with MockitoSugar with TestConfig {

  class Helper() {
    val xivoManager = mock[XivoInterface]
    val proxyValidator = new ProxyValidator(xivoManager)
  }

  "The ProxyValidator" should {
    "parse a ProxyCreation from JSON" in new Helper() {
      proxyValidator.fromJson(Json.obj(
        "name" -> "proxy2",
        "host" -> "192.168.2.1",
        "contextName" -> "from_proxy2",
        "intervals" -> List(
          Json.obj("start" -> "1000", "end" -> "1099", "routing_mode" -> "routed"),
          Json.obj("start" -> "200", "end" -> "209", "routing_mode" -> "routed", "label" -> "test"))
      )) shouldEqual ProxyCreation("proxy2", "192.168.2.1", "from_proxy2", List(
        Interval(None, "1000", "1099", IntervalRoutingModes.Routed), Interval(None, "200", "209", label = "test")))
    }

    "throw an InvalidParametersException if contextName is missing" in new Helper() {
      intercept[InvalidParameterException] {
        proxyValidator.fromJson(Json.obj(
          "name" -> "xivo2",
          "host" -> "192.168.2.1"
        ))
      }
    }

    "throw an InvalidParametersException if parameters are missing" in new Helper() {
      intercept[InvalidParameterException] {
        proxyValidator.fromJson(Json.obj(
          "host" -> "192.168.2.1"
        ))
      }
    }

    "check the name and the host are unique" in new Helper() {
      stub(xivoManager.list).toReturn(List(Xivo(Some(1), UUID.randomUUID(), "proxy1", "192.168.1.2", "to_proxy1", None, None, true)))
      val e = intercept[InvalidParameterException] {
        proxyValidator.validate(ProxyCreation("proxy1", "192.168.5.3", "to_proxy1", List(
          Interval(None, "1000", "1099", IntervalRoutingModes.Routed), Interval(None, "200", "209", label = "label"))))
      }.getMessage shouldBe XivoValidator.ErrorUniqueValues
      intercept[InvalidParameterException] {
        proxyValidator.validate(ProxyCreation("proxy2", "192.168.1.2", "to_proxy2", List(
          Interval(None, "1000", "1099", IntervalRoutingModes.Routed), Interval(None, "200", "209", label = "label"))))
      }.getMessage shouldBe XivoValidator.ErrorUniqueValues
    }

    "check if the contextName contains only allowed characters" in new Helper() {
      val pc1 = ProxyCreation("proxy3", "192.168.1.4", "éàçtrt", List(
        Interval(None, "1000", "1099", IntervalRoutingModes.Routed), Interval(None, "200", "209", label = "label")), None)
      stub(xivoManager.list).toReturn(List())

      intercept[InvalidParameterException] {
        proxyValidator.validate(pc1)
      }.getMessage shouldBe XivoValidator.ErrorWrongContextName

      intercept[InvalidParameterException] {
        proxyValidator.validate(pc1.copy(contextName = "(e)"))
      }.getMessage shouldBe XivoValidator.ErrorWrongContextName

      proxyValidator.validate(pc1.copy(contextName = "e15_5-A"))
    }
  }
}

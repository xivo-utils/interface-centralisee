package model

import java.util.UUID

import org.specs2.specification.Scope
import play.api.test.PlaySpecification

class UserSpec extends PlaySpecification {

  class Helper extends Scope {
    val xivo = Xivo(Some(1), UUID.randomUUID(), "xivo1", "192.168.1.123", "to_xivo1", None, Some("token"))
    val entity1 = Entity(Some(1), CombinedId(xivo.uuid, "avranches"), "avranches", "Avranches", xivo, List(
      Interval(Some(12), "2000", "2099", IntervalRoutingModes.RoutedWithDirectNumber, Some("55")),
      Interval(None, "2200", "2299")), "52000", "default")
    val user = User(None, entity1, "David", "Séchard", "2000", Some("52000"), Some("dsechard@example.com"),
      Some("dsechard"), Some("0000"), None, None, Some(1))
  }

  "The User constructor" should {

    "prevent empty firstName" in new Helper {
      user.copy(firstName = "") should throwA[IllegalArgumentException](User.errorFieldEmpty("firstName"))
    }

    "prevent too long firstName" in new Helper {
      user.copy(firstName = "x" * (User.FirstNameMaxLength + 1)) should throwA[IllegalArgumentException](User.errorFieldTooLong("firstName"))
    }

    "prevent too long lastName" in new Helper {
      user.copy(lastName = "x" * (User.LastNameMaxLength + 1)) should throwA[IllegalArgumentException](User.errorFieldTooLong("lastName"))
    }

    "prevent too long mail" in new Helper {
      user.copy(mail = Some("x" * (User.MailMaxLength + 1))) should throwA[IllegalArgumentException](User.errorFieldTooLong("mail"))
    }

    "prevent empty internalNumber" in new Helper {
      user.copy(internalNumber = "") should throwA[IllegalArgumentException](User.errorFieldEmpty("internalNumber"))
    }

    "prevent too long internalNumber" in new Helper {
      user.copy(internalNumber = "x" * (User.InternalNumberMaxLength + 1)) should throwA[IllegalArgumentException](User.errorFieldTooLong("internalNumber"))
    }

    "prevent too long ctiLogin" in new Helper {
      user.copy(ctiLogin = Some("x" * (User.CtiLoginMaxLength + 1))) should throwA[IllegalArgumentException](User.errorFieldTooLong("ctiLogin"))
    }
    
    "prevent too long ctiPassword" in new Helper {
      user.copy(ctiPassword = Some("x" * (User.CtiPasswordMaxLength + 1))) should throwA[IllegalArgumentException](User.errorFieldTooLong("ctiPassword"))
    }

    "prevent ctiLogin and ctiPassword to be one empty and other not" in new Helper {
      type Samples = Seq[(Option[String], Option[String])]

      def valid: Samples = Seq(
        (None, None),
        (Some(""), None),
        (None, Some("")),
        (Some(""), Some("")),
        (Some("foo"), Some("bar"))
      )
      valid.foreach { case (login, pass) => user.copy(ctiLogin = login, ctiPassword = pass) }

      def invalid: Samples = Seq(
        (Some("foo"), None),
        (Some("foo"), Some("")),
        (None, Some("bar")),
        (Some(""), Some("bar"))
      )
      invalid.foreach { case (login, pass) =>
        user.copy(ctiLogin = login, ctiPassword = pass) should throwA[IllegalArgumentException](User.ErrCtiCredentials)
      }

    }

    "prevent too long customCallerId" in new Helper {
      user.copy(customCallerId = Some("x" * (User.CustomCallerIdMaxLength + 1))) should throwA[IllegalArgumentException](User.errorFieldTooLong("customCallerId"))
    }

  }
}

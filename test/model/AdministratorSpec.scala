package model

import java.util.UUID

import model.rights.Role
import org.mockito.Mockito.stub
import org.scalatest.mock.MockitoSugar
import play.api.db.Database
import play.api.test.WithApplication
import play.api.{Application, Configuration}
import play.api.libs.json.Json
import services.AdministratorDb
import testutils.{BeforeAndAfterEach, TestDBUtil, DbTest, RawAdmin}

class AdministratorSpec extends BeforeAndAfterEach with DbTest with MockitoSugar {

  val xivo = insertXivo(Xivo(None, UUID.randomUUID(), "xivo1", "192.168.5.3", "to-xivo1", None, Some("token")))
  val entity1 = insertEntity(Entity(None, mock[CombinedId], "pontorson", "Pontorson", xivo, List(Interval(None, "1000", "1099")), "562000", "default"))
  val entity2 = insertEntity(Entity(None, mock[CombinedId], "avranches", "Avranches", xivo, List(Interval(None, "1100", "1199")), "562001", "default"))
  override lazy val restrictedUrls = List("/testko", "/test_post_ko,POST")

  override protected def beforeEach(): Unit = {
    cleanTable("administrators")
  }

  case class Helper(administratorManager: AdministratorInterface, configuration: Configuration, db: Database,
                    entities: EntityInterface)
  class HelperApp(app: Application) extends WithApplication(app) {
    def get(): Helper = {
      val configuration = app.configuration
      def db = app.injector.instanceOf[Database]
      val entities = mock[EntityInterface]
      val administratorManager = new AdministratorImpl(configuration, db, entities)
      Helper(administratorManager, configuration, db, entities)
    }
  }

  "The UIAdministrator" should {
    "serialize to JSON" in {
      val uia = UIAdministrator(Some(1), "aName", "aLogin", "aPassword", false, false, List(Role(Some(10), "rName")))
      val expected = Json.obj("id" -> 1, "name" -> "aName", "login" -> "aLogin", "password" -> "aPassword",
      "superAdmin" -> false, "ldap" -> false, "roles" -> Json.arr(Json.obj("id" -> 10, "name" -> "rName")))

      Json.toJson(uia) shouldEqual expected
    }

    "create from adminDb and list of roles" in {
      val adb = AdministratorDb(Some(1), "aName", "aLogin", "hashedPassword", false, false)
      val roles = List(Role(Some(10), "rName"))
      UIAdministrator(adb, roles) shouldEqual
        UIAdministrator(Some(1), "aName", "aLogin", "hashedPassword", false, false, roles)
    }
  }

  "The Administrator singleton" should {
    "serialize an Administrator to JSON without the password" in {
      Json.toJson(Administrator(Some(5), "dupond", "Jean Dupond", "test", false, false, List(entity1, entity2))) shouldEqual
        Json.obj("id" -> 5,
        "login" -> "dupond",
        "name" -> "Jean Dupond",
        "password" -> "test",
        "superAdmin" -> false,
        "ldap" -> false,
        "entities" -> List(entity1, entity2))
    }

    "list the existing administrators" in new HelperApp(getApp) {
      val helper = get()
      val admin1 = insertAdmin(Administrator(None, "myadmin", "my admin", "haha", false, false, List(entity1)), "myhash")
      val admin2 = insertAdmin(Administrator(None, "youradmin", "your admin", "hihi", true, false, List()), "myhash")
      stub(helper.entities.getEntity(entity1.id.get)).toReturn(Some(entity1))

      helper.administratorManager.list shouldEqual List(admin1, admin2)
    }

    "create an administrator with a hashed password" in new HelperApp(getApp) {
      val helper = get()
      val admin = Administrator(None, "durand", "Jean Durand", "toto", false, false, List(entity1))

      val newAdmin = helper.administratorManager.create(admin)

      newAdmin.id.isDefined should beTrue
            val rawAdmins = listRawAdmins
      rawAdmins.size shouldEqual 1
      val rawAdmin = rawAdmins.head
      rawAdmin.id shouldEqual newAdmin.id
      rawAdmin.login shouldEqual "durand"
      rawAdmin.name shouldEqual "Jean Durand"
      rawAdmin.entityIds shouldEqual List(entity1.id.get)
      rawAdmin.superAdmin shouldEqual false
      rawAdmin.ldap shouldEqual false
      rawAdmin.hashedPassword.length should beGreaterThan(0)
      rawAdmin.hashedPassword shouldEqual newAdmin.password
    }

    "return the hashed password associated to the login" in new HelperApp(getApp) {
      val helper = get()
      insertAdmin(Administrator(None, "dupond", "Jean Durand", "haha", false, false, List(entity1)), "test_hash")

      helper.administratorManager.getHashedPassword("dupond") shouldEqual Some("test_hash")
      helper.administratorManager.getHashedPassword("durand") shouldEqual None
    }

    "check if the provided password is correct" in new HelperApp(getApp) {
      val helper = get()
      val admin = helper.administratorManager.create( Administrator(None, "dupond", "Jean Durand", "test_password", false, false, List(entity1)))

      helper.administratorManager.passwordMatches("test_password", admin.password) should beTrue
      helper.administratorManager.passwordMatches("wrong_password", admin.password) should beFalse
    }

    "refuse access to the restricted urls to a non superadmin user" in new HelperApp(getApp) {
      val helper = get()
      insertAdmin(Administrator(None, "superadmin", "super admin", "test", true, false, List()), "")
      insertAdmin(Administrator(None, "admin", "admin name", "test", false, false, List(entity1)), "")

      helper.administratorManager.isUrlAllowed("superadmin", "/testok/next", "POST") should beTrue
      helper.administratorManager.isUrlAllowed("superadmin", "/testko/next", "GET") should beTrue
      helper.administratorManager.isUrlAllowed("superadmin", "/test_post_ko/next", "POST") should beTrue
      helper.administratorManager.isUrlAllowed("admin", "/testok/next", "POST") should beTrue
      helper.administratorManager.isUrlAllowed("admin", "/testko/next", "GET") should beFalse
      helper.administratorManager.isUrlAllowed("admin", "/test_post_ko/next", "POST") should beFalse
      helper.administratorManager.isUrlAllowed("admin", "/test_post_ko/next", "GET") should beTrue
    }

    "get an admin by id" in new HelperApp(getApp) {
      val helper = get()
      val admin = insertAdmin(Administrator(None, "test", "test name", "test", true, false, List()), "")

      helper.administratorManager.getAdmin(admin.id.get) shouldEqual Some(admin)
      helper.administratorManager.getAdmin(admin.id.get + 1) shouldEqual None
    }

    "find if admin exists" in new HelperApp(getApp) {
      val helper = get()
      val admin = insertAdmin(Administrator(None, "test", "test name", "test", true, false, List()), "")

      helper.administratorManager.exists(admin.login) shouldEqual true
      helper.administratorManager.exists("noadmin") shouldEqual false
    }

    "update an administrator" in new HelperApp(getApp) {
      val helper = get()
      val oldAdmin = insertAdmin(Administrator(None, "test", "test name", "test", false, false, List(entity1)), "hash")
      val newAdmin = Administrator(oldAdmin.id, "test2", "test second name" ,"newpass", false, false, List(entity2))

      helper.administratorManager.update(oldAdmin, newAdmin)

      listRawAdmins.map(_.copy(hashedPassword = "")) shouldEqual List(RawAdmin(newAdmin.id, newAdmin.login,
        newAdmin.name, "", false, false, List(entity2.id.get)))
    }

    "not update the password if the new password is equal to the old hash" in new HelperApp(getApp) {
      val helper = get()
      val oldAdmin = insertAdmin(Administrator(None, "test", "test name", "test", false, false, List(entity1)), "hash")
      val newAdmin = Administrator(oldAdmin.id, "test2", "test 2 name", "hash", false, false, List(entity2))

      helper.administratorManager.update(oldAdmin, newAdmin)

      listRawAdmins shouldEqual List(RawAdmin(newAdmin.id, newAdmin.login, newAdmin.name, "hash", false, false, List(entity2.id.get)))
    }

    "delete an administrator" in new HelperApp(getApp) {
      val helper = get()
      val admin = insertAdmin(Administrator(None, "test", "test name", "test", false, false, List(entity1)), "hash")

      helper.administratorManager.delete(admin.id.get)

      listRawAdmins.isEmpty should beTrue
    }

    "tell if an admin is the last superadmin" in new HelperApp(getApp) {
      val helper = get()
      val admin = insertAdmin(Administrator(None, "test", "test name", "test", true, false, List()), "hash")
      helper.administratorManager.isLastSuperAdmin(admin.id.get) should beTrue

      val admin2 = insertAdmin(Administrator(None, "test2", "test 2 name", "test", true, false, List()), "hash")
      helper.administratorManager.isLastSuperAdmin(admin.id.get) should beFalse
    }
  }
}

package model

import org.scalatest._
import play.api.libs.json._

class templateEnumsSpec  extends WordSpec with ShouldMatchers {

  "The PeerSipNameMode" should {
    "decode from json" in {
      val expected: PeerSipNameMode.Type = PeerSipNameMode.Auto
      val json = "\"auto\""

      val parsedJson: JsValue = Json.parse(json)

      parsedJson.validate[PeerSipNameMode.Type] match {
        case JsSuccess(o, _) => o shouldEqual expected
        case JsError(error) => fail(JsError.toJson(error).toString)
      }
    }
    "encode to json" in {
      val data: PeerSipNameMode.Type = PeerSipNameMode.Auto
      val expected = JsString("auto")

      val json= Json.toJson(data)
      json shouldEqual(expected)
    }
  }

  "The PeerSipNameMode WebRTC" should {
    "decode from json" in {
      val expected: PeerSipNameMode.Type = PeerSipNameMode.WebRTC
      val json = "\"webrtc\""

      val parsedJson: JsValue = Json.parse(json)

      parsedJson.validate[PeerSipNameMode.Type] match {
        case JsSuccess(o, _) => o shouldEqual expected
        case JsError(error) => fail(JsError.toJson(error).toString)
      }
    }
    "encode to json" in {
      val data: PeerSipNameMode.Type = PeerSipNameMode.WebRTC
      val expected = JsString("webrtc")

      val json= Json.toJson(data)
      json shouldEqual(expected)
    }
  }

  "The PeerSipNameMode Unique Account" should {
    "decode from json" in {
      val expected: PeerSipNameMode.Type = PeerSipNameMode.UniqueAccount
      val json = "\"ua\""

      val parsedJson: JsValue = Json.parse(json)

      parsedJson.validate[PeerSipNameMode.Type] match {
        case JsSuccess(o, _) => o shouldEqual expected
        case JsError(error) => fail(JsError.toJson(error).toString)
      }
    }
    "encode to json" in {
      val data: PeerSipNameMode.Type = PeerSipNameMode.UniqueAccount
      val expected = JsString("ua")

      val json= Json.toJson(data)
      json shouldEqual(expected)
    }
  }

  "The CallerIdMode" should {
    "decode from json" in {
      val expected: CallerIdMode.Type = CallerIdMode.IncomingNo
      val json = "\"incomingNo\""

      val parsedJson: JsValue = Json.parse(json)

      parsedJson.validate[CallerIdMode.Type] match {
        case JsSuccess(o, _) => o shouldEqual expected
        case JsError(error) => fail(JsError.toJson(error).toString)
      }
    }
    "encode to json" in {
      val data: CallerIdMode.Type = CallerIdMode.Anonymous
      val expected = JsString("anonymous")

      val json= Json.toJson(data)
      json shouldEqual(expected)
    }
  }

  "The VoiceMailNumberMode" should {
    "decode from json" in {
      val expected: VoiceMailNumberMode.Type = VoiceMailNumberMode.Custom
      val json = "\"custom\""

      val parsedJson: JsValue = Json.parse(json)

      parsedJson.validate[VoiceMailNumberMode.Type] match {
        case JsSuccess(o, _) => o shouldEqual expected
        case JsError(error) => fail(JsError.toJson(error).toString)
      }
    }
    "encode to json" in {
      val data: VoiceMailNumberMode.Type = VoiceMailNumberMode.ShortNumber
      val expected = JsString("short_number")

      val json= Json.toJson(data)
      json shouldEqual(expected)
    }
  }


}


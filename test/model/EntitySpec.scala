package model

import java.security.InvalidParameterException
import java.util.UUID

import model.rights.{Operation, Permissions, Role, RolePermission}
import org.mockito.Mockito.{stub, verify, reset, never}
import org.scalatest.mock.MockitoSugar
import play.api.Application
import play.api.inject._
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import model.utils.{ConfigParser, XivoConnectorPool}
import org.scalatest.TestData
import org.scalatestplus.play.OneAppPerTest
import services.{AvailableNumbersService, UserFinder, UserManager}
import services.rights.{OperationManager, PermissionManager, RolePermissionManager}
import testutils._
import xivo.ldap.xivoconnection.XivoConnector
import xivo.restapi.model.{Context, ContextInterval}
import scala.collection.JavaConversions._
import scala.util.Success

class EntitySpec extends PlayShouldSpec with OneAppPerTest with DbTest with MockitoSugar with ConnectorProviderStub {

  override var xivoConnector: XivoConnector = _

  override def newAppForTest(testData: TestData): Application = new GuiceApplicationBuilder()
    .configure(testConfig)
    .overrides(bind[UserManager].toInstance(mock[UserManager]))
    .overrides(bind[UserFinder].toInstance(mock[UserFinder]))
    .overrides(bind[XivoInterface].toInstance(mock[XivoInterface]))
    .overrides(bind[RolePermissionManager].toInstance(mock[RolePermissionManager]))
    .overrides(bind[PermissionManager].toInstance(mock[PermissionManager]))
    .overrides(bind[OperationManager].toInstance(mock[OperationManager]))
    .overrides(bind[AvailableNumbersService].toInstance(mock[AvailableNumbersService]))
    .build

  class Helper() {
    List("entities", "entity_intervals", "route", "administrators", "administrator_entity", "xivo")
      .foreach(cleanTable)
    val uuid = UUID.randomUUID()
    val xivo = insertXivo(Xivo(None, uuid, "xivo1", "192.168.1.123", "to-xivo1", None, Some("token")))
    val configParser = mock[ConfigParser]
    val connectorPool = new XivoConnectorPool(configParser) with TestXivoConnectorProvider
    val admins = mock[AdministratorInterface]
    xivoConnector = mock[XivoConnector]
    val users = app.injector.instanceOf(classOf[UserManager])
    val usersDbQueries = app.injector.instanceOf(classOf[UserFinder])
    val xivoManager = app.injector.instanceOf(classOf[XivoInterface])
    val entityManager = app.injector.instanceOf(classOf[EntityImpl])
    val rolePermissionManager = app.injector.instanceOf(classOf[RolePermissionManager])
    val permissionManager = app.injector.instanceOf(classOf[PermissionManager])
    val operationManager = app.injector.instanceOf(classOf[OperationManager])
    val availableNumbersService = app.injector.instanceOf(classOf[AvailableNumbersService])
    entityManager.connectorPool = connectorPool
  }

  "The Entity singleton" should {
    "create an entity with Routed" in new Helper() {
      val entity = Entity(None, CombinedId(xivo.uuid, "avranches"), "avranches", "Avranches", xivo, List(
        Interval(None, "1000", "1099", IntervalRoutingModes.Routed), Interval(None, "200", "209", label = "label")),
        "52000", "default")

      val entityWithId = entityManager.create(entity)

      entityWithId.copy(id = None).copy(intervals=entityWithId.intervals.map(i => i.copy(id=None))) shouldEqual entity
      entityWithId.id.isDefined shouldEqual true
      listEntities shouldEqual List(entityWithId)
      verify(xivoConnector).createContext(new Context("avranches", List(new ContextInterval("1000", "1099", 0), new ContextInterval("200", "209", 0))))
      verify(xivoConnector).createContextInclusion("avranches", "default", 0)
      verify(xivoConnector).createContextInclusion("default", "avranches", 0)
    }

    "create an entity with RoutedWithCustomDirectNumber" in new Helper() {
      val entity = Entity(None, CombinedId(xivo.uuid, "avranches"), "avranches", "Avranches", xivo, List(
        Interval(None, "1000", "1099", IntervalRoutingModes.RoutedWithCustomDirectNumber), Interval(None, "200", "209", label = "label")),
        "52000", "default")

      val entityWithId = entityManager.create(entity)

      entityWithId.copy(id = None).copy(intervals=entityWithId.intervals.map(i => i.copy(id=None))) shouldEqual entity
      entityWithId.id.isDefined shouldEqual true
      listEntities shouldEqual List(entityWithId)
      verify(xivoConnector).createContext(new Context("avranches", List(new ContextInterval("1000", "1099", 0), new ContextInterval("200", "209", 0))))
      verify(xivoConnector).createContextInclusion("avranches", "default", 0)
      verify(xivoConnector).createContextInclusion("default", "avranches", 0)
    }

    "create an entity with RoutedWithDirectNumber" in new Helper() {
      val entity = Entity(None, CombinedId(xivo.uuid, "avranches"), "avranches", "Avranches", xivo, List(
        Interval(None, "1000", "1099", IntervalRoutingModes.RoutedWithDirectNumber), Interval(None, "200", "209", label = "label")),
        "52000", "default")

      val entityWithId = entityManager.create(entity)

      entityWithId.copy(id = None).copy(intervals=entityWithId.intervals.map(i => i.copy(id=None))) shouldEqual entity
      entityWithId.id.isDefined shouldEqual true
      listEntities shouldEqual List(entityWithId)
      verify(xivoConnector).createContext(new Context("avranches", List(new ContextInterval("1000", "1099", 0), new ContextInterval("200", "209", 0))))
      verify(xivoConnector).createContextInclusion("avranches", "default", 0)
      verify(xivoConnector).createContextInclusion("default", "avranches", 0)
    }

    "list existing entities" in new Helper() {
      stub(xivoManager.list).toReturn(List(xivo))
      stub(xivoManager.getXivo(xivo.id.get)).toReturn(Some(xivo))
      val e1 = insertEntity(Entity(None, CombinedId(xivo.uuid, "avranches"), "avranches", "avranches", xivo,
        List(Interval(None, "1000", "1099"), Interval(None, "1200", "1219")), "inbNo", "default"))
      val e2 = insertEntity(Entity(None, CombinedId(xivo.uuid, "cherbourg"), "cherbourg", "cherbourg", xivo,
        List(Interval(None, "2000", "2099", IntervalRoutingModes.RoutedWithDirectNumber, Some("123"))), "inbNo", "default"))
      val c1 = new Context("avranches", List(new ContextInterval("1000", "1099"), new ContextInterval("1200", "1219")))
      val c2 = new Context("cherbourg", List(new ContextInterval("2000", "2099")))

      stub(xivoConnector.listUserContexts()).toReturn(List(c1, c2))
      entityManager.list shouldEqual List(e1, e2)
      listEntities shouldEqual List(e1, e2)
    }

    "list and persist existing entities" in new Helper() {
      stub(xivoManager.list).toReturn(List(xivo))
      stub(xivoManager.getXivo(xivo.id.get)).toReturn(Some(xivo))
      val e1 = insertEntity(Entity(None, CombinedId(xivo.uuid, "avranches"), "avranches", "avranches", xivo,
        List(Interval(None, "1000", "1099"), Interval(None, "1200", "1219")), "inbNo", "default"))

      val c1 = new Context("avranches", List(new ContextInterval("1000", "1099"), new ContextInterval("1200", "1219")))
      val c2 = new Context("cherbourg", List(new ContextInterval("2000", "2099")))

      val e2 = Entity(None, CombinedId(xivo.uuid, "cherbourg"), "cherbourg", "cherbourg", xivo,
        List(Interval(None, "2000", "2099", IntervalRoutingModes.RoutedWithDirectNumber, Some("123"))), "inbNo", "default")

      stub(xivoConnector.listUserContexts()).toReturn(List(c1, c2))
      entityManager.list.size shouldEqual 2
      listEntities.size shouldEqual 2

      entityManager.list.head.name shouldEqual e1.name
      entityManager.list.last.name shouldEqual e2.name
    }

    "list existing entities including those with _ or -" in new Helper() {
      stub(xivoManager.list).toReturn(List(xivo))
      stub(xivoManager.getXivo(xivo.id.get)).toReturn(Some(xivo))
      val e1 = insertEntity(Entity(None, CombinedId(xivo.uuid, "av-ranches"), "av-ranches", "av-ranches", xivo,
        List(Interval(None, "1000", "1099"), Interval(None, "1200", "1219")), "inbNo", "default"))
      val e2 = insertEntity(Entity(None, CombinedId(xivo.uuid, "cher_bourg"), "cher_bourg", "cher_bourg", xivo,
        List(Interval(None, "2000", "2099", IntervalRoutingModes.RoutedWithDirectNumber, Some("123"))), "inbNo", "default"))
      val c1 = new Context("av-ranches", List(new ContextInterval("1000", "1099"), new ContextInterval("1200", "1219")))
      val c2 = new Context("cher_bourg", List(new ContextInterval("2000", "2099")))

      stub(xivoConnector.listUserContexts()).toReturn(List(c1, c2))
      entityManager.list shouldEqual List(e1, e2)
      listEntities shouldEqual List(e1, e2)
    }

    "list existing entities ignoring those not matching the correct pattern" in new Helper() {
      stub(xivoManager.list).toReturn(List(xivo))
      stub(xivoManager.getXivo(xivo.id.get)).toReturn(Some(xivo))
      val e1 = insertEntity(Entity(None, CombinedId(xivo.uuid, "av-ranches"), "av-ranches", "av-ranches", xivo,
        List(Interval(None, "1000", "1099"), Interval(None, "1200", "1219")), "inbNo", "default"))
      val e2 = insertEntity(Entity(None, CombinedId(xivo.uuid, "cher_bourg"), "cher_bourg", "cher_bourg", xivo,
        List(Interval(None, "2000", "2099", IntervalRoutingModes.RoutedWithDirectNumber, Some("123"))), "inbNo", "default"))
      val c1 = new Context("av-ranches", List(new ContextInterval("1000", "1099"), new ContextInterval("1200", "1219")))
      val c2 = new Context("cher_bourg", List(new ContextInterval("2000", "2099")))
      val c3 = new Context("invalid$$$", List(new ContextInterval("3000", "3099")))

      stub(xivoConnector.listUserContexts()).toReturn(List(c1, c2, c3))
      entityManager.list shouldEqual List(e1, e2)
      listEntities shouldEqual List(e1, e2)
    }

    "encode an entity in JSON" in new Helper() {
      val e = Entity(Some(3.toLong), CombinedId(xivo.uuid, "avranches"), "avranches", "Avranches", xivo,
        List(Interval(None, "2000", "2099"), Interval(None, "1100", "1199", IntervalRoutingModes.RoutedWithDirectNumber)), "52000", "default")
      Json.toJson(e) shouldEqual Json.obj(
        "id" -> 3,
        "combinedId" -> s"avranches@${xivo.uuid.toString}",
        "name" -> "avranches",
        "displayName" -> "Avranches",
        "xivo" -> Json.toJson(xivo),
        "intervals" -> List(
          Json.obj("start" -> "2000", "end" -> "2099", "routing_mode" -> "routed", "label" -> "", "start_ext_num" -> "2000", "end_ext_num" -> "2099"),
          Json.obj("start" -> "1100", "end" -> "1199", "routing_mode" -> "with_direct_number", "label" -> "")),
        "presentedNumber" -> "52000",
        "mdsName" -> "default")
    }

    "get an Entity by id" in new Helper() {
      val entity = insertEntity(Entity(None, CombinedId(xivo.uuid, "cherbourg"), "cherbourg", "Cherbourg", xivo, List(Interval(None, "2000", "2099")), "52000", "default"))
      stub(xivoManager.getXivo(xivo.id.get)).toReturn(Some(xivo))

      entityManager.getEntity(entity.id.get) shouldEqual Some(entity)
    }

    "get an Entity by combined id" in new Helper() {
      val entity = insertEntity(Entity(None, CombinedId(xivo.uuid, "cherbourg"), "cherbourg", "cherbourg", xivo, List(Interval(None, "2000", "2099")), "inbNo", "default"))
      stub(xivoManager.getXivo(xivo.id.get)).toReturn(Some(xivo))

      entityManager.getEntity(entity.combinedId) shouldEqual Some(entity)
    }

    "list the available numbers for an entity" in new Helper() {
      val intervals = List(Interval(None, "02000", "02002"), Interval(None, "3020", "3025"))
      val entity = insertEntity(Entity(None, CombinedId(xivo.uuid, "cherbourg"), "cherbourg", "Cherbourg", xivo, intervals, "inbNo", "default"))
      stub(xivoManager.getXivo(xivo.id.get)).toReturn(Some(xivo))
      private val availableNumbers = List("02002", "02001", "02002", "3023", "3024")
      stub(availableNumbersService.findAvailableNumbers(entity.intervals)).toReturn(availableNumbers)

      entityManager.availableNumbers(entity.combinedId, None) shouldEqual availableNumbers
    }

    "list the available numbers for an interval" in new Helper() {
      val intervals = List(Interval(Some(100), "02000", "02002"), Interval(Some(200), "3020", "3025"))
      val entity = insertEntity(Entity(None, CombinedId(xivo.uuid, "cherbourg"), "cherbourg", "Cherbourg", xivo, intervals, "inbNo", "default"))
      val secondInterval = List(entity.intervals(1))
      stub(xivoManager.getXivo(xivo.id.get)).toReturn(Some(xivo))
      private val availableNumbers = List("3020", "3021", "3022", "3023", "3025")
      stub(availableNumbersService.findAvailableNumbers(secondInterval)).toReturn(availableNumbers)

      entityManager.availableNumbers(entity.combinedId, secondInterval.head.id) shouldEqual availableNumbers
    }

    "lookup external number for internal number" in new Helper() {
      val intervals = List(
        Interval(Some(100), "1200", "1299", IntervalRoutingModes.Routed),
        Interval(Some(12), "2000", "2099", IntervalRoutingModes.RoutedWithDirectNumber, Some("0550"))
      )
      val entity = insertEntity(Entity(None, CombinedId(xivo.uuid, "cherbourg"), "cherbourg", "Cherbourg", xivo, intervals, "inbNo", "default"))
      stub(xivoManager.getXivo(xivo.id.get)).toReturn(Some(xivo))

      entityManager.externalNumber(entity.combinedId, "1210") shouldEqual Some("1210")
      entityManager.externalNumber(entity.combinedId, "2017") shouldEqual Some("05502017")
    }

    "list all entities if the user is a superadmin (when getting entity list from XiVO))" in new Helper() {
      stub(xivoManager.list).toReturn(List(xivo))
      val e1 = insertEntity(Entity(None, CombinedId(xivo.uuid, "avranches"), "avranches", "avranches", xivo, List(Interval(None, "1000", "1099"), Interval(None, "1200", "1219")), "inbNo", "default"))
      val e2 = insertEntity(Entity(None, CombinedId(xivo.uuid, "cherbourg"), "cherbourg", "cherbourg", xivo, List(Interval(None, "2000", "2099")), "inbNo", "default"))
      val c1 = new Context("avranches", List(new ContextInterval("1000", "1099"), new ContextInterval("1200", "1219")))
      val c2 = new Context("cherbourg", List(new ContextInterval("2000", "2099")))
      stub(xivoConnector.listUserContexts()).toReturn(List(c1, c2))
      stub(xivoManager.getXivo(xivo.id.get)).toReturn(Some(xivo))

      val superAdmin = UIAdministrator(Some(1), "name", "login", "password", superAdmin = true, ldap = false, List(Role(Some(10), "role")))
      entityManager.listForAdmin(superAdmin, updateFromXivo = true) shouldEqual List(e1, e2)
    }

    "list all entities if the user is a superadmin (when getting entity list from db)" in new Helper() {
      stub(xivoManager.list).toReturn(List(xivo))
      stub(xivoManager.getXivo(xivo.id.get)).toReturn(Some(xivo))

      val e1 = insertEntity(Entity(None, CombinedId(xivo.uuid, "avranches"), "avranches", "avranches", xivo,
        List(Interval(None, "1000", "1099", IntervalRoutingModes.Routed, Some("00150"), "l1"),
        Interval(None, "1200", "1219", IntervalRoutingModes.Routed, Some("0160"), "l2")), "inbNo", "default"))
      val e2 = insertEntity(Entity(None, CombinedId(xivo.uuid, "cherbourg"), "cherbourg", "cherbourg", xivo,
        List(Interval(None, "2000", "2099", IntervalRoutingModes.Routed, Some("170"), "l3")), "inbNo", "default"))

      val superAdmin = UIAdministrator(Some(1), "name", "login", "password", superAdmin = true, ldap = false, List(Role(Some(10), "role")))
      entityManager.listForAdmin(superAdmin, updateFromXivo = false) shouldEqual List(e1, e2)
    }

    "list only the entities readable by a user if he is not superadmin (when getting entity list from XiVO)" in new Helper() {
      stub(xivoManager.list).toReturn(List(xivo))
      val e1 = insertEntity(Entity(None, CombinedId(xivo.uuid, "avranches"), "avranches", "avranches", xivo, List(Interval(None, "1000", "1099"), Interval(None, "1200", "1219")), "inbNo", "default"))
      val e2 = insertEntity(Entity(None, CombinedId(xivo.uuid, "cherbourg"), "cherbourg", "cherbourg", xivo, List(Interval(None, "2000", "2099")), "inbNo", "default"))
      val c1 = new Context("avranches", List(new ContextInterval("1000", "1099"), new ContextInterval("1200", "1219")))
      val c2 = new Context("cherbourg", List(new ContextInterval("2000", "2099")))
      stub(xivoConnector.listUserContexts()).toReturn(List(c1, c2))
      stub(xivoManager.getXivo(xivo.id.get)).toReturn(Some(xivo))
      val admin = UIAdministrator(Some(1), "name", "login", "password", superAdmin = false, ldap = false, List(Role(Some(10), "role")))
      stub(rolePermissionManager.getForRole(10)).toReturn(Success(List(RolePermission(10, 100))))
      stub(permissionManager.get(100)).toReturn(Success(Permissions(Some(100), e2.id.get, 1000, "perm")))
      stub(operationManager.get(1000)).toReturn(Success(Operation(Some(1000), "ope", true, false, false, false)))
      entityManager.listForAdmin(admin, updateFromXivo = true) shouldEqual List(e2)
    }

    "list only the entities readable by a user if he is not superadmin (when getting entity list from db)" in new Helper() {
      stub(xivoManager.list).toReturn(List(xivo))
      val e1 = insertEntity(Entity(None, CombinedId(xivo.uuid, "avranches"), "avranches", "avranches", xivo, List(Interval(None, "1000", "1099"), Interval(None, "1200", "1219")), "inbNo", "default"))
      val e2 = insertEntity(Entity(None, CombinedId(xivo.uuid, "cherbourg"), "cherbourg", "cherbourg", xivo, List(Interval(None, "2000", "2099")), "inbNo", "default"))
      val c1 = new Context("avranches", List(new ContextInterval("1000", "1099"), new ContextInterval("1200", "1219")))
      val c2 = new Context("cherbourg", List(new ContextInterval("2000", "2099")))
      stub(xivoManager.getXivo(xivo.id.get)).toReturn(Some(xivo))
      val admin = UIAdministrator(Some(1), "name", "login", "password", superAdmin = false, ldap = false, List(Role(Some(10), "role")))
      stub(rolePermissionManager.getForRole(10)).toReturn(Success(List(RolePermission(10, 100))))
      stub(permissionManager.get(100)).toReturn(Success(Permissions(Some(100), e2.id.get, 1000, "perm")))
      stub(operationManager.get(1000)).toReturn(Success(Operation(Some(1000), "ope", true, false, false, false)))
      entityManager.listForAdmin(admin, updateFromXivo = false) shouldEqual List(e2)
    }

    "delete an entity, its associated users and the rights associated to it" in new Helper() {
      val entity1 = insertEntity(Entity(None, CombinedId(xivo.uuid, "cherbourg"), "cherbourg", "Cherbourg", xivo, List(Interval(None, "02000", "02010")), "52000", "default"))
      val entity2 = insertEntity(Entity(None, CombinedId(xivo.uuid, "avranches"), "avranches", "Avranches", xivo, List(Interval(None, "03000", "03010")), "52000", "default"))
      val admin = insertAdmin(Administrator(None, "dupond", "Jean Dupond", "test", false, false, List(entity1)), "hash")
      stub(usersDbQueries.listUserIdsForEntity(entity1.id.get)).toReturn(List(2, 3, 4))
      stub(xivoManager.getXivo(xivo.id.get)).toReturn(Some(xivo))

      entityManager.delete(entity1.combinedId)

      verify(users).delete(2, entityManager, "__application", withConfiguration = true)
      verify(users).delete(3, entityManager, "__application", withConfiguration = true)
      verify(users).delete(4, entityManager, "__application", withConfiguration = true)
      verify(xivoConnector).deleteContext(entity1.name)
      listEntities shouldEqual List(entity2)
      listRawAdmins shouldEqual List(RawAdmin(admin.id, admin.login, admin.name, admin.password, admin.superAdmin, admin.ldap, List()))
      listRoutes shouldEqual List()
    }

    "throw an InvalidParameterExcception if the entity does not exist" in new Helper() {
      stub(xivoManager.getXivo(xivo.uuid)).toReturn(None)
      val e = intercept[InvalidParameterException] {
        entityManager.delete(CombinedId(xivo.uuid, "cherbourg"))
      }
      e.getMessage shouldEqual(Entity.ErrorNoSuchEntity.format(CombinedId(xivo.uuid, "cherbourg")))
    }

    "update an entity for a XiVO" in new Helper() {
      val oldEntity = insertEntity(Entity(None, CombinedId(xivo.uuid, "avranches"), "avranches", "Avranche", xivo,
        List(Interval(None, "2000", "2099")), "52000", "default"))
      val newEntity = Entity(oldEntity.id, CombinedId(xivo.uuid, "avranches"), "avranches", "Avranches", xivo, List(
        Interval(oldEntity.intervals.get(0).id, "2000", "2199", label="label"),
        Interval(None, "3000", "3009", IntervalRoutingModes.Routed)), "52001", "default")
      val user1 = User(Some(1), oldEntity, "Jean", "Dupond", "2000", None, None, None, None, None)
      val user2 = User(Some(2), oldEntity, "Pierre", "Durand", "2001", None, None, None, None, None)
      stub(users.getFromXivoAndCacheForEntity(oldEntity)).toReturn(UserList(List(user1, user2), List()))

      val updateResult = entityManager.update(oldEntity, newEntity)
      updateResult.success shouldEqual true
      listEntities shouldEqual List(updateResult.entity)

      verify(xivoConnector).updateContext(new Context("avranches", List(
        new ContextInterval("2000", "2199", 0),
        new ContextInterval("3000", "3009", 0))))
    }

    "update an entity for a proxy" in new Helper() {
      val oldEntity = insertEntity(Entity(None, CombinedId(xivo.uuid, "avranches"), "avranches", "Avranche", xivo.copy(isProxy = true),
        List(Interval(None, "2000", "2099")), "52000", "default"))
      val newEntity = Entity(oldEntity.id, CombinedId(xivo.uuid, "avranches"), "avranches", "Avranches", xivo.copy(isProxy = true), List(
        Interval(oldEntity.intervals.get(0).id, "2000", "2199", label="label"),
        Interval(None, "3000", "3009", IntervalRoutingModes.Routed)), "52001", "default")
      val user1 = User(Some(1), oldEntity, "Jean", "Dupond", "2000", None, None, None, None, None)
      val user2 = User(Some(2), oldEntity, "Pierre", "Durand", "2001", None, None, None, None, None)
      stub(users.getFromXivoAndCacheForEntity(oldEntity)).toReturn(UserList(List(user1, user2), List()))

      val updateResult = entityManager.update(oldEntity, newEntity, configureXivo = false)
      updateResult.success shouldEqual true
      listEntities shouldEqual List(updateResult.entity)

      verify(xivoConnector, never()).updateContext(new Context("avranches", List(
        new ContextInterval("2000", "2199", 0),
        new ContextInterval("3000", "3009", 0))))
    }

    "list all entities but one" in new Helper() {
      stub(xivoManager.list).toReturn(List(xivo))
      val e1 = insertEntity(Entity(None, CombinedId(xivo.uuid, "avranches"), "avranches", "avranches", xivo,
        List(Interval(None, "1000", "1099"), Interval(None, "1200", "1219")), "inbNo", "default"))
      val e2 = insertEntity(Entity(None, CombinedId(xivo.uuid, "cherbourg"), "cherbourg", "cherbourg", xivo,
        List(Interval(None, "2000", "2099")), "inbNo", "default"))
      val c1 = new Context("avranches", List(new ContextInterval("1000", "1099"), new ContextInterval("1200", "1219")))
      val c2 = new Context("cherbourg", List(new ContextInterval("2000", "2099")))
      stub(xivoConnector.listUserContexts()).toReturn(List(c1, c2))
      stub(xivoManager.getXivo(xivo.id.get)).toReturn(Some(xivo))

      entityManager.allBut(e1.combinedId) shouldEqual List(e2)
    }
  }

}

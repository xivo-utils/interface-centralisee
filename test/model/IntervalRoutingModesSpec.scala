package model

import org.scalatest._
import play.api.libs.json._

class IntervalRoutingModesSpec extends WordSpec with ShouldMatchers {

  "The IntervalRoutingModes" should {
    "decode RoutedWithDirectNumber from json" in {
      val expected: IntervalRoutingModes.Type = IntervalRoutingModes.RoutedWithDirectNumber
      val json = "\"with_direct_number\""

      val parsedJson: JsValue = Json.parse(json)

      parsedJson.validate[IntervalRoutingModes.Type] match {
        case JsSuccess(o, _) => o shouldEqual expected
        case JsError(error) => fail(JsError.toJson(error).toString)
      }
    }
    "decode RoutedWithCustomDirectNumber from json" in {
      val expected: IntervalRoutingModes.Type = IntervalRoutingModes.RoutedWithCustomDirectNumber
      val json = "\"with_customized_direct_number\""

      val parsedJson: JsValue = Json.parse(json)

      parsedJson.validate[IntervalRoutingModes.Type] match {
        case JsSuccess(o, _) => o shouldEqual expected
        case JsError(error) => fail(JsError.toJson(error).toString)
      }
    }

    "decode Routed from json" in {
      val expected: IntervalRoutingModes.Type = IntervalRoutingModes.Routed
      val json = "\"routed\""

      val parsedJson: JsValue = Json.parse(json)

      parsedJson.validate[IntervalRoutingModes.Type] match {
        case JsSuccess(o, _) => o shouldEqual expected
        case JsError(error) => fail(JsError.toJson(error).toString)
      }
    }

    "fail properly if the string doesn't correspond to the enum values" in {
      val json = "\"not_fromEnum\""

      val parsedJson: JsValue = Json.parse(json)

      parsedJson.validate[IntervalRoutingModes.Type] match {
        case JsError(error) => error.head._2.head.message shouldEqual("Expected one of routed/with_direct_number/with_customized_direct_number")
        case _ => fail("Expected an error")
      }
    }
    "encode Routed to json" in {
      val data: IntervalRoutingModes.Type = IntervalRoutingModes.Routed
      val expected = JsString("routed")

      val json = Json.toJson(data)
      json shouldEqual (expected)
    }

    "encode RoutedWithDirectNumber to json" in {
      val data: IntervalRoutingModes.Type = IntervalRoutingModes.RoutedWithDirectNumber
      val expected = JsString("with_direct_number")

      val json = Json.toJson(data)
      json shouldEqual (expected)
    }

    "encode RoutedWithCustomDirectNumber to json" in {
      val data: IntervalRoutingModes.Type = IntervalRoutingModes.RoutedWithCustomDirectNumber
      val expected = JsString("with_customized_direct_number")

      val json = Json.toJson(data)
      json shouldEqual (expected)
    }
  }
}

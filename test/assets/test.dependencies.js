EnvJasmine.loadGlobal(EnvJasmine.testDir + "testlib/angular_jasmine_sbt.js");
EnvJasmine.loadGlobal(EnvJasmine.libDir + "angular-mocks.js");
EnvJasmine.loadGlobal(EnvJasmine.libDir + "angular-translate/angular-translate.js");
EnvJasmine.loadGlobal(EnvJasmine.libDir + "angular-translate/angular-translate-loader-partial.js");
EnvJasmine.loadGlobal(EnvJasmine.libDir + "angular-messages/angular-messages.js");
EnvJasmine.loadGlobal(EnvJasmine.libDir + "ui-bootstrap-tpls-0.12.1.min.js");
EnvJasmine.loadGlobal(EnvJasmine.libDir + "jquery-2.0.3.min.js");
EnvJasmine.loadGlobal(EnvJasmine.libDir + "angular-ui-select/select.min.js");
EnvJasmine.loadGlobal(EnvJasmine.libDir + "angular-ui-tree/angular-ui-tree.js");
var tmp = define.amd;
delete define.amd;
EnvJasmine.loadGlobal(EnvJasmine.libDir + "lodash.min.js");
define.amd = tmp;

EnvJasmine.loadGlobal(EnvJasmine.rootDir + "directives/checklist-model.directive.js");

EnvJasmine.loadGlobal(EnvJasmine.rootDir + "modules/administrators.module.js");
EnvJasmine.loadGlobal(EnvJasmine.rootDir + "modules/dashboard.module.js");
EnvJasmine.loadGlobal(EnvJasmine.rootDir + "modules/entities.module.js");
EnvJasmine.loadGlobal(EnvJasmine.rootDir + "modules/templates.module.js");
EnvJasmine.loadGlobal(EnvJasmine.rootDir + "modules/roles.module.js");
EnvJasmine.loadGlobal(EnvJasmine.rootDir + "modules/users.module.js");
EnvJasmine.loadGlobal(EnvJasmine.rootDir + "modules/validation.module.js");
EnvJasmine.loadGlobal(EnvJasmine.rootDir + "modules/xivo.module.js");

EnvJasmine.loadGlobal(EnvJasmine.rootDir + "directives/compare-to.directive.js");
EnvJasmine.loadGlobal(EnvJasmine.rootDir + "services/administrators.factory.js");
EnvJasmine.loadGlobal(EnvJasmine.rootDir + "controllers/administrators.controller.js");

EnvJasmine.loadGlobal(EnvJasmine.rootDir + "controllers/dashboard/dashboard.controller.js");

EnvJasmine.loadGlobal(EnvJasmine.rootDir + "services/entities.factory.js");
EnvJasmine.loadGlobal(EnvJasmine.rootDir + "controllers/entities.controller.js");

EnvJasmine.loadGlobal(EnvJasmine.rootDir + "services/templates.factory.js");
EnvJasmine.loadGlobal(EnvJasmine.rootDir + "controllers/template/templates.controller.js");
EnvJasmine.loadGlobal(EnvJasmine.rootDir + "controllers/template/templateAction.controller.js");

EnvJasmine.loadGlobal(EnvJasmine.rootDir + "services/users.factory.js");
EnvJasmine.loadGlobal(EnvJasmine.rootDir + "controllers/users.controller.js");
EnvJasmine.loadGlobal(EnvJasmine.rootDir + "controllers/userSearch.controller.js");
EnvJasmine.loadGlobal(EnvJasmine.rootDir + "controllers/userCreation.controller.js");
EnvJasmine.loadGlobal(EnvJasmine.rootDir + "controllers/userEdition.controller.js");
EnvJasmine.loadGlobal(EnvJasmine.rootDir + "controllers/userForm.controller.js");

EnvJasmine.loadGlobal(EnvJasmine.rootDir + "controllers/validation.controller.js");

EnvJasmine.loadGlobal(EnvJasmine.rootDir + "services/xivo.factory.js");
EnvJasmine.loadGlobal(EnvJasmine.rootDir + "controllers/xivo.controller.js");

EnvJasmine.loadGlobal(EnvJasmine.rootDir + "services/roles.factory.js");
EnvJasmine.loadGlobal(EnvJasmine.rootDir + "controllers/role/roles.controller.js");
EnvJasmine.loadGlobal(EnvJasmine.rootDir + "controllers/role/roleAction.controller.js");


describe('UsersSearch controller', function() {
    var $rootScope;
    var $scope;
    var window;
    var usersSearchCtrl;
    var roleFactory;
    var response;

    beforeEach(module('roles'));
    beforeEach(module('users', function ($provide, $translateProvider) {

      $provide.factory('customLoader', function ($q) {
        return function () {
          var deferred = $q.defer();
          deferred.resolve({});
          return deferred.promise;
        };
      });

      $translateProvider.useLoader('customLoader');

    }));

    beforeEach(inject(function(RoleFactory, _$rootScope_, $controller, $q) {
        $rootScope =_$rootScope_;
        window = { location: {origin: "test"}};
        $scope = $rootScope.$new();
        roleFactory = RoleFactory;
        response = {ghostIds: [1,2,3]};

        spyOn(roleFactory, 'myself').and.callFake(function() {
            var deferred = $q.defer();
            deferred.resolve({'xivo': [{'xivoId': 1, 'entities': [
            {'id': entityId, 'operations': {'creating': true, 'editing': true, 'deleting': true}}]}]});
            return deferred.promise;
        });

        usersSearchCtrl = $controller('UsersSearchController', {
            '$scope' : $scope, '$window': window, 'RoleFactory': roleFactory});
    }));


    it('can instantiate UsersSearch controller', function(){
        expect(usersSearchCtrl).not.toBeUndefined();
    });

    it('provide utility function to detect if the xivo user is created by GCU and can be edited', function() {
        $scope.canEdit = true;

      var user = {
          id: 2,
          firstName: 'Catherine',
          lastName: 'Johnson',
          provisioningNumber: '147',
          internalNumber: '5022',
          entity: {
              xivo: {
                  isProxy:false
              }
          }
      };
      $rootScope.$digest();

      expect($scope.getEditRoute(user)).toEqual('/users/edit/' + 2);
      expect($scope.editButtonIsEnabled(user)).toBeTruthy();
      expect($scope.canEdit).toBeTruthy();
  });

  it('cannot edit a user on proxy in the search results', function() {
     
    var user = {
        id: 2,
        firstName: 'Catherine',
        lastName: 'Johnson',
        provisioningNumber: '147',
        internalNumber: '5022',
        entity: {
            xivo: {
                isProxy:true
            }
        }
    };
    $rootScope.$digest();

    expect($scope.editButtonIsEnabled(user)).toBeFalsy();
    expect($scope.getEditRoute(user)).toEqual('#');
  });

});
describe('UsersForm controller', function() {
    var $rootScope;
    var $scope;
    var $q;
    var userFormCtrl;
    var entityFactory;
    var entityId = 'entityId';

    var cId = 'combinedId'; // TODO keep?
    var iId = 'intervalId'; // TODO keep?

    beforeEach(module('roles'));
    beforeEach(module('users', function ($provide, $translateProvider) {

      $provide.factory('customLoader', function ($q) {
        return function () {
          var deferred = $q.defer();
          deferred.resolve({});
          return deferred.promise;
        };
      });

      $translateProvider.useLoader('customLoader');

    }));

    beforeEach(inject(function(_EntityFactory_, RoleFactory, _$rootScope_, $controller, $q) {
        $rootScope =_$rootScope_;
        $scope = $rootScope.$new();
        entityFactory = _EntityFactory_;

        spyOn(entityFactory, 'listEntities').and.callFake(function() {
            var deferred = $q.defer();
            deferred.resolve({'items': [ {'id': 1} ]});
            return deferred.promise;
        });
        spyOn(entityFactory, 'getExternalNumberForInternalNumber').and.callFake(function(pcId, internalNumber) {
            if (pcId == cId && internalNumber == "1004") {
                var deferred = $q.defer();
                deferred.resolve({externalNumber: "0101004"});
                return deferred.promise;
            }
            else {
                return 'EntityFactory.getExternalNumberForInternalNumber called with wrong parameters: '
                    + pcId + ' ' + internalNumber;
            }
        });
        spyOn(entityFactory, 'checkExternalNumber').and.callFake(function(eId, intervalId, externalNumber, userId) {
            if (eId == cId && intervalId == $scope.user.intervalId && externalNumber == $scope.user.externalNumber
            && userId == $scope.user.id) {
                var deferred = $q.defer();
                deferred.resolve({available: false, message: "foo bar"});
                return deferred.promise;
            }
            else {
                return 'EntityFactory.checkExternalNumber called with wrong parameters';
            }
        });

        userFormCtrl = $controller('UserFormController', { '$scope' : $scope, 'EntityFactory': entityFactory});
        $scope.$apply();
   }));

    it('can instantiate Users controller', function(){
        expect(userFormCtrl).not.toBeUndefined();
    });

    it('get external number for internal number', function() {
        $scope.entity = {combinedId: cId, isXivoEntity: true};
        $scope.user = {internalNumber: '1004'};
        $scope.userForm = {externalNumber: {$pristine: true} };
        $scope.updateExternalNumberForInternalNumber();
        $scope.$apply();
        expect($scope.user.externalNumber).toEqual('0101004');
    });

    it('do not update external number after user changed it manually', function() {
        $scope.entity = {combinedId: cId};
        $scope.user = {internalNumber: '1004', externalNumber: '009999'};
        $scope.userForm = {externalNumber: {$pristine: false} };
        $scope.updateExternalNumberForInternalNumber();
        $scope.$apply();
        expect($scope.user.externalNumber).toEqual('009999');
    });

    it('get from-to bounds for external number', function() {
        $scope.user = {intervalId: 123};
        $scope.intervals = [{id: 123}];
        expect($scope.externalNumberFromTo()).toEqual('');
        $scope.intervals = [{id: 123, start_ext_num: "001230100", end_ext_num: "001230199"}];
        expect($scope.externalNumberFromTo()).toEqual('(001230100 - 001230199)');
    });

    it('show external number for intervals with customized direct number only', function() {
        $scope.user = {intervalId: 123};
        $scope.intervals = [{id: 123, routing_mode: 'routed'}];
        expect($scope.showExternalNumber()).toEqual(false);
        $scope.intervals = [{id: 123, routing_mode: 'with_customized_direct_number'}];
        expect($scope.showExternalNumber()).toEqual(true);
    });

    it('check external number for intervals with customized direct number only', function() {
        $scope.entity = {combinedId: cId};
        $scope.user = {id: 111, intervalId: 222, externalNumber: "012310555"};
        var result = {available: false, message: "foo bar"};

        $scope.checkExternalNumber();
        $scope.$apply();

        expect($scope.externalNumberError).toEqual(true);
        expect($scope.externalNumberErrMsg).toEqual("foo bar");
    });

    it('show internal number if interval is defined', function() {
        $scope.user = {intervalId: 123};
        $scope.intervals = [{id: 123, routing_mode: 'routed'}];

        expect($scope.showInternalNumber()).toEqual(true);

        $scope.intervals = [];
        expect($scope.showInternalNumber()).toEqual(false);

        $scope.intervals = null;
        expect($scope.showInternalNumber()).toEqual(false);
    });

});
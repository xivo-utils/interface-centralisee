describe('Users creation controller', function() {
    var $rootScope;
    var $scope;
    var window;
    var usersCtrl;
    var userCreationCtrl;
    var entityFactory;
    var templateFactory;
    var response;
    var cId = 'combinedId';
    var iId = 'intervalId';

    beforeEach(module('users', function ($provide, $translateProvider) {

      $provide.factory('customLoader', function ($q) {
        return function () {
          var deferred = $q.defer();
          deferred.resolve({});
          return deferred.promise;
        };
      });

      $translateProvider.useLoader('customLoader');

    }));

    beforeEach(inject(function(_EntityFactory_, _TemplateFactory_, _$rootScope_, $controller, $q) {
        $rootScope =_$rootScope_;
        window = { location: {origin: "test"}};
        $scope = $rootScope.$new();
        entityFactory = _EntityFactory_;
        templateFactory = _TemplateFactory_;
        response = {combinedId: cId};

        spyOn(entityFactory, 'getEntity').and.callFake(function(param) {
            if (param == cId) {
                var deferred = $q.defer();
                deferred.resolve(response);
                return deferred.promise;
            }
            else {
                return 'EntityFactory.getEntity called with wrong combinedId: ' + param;
            }
        });
        spyOn(entityFactory, 'getIntervalAvailableNumbers').and.callFake(function(pcId, piId) {
            if (pcId == cId && piId == iId) {
                var deferred = $q.defer();
                deferred.resolve({items: ['1004', '1122', '1222']});
                return deferred.promise;
            }
            else {
                return 'EntityFactory.getIntervalAvailableNumbers called with wrong parameters: ', pcId, piId;
            }
        });

        userCreationCtrl = $controller('UserCreationController', {
            '$scope' : $scope,
            '$window': window,
            'EntityFactory': entityFactory,
            'TemplateFactory': templateFactory
        });
    }));

    it('can instantiate UserCreationController', function(){
        expect(userCreationCtrl).not.toBeUndefined();
    });

    it('updates user from model', function() {
        $scope.user = {templateId: 4};
        $scope.entity = {};
        $scope.templates = [{id: 4, customCallerId: '4'}, {id: 5, customCallerId: '5'}];
        $scope.updateFromModel();
        expect($scope.user.externalNumber).toEqual('4');
        expect($scope.template).toEqual({id: 4, customCallerId: '4'});
    });

    it('update intervals list from entity and model', function() {
        expect($scope.mailRequired()).toEqual(false);
        $scope.entity = {id: 1, intervals: [{id: 55, name: '55'}, {id: 66, name: '66'}, {id: 88, name: '88'}]};
        $scope.template = {id: 4, intervals: [55, 66, 77], voiceMailSendEmail: true};
        $scope.updateIntervals($scope.entity);
        expect($scope.intervals).toEqual([{id: 55, name: '55'}, {id: 66, name: '66'}]);
        expect($scope.mailRequired()).toEqual(true);
    });

    it('update available numbers', function() {
        $scope.entity = {combinedId: cId};
        $scope.user = {intervalId: iId};

        $scope.container = {};
        $scope.updateAvailableNumbers(true);
        $scope.$apply();
        expect($scope.container.availableNumbers).toEqual(['1004', '1122', '1222']);
        expect($scope.user.internalNumber).toEqual(null);
    });

});

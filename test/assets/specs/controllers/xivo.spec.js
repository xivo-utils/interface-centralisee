describe('Xivo module', function() {
    var $rootScope;
    var $scope;
    var $translateProvider;
    var xivoCtrl;
    var xivoCreationCtrl;
    var proxyCreationCtrl;
    var window = { location: {origin: "test"}};

    beforeEach(module('xivo', function ($provide, $translateProvider) {

      $provide.factory('customLoader', function ($q) {
        return function () {
          var deferred = $q.defer();
          deferred.resolve({});
          return deferred.promise;
        };
      });

      $translateProvider.useLoader('customLoader');

    }));


    beforeEach(module('entities', function ($provide, $translateProvider) {

      $provide.factory('customLoader', function ($q) {
        return function () {
          var deferred = $q.defer();
          deferred.resolve({});
          return deferred.promise;
        };
      });

      $translateProvider.useLoader('customLoader');

    }));

    beforeEach(inject(function(_$rootScope_, $controller) {
        $rootScope =_$rootScope_;
        $scope = $rootScope.$new();
        xivoCtrl = $controller('XivoController', { '$scope' : $scope, '$window': window });
        xivoCreationCtrl = $controller('XivoCreationController', { '$scope' : $scope, '$window': window });
        proxyCreationCtrl = $controller('ProxyCreationController', { '$scope' : $scope, '$window': window });


    }));


    it('can instantiate XivoController', function(){
        expect(xivoCtrl).not.toBeUndefined();
    });

    it('can instantiate XivoCreationController', function(){
        expect(xivoCreationCtrl).not.toBeUndefined();
    });

    it('can instantiate ProxyCreationController', function(){
      expect(proxyCreationCtrl).not.toBeUndefined();
  });

   it('update trunkName as the xivo name is updated', function() {
        $scope.xivo.name = 'xivo name';
        
        $scope.updateXivoContextName();
        expect($scope.xivo.contextName).toEqual('to_xivo_name');
    });
    
    it('update trunkName as the proxy name is updated', function() {
      $scope.proxy.name = 'Proxy Ambrières-les-vallées';

      $scope.updateProxyContextName();
      expect($scope.proxy.contextName).toEqual('to_proxy_ambrieres_les_vallees');
    });
  });

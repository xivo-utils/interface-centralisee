describe('administrators', function() {
    var $rootScope;
    var $scope;
    var ctrl, creationCtrl, editionCtrl, formCtrl;

    beforeEach(module('roles'));
    beforeEach(module('admin', function($provide) {

      $provide.factory('customLoader', function ($q) {
        return function () {
          var deferred = $q.defer();
          deferred.resolve({});
          return deferred.promise;
        };
      });

    }));

    beforeEach(inject(function(_$rootScope_, $controller) {
        $rootScope =_$rootScope_;
        $scope = $rootScope.$new();

        ctrl = $controller('AdminController', {'$scope' : $scope});
        creationCtrl = $controller('AdminCreationController', {'$scope' : $scope});
        editionCtrl = $controller('AdminEditionController', {'$scope' : $scope});
        formCtrl = $controller('AdminFormController', {'$scope' : $scope});
    }));

    it('can instantiate admin controller', function(){
        expect(ctrl).not.toBeUndefined();
    });

    it('can instantiate admin creation controller', function(){
        expect(creationCtrl).not.toBeUndefined();
    });

    it('can instantiate admin edition controller', function(){
        expect(editionCtrl).not.toBeUndefined();
    });

    it('can instantiate admin form controller', function(){
        expect(formCtrl).not.toBeUndefined();
    });

});

'use strict';

describe('RoleController', function() {

    var $rootScope, ctrl, roleFactory, response;
    var id = 12;

    beforeEach(module('roles'));

    beforeEach(module('roles', function ($provide, $translateProvider) {

      $provide.factory('customLoader', function ($q) {
        return function () {
          var deferred = $q.defer();
          deferred.resolve({});
          return deferred.promise;
        };
      });

      $translateProvider.useLoader('customLoader');

    }));

    beforeEach(inject(function(_$rootScope_, $controller, _RoleFactory_, $q) {
        $rootScope =_$rootScope_;
        $scope = $rootScope.$new();
        roleFactory = _RoleFactory_;
        response = [{id: 1}, {id: 2}]
        spyOn(roleFactory, 'list').and.callFake(function() {
            var deferred = $q.defer();
            deferred.resolve(response);
            return deferred.promise;
        });

        spyOn(roleFactory, 'remove').and.callFake(function(param) {
            if (param == id) {
                var deferred = $q.defer();
                deferred.resolve({});
                return deferred.promise;
            } else {
                return('RoleFactory.remove called with wrong parameter ', param);
            }
        });

        ctrl = $controller('Role', { '$scope' : $scope });
    }));

    it('can instantiate Role controller', function() {
        expect(ctrl).not.toBeUndefined();
    });

    it('refreshes roles on init', function() {
        expect(roleFactory.list).toHaveBeenCalled();
        $scope.$digest();
        expect($scope.roles).toEqual(response);
    });

    it('refreshes roles on delete', function() {
        $scope.refreshRoles();
        $scope.removeRole(id);
        expect(roleFactory.remove).toHaveBeenCalled();
        expect(roleFactory.list).toHaveBeenCalled();
        $scope.$digest();
        expect($scope.roles).toEqual(response);
    });

});

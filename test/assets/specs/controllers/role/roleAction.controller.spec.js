'use strict';

describe('RoleActionController', function() {

    var $rootScope, ctrl, roleFactory, response;
    var id = 12;
    var $window= {location: {pathname: "http://testURL/root/testId"}}

    beforeEach(module('roles'));

    beforeEach(module('roles', function ($provide, $translateProvider) {

      $provide.factory('customLoader', function ($q) {
        return function () {
          var deferred = $q.defer();
          deferred.resolve({});
          return deferred.promise;
        };
      });

      $translateProvider.useLoader('customLoader');

      $provide.value('$window', $window);

    }));

    beforeEach(inject(function(_$rootScope_, $controller, _RoleFactory_, $q) {
        $rootScope =_$rootScope_;
        $scope = $rootScope.$new();
        roleFactory = _RoleFactory_;
        response = {xivo: [
            {id: 1, entities: [
                {id: 1, operations:
                    {reading: true, editing: true, deleting: false, updating: true}
                }]
            }]
        };
        spyOn(roleFactory, 'getEmpty').and.callFake(function() {
            var deferred = $q.defer();
            deferred.resolve(response);
            return deferred.promise;
        });

        spyOn(roleFactory, 'get').and.callFake(function(id) {
            var deferred = $q.defer();
            if(id==='testId') {
                deferred.resolve(response);
            } else {
                deferred.resolve({ name: "get role called with wrongId" });
            }
            return deferred.promise;
        });

        ctrl = $controller('RoleAction', { '$scope' : $scope });
    }));

    it('can instantiate RoleAction controller', function() {
        expect(ctrl).not.toBeUndefined();
    });

    it('gets empty role on initCreate', function() {
        $scope.initCreate();
        expect(roleFactory.getEmpty).toHaveBeenCalled();
        $scope.$digest();
        expect($scope.role).toEqual(response);
        expect($scope.role.xivo[0].reading).toEqual(true);
    });

    it('gets role on initEdit', function() {
        $scope.initEdit();
        expect(roleFactory.get).toHaveBeenCalled();
        $scope.$digest();
        expect($scope.role).toEqual(response);
        expect($scope.role.xivo[0].reading).toEqual(true);
    });

    it('sets xivo operation check to true if all the operation is true for all entities', function () {
        var xivo = {id: 1, entities: [{id: 1, operations: {reading: true}}, {id: 2, operations: {reading: true}}]};
        $scope.updateXivoCheck(xivo, 'reading');
        expect(xivo.reading).toEqual(true);
    });

    it('sets xivo operation check to false if all the operation is false for all entities', function () {
        var xivo = {id: 1, entities: [{id: 1, operations: {reading: false}}, {id: 2, operations: {reading: false}}]};
        $scope.updateXivoCheck(xivo, 'reading');
        expect(xivo.reading).toEqual(false);
    });

    it('unset xivo operation if the operation value is not the same for all entities', function () {
        var xivo = {id: 1, reading: true, entities: [{id: 1, operations: {reading: false}}, {id: 2, operations: {reading: true}}]};
        $scope.updateXivoCheck(xivo, 'reading');
        expect(xivo.reading).toBeUndefined();
    });

    it('toggles entity check and update xivo check', function() {
        var entity = {id: 1, operations: {editing: true}};
        var xivo = {id: 1, entities: [entity]};
        $scope.toggleEntityCheck(xivo, entity, 'editing');
        expect(entity.operations.editing).toEqual(false)
        expect(xivo.editing).toEqual(false);
    });

    it('toggles xivo check and updates entities', function() {
        var entities = [
            {id: 1, operations: {deleting: true}},
            {id: 2, operations: {deleting: false}}
        ];
        var xivo = {id: 1, deleting: true, entities: entities};
        $scope.toggleXivoCheck(xivo, 'deleting');
        expect(xivo.deleting).toEqual(false);
        expect(entities[0].operations.deleting).toEqual(false);
    });

});

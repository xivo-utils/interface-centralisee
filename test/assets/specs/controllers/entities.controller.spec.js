describe('Entities module', function() {
    var $rootScope;
    var $scope;
    var $httpBackend;
    var $translateProvider;
    var window = { location: {origin: "test"}};
    var roleFactory;
    var entitiesCtrl;
    var entityCreationCtrl;
    var entityEditionCtrl;

    beforeEach(module('roles'));
    beforeEach(module('entities', function ($provide, $translateProvider) {

      $provide.factory('customLoader', function ($q) {
        return function () {
          var deferred = $q.defer();
          deferred.resolve({});
          return deferred.promise;
        };
      });

      $translateProvider.useLoader('customLoader');

      document.querySelector = function() {
        return { "button": function() {} };
      };

    }));

    beforeEach(inject(function($injector, _$rootScope_, $controller, RoleFactory) {
        $rootScope =_$rootScope_;
        $scope = $rootScope.$new();
        roleFactory = RoleFactory;
        entitiesCtrl = $controller('EntitiesController', { $scope : $scope, $window: window, 'RoleFactory': roleFactory});

        spyOn(roleFactory, 'myself').and.callFake(function() {
            var deferred = $q.defer();
            deferred.resolve({});
            return deferred.promise;
        });

    }));

    it('can instantiate Entities controller', function(){
        expect(entitiesCtrl).not.toBeUndefined();
    });

});

describe('Entities module', function() {
    var $rootScope;
    var $scope;
    var $httpBackend;
    var $translateProvider;
    var window = { location: {origin: "test"}};
    var roleFactory;
    var entitiesCtrl;
    var entityCreationCtrl;
    var entityEditionCtrl;

    beforeEach(module('roles'));
    beforeEach(module('entities', function ($provide, $translateProvider) {

      $provide.factory('customLoader', function ($q) {
        return function () {
          var deferred = $q.defer();
          deferred.resolve({});
          return deferred.promise;
        };
      });

      $translateProvider.useLoader('customLoader');

      document.querySelector = function() {
        return { "button": function() {} };
      };

    }));

    beforeEach(inject(function($injector, _$rootScope_, $controller, RoleFactory) {
        $rootScope =_$rootScope_;
        $scope = $rootScope.$new();
        roleFactory = RoleFactory;
        entityCreationCtrl = $controller('EntityCreationController', { $scope : $scope, $window: window });

        spyOn(roleFactory, 'myself').and.callFake(function() {
            var deferred = $q.defer();
            deferred.resolve({});
            return deferred.promise;
        });

    }));
     
    it('can instantiate EntityCreationController', function(){
        expect(entityCreationCtrl).not.toBeUndefined();
    });

    it('selects media server when requested', function() {
        $scope.preSelectedMds =  {name: 'default'};
        var mds = {name : 'mds1'};  
        $scope.setPreselectedMds(mds);
        expect($scope.preSelectedMds.name).toEqual('mds1');
    });

    it('should return undefined if mdsList does not have mds default or is empty', function(){
        $scope.mdsList = [{name: 'mds1', displayName: 'my mds 1'}, {name: 'mds2', displayName: 'my mds 2'}];
        var preselectedMds = $scope.getPreSelectedMds($scope.mdsList);
        expect(preselectedMds).toBeUndefined();

        $scope.mdsList = [];
        var preselectedMds = $scope.getPreSelectedMds($scope.mdsList);
        expect(preselectedMds).toBeUndefined();

        $scope.mdsList = [{name: 'default', displayName: 'my mds 1'}, {name: 'mds2', displayName: 'my mds 2'}];
        var preselectedMds = $scope.getPreSelectedMds($scope.mdsList);
        expect(preselectedMds).toEqual({name: 'default', displayName: 'my mds 1'});
    });
});

describe('Entities module', function() {
    var $rootScope;
    var $scope;
    var $httpBackend;
    var $translateProvider;
    var window = { location: {origin: "test"}};
    var roleFactory;
    var entitiesCtrl;
    var entityCreationCtrl;
    var entityEditionCtrl;

    beforeEach(module('roles'));
    beforeEach(module('entities', function ($provide, $translateProvider) {

      $provide.factory('customLoader', function ($q) {
        return function () {
          var deferred = $q.defer();
          deferred.resolve({});
          return deferred.promise;
        };
      });

      $translateProvider.useLoader('customLoader');

      document.querySelector = function() {
        return { "button": function() {} };
      };

    }));

    beforeEach(inject(function($injector, _$rootScope_, $controller, RoleFactory) {
        $rootScope =_$rootScope_;
        $scope = $rootScope.$new();
        roleFactory = RoleFactory;
        entityEditionCtrl = $controller('EntityEditionController', { $scope : $scope, $window: window });

        spyOn(roleFactory, 'myself').and.callFake(function() {
            var deferred = $q.defer();
            deferred.resolve({});
            return deferred.promise;
        });

    }));

    it('can instantiate EntityEditionController', function(){
        expect(entityEditionCtrl).not.toBeUndefined();
    });

    it('selects media server when requested', function() {
        $scope.preSelectedMds =  {name: 'mds1'};
        mds = {name : 'mds2'};  
        $scope.setPreselectedMds(mds);
        expect($scope.preSelectedMds.name).toEqual('mds2');
    });

    it('on init, preselected mds should match entity mdsName', function(){
        $scope.entity = {mdsName: 'default'};
        $scope.mdsList = [{name: 'default', displayName: 'my mds 1'}, {name: 'mds2', displayName: 'my mds 2'}];
         var preselectedMds = $scope.getPreSelectedMds($scope.mdsList);
        expect(preselectedMds.name).toEqual($scope.entity.mdsName);
    });
});

describe('EntityFormController', function() {
    var $rootScope;
    var $scope;
    var $httpBackend;
    var $translateProvider;
    var window = { location: {origin: "test"}};
    var entitiesCtrl;
    var entityCreationCtrl;
    var entityEditionCtrl;
    var entityFormCtrl;

    beforeEach(module('entities', function ($provide, $translateProvider) {

      $provide.factory('customLoader', function ($q) {
        return function () {
          var deferred = $q.defer();
          deferred.resolve({});
          return deferred.promise;
        };
      });

      $translateProvider.useLoader('customLoader');

    }));


    beforeEach(inject(function($injector, _$rootScope_, $controller) {
        $rootScope =_$rootScope_;
        $scope = $rootScope.$new();
        entityFormCtrl = $controller('EntityFormController', { $scope : $scope, $window: window });
    }));

    it('can instantiate EntityFormController', function(){
        expect(entityFormCtrl).not.toBeUndefined();
    });

    it('init routing modes', function() {
        expect($scope.routingModes).not.toBeUndefined();
    });

    it('updatesMode when requested', function() {
        var i = {routing_mode: ''};
        var mode = $scope.routingModes[1];
        $scope.updateMode(i, mode);
        expect(i.routing_mode).toEqual($scope.routingModes[1].code);
    });

    it('controls the hiding of the direct number', function() {
        var i = {routing_mode: ''};
        expect($scope.showDirectNumber(i)).toEqual(false);
        i.routing_mode = 'with_direct_number';
        expect($scope.showDirectNumber(i)).toEqual(true);
        i.routing_mode = 'with_customized_direct_number';
        expect($scope.showDirectNumber(i)).toEqual(true);
    });

    it('controls the hiding of the last direct number', function() {
        var i = {routing_mode: ''};
        expect($scope.showLastDirectNumber(i)).toEqual(false);
        i.routing_mode = 'with_direct_number';
        expect($scope.showLastDirectNumber(i)).toEqual(false);
        i.routing_mode = 'with_customized_direct_number';
        expect($scope.showLastDirectNumber(i)).toEqual(true);
    });

    it('calculate last direct number', function() {
        var i1 = { routing_mode: 'with_customized_direct_number', start: "1101", end: "1199", direct_number: "20000" };
        expect($scope.lastDirectNumber(i1)).toEqual("20098");
        var i1 = { routing_mode: 'with_customized_direct_number', start: "1101", end: "1199", direct_number: "0020000" };
        expect($scope.lastDirectNumber(i1)).toEqual("0020098");
        var i1 = { routing_mode: 'with_direct_number', start: "1101", end: "1199", direct_number: "20000" };
        expect($scope.lastDirectNumber(i1)).toEqual("");
        var i1 = { routing_mode: 'with_customized_direct_number', start: "", end: "1199", direct_number: "20000" };
        expect($scope.lastDirectNumber(i1)).toEqual("");
        var i1 = { routing_mode: 'with_customized_direct_number', start: "1101", end: "bbb", direct_number: "20000" };
        expect($scope.lastDirectNumber(i1)).toEqual("");
        var i1 = { routing_mode: 'with_customized_direct_number', start: "1101", end: "1199", direct_number: "***" };
        expect($scope.lastDirectNumber(i1)).toEqual("");
    });

    it('returns the mode name', function() {
        $scope.routingModes = [{code: 'first', name: 'WRONG'}, {code: 'routed', name: 'NAME'}];
        expect($scope.getModeName('routed')).toEqual('NAME');
    });

    it('add interval with routing_mode default value \'routed\r', function() {
        $scope.entity = {intervals: []};
        $scope.addInterval();
        expect($scope.entity.intervals[0].routing_mode).toEqual('routed');
    });
});

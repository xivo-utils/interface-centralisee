'use strict';

describe('TemplateController', function() {

    var $rootScope, ctrl;

    beforeEach(module('templates'));

    beforeEach(inject(function(_$rootScope_, $controller) {
        $rootScope =_$rootScope_;
        $scope = $rootScope.$new();
        ctrl = $controller('Template', { '$scope' : $scope });
    }));

    it('can instantiate Template controller', function() {
        expect(ctrl).not.toBeUndefined();
    });
});

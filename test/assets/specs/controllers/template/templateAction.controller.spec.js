'use strict';

describe('TemplateActionController', function() {

    var $rootScope, $q, ctrl, xivoFactory;
    var $window= {location: {pathname: "http://testURL/root/testId"}}

    beforeEach(function() {
        module('templates');

        module(function($provide) {
            $provide.value('XivoFactory', {
                listXivos: function() {
                    return {
                        then: function(callback) { return callback({items: ["mockedXivoList"]});}
                    };
                }
            });

            $provide.value('TemplateFactory', {
                getTemplate: function(id) {
                    return {
                        then: function(callback) {
                            if(id==='testId') {
                                return callback({
                                    ringingTime: 30,
                                    name: "testTemplateName"
                                });
                            } else {
                                return callback({ name: "get template called with wrongId" });
                            }
                        }
                    };
                }
            });

            $provide.value('$window', $window);
        });
    });

    beforeEach(inject(function(_$rootScope_, _$q_, $controller, _XivoFactory_) {
        $rootScope =_$rootScope_;
        $q = _$q_;
        $scope = $rootScope.$new();
        xivoFactory = _XivoFactory_;
        ctrl = $controller('TemplateAction', { '$scope' : $scope });
    }));

    it('can instantiate TemplateAction controller', function() {
        expect(ctrl).not.toBeUndefined();
    });

    it('init template creation', function() {
        $scope.initCreate();
        expect($scope.template).toEqual({
            name: "",
            peerSipName: "auto",
            callerIdMode: "incomingNo",
            voiceMailEnabled: false,
            voiceMailNumberMode: "short_number",
            xivos: [],
            entities: [],
            ringingTime: 20
        });
        expect($scope.ringTimes.length).toEqual(12)
        expect($scope.xivosList).toEqual(["mockedXivoList"]);
    });

    it('init template edition', function() {
        $scope.initEdit();
        expect($scope.template).toEqual({
            ringingTime: 30,
            name: "testTemplateName"
        });
        expect($scope.ringTimes.length).toEqual(12);
        expect($scope.xivosList).toEqual(["mockedXivoList"]);
    });

    it('extract id from path', function() {
        expect($scope.getId({pathname: 'http://url/test/12'})).toEqual('12');
    });

    it('should update selected entities list', function() {
        $scope.entitiesList = [{id: 1, combinedId: 'id1'}, {id: 2, combinedId: 'id2'}, {id: 3, combinedId: "id3"}];
        $scope.template = {entities: ['id1', 'id2']};
        var expected = [{id: 1, combinedId: 'id1'}, {id: 2, combinedId: 'id2'}];
        $scope.refreshSelectedEntities()
        expect($scope.selectedEntities).toEqual(expected)
    });

    it('should merge intervals into one array', function() {
        var intervals = {0: [15], 1: [27,28]};
        expect($scope.mergeIntervals(intervals)).toEqual([15,27,28]);
    });

    it('should dispatch intervals into arrays by entity combined id', function() {
        var intervals = [15,27,28,30];
        var selectedEntities = [{combinedId: 'er@123-1', intervals: [{id: 15}]},
                                {combinedId: 'gg@123-4', intervals: [{id:27},{id:28}]},
                                {combinedId: 'jj@123-9', intervals: [{id: 1}]}];
        var expected = {'er@123-1': [15], 'gg@123-4': [27, 28]};
        expect($scope.dispatchIntervals(intervals, selectedEntities)).toEqual(expected);
    });
});

describe('Validation module', function() {
    var $rootScope;
    var $scope;
    var validationCtrl;

    beforeEach(module('validation'))

    beforeEach(inject(function(_$rootScope_, $controller) {
        $rootScope =_$rootScope_;
        $scope = $rootScope.$new();
        validationCtrl = $controller('ValidationController', { '$scope' : $scope });
    }));


    it('can instantiate Validation controller', function(){
        expect(validationCtrl).not.toBeUndefined();
    });
});

var URLSearchParams = function (location) {
    this.get = function() {
        return '5022';
    }
}

describe('Users controller', function() {
    var $rootScope;
    var $scope;
    var window;
    var usersCtrl;
    var entityFactory;
    var roleFactory;
    var response;
    var entityId = 'entityId';

    beforeEach(module('roles'));
    beforeEach(module('users', function ($provide, $translateProvider) {

      $provide.factory('customLoader', function ($q) {
        return function () {
          var deferred = $q.defer();
          deferred.resolve({});
          return deferred.promise;
        };
      });

      $translateProvider.useLoader('customLoader');

    }));

    beforeEach(inject(function(_EntityFactory_, RoleFactory, _$rootScope_, $controller, $q, $document) {
        $rootScope =_$rootScope_;
        window = { location: {origin: "test", search: "?entityId=marketingdepartment@d87c4709-e800-436a-a617-72866b9d2480&intnb=5022"}};
        $scope = $rootScope.$new();
        entityFactory = _EntityFactory_;
        roleFactory = RoleFactory;

        response = {
            items: [{
                firstName: 'Jane',
                lastName: 'Doe',
                provisioningNumber: '123456',
                internalNumber: '123',
            },
            {
                firstName: 'Catherine',
                lastName: 'Johnson',
                provisioningNumber: '147',
                internalNumber: '5022'
            }],
            ghostIds: [1,2,3]
        };

        spyOn(entityFactory, 'getCachedEntityUsers').and.callFake(function(param) {
            if (param == entityId) {
                var deferred = $q.defer();
                deferred.resolve(response);
                return deferred.promise;
            }
            else {
                return('EntityFactory.getCachedEntityUsers called with wrong parameter');
            }
        });
        spyOn(entityFactory, 'getUpdatedEntityUsers').and.callFake(function(param) {
            if (param == entityId) {
                var deferred = $q.defer();
                deferred.resolve(response);
                return deferred.promise;
            }
            else {
                return('EntityFactory.getUpdatedEntityUsers called with wrong parameter');
            }
        });
        spyOn(entityFactory, 'getEntity').and.callFake(function() {
            var deferred = $q.defer();
            deferred.resolve({'xivo': {'id': 1}});
            return deferred.promise;
        });
        spyOn(roleFactory, 'myself').and.callFake(function() {
            var deferred = $q.defer();
            deferred.resolve({'xivo': [{'xivoId': 1, 'entities': [
            {'id': entityId, 'operations': {'creating': true, 'editing': true, 'deleting': true}}]}]});
            return deferred.promise;
        });

        usersCtrl = $controller('UsersController', {
            '$scope' : $scope, '$window': window, 'EntityFactory': entityFactory, 'RoleFactory': roleFactory});
    }));


    it('can instantiate Users controller', function(){
        expect(usersCtrl).not.toBeUndefined();
    });

    it('Users controller should load ghost ids on init', function() {
        $scope.init(entityId);
        $rootScope.$digest();

        expect(entityFactory.getCachedEntityUsers).toHaveBeenCalled();
        expect(entityFactory.getUpdatedEntityUsers).toHaveBeenCalled();
        expect(entityFactory.getEntity).toHaveBeenCalled();
        expect($scope.ghostIds).toEqual([1,2,3]);
    });

    it('recognizes when a new user was created with the internal number that is in the url', function() {
        $scope.updateUsers(entityId);
        $rootScope.$digest();
        expect($scope.provisioning.number).toEqual('147');
        expect($scope.provisioning.user).toEqual('Catherine Johnson');
    });

    it('provide utility function to detect if the xivo user is created by GCU and can be edited', function() {
  
        $scope.canEdit = true;

        var user = {
            id: 2,
            firstName: 'Catherine',
            lastName: 'Johnson',
            provisioningNumber: '147',
            internalNumber: '5022',
            entity: {
                xivo: {
                    isProxy:false,
                }
            }
        };
        $rootScope.$digest();
        expect($scope.getEditRoute(user)).toEqual('/users/edit/' + 2);
        expect($scope.editButtonIsEnabled(user)).toBeTruthy();
    });

    it('cannot edit a user on proxy', function() {
     
        var user = {
            id: 2,
            firstName: 'Catherine',
            lastName: 'Johnson',
            provisioningNumber: '147',
            internalNumber: '5022',
            entity: {
                xivo: {
                    isProxy:true
                }
            }
        };
        $rootScope.$digest();

        expect($scope.editButtonIsEnabled(user)).toBeFalsy();
        expect($scope.getEditRoute(user)).toEqual('#');
    });
});
describe('Dashboard module', function() {
    var $rootScope;
    var $scope;

    beforeEach(module('dashboard', function ($provide, $translateProvider) {

      $provide.factory('customLoader', function ($q) {
        return function () {
          var deferred = $q.defer();
          deferred.resolve({});
          return deferred.promise;
        };
      });

      $translateProvider.useLoader('customLoader');

    }));

    beforeEach(inject(function(_$rootScope_, $controller) {
        $rootScope =_$rootScope_;
        $scope = $rootScope.$new();
        dashboardIndexCtrl = $controller('DashboardIndexController', { '$scope' : $scope });
    }));


    it('can instantiate DashboardIndexController', function(){
        expect(dashboardIndexCtrl).not.toBeUndefined();
    });
});

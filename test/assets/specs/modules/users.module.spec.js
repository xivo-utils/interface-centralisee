describe('Users module', function() {
    var $rootScope;
    var $scope;
    var window;
    var usersCtrl;
    var userEditionCtrl;
    var userFormCtrl;

    beforeEach(module('users', function ($provide, $translateProvider) {

      $provide.factory('customLoader', function ($q) {
        return function () {
          var deferred = $q.defer();
          deferred.resolve({});
          return deferred.promise;
        };
      });

      $translateProvider.useLoader('customLoader');

    }));

    beforeEach(inject(function(_EntityFactory_, _$rootScope_, $controller, $q) {
        $rootScope =_$rootScope_;
        window = { location: {origin: "test"}};
        $scope = $rootScope.$new();

        userEditionCtrl = $controller('UserEditionController', {
            '$scope' : $scope, '$window': window });
        userFormCtrl = $controller('UserFormController', {
            '$scope' : $scope, '$window': window});
    }));

    it('can instantiate UserEditionController', function(){
        expect(userEditionCtrl).not.toBeUndefined();
    });

    it('can instantiate UserFormController', function(){
        expect(userFormCtrl).not.toBeUndefined();
    });

});

'use strict';

describe('AdministratorService', function() {

    var $httpBackend, $rootScope, service, baseUrl;
    var window = {location: {origin: 'test'}};

    beforeEach(function() {
        module('admin');
        module(function($provide) {
            $provide.value('$window', window);
        });
    });

    beforeEach(inject(function($injector) {
        $httpBackend = $injector.get('$httpBackend');
        $rootScope = $injector.get('$rootScope');
        service = $injector.get('AdministratorFactory');
        baseUrl = window.location.origin + '/api/1.0/administrators';
    }));

    beforeEach(function () {
        $httpBackend.when(
            'GET',
             '/assets/i18n/form-fr.json'
        ).respond({'mock': 'mock'});
        $httpBackend.when(
            'GET',
             '/assets/i18n/templates-fr.json'
        ).respond({'mock': 'mock'});
    });

    afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it('should list administrators', function() {
        var administrators = [{'id': 1, 'login': 'admin1', 'name': 'Administrator 1'}, {'id': 2, 'login': 'admin2', 'name': 'Administrator 2'}];
        $httpBackend.expectGET(baseUrl)
            .respond(function(method, url, data, headers, params) {
                return [200, administrators];
        });
        var result;
        service.listAdministrators().then(function(data) {
            result = data
        });
        $httpBackend.flush();
        expect(result).toEqual(administrators);
    });

    it('should get administrator by id', function() {
        var administrator = {'id': 1, 'login': 'admin1', 'name': 'Administrator 1'};
        $httpBackend.expectGET(baseUrl + '/1')
            .respond(function(method, url, data, headers, params) {
                return [200, administrator];
        });
        var result;
        service.getAdministrator(1).then(function(data) {
            result = data
        })
        $httpBackend.flush();
        expect(result).toEqual(administrator);
    });

    it('should create administrator', function() {
        var administrator = {'id': 3, 'login': 'admin3', 'name': 'Administrator 3'};
        $httpBackend.expectPOST(baseUrl, administrator)
            .respond(201, 'Created');
        var result;
        service.createAdministrator(administrator).then(function(data) {
            result = data
        });
        $httpBackend.flush();
        expect(result).toEqual(201);
    });

    it('should edit administrator', function() {
        var administrator = {'id': 3, 'login': 'admin3-new', 'name': 'Administrator 3 New'};
        $httpBackend.expectPUT(baseUrl + '/3', administrator)
            .respond(204, 'Edited');
        var result;
        service.editAdministrator(3, administrator).then(function(data) {
            result = data
        });
        $httpBackend.flush();
        expect(result).toEqual(204);
    });

    it('should delete administrator', function() {
        $httpBackend.expectDELETE(baseUrl + '/3')
            .respond(204, 'Deleted');
        var result;
        service.deleteAdministrator(3).then(function(data) {
            result = data
        });
        $httpBackend.flush();
        expect(result).toEqual(204);
    });

});

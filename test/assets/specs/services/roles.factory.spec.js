'use strict';

describe('RoleService', function() {

    var $httpBackend, $rootScope, service, baseUrl;
    var window = {location: {origin: 'test'}};

    beforeEach(module('roles', function ($provide, $translateProvider) {

      $provide.factory('customLoader', function ($q) {
        return function () {
          var deferred = $q.defer();
          deferred.resolve({});
          return deferred.promise;
        };
      });

      $translateProvider.useLoader('customLoader');

    }));

    beforeEach(function() {
        module('roles');
        module(function($provide) {
            $provide.value('$window', window);
        });
    });

    beforeEach(inject(function($injector) {
        $httpBackend = $injector.get('$httpBackend');
        $rootScope = $injector.get('$rootScope');
        service = $injector.get('RoleFactory');
        baseUrl = window.location.origin + '/api/1.0/roles';
    }));

    beforeEach(function () {
        $httpBackend.when(
            'GET',
             '/assets/i18n/form-fr.json'
        ).respond({'mock': 'mock'});
        $httpBackend.when(
            'GET',
             '/assets/i18n/templates-fr.json'
        ).respond({'mock': 'mock'});
    });

    afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it('should list roles', function() {
        var roles = [{'id': 1, 'name': 'test1'}, {'id': 2, 'name': 'other'}];
        $httpBackend.expectGET(window.location.origin + '/api/1.0/roles')
            .respond(function(method, url, data, headers, params) {
                return [200, roles];
        });
        var result
        service.list().then(function(data) {
            result = data
        });
        $httpBackend.flush();
        expect(result).toEqual(roles);
    });

    it('should get role by id', function() {
        var role = {'id': 3, 'name': 'test3'};
        $httpBackend.expectGET(window.location.origin + '/api/1.0/roles/3')
            .respond(function(method, url, data, headers, params) {
                return [200, role];
        });
        var result;
        service.get('3').then(function(data) {
            result = data
        })
        $httpBackend.flush();
        expect(result).toEqual(role);
    });

    it('should get empty role', function() {
        var emptyRole = {'name': 'empty', "xivo": []};
        $httpBackend.expectGET(window.location.origin + '/api/1.0/emptyRole')
            .respond(function(method, url, data, headers, params) {
                return [200, emptyRole];
        });
        var result;
        service.getEmpty().then(function(data) {
            result = data
        })
        $httpBackend.flush();
        expect(result).toEqual(emptyRole);
    });

    it('should get its own role', function() {
        var role = {'id': 1, 'name': 'myself', "xivo": []};
        $httpBackend.expectGET(window.location.origin + '/api/1.0/myself')
            .respond(function(method, url, data, headers, params) {
                return [200, role];
        });
        var result;
        service.myself().then(function(data) {
            result = data
        })
        $httpBackend.flush();
        expect(result).toEqual(role);
    });

    it('should create role', function() {
        var role = {'id': 4, 'name': 'test4'};
        $httpBackend.expectPOST(window.location.origin + '/api/1.0/roles', role)
            .respond(201, 'Created');
        var result;
        service.create(role).then(function(data) {
            result = data
        });
        $httpBackend.flush();
        expect(result).toEqual(201);
    });

    it('should edit role', function() {
        var role = {'id': 4, 'name': 'test4'};
        $httpBackend.expectPUT(window.location.origin + '/api/1.0/roles/4', role)
            .respond(204, 'Edited');
        var result;
        service.update(role).then(function(data) {
            result = data
        });
        $httpBackend.flush();
        expect(result).toEqual(204);
    });

    it('should delete role', function() {
        $httpBackend.expectDELETE(baseUrl + '/3')
            .respond(204, 'Deleted');
        var result;
        service.remove(3).then(function(data) {
            result = data
        });
        $httpBackend.flush();
        expect(result).toEqual(204);
    });

});

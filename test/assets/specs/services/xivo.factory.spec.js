'use strict';

describe('XivoService', function() {

    var $httpBackend, $rootScope, service, baseUrl;
    var window = {location: {origin: 'test'}};

    beforeEach(function() {
        module('xivo');
        module(function($provide) {
            $provide.value('$window', window);
        });
    });

    beforeEach(inject(function($injector) {
        $httpBackend = $injector.get('$httpBackend');
        $rootScope = $injector.get('$rootScope');
        service = $injector.get('XivoFactory');
        baseUrl = window.location.origin + '/api/1.0/xivo';
    }));

    beforeEach(function () {
        $httpBackend.when(
            'GET',
             '/assets/i18n/form-fr.json'
        ).respond({'mock': 'mock'});
        $httpBackend.when(
            'GET',
             '/assets/i18n/templates-fr.json'
        ).respond({'mock': 'mock'});
    });

    afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it('should list xivos', function() {
        var xivos = [{'id': 1, 'name': 'xivo1', 'host': '192.168.56.2'}, {'id': 2, 'name': 'xivo2', 'host': 'xivo.avencall.com'}];
        $httpBackend.expectGET(baseUrl)
            .respond(function(method, url, data, headers, params) {
                return [200, xivos];
        });
        var result;
        service.listXivos().then(function(data) {
            result = data
        });
        $httpBackend.flush();
        expect(result).toEqual(xivos);
    });

    it('should get xivo by id', function() {
        var xivo = {'id': 3, 'name': 'xivo3', 'host': 'xivo3.avencall.com'};
        $httpBackend.expectGET(baseUrl + '/3')
            .respond(function(method, url, data, headers, params) {
                return [200, xivo];
        });
        var result;
        service.getXivo('3').then(function(data) {
            result = data
        })
        $httpBackend.flush();
        expect(result).toEqual(xivo);
    });

    it('should create xivo', function() {
        var template = {'id': 4, 'name': 'xivo4', 'host': 'xivo4.avencall.com'};
        $httpBackend.expectPOST(baseUrl, template)
            .respond(201, 'Created');
        var result;
        service.createXivo(template).then(function(data) {
            result = data
        });
        $httpBackend.flush();
        expect(result.status).toEqual(201);
    });

    it('should synchronize config files of all xivos', function() {
        var xivos = [{'id': 5, 'name': 'xivo5', 'host': 'xivo5.avencall.com'}, {'id': 6, 'name': 'xivo6', 'host': 'xivo6.avencall.com'}];
        $httpBackend.expectGET(baseUrl + '/synchronize_config_files')
            .respond(function(method, url, data, headers, params) {
                return [200, ""];
        });
        var result;
        service.synchronizeConfigFiles().then(function(data) {
            result = data
        })
        $httpBackend.flush();
        expect(result).toEqual("");
    });

});

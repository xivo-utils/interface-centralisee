'use strict';

describe('UserService', function() {

    var $httpBackend, $rootScope, service, baseUrl;
    var window = {location: {origin: 'test'}};

    beforeEach(module('users', function ($provide, $translateProvider) {

      $provide.factory('customLoader', function ($q) {
        return function () {
          var deferred = $q.defer();
          deferred.resolve({});
          return deferred.promise;
        };
      });

      $translateProvider.useLoader('customLoader');

    }));

    beforeEach(function() {
        module('users');
        module(function($provide) {
            $provide.value('$window', window);
        });
    });

    beforeEach(inject(function($injector) {
        $httpBackend = $injector.get('$httpBackend');
        $rootScope = $injector.get('$rootScope');
        service = $injector.get('UserFactory');
        baseUrl = window.location.origin + '/api/1.0/users';
        MdsDisplayNameUrl = window.location.origin + '/api/1.0/mediaservers/xivo';
    }));

    beforeEach(function () {
        $httpBackend.when(
            'GET',
             '/assets/i18n/form-fr.json'
        ).respond({'mock': 'mock'});
        $httpBackend.when(
            'GET',
             '/assets/i18n/templates-fr.json'
        ).respond({'mock': 'mock'});
    });

    afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it('should get user by id', function() {
        var user = {'id': 1, 'firstName': 'Jon', 'lastName': 'Snow'};
        $httpBackend.expectGET(baseUrl + '/1')
            .respond(function(method, url, data, headers, params) {
                return [200, user];
        });
        var result;
        service.getUser(1).then(function(data) {
            result = data
        })
        $httpBackend.flush();
        expect(result).toEqual(user);
    });

    it('should create user on xivo', function() {
        var user = {'id': 2, 'firstName': 'Sansa', 'lastName': 'Stark'};
        $httpBackend.expectPOST(baseUrl, user)
            .respond(201, 'Created');
        var result;
        service.createUser(user, true).then(function(data) {
            result = data
        });
        $httpBackend.flush();
        expect(result).toEqual(201);
    });

    it('should create user on proxy', function() {
        var user = {'id': 2, 'firstName': 'Sansa', 'lastName': 'Stark'};
        var configuration = '?withConfiguration=false';
        $httpBackend.expectPOST(baseUrl + configuration, user)
            .respond(201, 'Created');
        var result;
        service.createUser(user, false).then(function(data) {
            result = data
        });
        $httpBackend.flush();
        expect(result).toEqual(201);
    });
  
    it('should edit user on xivo', function() {
        var user = {'id': 2, 'firstName': 'Sansa', 'lastName': 'Stark'};
        $httpBackend.expectPUT(baseUrl + '/2', user)
            .respond(204, 'Edited');
        var result;
        service.editUser(2, user, true).then(function(data) {
            result = data
        });
        $httpBackend.flush();
        expect(result).toEqual(204);
    });

    it('should edit user on proxy', function() {
        var user = {'id': 2, 'firstName': 'Sansa', 'lastName': 'Stark'};
        var configuration = '?withConfiguration=false';
        $httpBackend.expectPUT(baseUrl + '/2' + configuration, user)
            .respond(204, 'Edited');
        var result;
        service.editUser(2, user, false).then(function(data) {
            result = data
        });
        $httpBackend.flush();
        expect(result).toEqual(204);
    });

    it('should delete user on xivo', function() {
        $httpBackend.expectDELETE(baseUrl + '/3')
            .respond(204, 'Deleted');
        var result;
        service.deleteUser(3, true).then(function(data) {
            result = data
        });
        $httpBackend.flush();
        expect(result).toEqual(204);
    });

    it('should delete user on proxy', function() {
        $httpBackend.expectDELETE(baseUrl + '/3?withConfiguration=false')
            .respond(204, 'Deleted');
        var result;
        service.deleteUser(3, false).then(function(data) {
            result = data
        });
        $httpBackend.flush();
        expect(result).toEqual(204);
    });

    it('should import users from CSV', function() {
        var usersCSV = "Jon,Snow,J. Snow,1000\nSansa,Stark, S. Stark,1001\n";
        $httpBackend.expectPOST(baseUrl + '/csv', usersCSV)
            .respond(201, 'Created');
        var result;
        service.importUsersFromCSV(usersCSV).then(function(data) {
            result = data
        });
        $httpBackend.flush();
        expect(result).toEqual(201);
    });

    it('should search users', function() {
        var query = 'someQuery'
        var user = {'id': 1, 'firstName': 'Jon', 'lastName': 'Snow'};
        $httpBackend.expectGET(baseUrl + '/search?query=' + query)
            .respond(function(method, url, data, headers, params) {
                return [200, [user]];
        });
        var result;
        service.search(query).then(function(data) {
            result = data
        })
        $httpBackend.flush();
        expect(result).toEqual([user]);
    });

    it('should get the mds displaName', function(){
        var mdsDisplayName = [{ 'id': 1, 'displayName': 'My Mds 1', 'mdsName': 'mds1'}, {'id': 2, 'displayName': 'My Mds 2', 'mdsName': 'mds2'}];

        $httpBackend.expectGET(MdsDisplayNameUrl + '/' + 2 + '/' + 'mds2')
            .respond(function(method, url, data, headers, params) {
                return [200, mdsDisplayName];
        });
        var result;
        service.getMdsDisplayName(2, 'mds2').then(function(data) {
            result = data
        });
        $httpBackend.flush();
        expect(result).toEqual(mdsDisplayName);
    });

    it('should generate random passwords', function(){
      var regex = service.getPasswordPolicy();
      var password;
      for (i = 0; i < 10; i++) {
        password = service.generatePassword(8);
        expect(password.match(regex)).toBeTruthy();
        expect(password.length).toBe(8);
      }
    });

});

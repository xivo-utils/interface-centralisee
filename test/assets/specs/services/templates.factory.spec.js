'use strict';

describe('TemplateService', function() {

    var $httpBackend, $rootScope, service, baseUrl;
    var window = {location: {origin: 'test'}};

    beforeEach(module('templates', function ($provide, $translateProvider) {

      $provide.factory('customLoader', function ($q) {
        return function () {
          var deferred = $q.defer();
          deferred.resolve({});
          return deferred.promise;
        };
      });

      $translateProvider.useLoader('customLoader');

    }));

    beforeEach(function() {
        module('templates');
        module(function($provide) {
            $provide.value('$window', window);
        });
    });

    beforeEach(inject(function($injector) {
        $httpBackend = $injector.get('$httpBackend');
        $rootScope = $injector.get('$rootScope');
        service = $injector.get('TemplateFactory');
        baseUrl = window.location.origin + '/api/1.0/templates';
    }));

    beforeEach(function () {
        $httpBackend.when(
            'GET',
             '/assets/i18n/form-fr.json'
        ).respond({'mock': 'mock'});
        $httpBackend.when(
            'GET',
             '/assets/i18n/templates-fr.json'
        ).respond({'mock': 'mock'});
    });

    afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it('should list templates', function() {
        var templates = [{'id': 1, 'name': 'test1'}, {'id': 2, 'name': 'other'}];
        $httpBackend.expectGET(window.location.origin + '/api/1.0/templates')
            .respond(function(method, url, data, headers, params) {
                return [200, templates];
        });
        var result
        service.listTemplates().then(function(data) {
            result = data
        });
        $httpBackend.flush();
        expect(result).toEqual(templates);
    });

    it('should list templates for entity', function() {
        var templates = [{'id': 1, 'name': 'test1'}, {'id': 2, 'name': 'other'}];
        $httpBackend.expectGET(window.location.origin + '/api/1.0/templates?entityId=uuid@test')
            .respond(function(method, url, data, headers, params) {
                return [200, templates];
        });
        var result
        service.listForEntity("uuid@test").then(function(data) {
            result = data
        });
        $httpBackend.flush();
        expect(result).toEqual(templates);
    });

    it('should get template by id', function() {
        var template = {'id': 3, 'name': 'test3'};
        $httpBackend.expectGET(window.location.origin + '/api/1.0/templates/3')
            .respond(function(method, url, data, headers, params) {
                return [200, template];
        });
        var result;
        service.getTemplate('3').then(function(data) {
            result = data
        })
        $httpBackend.flush();
        expect(result).toEqual(template);
    });

    it('should create template', function() {
        var template = {'id': 4, 'name': 'test4'};
        $httpBackend.expectPOST(window.location.origin + '/api/1.0/templates', template)
            .respond(201, 'Created');
        var result;
        service.createTemplate(template).then(function(data) {
            result = data
        });
        $httpBackend.flush();
        expect(result).toEqual(201);
    });

    it('should edit template', function() {
        var template = {'id': 4, 'name': 'test4'};
        $httpBackend.expectPUT(window.location.origin + '/api/1.0/templates/4', template)
            .respond(204, 'Edited');
        var result;
        service.updateTemplate(template).then(function(data) {
            result = data
        });
        $httpBackend.flush();
        expect(result).toEqual(204);
    });

    it('should delete template', function() {
        $httpBackend.expectDELETE(baseUrl + '/3')
            .respond(204, 'Deleted');
        var result;
        service.deleteTemplate(3).then(function(data) {
            result = data
        });
        $httpBackend.flush();
        expect(result).toEqual(204);
    });

});

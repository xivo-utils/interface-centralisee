'use strict';

describe('EntityService', function() {

    var $httpBackend, $rootScope, service, baseUrl, mediaServerUrl, MdsDisplayNameUrl;
    var window = {location: {origin: 'test'}};

    beforeEach(function() {
        module('entities');
        module(function($provide) {
            $provide.value('$window', window);
        });
    });

    beforeEach(inject(function($injector) {
        $httpBackend = $injector.get('$httpBackend');
        $rootScope = $injector.get('$rootScope');
        service = $injector.get('EntityFactory');
        baseUrl = window.location.origin + '/api/1.0/entities';
        mediaServerUrl = window.location.origin + '/api/1.0/mediaservers/xivo';
    }));

    beforeEach(function () {
        $httpBackend.when(
            'GET',
             '/assets/i18n/form-fr.json'
        ).respond({'mock': 'mock'});
        $httpBackend.when(
            'GET',
             '/assets/i18n/templates-fr.json'
        ).respond({'mock': 'mock'});
    });

    afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it('should list entities', function() {
        var entities = [{'id': 1, 'name': 'entity1'}, {'id': 2, 'name': 'entity2'}];
        $httpBackend.expectGET(baseUrl)
            .respond(function(method, url, data, headers, params) {
                return [200, entities];
        });
        var result;
        service.listEntities().then(function(data) {
            result = data
        });
        $httpBackend.flush();
        expect(result).toEqual(entities);
    });

    it('should get entity by id', function() {
        var entity = {'id': 1, 'name': 'entity1'};
        $httpBackend.expectGET(baseUrl + '/1')
            .respond(function(method, url, data, headers, params) {
                return [200, entity];
        });
        var result;
        service.getEntity(1).then(function(data) {
            result = data
        })
        $httpBackend.flush();
        expect(result).toEqual(entity);
    });

    it('should create entity', function() {
        var entity = {'id': 3, 'name': 'entity3'};
        $httpBackend.expectPOST(baseUrl, entity)
            .respond(201, 'Created');
        var result;
        service.createEntity(entity).then(function(data) {
            result = data
        });
        $httpBackend.flush();
        expect(result).toEqual(201);
    });

    it('should edit entity', function() {
        var entity = {'id': 3, 'name': 'entity3-new'};
        $httpBackend.expectPUT(baseUrl + '/3', entity)
            .respond(200, 'Edited');
        var result;
        service.editEntity(3, entity).then(function(data) {
            result = data
        });
        $httpBackend.flush();
        expect(result.status).toEqual(200);
    });

    it('should delete entity', function() {
        $httpBackend.expectDELETE(baseUrl + '/3')
            .respond(204, 'Deleted');
        var result;
        service.deleteEntity(3).then(function(data) {
            result = data
        });
        $httpBackend.flush();
        expect(result).toEqual(204);
    });

    it('should list available numbers of an entity', function() {
        var availableNumbers = ['1000', '1001', '1003'];
        $httpBackend.expectGET(baseUrl + '/1/available_numbers')
            .respond(function(method, url, data, headers, params) {
                return [200, availableNumbers];
        });
        var result;
        service.getEntityAvailableNumbers(1).then(function(data) {
            result = data
        })
        $httpBackend.flush();
        expect(result).toEqual(availableNumbers);
    });

    it('should list available numbers of an interval', function() {
        var availableNumbers = ['1000', '1001', '1003'];
        $httpBackend.expectGET(baseUrl + '/1/available_numbers?interval=2')
            .respond(function(method, url, data, headers, params) {
                return [200, availableNumbers];
        });
        var result;
        service.getIntervalAvailableNumbers(1, 2).then(function(data) {
            result = data
        })
        $httpBackend.flush();
        expect(result).toEqual(availableNumbers);
    });

    it('find corresponding external number for internal number', function() {
        var expected = {externalNumber: "00101234"};
        $httpBackend.expectGET(baseUrl + '/1/external_number/1234')
            .respond(function(method, url, data, headers, params) {
                return [200, expected];
        });
        var result;
        service.getExternalNumberForInternalNumber(1, 1234).then(function(data) {
            result = data
        })
        $httpBackend.flush();
        expect(result).toEqual(expected);
    });

    it('check availability of external number', function() {
        var expected = {available: false, message: "foo bar"};
        $httpBackend.expectGET(baseUrl + '/1/check_external_number/3/00101234?userId=55')
            .respond(function(method, url, data, headers, params) {
                return [200, expected];
        });
        var result;
        service.checkExternalNumber(1, 3, "00101234", 55).then(function(data) {
            result = data
        })
        $httpBackend.flush();
        expect(result).toEqual(expected);
    });

    it('should delete stale users', function() {
        $httpBackend.expectGET(baseUrl + '/entityId/delete_stale_users')
            .respond(200, 'OK');
        var result;
        service.deleteStaleUsers('entityId').then(function(response) {
            result = response;
        });
        $httpBackend.flush();
        expect(result.status).toEqual(200);
    });

    it('should list mds for the selected xivo', function(){        
        var mds = [{'id': 1, 'name': 'default1'},{'id': 2, 'name': 'mds2'}];

        $httpBackend.expectGET(mediaServerUrl + '/' + 1)
            .respond(function(method, url, data, headers, params) {
                return [200, mds];
        });
        var result;
        service.getMdsListForSelectedXivo(1).then(function(data) {
            result = data
        });
        $httpBackend.flush();
        expect(result).toEqual(mds);
    });

});

describe('compare-to', function() {
    var $compile, $scope, directiveElem;

    beforeEach(function(){
        module('admin', function ($provide, $translateProvider) {

            $provide.factory('customLoader', function ($q) {
                return function () {
                    var deferred = $q.defer();
                    deferred.resolve({});
                    return deferred.promise;
                };
            });

            $translateProvider.useLoader('customLoader');
        });

        inject(function(_$compile_, _$rootScope_){
            $compile = _$compile_;
            $scope = _$rootScope_.$new();
        });

        directiveElem = getCompiledElement();
    });

    function getCompiledElement(){
        var element = angular.element('<input ng-model="confirmPassword" compare-to="password"></input>');
        var compiledElement = $compile(element)($scope);
        return compiledElement;
    }

    it('can instantiate element without exception', function(){
        expect(directiveElem).not.toBeUndefined();
    });

    it('set "ng-valid" class on the input element if the value corresponds to the referenced one', function() {
        $scope.password='test';
        $scope.confirmPassword='test';
        $scope.$digest();
        expect(directiveElem[1].className.indexOf('ng-valid') > -1).toBe(true);
    });

    it('set "ng-invalid" class on the input element if the value is different from the referenced one', function() {
        $scope.password='test';
        $scope.confirmPassword='tttt';
        $scope.$digest();
        expect(directiveElem[1].className.indexOf('ng-invalid') > -1).toBe(true);
    });

});

package integration

import controllers.Login
import model.AdministratorInterface
import org.mockito.Mockito._
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach}
import play.api.inject._
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.test.WithApplication
import testutils.IntegrationSpec
import sys.process._
import play.api.test._
import java.io.File

class LdapLoginSpec extends IntegrationSpec with BeforeAndAfterEach with BeforeAndAfterAll {

  val pwd = new File(".").getAbsolutePath()
  val scriptDir = s"$pwd/test/resources/ldap/"
  val startScript = Process(scriptDir + "runLdap.sh")
  val stopScript = Process(scriptDir + "stopLdap.sh")
  val admins = mock[AdministratorInterface]

  override def app = new GuiceApplicationBuilder()
    .configure(testConfig)
    .overrides(bind[AdministratorInterface].to(admins))
    .build

  override protected def beforeAll(): Unit = {
    val start = startScript.!
    if (start != 0) { stopScript.! }
    require(start == 0, "unable to start dockerized LDAP")
  }

  override protected def afterAll(): Unit = {
    if (stopScript.! > 0) {
      require(false, "Was not able to stop dockerized LDAP or clean its data")
    }
  }

  "The Login controller with local method" should {
    "return OK with a session if the ldap login is successful" in new WithApplication(app) {
      val json = Json.obj(
        "login" -> "dupont",
        "password" -> "toto"
      )
      stub(admins.exists("dupont")).toReturn(true)

      val res = route(app, FakeRequest(POST, "/api/1.0/login").withJsonBody(json)).get

      status(res) shouldEqual OK
      session(res).get("username") shouldEqual Some("dupont")
      verify(admins).exists("dupont")
    }

    "return OK with a session if the local login is successful" in new WithApplication(app) {
      val json = Json.obj(
        "login" -> "local",
        "password" -> "toto"
      )
      stub(admins.exists("local")).toReturn(true)
      stub(admins.getHashedPassword("local")).toReturn(Some("my_hash"))
      stub(admins.passwordMatches("toto", "my_hash")).toReturn(true)

      val res = route(app, FakeRequest(POST, "/api/1.0/login").withJsonBody(json)).get

      status(res) shouldEqual OK
      session(res).get("username") shouldEqual Some("local")
    }
  }
}

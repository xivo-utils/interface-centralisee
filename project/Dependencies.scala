import sbt._
import play.sbt.PlayImport._

object Version {
  val dbunit = "2.4.7"
  val postgresql = "9.4-1206-jdbc4"
  val xivoLibLdap = "2.4.13"
  val playAuthentication = "2.4-rc1"
  val mockitoAll = "1.9.5"
  val scalatest = "2.2.4"
  val scalatestplus = "1.5.0"
  val objectpool = "1.1.2"
  val jasypt = "1.9.2"
  val anorm = "2.5.1"
  val purecsv = "0.0.6"
  val deadbolt = "2.5.0"
  val akka      = "2.4.4"
}

object Library {
  val dbunit = "org.dbunit" % "dbunit" % Version.dbunit
  val postgres = "org.postgresql" % "postgresql" % Version.postgresql
  val xivoLibLdap = "xivo" % "xivo-lib-ldap" % Version.xivoLibLdap
  val playAuthentication = "xivo" %% "play-authentication" % Version.playAuthentication
  val mockitoAll = "org.mockito" % "mockito-all" % Version.mockitoAll
  val scalatest = "org.scalatest" %% "scalatest" % Version.scalatest
  val scalatestplus= "org.scalatestplus.play" %% "scalatestplus-play" % Version.scalatestplus
  val objectpool = "nf.fr.eraasoft" % "objectpool" % Version.objectpool
  val jasypt = "org.jasypt" % "jasypt" % Version.jasypt
  val anorm = "com.typesafe.play" %% "anorm" % Version.anorm
  val purecsv = "com.github.melrief" %% "purecsv" % Version.purecsv
  val deadbolt = "be.objectify" %% "deadbolt-scala" % Version.deadbolt
  val akkaTestkit = "com.typesafe.akka" %% "akka-testkit" % Version.akka
}

object Dependencies {

  import Library._

  val scalaVersion = "2.11.8"

  val resolutionRepos = Seq(
    "Local Maven Repository" at "file://" + Path.userHome.absolutePath + "/.m2/repository",
    "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases",
    "Typesafe repository" at "https://repo.typesafe.com/typesafe/releases/",
    Resolver.sonatypeRepo("releases")
  )

  val runDep = run(
    jdbc,
    anorm,
    evolutions,
    cache,
    ws,
    dbunit,
    postgres,
    xivoLibLdap,
    playAuthentication,
    jasypt,
    objectpool,
    purecsv,
    deadbolt
  )

  val testDep = test(
    mockitoAll,
    scalatest,
    scalatestplus,
    specs2,
    akkaTestkit
  )

  def run       (deps: ModuleID*): Seq[ModuleID] = deps
  def test      (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "test")}

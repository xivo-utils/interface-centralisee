import sbt._
import sbt.Keys._
import play.sbt.PlayScala
import com.typesafe.sbt.SbtNativePackager
import com.typesafe.sbt.SbtNativePackager._
import com.typesafe.sbt.packager.docker._
import com.typesafe.sbt.packager.Keys._
import com.joescii.SbtJasminePlugin._
import sbtbuildinfo.{BuildInfoKey,BuildInfoPlugin}
import sbtbuildinfo.BuildInfoKeys._
import org.clapper.sbt.editsource.EditSourcePlugin.autoImport._
import org.clapper.sbt.editsource.EditSourcePlugin
import scoverage.ScoverageKeys._

object ApplicationBuild extends Build {

  val appName = "interface-centralisee"
  val appVersion = sys.env.getOrElse("TARGET_VERSION", "dev-version")
  val appOrganisation = "xivo"

  lazy val macros = Project("macro", file("macro"))
    .settings(
      version := "0.1-SNAPSHOT",
      scalacOptions ++= Seq("-deprecation", "-feature"),
      scalaVersion := Dependencies.scalaVersion,
      resolvers ++= Dependencies.resolutionRepos,
      libraryDependencies ++= Seq(
        "org.scala-lang" % "scala-compiler" % Dependencies.scalaVersion,
        "org.scala-lang" % "scala-reflect" % Dependencies.scalaVersion
      ) ++ Dependencies.runDep ++ Dependencies.testDep
    )
    .settings(
      publish := {}
    )

  val main = Project(appName, file("."))
    .enablePlugins(PlayScala)
    .enablePlugins(SbtNativePackager)
    .enablePlugins(DockerPlugin)
    .enablePlugins(BuildInfoPlugin)
    .settings(
      name := appName,
      version := appVersion,
      scalaVersion := Dependencies.scalaVersion,
      organization := appOrganisation,
      resolvers ++= Dependencies.resolutionRepos,
      libraryDependencies ++= Dependencies.runDep ++ Dependencies.testDep
    )
    .settings(
      BuildInfoCustomSettings.settings: _*
    )
    .settings(
      Editsources.settings: _*
    )
    .settings(
      setVersionVarTask := { System.setProperty("IC_VERSION", appVersion) },
      edit in EditSource <<= (edit in EditSource) dependsOn (EditSourcePlugin.autoImport.clean in EditSource),
      packageBin in Compile <<= (packageBin in Compile) dependsOn (edit in EditSource),
      run in Compile <<= (run in Compile) dependsOn setVersionVarTask
    )
    .settings(Jasmine.settings: _*)
    .settings(
      parallelExecution in Test := false,
      javaOptions in Test += "-Dlogger.file=conf/logback-test.xml",
      javaOptions in Test += "-Dplay.evolutions.enabled=false" // dbUtil use should be removed
    )
    .settings(
      coverageExcludedPackages := "<empty>;views\\..*;Reverse.*;xuc\\.Routes"
    )
    .settings(
      DockerSettings.settings: _*
    )
    .aggregate(macros)
    .dependsOn(macros)

  lazy val setVersionVarTask = taskKey[Unit]("Set version to a env var")

  object Jasmine {
    val settings = jasmineSettings ++ Seq(
      appJsDir <+= baseDirectory / "app/assets/javascripts",
      appJsLibDir <+= baseDirectory / "public/javascripts/",
      jasmineTestDir <+= baseDirectory / "test/assets/",
      jasmineConfFile <+= baseDirectory / "test/assets/test.dependencies.js",
      (test in Test) <<= (test in Test) dependsOn (jasmine),
      jasmineEdition := 2
    )
  }

  object DockerSettings {
    val settings = Seq(
      maintainer in Docker := "Jirka HLAVACEK <jhlavacek@avencall.com>",
      dockerBaseImage := "openjdk:8u212-b04-jdk-slim-stretch",
      dockerExposedPorts in Docker := Seq(9000),
      dockerExposedVolumes in Docker := Seq("/conf"),
      dockerRepository := Some("xivoxc"),
      dockerEntrypoint := Seq("bin/ic_docker"),
      daemonUser in Docker := "root",
      dockerCommands ++= Seq(
        ExecCmd("RUN", "apt-get", "update"),
        ExecCmd("RUN", "apt-get", "install", "-y", "ssh")
      ),
      dockerCommands += Cmd("LABEL", s"""version="${appVersion}""""),
      dockerUpdateLatest := true
    )
  }

  object BuildInfoCustomSettings {
    val settings = Seq(
      buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion),
      buildInfoPackage := "ic.info"
    )
  }

  object Editsources {
    val settings = Seq(
      flatten in EditSource := true,
      mappings in Universal += file("target/version/appli.version") -> "conf/ic.version",
      targetDirectory in EditSource <<= baseDirectory(_/"target/version"),
      variables in EditSource <+= version {version => ("SBT_EDIT_APP_VERSION", appVersion)},
      (sources in EditSource) <++= baseDirectory map { bd =>
        (bd / "dist" * "appli.version").get
      }
    )
  }
}


